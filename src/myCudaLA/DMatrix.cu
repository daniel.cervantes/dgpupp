//#include <myCudaLA/DMatrix.hpp>

template <typename T>
DMatrix<T>::DMatrix(){
  _n = _m =0;
  elems = 0;
}

template <typename T>
DMatrix<T>::DMatrix(int n, int m)
{
  _n = n;
  _m = m;
  
  cudaMalloc(&elems, _n*_m*sizeof(T));  
}

template <typename T>
DMatrix<T>::~DMatrix(){
  _n = _m = 0;
  if(!elems)
    cudaFree(elems);
}

template <typename T>
void DMatrix<T>::resize(int n, int m){
  
  if( (_n != 0) && (_m != 0) ){  
    if(elems != 0)
      cudaFree(elems);
  }
  
  _n = n;
  _m = m;
  cudaMalloc((void**)&elems,_n*_m*sizeof(T));  
  
}


