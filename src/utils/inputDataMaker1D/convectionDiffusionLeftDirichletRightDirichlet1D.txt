outputDir = ../../../data

%Domain
a = 0.0
b = 1.0

%Elements (Subdomains)
K = 10;

%Time
finalTime = 1.0

%Initial condition function
u0 = exp(-x)

%CFL constant
CFL = 0.75

%PDE parameters  u_t+alpha u_x + beta u = epsilon u_xx + f
alpha = 1.0;
beta = 0.0;
epsilon = 0.1;

%Boundary conditions
leftBdy = Dirichlet
g0 = exp(0.11*t)

rightBdy = Dirichlet
g1 = exp(-1 + 0.11*t)

%Polinomial order basis function
N = 2
