
$cname = $ARGV[0];

open(F1, $cname);
while(<F1>) {
    chomp;
    next if($_ =~ /^%/);
    $outDir = $1    if($_ =~ /outputDir\s+=\s+(.+)\s*$/);
    $a    = 0.0+$1  if($_ =~ /a\s+=\s+(\d*\.*\d*)/);
    $b    = 0.0+$1  if($_ =~ /b\s+=\s+(\d*\.*\d+)/);

    $K = 0+$1    if($_ =~ /K\s+=\s+(\d+)/);
    $finalTime    = 0.0+$1  if($_ =~ /finalTime\s+=\s+(\d*\.*\d+)/);
    $u0   = $1      if($_ =~ /u0\s+=\s+(.+)$/);

    $CFL    = 0.0+$1  if($_ =~ /CFL\s+=\s+(\d*\.*\d+)/);

    $alpha   = 0.0+$1  if($_ =~ /alpha\s+=\s+(\d*\.*\d+)/);
    $beta    = 0.0+$1  if($_ =~ /beta\s+=\s+(\d*\.*\d+)/);
    $epsilon = 0.0+$1  if($_ =~ /epsilon\s+=\s+(\d*\.*\d+)/);
    

    $tbc0    = $1      if($_ =~ /leftBdy\s+=\s+(.+)$/);
    $fbc0    = $1      if($_ =~ /g0\s+=\s+(.+)$/);
    $tbc1    = $1      if($_ =~ /rightBdy\s+=\s+(.+)$/);
    $fbc1    = $1      if($_ =~ /g1\s+=\s+(.+)$/);

    $N = 0+$1    if($_ =~ /N\s+=\s+(\d+)/);
}
close(F1);

print "Output Dir = $outDir\n";
print "a    = $a\n";
print "b    = $b\n";
print "K = $K\n";
print "N = $N\n";
print "finalTime    = $finalTime\n";
print "CFL    = $CFL\n";
print "alpha   = $alpha\n";
print "beta    = $beta\n";
print "epsilon = $epsilon\n";
print "u0   = $u0\n";
print "Left BDY    = $tbc0\n";
print "g0    = $fbc0\n";
print "Right BDY    = $tbc1\n";
print "g1    = $fbc1\n";


# Tamaño de paso espacial
$dx = ($b - $a)/($K);

# Calculo del tamaño de paso temporal
$dt = $CFL*$dx/6.28318531;
$nSteps = int($finalTime/$dt) + 1;
print "dx      = $dx\n";
print "dt      = $dt\n";
print "nSteps  = $nSteps\n";

# Correccion del tamaño de paso temporal
$dt = $finalTime/$nSteps;

# Generacion de la discretizacion espacial
$x  = $a;
foreach $i(0..$K) {
    $vx[$i] = $x;
    $x  += $dx;
}

# Generacion de la discretizacion temporal
$t  =  0;
foreach $i(0..$nSteps) {
    $vt[$i] = $t;
    $t  += $dt;
}


# Generacion del archivo que tiene los valores de la condicion inicial
($tmp = $cname) =~ s/\.[^.]+$//;
open(F2, ">$outDir/initCondition_"."$tmp".".txt");
print F2 "xk              u0(xk)\n";
print F2 ($K+1) . "\n";
foreach $i(0..$#vx) {
    $x   = $vx[$i];
    $v   = &evalFnc($u0, $x, 'x');
    print F2 sprintf("%.8e  %.8e\n", $x,  $v);
}
close(F2);


# Generacion del archivo que tiene los valores de las condiciones de frontera
open(F2, ">$outDir/bdyConditions_"."$tmp".".txt");
print F2 "tk              Left_Condition_$tbc0(tk)   Right_Condition_$tbc1(tk)\n";
print F2 ($nSteps+1) . "\n";
foreach $i(0..$#vt) {
    $t   = $vt[$i];
    $v1  = &evalFnc($fbc0, $t, 't');
    $v2  = &evalFnc($fbc1, $t, 't');
    print F2 sprintf("%.9e  %.9e    %.9e\n", $t, $v1, $v2);
}
close(F2);

$codigo1 = 1;
$codigo2 = 1;
$codigo1 = 2   if($tbc0 =~ /^Ne/i);
$codigo2 = 2   if($tbc0 =~ /^Ne/i);
open(F2, ">$outDir/parameters_"."$tmp".".txt");
print F2 "alpha            beta             epsilon          N   Left_Condition   Right_Condition\n";
print F2 "1\n";
print F2 sprintf("%.9e  %.9e  %.9e  %d         %d              %d\n", $alpha, $beta, $epsilon, $N, $codigo1, $codigo2);
close(F2);



sub evalFnc {
    my $expr = @_[0];
    my $x    = @_[1];
    my $cvar = @_[2];
    my %vars = ( X => $x );

    $expr =~ s/(\W)$cvar/\1\$vars{X}/g;
    $expr =~ s/$cvar(\W)/\$vars{X}\1/g;
    return(eval $expr);
}



