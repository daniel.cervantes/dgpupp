#include<utils/DataReader.hpp>

namespace utils{
  
  void DataReader::read(MatrixXf & mat){

    ifstream file (_namefile.c_str());
    int n,m;
    
    if (file.is_open())
      {
        file >> n >> m;
        mat.resize(n,m);
        
        for(int i=0; i < n; i++)
          for(int j=0; j < m; j++)
            file >>  mat(i,j);  
      }
  }

  void DataReader::read(vector<T>& pulses, vector<T>& substrates){
    
    ifstream file (_namefile.c_str());
    int n,m;
    string s, blank;
    float x,y,p,r;
    
    if (file.is_open())
      {
        file >> n >> m;
        for(int i=0; i < n; i++) {  
          file >> x >> y >> p;
          pulses.push_back(T(x,y,p));
        }

        getline(file, blank);
        
        for(int i=0; i < m; i++) {  
          file >>  x >> y >> r;
          substrates.push_back(T(x,y,r));
        }
        
      }
    file.close();
  }

  
  void DataReader::readP(vector<T>& pulses, string namefile){
    
    ifstream file (namefile.c_str());
    int n;
    float x,y,z,p;
    
    if (file.is_open())
      {
        file >> n;
        for(int i=0; i < n; i++) {  
          file >> x >> y >> z;
          pulses.push_back(T(x,y,1.0));
        }        
        file.close();
      }
  }


  
  void DataReader::readS(vector<T>& substrates, string namefile, float alpha){
    
    ifstream file (namefile.c_str());
    int n;
    float x,y,z,r;
    
    if (file.is_open())
      {
        file >> n ;
        for(int i=0; i < n; i++) {  
          file >> x >> y >> z >> r;
          
          if(r > alpha){
            substrates.push_back(T(x,y,r));
            //cout << r << endl;
          }
        }        
        file.close();
      }
  }



    
  void DataReader::readPulses(vector<F>& pulses, string namefile){
    
    ifstream file (namefile.c_str());
    int n;
    float x,y,z,p;
    
    if (file.is_open())
      {
        file >> _nP;
        for(int i=0; i < _nP; i++) {  
          file >> x >> y >> z;
          pulses.push_back(F(x,y,z,p));
        }        
        file.close();
      }
  }


  
  void DataReader::readVoxels(vector<F>& voxels, string namefile){
    
    ifstream file (namefile.c_str());
    float x,y,z,r;
    
    if (file.is_open())
      {
        file >> _nS ;
        for(int i=0; i < _nS; i++) {  
          file >> x >> y >> z >> r;
          voxels.push_back(F(x,y,z,r));
        }        
        file.close();
      }
  }

  


  void DataReader::readDeltas(string nfile, vector<T>& pulses, int& nP){

    ifstream file (nfile.c_str());
    float x,y,z,p;
        
    if(file.is_open()){
      file >> nP;
      for(int i=0; i < nP; i++){
        file >> x >> y >> z;
        pulses.push_back(T(x,y,1.0));
      }      
    }
    file.close();
  }


  void DataReader::readDeltas(string nfile, vector<pair<int,int> >& pulses, int& nP){
    
    ifstream file (nfile.c_str());
    float x,y,p;
    
    if(file.is_open()){
      file >> nP;
      for(int i=0; i < nP; i++){
        file >> x >> y;
        pulses.push_back(make_pair(x,y));
        //cout << pulses[i].first <<" " << pulses[i].second << endl;
      }      
    }
    file.close();
  }

  int* DataReader::readDeltas(string nfile, int* deltasI, int & nP){

    ifstream file (nfile.c_str());    
    int i,j;
    
    if(file.is_open()){
      file >> nP;
      deltasI = new int[2*nP];
      for(int ii=0; ii < 2*nP; ii+=2){
        file >> i >> j;
        deltasI[ii]   = i;
        deltasI[ii+1] = j;
        // cout << deltasI[ii] << " " << deltasI[ii+1] << endl;
      }      
    }
    file.close();

    return deltasI;
  }
  
  void DataReader::readDeltas(string nfile, MatrixXf & dM){
    
    ifstream file(nfile.c_str());
    int n,m;

    file >> n >> m;
    dM.resize(n,m);

    for(int i=0; i < n; i++)
      for(int j=0; j < m; j++)
        file >> dM(i,j);  
    
    file.close();    
  }

  void DataReader::readDeltas(string nfile, MatrixXi& pulses){
    ifstream file (nfile.c_str());
    int n;

    file >> n;    
    pulses.resize(n,2);    
    for(int i=0; i <n; i++)
      file >> pulses(i,0) >> pulses(i,1);

    file.close();
  }
  
  void DataReader::readDCMatrix(string nfile, MatrixXf & k){
    ifstream file (nfile.c_str());
    int n,m;
    file >> n >> m;
    k.resize(n,m);
    
    for(int i=0; i < n; i++)
      for(int j=0; j < m; j++)
        file >> k(i,j);  

    file.close();
  }
  
  void DataReader::read(){

    ifstream file (_namefile.c_str());
    ifstream file2 (_namefile2.c_str());
    //cout << _namefile << " " << _namefile2 << endl;
    // cout << _namefile <<endl;
    int n1,n2;
    float x,y,z,w,r;
    
     if (file.is_open())
      {
        file >> n1;
        _nP = n1;
        //cout << n1 << endl;
        _pulses = new float[2*n1];
        
        for(int i=0; i < 2*n1; i+=2) {  
          file >> x >> y >> z;
          // cout << x << " " << y << " " << z << endl;
          _pulses[i]   = x;
          _pulses[i+1] = y;
          //cout << pulses[i] << " " << pulses[i+1] << " " << z << endl;
        }
      }     
     file.close();
     
     
     if (file2.is_open())
       {
         file2 >> n2;
         _nS = n2;
         //cout << "n2  " <<n2 <<endl;
         _substrates = new float[3*n2];         
         
         for(int i=0; i < 3*n2; i+=3) {  
           file2 >> x >> y >> z >> r;
           _substrates[i] = x;
           _substrates[i+1] = y;
           _substrates[i+2] = r;
         }
       }         
     file2.close();
    
  }


  
}
