#include <utils/DataWriter.hpp>

namespace utils{

  DataWriter::DataWriter(MatrixXf& m1, MatrixXf& m2, string namefile):_namefile(namefile){
    int n = m1.rows();
    int m = m1.cols();
       
    ofstream myfile;
    myfile.open("prueba.txt");
      
      for(int j=0; j < m; j++)
        for(int i=0; i < n; i++){        
          myfile << m1(i,j) << "  " << m2(i,j) << endl;  
        }
    
    myfile.close();
  }
 

  void DataWriter::write(MatrixXf& m1, MatrixXf& m2){
    int n = m1.rows();
    int m = m1.cols();
    //cout << n << " " << m << endl;
    ofstream myfile;
    //myfile.open (_namefile.c_str());
    //myfile.open ("./data/prueba.txt");
    //cout << _namefile.c_str() << endl;
    
    for(int j=0; j < m; j++)
      for(int i=0; i < n; i++) {       
        myfile << m1(i,j) << "  " << m2(i,j) << endl;
        //    cout  << m1(i,j) << "  " << m2(i,j) << endl;
      }
    
    myfile.close();
  }
  

  void DataWriter::write(MatrixXf& m1, MatrixXf& m2, MatrixXf& m3){
    int n = m1.rows();
    int m = m1.cols();
    
    ofstream myfile;
    myfile.open (_namefile.c_str());
    
    
    for(int j=0; j < m; j++)
      for(int i=0; i < n; i++) 
        myfile << m1(i,j) << "  " << m2(i,j) <<"  "<< m3(i,j) << endl;
        
    myfile.close();
  }


  void DataWriter::write(MatrixXf& m1, MatrixXf& m2, MatrixXf& m3,MatrixXf& m4){
    int n = m1.rows();
    int m = m1.cols();
    
    ofstream myfile;
    myfile.open (_namefile.c_str());
    
    
    for(int j=0; j < m; j++){
      for(int i=0; i < n; i++) 
        myfile << m1(i,j) << "  " << m2(i,j) <<"  "<< m3(i,j) <<"  "<< m4(i,j) << endl;
    }
    myfile.close();
  }

  void DataWriter::write(float a, float b, float c, float d, MatrixXf & mat){

    float epsilon = 1e-10;
    ofstream myfile;
    ofstream myfile2;
    ofstream myfile3;
    ofstream myfile4;
    float x,y;
    
    myfile.open (_namefile.c_str());
    //cout << "NOMBRE "<<_namefile << endl;
    std::string token = _namefile.substr(0, _namefile.find(".txt"));
    // myfile2.open ( (token+"_normalized.txt").c_str());
    myfile3.open( (token+"_x.txt").c_str());
    myfile4.open( (token+"_y.txt").c_str());

    
    
    int n = mat.rows();
    int m = mat.cols();
    float dx,dy;

    dx = (b-a)/float(n);
    dy = (d-c)/float(m);
      
    for(int i=0; i < n-1; i++) {         
      for(int j=0; j < m-1; j++){

        x = a + float(i)*dx + dx;
        y = c + float(j)*dy + dy;
        myfile << x <<" "<< y  << " "  << mat(i,j) << endl;

        if(i == n/2)
          myfile3 << y  << " "  << mat(i,j) << endl;
        if(j == m/2)
          myfile4 << x  << " "  << mat(i,j)  << endl;


      }
      myfile << endl;
    }

    myfile.close();
    myfile3.close();
    myfile4.close();
  }

  
  // void DataWriter::write(Mesh2D & mesh){
  //   ofstream myfile;
  //   myfile.open (_namefile.c_str());

  //   int K = mesh.K();
  //   int nV = mesh.nV();
        
  //   for(int i=0; i < K; i++){
  //     myfile << mesh.vx(mesh.eToV(i,0)-1) << " " << mesh.vy(mesh.eToV(i,0)-1) << endl;
  //     myfile << mesh.vx(mesh.eToV(i,1)-1) << " " << mesh.vy(mesh.eToV(i,1)-1) << endl;
  //     myfile << mesh.vx(mesh.eToV(i,2)-1) << " " << mesh.vy(mesh.eToV(i,2)-1) << endl;
  //     myfile << mesh.vx(mesh.eToV(i,0)-1) << " " << mesh.vy(mesh.eToV(i,0)-1) << endl;
  //     myfile << endl ;
  //   }

  //   myfile.close();
  // }


  void DataWriter::write(float a, float b, float c, float d, int K, int L, MatrixXf & mat){

    ofstream myfile,myfile2,myfile3;
    myfile.open (_namefile.c_str());
    
    std::string token = _namefile.substr(0, _namefile.find(".txt"));
    myfile2.open( (token+"_x.txt").c_str());
    myfile3.open( (token+"_y.txt").c_str());


    
    float dx,dy;
    float x,y;
    dx = (b-a)/float(L);
    dy = (d-c)/float(K);

            
    for(int i=1; i <=L; i++){
      for(int j=1; j <=K; j++){

        x = a + float(i)*dx + dx;
        y = c + float(j)*dy + dy;

        myfile << x  <<" "<< y  << " "  << mat(i-1,j-1)  << endl;
        if(i == K/2)
          myfile2 << y  << " "  << mat(i-1,j-1) << endl;
        if(j == L/2)
          myfile3 << x  << " "  << mat(i-1,j-1)  << endl;

        
      }
      myfile << endl;      
    }
         
    myfile.close();
    myfile2.close();
    myfile3.close();
  }



  void DataWriter::write(float a, float b, float c, float d, int K, int L, MatrixXf & mat, MatrixXf & mat2){

    ofstream myfile;
    myfile.open (_namefile.c_str());
    float uelem;
    ofstream myfile2;
    ofstream myfile3;

    
    myfile2.open( (_namefile+"_x.txt").c_str());
    myfile3.open( (_namefile+"_y.txt").c_str());
    
    // cout << (_namefile+"_x.txt").c_str() << endl;
    // cout << (_namefile+"_y.txt").c_str() << endl;
    
    float x,y;
    
    
    float dx,dy;
    dx = (b-a)/float(L);
    dy = (d-c)/float(K);


    float sum = 0.0;
        
    for(int i=1; i <=L; i++){
      for(int j=1; j <=K; j++){

        x =  a+float(i)*(dx/2.0) + (dx/2.0)*(i-1);
        y =  c+float(j)*(dy/2.0) + (dy/2.0)*(j-1);
    
        
        myfile << x <<" "<< y << " "  << (0.5*(mat(i,2*j) + mat(i,2*j+1)))  << " "  << mat2(K-1 -(j-1),i-1) <<endl;        


        // if(i == (K+1)/2)
        //   myfile2 << y  << " "  << (0.5*(mat(i,2*j) + mat(i,2*j+1))) << endl;
        // if(j == (L+1)/2)
        //   myfile3 << x  << " "  << (0.5*(mat(i,2*j) + mat(i,2*j+1)))  << endl;

        //myfile2 << a+float(i)*(dx/2.0) + (dx/2.0)*(i-1)  <<" "<< c + float(j)*(dy/2.0) + (dy/2.0)*(j-1)   << " "  << abs(0.5*(mat(i,2*j) + mat(i,2*j+1)))  << " "  << mat2(K-1 -(j-1),i-1) <<endl;        
        //cout <<  a+float(i)*(dx/2.0) + (dx/2.0)*(i-1) <<" "<< c + float(j)*(dy/2.0) + (dy/2.0)*(j-1)   << " "  << 0.5*(mat(i,2*j) + mat(i,2*j+1))  << endl;
        sum += 0.5*(mat(i,2*j) + mat(i,2*j+1));
      }
      
      myfile << endl;
      myfile2 << endl;
      myfile3 << endl;
    }

         
    myfile.close();
    myfile2.close();
    myfile3.close();

    cout << " suma " << sum << endl;

  }

  

  void DataWriter::write(float a, float b, float c, float d, int K, int L, MatrixXf & mat,
                         float mux,float muy, float T, float k,float (*bG)(float,float,float,float,float,float) ){
    
    ofstream myfile;
    ofstream myfile2;
    myfile.open (_namefile.c_str());    

    std::string token = _namefile.substr(0, _namefile.find(".txt"));
    myfile2.open ( (token+"_normalized.txt").c_str());
    
    float dx,dy;
    float x,y;
    float approx;
    float exact;
    float ecm = 0.0;
    float dtotal = 0.0; 
    
    dx = (b-a)/float(L);
    dy = (d-c)/float(K);

    float t1=0.0;
    float t2=0.0;
            
    for(int i=1; i <=L; i++){
      for(int j=1; j <=K; j++){
        x = a + float(i)*(dx/2.0) + (dx/2.0)*(i-1);
        y = c + float(j)*(dy/2.0) + (dy/2.0)*(j-1);

        t1+= 0.5*(mat(i,2*j) + mat(i,2*j+1));
        t2+= bG(x,y,mux,muy,T,k);
        
        myfile << x <<" "<< y  << " "  << 0.5*(mat(i,2*j) + mat(i,2*j+1)) << " " << bG(x,y,mux,muy,T,k)  << endl;
      }
      myfile << endl;
    }

    
    for(int i=1; i <=L; i++){
      for(int j=1; j <=K; j++){
        x = a + float(i)*(dx/2.0) + (dx/2.0)*(i-1);
        y = c + float(j)*(dy/2.0) + (dy/2.0)*(j-1);

        approx = 0.5*(mat(i,2*j) + mat(i,2*j+1))/t1;
        exact = bG(x,y,mux,muy,T,k)/t2;

        //ecm += (approx-exact)*(approx-exact);
        ecm += abs(approx-exact);
        dtotal += exact;
        myfile2 << x <<" "<< y  << " "  << approx << " " << exact   <<  endl;
      }
      myfile2 << endl;
    }
    
    myfile.close();
    myfile2.close();
    
    cout << "MRE: " << (ecm/dtotal)/(L*K) << endl;
    
  }

  

  void DataWriter::write(float a, float b, float c, float d, int K, int L, MatrixXf & mat,
                         float mux, float muy, MatrixXf & invSig, float detSig, float (*bG)(float,float,float,float,MatrixXf&,float) ){

    ofstream myfile;
    ofstream myfile2;
    ofstream myfile3;
    ofstream myfile4;

    
    myfile.open (_namefile.c_str());        
    std::string token = _namefile.substr(0, _namefile.find(".txt"));
    myfile2.open ( (token+"_normalized.txt").c_str());
    myfile3.open( (token+"_x.txt").c_str());
    myfile4.open( (token+"_y.txt").c_str());
                   
    
    float dx,dy;
    float x,y;
    float approx;
    float exact;
    float ecm = 0.0;
    float dtotal = 0.0; 
    
    dx = (b-a)/float(L);
    dy = (d-c)/float(K);

    float t1=0.0;
    float t2=0.0;
            
    for(int i=1; i <=L; i++){
      for(int j=1; j <=K; j++){
        
        x = a + float(i)*(dx/2.0) + (dx/2.0)*(i-1);
        y = c + float(j)*(dy/2.0) + (dy/2.0)*(j-1);

        t1+= 0.5*(mat(i,2*j) + mat(i,2*j+1));
        t2+= bG(x,y,mux,muy,invSig,detSig);
        
        myfile << x <<" "<< y  << " "  << 0.5*(mat(i,2*j) + mat(i,2*j+1)) << " " << bG(x,y,mux,muy,invSig,detSig)  << endl;

        if(i == L/2)
          myfile3 << y  << " "  << 0.5*(mat(i,2*j) + mat(i,2*j+1)) << endl;
        if(j == K/2)
          myfile4 << x  << " "  << 0.5*(mat(i,2*j) + mat(i,2*j+1)) << endl;
        
      }
      myfile << endl;
    }

    
    for(int i=1; i <=L; i++){
      for(int j=1; j <=K; j++){
        
        x = a + float(i)*(dx/2.0) + (dx/2.0)*(i-1);
        y = c + float(j)*(dy/2.0) + (dy/2.0)*(j-1);

        approx = 0.5*(mat(i,2*j) + mat(i,2*j+1))/t1;
        exact = bG(x,y,mux,muy,invSig,detSig)/t2;
        //ecm += (approx-exact)*(approx-exact);

        ecm += abs(approx-exact);
        dtotal += exact;
        myfile2 << x <<" "<< y  << " "  << approx << " " << exact  << " " << abs(approx-exact)  <<  endl;
      }
      myfile2 << endl;
    }
    
    myfile.close();
    myfile2.close();
    myfile3.close();
    myfile4.close();

    cout << "write " << endl;
    //cout << "MRE: " << (ecm/dtotal)/(L*K) << endl;
    
  }


  


  void DataWriter::write(float a, float b, float c, float d, int K, int L, MatrixXf & mat,
                         float mux,float muy, float T, float k, int n,float (*bG)(float,float,float,float,float,float,int) ){
    
    ofstream myfile;
    ofstream myfile2;
    myfile.open (_namefile.c_str());    

    std::string token = _namefile.substr(0, _namefile.find(".txt"));
    myfile2.open ( (token+"_normalized.txt").c_str());
    
    float dx,dy;
    float x,y;
    float approx;
    float exact;
    float ecm = 0.0;
    float dtotal = 0.0; 
    
    dx = (b-a)/float(L);
    dy = (d-c)/float(K);

    float t1=0.0;
    float t2=0.0;
            
    for(int i=1; i <=L; i++){
      for(int j=1; j <=K; j++){
        x = a + float(i)*(dx/2.0) + (dx/2.0)*(i-1);
        y = c + float(j)*(dy/2.0) + (dy/2.0)*(j-1);

        t1+= abs(0.5*(mat(i,2*j) + mat(i,2*j+1)));
        t2+= bG(x,y,mux,muy,T,k,n);
        
        myfile << x <<" "<< y  << " "  << 0.5*(mat(i,2*j) + mat(i,2*j+1)) << " " << bG(x,y,mux,muy,T,k,n)  << endl;
      }
      myfile << endl;
    }

    
    for(int i=1; i <=L; i++){
      for(int j=1; j <=K; j++){
        
        x = a + float(i)*(dx/2.0) + (dx/2.0)*(i-1);
        y = c + float(j)*(dy/2.0) + (dy/2.0)*(j-1);

        approx = abs(0.5*(mat(i,2*j) + mat(i,2*j+1)))/t1;
        exact = bG(x,y,mux,muy,T,k,n)/t2;

        //ecm += (approx-exact)*(approx-exact);
        ecm += abs(approx-exact);
        dtotal += exact;
        myfile2 << x <<" "<< y  << " "  << approx << " " << exact  << " " << abs(approx-exact)  <<  endl;
      }
      myfile2 << endl;
    }
    
    myfile.close();
    myfile2.close();
    
    //cout << "MRE: " << (ecm/dtotal)/(L*K) << endl;
    
  }


  void DataWriter::write(float a, float b, float c, float d, int K, int L, MatrixXf & mat, string namefile){

    ofstream myfile;
    myfile.open (namefile.c_str());
    
    float dx,dy;
    dx = (b-a)/float(L);
    dy = (d-c)/float(K);

            
    for(int i=1; i <=L; i++){
      for(int j=1; j <=K; j++){

        myfile << a+float(i)*(dx/2.0) + (dx/2.0)*(i-1)  <<" "<< c + float(j)*(dy/2.0) + (dy/2.0)*(j-1)   << " "  << abs(0.5*(mat(i,2*j) + mat(i,2*j+1)))  << endl;
        //cout << a+0.5*float(i)*dx  <<" "<< c + float(j)*dy   << " "  << 0.5*(mat(i,2*j) + mat(i,2*j+1))  << endl;
      }
      myfile << endl;
    }

         
    myfile.close();
  }

  


  


  
  
}
