#ifndef MATRIX_H
#define MATRIX_H


#include <iostream>
#include <stdlib.h>


namespace num {

  using namespace std;
  
  template<class T>
  class Dim3Matrix: public Matrix<T>{

  private:
    int _l;
    
  public:
    Dim3Matrix(int,int,int);
    Dim3Matrix(int,int,int,int);
    ~Dim3Matrix();
    T& operator()(int,int,int);
    T& operator()(int,int,int,int);
    
    friend Dim3Matrix& operator + (Dim3Matrix&,Dim3Matrix&);
    friend Dim3Matrix& operator - (Dim3Matrix&,Dim3Matrix&);
    friend Dim3Matrix& operator * (Dim3Matrix&,Dim3Matrix&);
    friend Dim3Matrix& operator * (T,Dim3Matrix&);
    friend ostream & operator << (ostream & , Dim3Matrix &);
    
    int l(){return _l;}

  };
}

#endif
