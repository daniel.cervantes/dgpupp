#include <num/Matrix.hpp>
#include <time.h>

namespace num {

  template class Matrix<float,size_t,size_t,size_t>;
  
  template<typename T,size_t ROWS, size_t COLS,size_t DIM>
  void Matrix<T,ROWS,COLS,DIM>::random(int a, int b){
    
    for(int i=0; i < _rows; i++)
      for(int j=0; j < _cols; j++)
        for(int d=0; d < _dim; d++){
          _data[i*_cols*_dim+j*_dim+d] = a + static_cast <T> (rand()) / ( static_cast <T> (RAND_MAX/(b-a)));          
        }
  }

  template<typename T,size_t ROWS, size_t COLS,size_t DIM>
  Matrix<T, COLS, ROWS, DIM>& Matrix<T,ROWS,COLS,DIM>::operator=(const Matrix<T,COLS,ROWS,DIM> & mr) {

      for (size_t i=0; i < _rows; ++i) {
        for(size_t j=0; j < _cols; ++j){
          for(int d =0; d < _dim; d++)
            _data[i*_cols*_dim+j*_dim+d] = mr(i,j,d);
        }
      }
      return *this;
    }

  template<typename T,size_t ROWS, size_t COLS,size_t DIM>
  template <class L, class R>
    Matrix<T,COLS,ROWS,DIM>& Matrix<T,ROWS,COLS,DIM>::operator=(const MatrixAdd<T,COLS,ROWS,DIM,L,R> mrs){
      for(size_t i=0; i <_rows; ++i)
        for(size_t j=0; j < _cols; ++j)
          for(size_t d=0; d < _dim; ++d)
            _data[i*_cols*_dim+j*_dim+d] = mrs(i,j,d);

      return *this;
    }

  template<typename T,size_t ROWS, size_t COLS,size_t DIM>
  ostream & operator << (ostream & out, Matrix<T,ROWS,COLS,DIM> & m){
    int cols = m.cols();
    int rows = m.rows();
    int dim  = m.dim();
    
    for(int i=0; i < rows; i++){
      for(int j=0; j < cols; j++){                  
        for(int d=0; d < dim; d++){
          //      cout << i << " " << j << " " << d<< endl;
          out << setw(10) << m(i,j,d) << " " ;
        }
        cout << "   ";
      }
      
      out << endl;
    }
    //out << endl;
    return out;
  }
  
  
  // template<typename T,size_t ROW, size_t COL,size_t DIM>
  // Matrix<T,size_t,size_t,size_t>::Matrix():_cols(COL),_rows(ROW),_dim(DIM){
  //   srand(time(0));
  //   _elems = new T[COL*ROW*DIM];
  // }
  
  // template<typename T,size_t ROW, size_t COL, size_t DIM>
  // Matrix<T,size_t,size_t,size_t>::~Matrix(){
 
  //   if(_elems != 0){
  //     delete  [] _elems;0
  //     _elems = 0;
  //   }
 
  // }

  // template<typename T,size_t ROW, size_t COL, size_t DIM>
  // T& Matrix<T,size_t,size_t,size_t>::operator()(size_t i, size_t j, size_t dim){
  //   return _data[i*_cols+j+dim];
  // }
  
  // template<typename T,size_t ROW, size_t COL, size_t DIM>
  // const T& Matrix<T,size_t,size_t,size_t>::operator()(size_t i, size_t j, int dim) const{
  //   return _data[i*_cols+j+dim];
  // }

  
  // template<class T>
  // Matrix<T>& operator + (Matrix<T>& m1, Matrix<T>& m2){
  //   int cols = m1.cols();
  //   int rows = m1.rows();
  //   int dim  = m1.dim();
    
  //   Matrix<T>& m  = new Matrix<T>(rows,cols,dim);
    
  //   for(int i=0; i < rows; i++)
  //     for(int j=0; j < cols; j++)
  //       for(int d=0; d < dim; d++)
  //         m(i,j,d) = m1(i,j,d)+m2(i,j,d);

  //   return m;
  // }

  // template<class T>
  // Matrix<T>& operator - (Matrix<T>& m1, Matrix<T>& m2){
  //   int cols = m1.cols();
  //   int rows = m1.rows();
  //   int dim  = m1.dim();
    
  //   Matrix<T>& m  = new Matrix<T>(rows,cols,dim);
    
  //   for(int i=0; i < rows; i++)
  //     for(int j=0; j < cols; j++)
  //       for(int d=0; d < dim; d++)
  //         m(i,j,d) = m1(i,j,d)-m2(i,j,d);

  //   return m;
    
  // }

  //friend Matrix& operator * (Matrix&,Matrix&);

  // template<class T>
  // Matrix<T>& operator * (T s, Matrix<T>& m){
  //   int cols = m.cols();
  //   int rows = m.rows();
  //   int dim  = m.dim();
    
  //   Matrix<T>& mr  = new Matrix<T>(rows,cols,dim);
    
  //   for(int i=0; i < rows; i++)
  //     for(int j=0; j < cols; j++)
  //       for(int d=0; d < dim; d++)
  //         mr(i,j,d) = s*m(i,j,d);

  //   return mr;

  //}

  // template<typename T,size_t ROW, size_t COL, size_t DIM>
  // void Matrix<T,ROW,COL,DIM>::random(int a, int b){
    
  //   for(int i=0; i < _rows; i++)
  //     for(int j=0; j < _cols; j++)
  //       for(int d=0; d < _dim; d++){
  //         _data[i*_cols+j+d] = a + static_cast <T> (rand()) / ( static_cast <T> (RAND_MAX/(b-a)));
          
  //       }
  // }
  
  // template class Matrix<int,size_t,size_t,size_t>;

  // template class Matrix<double,size_t,size_t,size_t>;
  

}


