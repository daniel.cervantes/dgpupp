#include <num/ThreeDimMatrix.hpp>
#include <num/TOperators.hpp>

using namespace num;
typedef  ThreeDimMatrix<float,4,4,4,2> ThreeDimMatrixf4442;
int main(){
  
  ThreeDimMatrixf4442 m1;
  ThreeDimMatrixf4442 m2;
  ThreeDimMatrixf4442 m3;
  ThreeDimMatrixf4442 m4;

  ThreeDimMatrix<float,Dynamic,Dynamic,Dynamic,2> m5;
  ThreeDimMatrix<float,Dynamic,Dynamic,Dynamic,8> m6;
  
  float scalar = 0.5;
   
  // m1.random(0,10);
  // m2.random(0,10);
  // m3.random(0,10);
  // m5.random(0,10);
  
  // cout << m1 << endl;
  // cout <<  endl;
  // cout << m2 << endl;
  // cout <<  endl;
  // cout << m3 << endl;

  // m4 = scalar*m1 + scalar*m2 + m3 + scalar*m5;
  // cout << endl;

  // cout << m4 << endl;

  m6.resize(12,12,12);

  // for(int i=0; i < 10; ++i)
  //   for(int j=0; j < 10; ++j){
      
  //     m6(i,j,1,0) = 1;
  //     m6(i,j,1,1) = 1;
  //     m6(i,j,1,2) = 1;
  //     m6(i,j,1,3) = 1;
  //     m6(i,j,1,4) = 1;
  //     m6(i,j,1,5) = 1;
  //     m6(i,j,1,6) = 1;
  //     m6(i,j,1,7) = 1;
      
  //   }


  m6(6,6,5,0) = 5;
  m6(6,6,5,1) = 5;
  m6(6,6,5,2) = 5;
  m6(6,6,5,3) = 5;
  m6(6,6,5,4) = -10;
  m6(6,6,5,5) = -10;
  m6(6,6,5,6) = -10;
  m6(6,6,5,7) = -10;

  m6(6,6,6,0) = -15;
  m6(6,6,6,1) = -15;
  m6(6,6,6,2) = -15;
  m6(6,6,6,3) = -15;
  m6(6,6,6,4) = 15;
  m6(6,6,6,5) = 15;
  m6(6,6,6,6) = 15;
  m6(6,6,6,7) = 15;

  m6(6,6,7,0) = 10;
  m6(6,6,7,1) = 10;
  m6(6,6,7,2) = 10;
  m6(6,6,7,3) = 10;
  m6(6,6,7,4) = -5;
  m6(6,6,7,5) = -5;
  m6(6,6,7,6) = -5;
  m6(6,6,7,7) = -5;

  // cout << m6 << endl;



  // for(int n=1; n < 11; n++)
  //   for(int m=1; m < 11; m++)
  //     for(int l=1; l < 11; l++)
  //       cout <<n <<" " << m << " " << l  << "--> " <<m6(n,m,l,0) << " " <<  m6(n,m,l,1) << " "<< m6(n,m,l,2) << " " <<  m6(n,m,l,3) << " "  
  //            << m6(n,m,l,4) << " " <<  m6(n,m,l,5) << " "<< m6(n,m,l,6) << " " <<  m6(n,m,l,7)  << endl;

  
  for(int n=1; n < 11; n++)
    for(int m=1; m < 11; m++)
      for(int l=1; l < 11; l++)
        cout <<n <<" " << m << " " << l-1  << "--> " <<m6(n,m,l-1,0) << " " <<  m6(n,m,l-1,1) << " "<< m6(n,m,l-1,2) << " " <<  m6(n,m,l-1,3) << " "  
             << m6(n,m,l-1,4) << " " <<  m6(n,m,l-1,5) << " "<< m6(n,m,l-1,6) << " " <<  m6(n,m,l-1,7)  << endl;


  
  return 0;
    
}
