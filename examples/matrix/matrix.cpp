#include <num/TOperators.hpp>

using namespace num;

int main(){

  Matrix<float,3,3,4> m1;
  Matrix<float,3,3,4> m2;
  Matrix<float,3,3,4> m3;
  Matrix<float,3,3,4> m4;
  Matrix<float,3,3,4> m5;
  Matrix<float,3,3,4> m6;


  Matrix<float,Dynamic,Dynamic,4> m7;
  
  Matrix<float,3,3,1> m8;
  Matrix<float,3,3,1> m9;

  Matrix<float,3,3,1> m18;
  Matrix<float,3,3,1> m19;

  Matrix<float,3,3,1> m10;
  
  
  Matrix<float,3,1,1> col;
  Matrix<float,3,1,1> col1;
  Matrix<float,3,1,1> col2;
  
  float scalar = 0.5;
  
  m1.random(10,12);
  m2.random();
  m3.random();
  m4.random();
  m5.random();

  m7.resize(3,3);
  m7.random();
  
  // cout << m1 << endl;
  // cout << m2 << endl;
  // cout << m3 << endl;
  // cout << m4 << endl;
  // cout << m5 << endl;
  
  // cout << m7 << endl;
  
  m6 = scalar*m1 + scalar*m2 + scalar*m3 + scalar*m4 + scalar*m7 ;
  cout << m6 << endl;

  m8.random();
  m9.random();
  m18.random();
  m19.random();
  
  col.random();
  col1.random();
  col2.random();

  //cout << col << endl;
  //cout << m9 << endl;  
  cout << "---------------------" << endl;
  cout << m8 << endl;
  cout << col << endl;
  
  col =  m8*col;
  cout << col2 << endl;

  cout << "-------------------- "<< endl; 
  cout << m8 << endl;
  cout << m9 << endl;

  cout << m18 << endl;
  cout << m19 << endl;

  m10 = m8*m9 + m18*m19 + m18*m19 + m8*m9 ;
  cout << m10 << endl;
  
  return 0;

    
}
