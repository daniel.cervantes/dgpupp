// This file is part of DGPU++

//    Foobar is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    Foobar is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.



#include <stdio.h>
//Para compilar dese la carpeta superior
//cmake .
//make
//Para correr:
//./bin/aDiff3D_histo_cuda -1 1 -1 1 -1 1 30 30 30 .01 .1
//./bin/aDiff3D_subdom_histo_cuda 0 50 0 50 0 35 400 400 275 .01 .1 
//./bin/aDiff3D_subdom_histo_cuda 0 50 0 50 0 50 100 100 100 .01 6
#include <fstream>
#include <utils/GNUplot.hpp>
#include <string>
#include <iostream>
#include <vector>
#include <sstream>


#define BLOCK_SIZE 8

using namespace std;

const int DIM = 8;

struct DIType {
  int k;
  int l;
  int m;
};




__device__ __host__ inline float& klf(int k, int l, int DIM, float *array){return array[k*DIM+l];}
__device__ __host__ inline float klf(int k, int l, float *array){return array[k*DIM+l];}
__device__ __host__ inline void  klf(int k, int l, float *array, float value){array[k*DIM+l] = value;}
__device__ __host__ inline void  klmdf( int k, int l, int m, int K, int L, int M, float *array, int d, float value){array[k*M*L*DIM+l*M*DIM+m*DIM+d] = value;}
__device__ __host__ inline float klmdf( int k, int l, int m, int K, int L, int M, float *array, int d){return array[k*M*L*DIM+l*M*DIM+m*DIM+d];}
__device__ __host__ inline float klmf(int k, int l, int m, int K, int L, int M, float *array){return array[k*M*L+l*M+m];} 
__device__ __host__ inline void  klmf(int k, int l, int m, int K, int L, int M, float *array, float value){array[k*M*L+l*M+m] = value;} 





void init_host_Matrices(float dx, float dy, float dz, 
                        float* h_ml, float* h_mltri,
                        float *h_mllR, float *h_mplR, float *h_mllL,  float *h_mplL,  float *h_mx,
                        float *h_mllT, float *h_mplT, float *h_mllBo, float *h_mplBo, float *h_my,
                        float *h_mllF, float *h_mplF, float *h_mllBa, float *h_mplBa, float *h_mz,
                        float* h_b){
  
  

  float a00, a01, a02, a03, a04, a05, a06, a07;
  float a10, a11, a12, a13, a14, a15, a16, a17;
  float a20, a21, a22, a23, a24, a25, a26, a27;
  float a30, a31, a32, a33, a34, a35, a36, a37;
  float a40, a41, a42, a43, a44, a45, a46, a47;
  float a50, a51, a52, a53, a54, a55, a56, a57;
  float a60, a61, a62, a63, a64, a65, a66, a67;
  float a70, a71, a72, a73, a74, a75, a76, a77;


  
  h_ml[0]  = (dx*dy*dz)/27;   h_ml[1]  = (dx*dy*dz)/54;   h_ml[2] = (dx*dy*dz)/108;  h_ml[3] = (dx*dy*dz)/54;   h_ml[4] = (dx*dy*dz)/54;    h_ml[5] = (dx*dy*dz)/108;  h_ml[6]  = (dx*dy*dz)/216; h_ml[7]  = (dx*dy*dz)/108;
  h_ml[8]  = (dx*dy*dz)/54;   h_ml[9]  = (dx*dy*dz)/27;  h_ml[10] = (dx*dy*dz)/54;  h_ml[11] = (dx*dy*dz)/108;  h_ml[12] = (dx*dy*dz)/108;  h_ml[13] = (dx*dy*dz)/54;  h_ml[14] = (dx*dy*dz)/108; h_ml[15] = (dx*dy*dz)/216;
  h_ml[16] = (dx*dy*dz)/108;  h_ml[17] = (dx*dy*dz)/54;  h_ml[18] = (dx*dy*dz)/27;  h_ml[19] = (dx*dy*dz)/54;   h_ml[20] = (dx*dy*dz)/216;  h_ml[21] = (dx*dy*dz)/108; h_ml[22] = (dx*dy*dz)/54;  h_ml[23] = (dx*dy*dz)/108;
  h_ml[24] = (dx*dy*dz)/54;   h_ml[25] = (dx*dy*dz)/108; h_ml[26] =  (dx*dy*dz)/54; h_ml[27] = (dx*dy*dz)/27;   h_ml[28] = (dx*dy*dz)/108;  h_ml[29] = (dx*dy*dz)/216; h_ml[30] = (dx*dy*dz)/108; h_ml[31] = (dx*dy*dz)/54;
  h_ml[32] = (dx*dy*dz)/54;   h_ml[33] = (dx*dy*dz)/108; h_ml[34] = (dx*dy*dz)/216; h_ml[35] = (dx*dy*dz)/108;  h_ml[36] = (dx*dy*dz)/27;   h_ml[37] = (dx*dy*dz)/54;  h_ml[38] = (dx*dy*dz)/108; h_ml[39] = (dx*dy*dz)/54;
  h_ml[40] = (dx*dy*dz)/108;  h_ml[41] = (dx*dy*dz)/54;  h_ml[42] = (dx*dy*dz)/108; h_ml[43] = (dx*dy*dz)/216;  h_ml[44] =  (dx*dy*dz)/54;  h_ml[45] = (dx*dy*dz)/27;  h_ml[46] = (dx*dy*dz)/54;  h_ml[47] = (dx*dy*dz)/108;
  h_ml[48] = (dx*dy*dz)/216;  h_ml[49] = (dx*dy*dz)/108; h_ml[50] = (dx*dy*dz)/54;  h_ml[51] = (dx*dy*dz)/108;  h_ml[52] = (dx*dy*dz)/108;  h_ml[53] = (dx*dy*dz)/54;  h_ml[54] = (dx*dy*dz)/27;  h_ml[55] = (dx*dy*dz)/54;
  h_ml[56] = (dx*dy*dz)/108;  h_ml[57] = (dx*dy*dz)/216; h_ml[58] = (dx*dy*dz)/108; h_ml[59] = (dx*dy*dz)/54;   h_ml[60] = (dx*dy*dz)/54;   h_ml[61] = (dx*dy*dz)/108; h_ml[62] = (dx*dy*dz)/54;  h_ml[63] = (dx*dy*dz)/27;  


  a00 = (dx*dy*dz)/27;    a01  = (dx*dy*dz)/54; a02 = (dx*dy*dz)/108; a03 = (dx*dy*dz)/54;   a04 = (dx*dy*dz)/54;   a05 = (dx*dy*dz)/108; a06  = (dx*dy*dz)/216; a07 = (dx*dy*dz)/108;
  a10 = (dx*dy*dz)/54;    a11  = (dx*dy*dz)/27; a12 = (dx*dy*dz)/54;  a13 = (dx*dy*dz)/108;  a14 = (dx*dy*dz)/108;  a15 = (dx*dy*dz)/54;  a16 = (dx*dy*dz)/108;  a17 = (dx*dy*dz)/216;
  a20 = (dx*dy*dz)/108;   a21 = (dx*dy*dz)/54;  a22 = (dx*dy*dz)/27;  a23 = (dx*dy*dz)/54;   a24 = (dx*dy*dz)/216;  a25 = (dx*dy*dz)/108; a26 = (dx*dy*dz)/54;   a27 = (dx*dy*dz)/108;
  a30 = (dx*dy*dz)/54;    a31 = (dx*dy*dz)/108; a32 =  (dx*dy*dz)/54; a33 = (dx*dy*dz)/27;   a34 = (dx*dy*dz)/108;  a35 = (dx*dy*dz)/216; a36 = (dx*dy*dz)/108;  a37 = (dx*dy*dz)/54;
  a40 = (dx*dy*dz)/54;    a41 = (dx*dy*dz)/108; a42 = (dx*dy*dz)/216; a43 = (dx*dy*dz)/108;  a44 = (dx*dy*dz)/27;   a45 = (dx*dy*dz)/54;  a46 = (dx*dy*dz)/108;  a47 = (dx*dy*dz)/54;
  a50 = (dx*dy*dz)/108;   a51 = (dx*dy*dz)/54;  a52 = (dx*dy*dz)/108; a53 = (dx*dy*dz)/216;  a54 =  (dx*dy*dz)/54;  a55 = (dx*dy*dz)/27;  a56 = (dx*dy*dz)/54;   a57 = (dx*dy*dz)/108;
  a60 = (dx*dy*dz)/216;   a61 = (dx*dy*dz)/108; a62 = (dx*dy*dz)/54;  a63 = (dx*dy*dz)/108;  a64 = (dx*dy*dz)/108;  a65 = (dx*dy*dz)/54;  a66 = (dx*dy*dz)/27;   a67 = (dx*dy*dz)/54;
  a70 = (dx*dy*dz)/108;   a71 = (dx*dy*dz)/216; a72 = (dx*dy*dz)/108; a73 = (dx*dy*dz)/54;   a74 = (dx*dy*dz)/54;   a75 = (dx*dy*dz)/108; a76 = (dx*dy*dz)/54;   a77 = (dx*dy*dz)/27;  

  klf(0,0,DIM,h_mltri) = a00;
  klf(1,0,DIM,h_mltri) = a10;
  klf(2,0,DIM,h_mltri) = a20;
  klf(3,0,DIM,h_mltri) = a30;
  klf(4,0,DIM,h_mltri) = a40;
  klf(5,0,DIM,h_mltri) = a50;
  klf(6,0,DIM,h_mltri) = a60;
  klf(7,0,DIM,h_mltri) = a70;
  
  klf(1,0,DIM,h_ml) -= (a10/a00)*a00; klf(1,1,DIM,h_ml) -= (a10/a00)*a01; klf(1,2,DIM,h_ml) -= (a10/a00)*a02; klf(1,3,DIM,h_ml) -= (a10/a00)*a03; klf(1,4,DIM,h_ml) -= (a10/a00)*a04; klf(1,5,DIM,h_ml) -= (a10/a00)*a05; klf(1,6,DIM,h_ml) -= (a10/a00)*a06; klf(1,7,DIM,h_ml) -= (a10/a00)*a07; 
  klf(2,0,DIM,h_ml) -= (a20/a00)*a00; klf(2,1,DIM,h_ml) -= (a20/a00)*a01; klf(2,2,DIM,h_ml) -= (a20/a00)*a02; klf(2,3,DIM,h_ml) -= (a20/a00)*a03; klf(2,4,DIM,h_ml) -= (a20/a00)*a04; klf(2,5,DIM,h_ml) -= (a20/a00)*a05; klf(2,6,DIM,h_ml) -= (a20/a00)*a06; klf(2,7,DIM,h_ml) -= (a20/a00)*a07; 
  klf(3,0,DIM,h_ml) -= (a30/a00)*a00; klf(3,1,DIM,h_ml) -= (a30/a00)*a01; klf(3,2,DIM,h_ml) -= (a30/a00)*a02; klf(3,3,DIM,h_ml) -= (a30/a00)*a03; klf(3,4,DIM,h_ml) -= (a30/a00)*a04; klf(3,5,DIM,h_ml) -= (a30/a00)*a05; klf(3,6,DIM,h_ml) -= (a30/a00)*a06; klf(3,7,DIM,h_ml) -= (a30/a00)*a07; 
  klf(4,0,DIM,h_ml) -= (a40/a00)*a00; klf(4,1,DIM,h_ml) -= (a40/a00)*a01; klf(4,2,DIM,h_ml) -= (a40/a00)*a02; klf(4,3,DIM,h_ml) -= (a40/a00)*a03; klf(4,4,DIM,h_ml) -= (a40/a00)*a04; klf(4,5,DIM,h_ml) -= (a40/a00)*a05; klf(4,6,DIM,h_ml) -= (a40/a00)*a06; klf(4,7,DIM,h_ml) -= (a40/a00)*a07; 
  klf(5,0,DIM,h_ml) -= (a50/a00)*a00; klf(5,1,DIM,h_ml) -= (a50/a00)*a01; klf(5,2,DIM,h_ml) -= (a50/a00)*a02; klf(5,3,DIM,h_ml) -= (a50/a00)*a03; klf(5,4,DIM,h_ml) -= (a50/a00)*a04; klf(5,5,DIM,h_ml) -= (a50/a00)*a05; klf(5,6,DIM,h_ml) -= (a50/a00)*a06; klf(5,7,DIM,h_ml) -= (a50/a00)*a07; 
  klf(6,0,DIM,h_ml) -= (a60/a00)*a00; klf(6,1,DIM,h_ml) -= (a60/a00)*a01; klf(6,2,DIM,h_ml) -= (a60/a00)*a02; klf(6,3,DIM,h_ml) -= (a60/a00)*a03; klf(6,4,DIM,h_ml) -= (a60/a00)*a04; klf(6,5,DIM,h_ml) -= (a60/a00)*a05; klf(6,6,DIM,h_ml) -= (a60/a00)*a06; klf(6,7,DIM,h_ml) -= (a60/a00)*a07; 
  klf(7,0,DIM,h_ml) -= (a70/a00)*a00; klf(7,1,DIM,h_ml) -= (a70/a00)*a01; klf(7,2,DIM,h_ml) -= (a70/a00)*a02; klf(7,3,DIM,h_ml) -= (a70/a00)*a03; klf(7,4,DIM,h_ml) -= (a70/a00)*a04; klf(7,5,DIM,h_ml) -= (a70/a00)*a05; klf(7,6,DIM,h_ml) -= (a70/a00)*a06; klf(7,7,DIM,h_ml) -= (a70/a00)*a07; 


  a10 = klf(1,0,DIM,h_ml); a11 = klf(1,1,DIM,h_ml); a12 = klf(1,2,DIM,h_ml); a13 = klf(1,3,DIM,h_ml); a14 = klf(1,4,DIM,h_ml); a15 = klf(1,5,DIM,h_ml); a16 = klf(1,6,DIM,h_ml); a17 = klf(1,7,DIM,h_ml); 
  a20 = klf(2,0,DIM,h_ml); a21 = klf(2,1,DIM,h_ml); a22 = klf(2,2,DIM,h_ml); a23 = klf(2,3,DIM,h_ml); a24 = klf(2,4,DIM,h_ml); a25 = klf(2,5,DIM,h_ml); a26 = klf(2,6,DIM,h_ml); a27 = klf(2,7,DIM,h_ml); 
  a30 = klf(3,0,DIM,h_ml); a31 = klf(3,1,DIM,h_ml); a32 = klf(3,2,DIM,h_ml); a33 = klf(3,3,DIM,h_ml); a34 = klf(3,4,DIM,h_ml); a35 = klf(3,5,DIM,h_ml); a36 = klf(3,6,DIM,h_ml); a37 = klf(3,7,DIM,h_ml); 
  a40 = klf(4,0,DIM,h_ml); a41 = klf(4,1,DIM,h_ml); a42 = klf(4,2,DIM,h_ml); a43 = klf(4,3,DIM,h_ml); a44 = klf(4,4,DIM,h_ml); a45 = klf(4,5,DIM,h_ml); a46 = klf(4,6,DIM,h_ml); a47 = klf(4,7,DIM,h_ml); 
  a50 = klf(5,0,DIM,h_ml); a51 = klf(5,1,DIM,h_ml); a52 = klf(5,2,DIM,h_ml); a53 = klf(5,3,DIM,h_ml); a54 = klf(5,4,DIM,h_ml); a55 = klf(5,5,DIM,h_ml); a56 = klf(5,6,DIM,h_ml); a57 = klf(5,7,DIM,h_ml); 
  a60 = klf(6,0,DIM,h_ml); a61 = klf(6,1,DIM,h_ml); a62 = klf(6,2,DIM,h_ml); a63 = klf(6,3,DIM,h_ml); a64 = klf(6,4,DIM,h_ml); a65 = klf(6,5,DIM,h_ml); a66 = klf(6,6,DIM,h_ml); a67 = klf(6,7,DIM,h_ml); 
  a70 = klf(7,0,DIM,h_ml); a71 = klf(7,1,DIM,h_ml); a72 = klf(7,2,DIM,h_ml); a73 = klf(7,3,DIM,h_ml); a74 = klf(7,4,DIM,h_ml); a75 = klf(7,5,DIM,h_ml); a76 = klf(7,6,DIM,h_ml); a77 = klf(7,7,DIM,h_ml); 


  klf(1,1,DIM,h_mltri) = a11;
  klf(2,1,DIM,h_mltri) = a21;
  klf(3,1,DIM,h_mltri) = a31;
  klf(4,1,DIM,h_mltri) = a41;
  klf(5,1,DIM,h_mltri) = a51;
  klf(6,1,DIM,h_mltri) = a61;
  klf(7,1,DIM,h_mltri) = a71;

  
  klf(2,1,DIM,h_ml) -= (a21/a11)*a11; klf(2,2,DIM,h_ml) -= (a21/a11)*a12; klf(2,3,DIM,h_ml) -= (a21/a11)*a13; klf(2,4,DIM,h_ml) -= (a21/a11)*a14; klf(2,5,DIM,h_ml) -= (a21/a11)*a15; klf(2,6,DIM,h_ml) -= (a21/a11)*a16; klf(2,7,DIM,h_ml) -= (a21/a11)*a17; 
  klf(3,1,DIM,h_ml) -= (a31/a11)*a11; klf(3,2,DIM,h_ml) -= (a31/a11)*a12; klf(3,3,DIM,h_ml) -= (a31/a11)*a13; klf(3,4,DIM,h_ml) -= (a31/a11)*a14; klf(3,5,DIM,h_ml) -= (a31/a11)*a15; klf(3,6,DIM,h_ml) -= (a31/a11)*a16; klf(3,7,DIM,h_ml) -= (a31/a11)*a17; 
  klf(4,1,DIM,h_ml) -= (a41/a11)*a11; klf(4,2,DIM,h_ml) -= (a41/a11)*a12; klf(4,3,DIM,h_ml) -= (a41/a11)*a13; klf(4,4,DIM,h_ml) -= (a41/a11)*a14; klf(4,5,DIM,h_ml) -= (a41/a11)*a15; klf(4,6,DIM,h_ml) -= (a41/a11)*a16; klf(4,7,DIM,h_ml) -= (a41/a11)*a17; 
  klf(5,1,DIM,h_ml) -= (a51/a11)*a11; klf(5,2,DIM,h_ml) -= (a51/a11)*a12; klf(5,3,DIM,h_ml) -= (a51/a11)*a13; klf(5,4,DIM,h_ml) -= (a51/a11)*a14; klf(5,5,DIM,h_ml) -= (a51/a11)*a15; klf(5,6,DIM,h_ml) -= (a51/a11)*a16; klf(5,7,DIM,h_ml) -= (a51/a11)*a17; 
  klf(6,1,DIM,h_ml) -= (a61/a11)*a11; klf(6,2,DIM,h_ml) -= (a61/a11)*a12; klf(6,3,DIM,h_ml) -= (a61/a11)*a13; klf(6,4,DIM,h_ml) -= (a61/a11)*a14; klf(6,5,DIM,h_ml) -= (a61/a11)*a15; klf(6,6,DIM,h_ml) -= (a61/a11)*a16; klf(6,7,DIM,h_ml) -= (a61/a11)*a17; 
  klf(7,1,DIM,h_ml) -= (a71/a11)*a11; klf(7,2,DIM,h_ml) -= (a71/a11)*a12; klf(7,3,DIM,h_ml) -= (a71/a11)*a13; klf(7,4,DIM,h_ml) -= (a71/a11)*a14; klf(7,5,DIM,h_ml) -= (a71/a11)*a15; klf(7,6,DIM,h_ml) -= (a71/a11)*a16; klf(7,7,DIM,h_ml) -= (a71/a11)*a17; 


  a21 = klf(2,1,DIM,h_ml); a22 = klf(2,2,DIM,h_ml); a23 = klf(2,3,DIM,h_ml); a24 = klf(2,4,DIM,h_ml); a25 = klf(2,5,DIM,h_ml); a26 = klf(2,6,DIM,h_ml); a27 = klf(2,7,DIM,h_ml); 
  a31 = klf(3,1,DIM,h_ml); a32 = klf(3,2,DIM,h_ml); a33 = klf(3,3,DIM,h_ml); a34 = klf(3,4,DIM,h_ml); a35 = klf(3,5,DIM,h_ml); a36 = klf(3,6,DIM,h_ml); a37 = klf(3,7,DIM,h_ml); 
  a41 = klf(4,1,DIM,h_ml); a42 = klf(4,2,DIM,h_ml); a43 = klf(4,3,DIM,h_ml); a44 = klf(4,4,DIM,h_ml); a45 = klf(4,5,DIM,h_ml); a46 = klf(4,6,DIM,h_ml); a47 = klf(4,7,DIM,h_ml); 
  a51 = klf(5,1,DIM,h_ml); a52 = klf(5,2,DIM,h_ml); a53 = klf(5,3,DIM,h_ml); a54 = klf(5,4,DIM,h_ml); a55 = klf(5,5,DIM,h_ml); a56 = klf(5,6,DIM,h_ml); a57 = klf(5,7,DIM,h_ml); 
  a61 = klf(6,1,DIM,h_ml); a62 = klf(6,2,DIM,h_ml); a63 = klf(6,3,DIM,h_ml); a64 = klf(6,4,DIM,h_ml); a65 = klf(6,5,DIM,h_ml); a66 = klf(6,6,DIM,h_ml); a67 = klf(6,7,DIM,h_ml); 
  a71 = klf(7,1,DIM,h_ml); a72 = klf(7,2,DIM,h_ml); a73 = klf(7,3,DIM,h_ml); a74 = klf(7,4,DIM,h_ml); a75 = klf(7,5,DIM,h_ml); a76 = klf(7,6,DIM,h_ml); a77 = klf(7,7,DIM,h_ml); 


  klf(2,2,DIM,h_mltri) = a22;
  klf(3,2,DIM,h_mltri) = a32;
  klf(4,2,DIM,h_mltri) = a42;
  klf(5,2,DIM,h_mltri) = a52;
  klf(6,2,DIM,h_mltri) = a62;
  klf(7,2,DIM,h_mltri) = a72;
  
  
  klf(3,2,DIM,h_ml) -= (a32/a22)*a22; klf(3,3,DIM,h_ml) -= (a32/a22)*a23; klf(3,4,DIM,h_ml) -= (a32/a22)*a24; klf(3,5,DIM,h_ml) -= (a32/a22)*a25; klf(3,6,DIM,h_ml) -= (a32/a22)*a26; klf(3,7,DIM,h_ml) -= (a32/a22)*a27; 
  klf(4,2,DIM,h_ml) -= (a42/a22)*a22; klf(4,3,DIM,h_ml) -= (a42/a22)*a23; klf(4,4,DIM,h_ml) -= (a42/a22)*a24; klf(4,5,DIM,h_ml) -= (a42/a22)*a25; klf(4,6,DIM,h_ml) -= (a42/a22)*a26; klf(4,7,DIM,h_ml) -= (a42/a22)*a27; 
  klf(5,2,DIM,h_ml) -= (a52/a22)*a22; klf(5,3,DIM,h_ml) -= (a52/a22)*a23; klf(5,4,DIM,h_ml) -= (a52/a22)*a24; klf(5,5,DIM,h_ml) -= (a52/a22)*a25; klf(5,6,DIM,h_ml) -= (a52/a22)*a26; klf(5,7,DIM,h_ml) -= (a52/a22)*a27; 
  klf(6,2,DIM,h_ml) -= (a62/a22)*a22; klf(6,3,DIM,h_ml) -= (a62/a22)*a23; klf(6,4,DIM,h_ml) -= (a62/a22)*a24; klf(6,5,DIM,h_ml) -= (a62/a22)*a25; klf(6,6,DIM,h_ml) -= (a62/a22)*a26; klf(6,7,DIM,h_ml) -= (a62/a22)*a27; 
  klf(7,2,DIM,h_ml) -= (a72/a22)*a22; klf(7,3,DIM,h_ml) -= (a72/a22)*a23; klf(7,4,DIM,h_ml) -= (a72/a22)*a24; klf(7,5,DIM,h_ml) -= (a72/a22)*a25; klf(7,6,DIM,h_ml) -= (a72/a22)*a26; klf(7,7,DIM,h_ml) -= (a72/a22)*a27; 


  a32 = klf(3,2,DIM,h_ml); a33 = klf(3,3,DIM,h_ml); a34 = klf(3,4,DIM,h_ml); a35 = klf(3,5,DIM,h_ml); a36 = klf(3,6,DIM,h_ml); a37 = klf(3,7,DIM,h_ml); 
  a42 = klf(4,2,DIM,h_ml); a43 = klf(4,3,DIM,h_ml); a44 = klf(4,4,DIM,h_ml); a45 = klf(4,5,DIM,h_ml); a46 = klf(4,6,DIM,h_ml); a47 = klf(4,7,DIM,h_ml); 
  a52 = klf(5,2,DIM,h_ml); a53 = klf(5,3,DIM,h_ml); a54 = klf(5,4,DIM,h_ml); a55 = klf(5,5,DIM,h_ml); a56 = klf(5,6,DIM,h_ml); a57 = klf(5,7,DIM,h_ml); 
  a62 = klf(6,2,DIM,h_ml); a63 = klf(6,3,DIM,h_ml); a64 = klf(6,4,DIM,h_ml); a65 = klf(6,5,DIM,h_ml); a66 = klf(6,6,DIM,h_ml); a67 = klf(6,7,DIM,h_ml); 
  a72 = klf(7,2,DIM,h_ml); a73 = klf(7,3,DIM,h_ml); a74 = klf(7,4,DIM,h_ml); a75 = klf(7,5,DIM,h_ml); a76 = klf(7,6,DIM,h_ml); a77 = klf(7,7,DIM,h_ml); 


  klf(3,3,DIM,h_mltri) = a33;
  klf(4,3,DIM,h_mltri) = a43;
  klf(5,3,DIM,h_mltri) = a53;
  klf(6,3,DIM,h_mltri) = a63;
  klf(7,3,DIM,h_mltri) = a73;

  
  klf(4,3,DIM,h_ml) -= (a43/a33)*a33; klf(4,4,DIM,h_ml) -= (a43/a33)*a34; klf(4,5,DIM,h_ml) -= (a43/a33)*a35; klf(4,6,DIM,h_ml) -= (a43/a33)*a36; klf(4,7,DIM,h_ml) -= (a43/a33)*a37;
  klf(5,3,DIM,h_ml) -= (a53/a33)*a33; klf(5,4,DIM,h_ml) -= (a53/a33)*a34; klf(5,5,DIM,h_ml) -= (a53/a33)*a35; klf(5,6,DIM,h_ml) -= (a53/a33)*a36; klf(5,7,DIM,h_ml) -= (a53/a33)*a37;
  klf(6,3,DIM,h_ml) -= (a63/a33)*a33; klf(6,4,DIM,h_ml) -= (a63/a33)*a34; klf(6,5,DIM,h_ml) -= (a63/a33)*a35; klf(6,6,DIM,h_ml) -= (a63/a33)*a36; klf(6,7,DIM,h_ml) -= (a63/a33)*a37;
  klf(7,3,DIM,h_ml) -= (a73/a33)*a33; klf(7,4,DIM,h_ml) -= (a73/a33)*a34; klf(7,5,DIM,h_ml) -= (a73/a33)*a35; klf(7,6,DIM,h_ml) -= (a73/a33)*a36; klf(7,7,DIM,h_ml) -= (a73/a33)*a37;


  a43 = klf(4,3,DIM,h_ml); a44 = klf(4,4,DIM,h_ml); a45 = klf(4,5,DIM,h_ml); a46 = klf(4,6,DIM,h_ml); a47 = klf(4,7,DIM,h_ml); 
  a53 = klf(5,3,DIM,h_ml); a54 = klf(5,4,DIM,h_ml); a55 = klf(5,5,DIM,h_ml); a56 = klf(5,6,DIM,h_ml); a57 = klf(5,7,DIM,h_ml); 
  a63 = klf(6,3,DIM,h_ml); a64 = klf(6,4,DIM,h_ml); a65 = klf(6,5,DIM,h_ml); a66 = klf(6,6,DIM,h_ml); a67 = klf(6,7,DIM,h_ml); 
  a73 = klf(7,3,DIM,h_ml); a74 = klf(7,4,DIM,h_ml); a75 = klf(7,5,DIM,h_ml); a76 = klf(7,6,DIM,h_ml); a77 = klf(7,7,DIM,h_ml); 


  klf(4,4,DIM,h_mltri) = a44;
  klf(5,4,DIM,h_mltri) = a54;
  klf(6,4,DIM,h_mltri) = a64;
  klf(7,4,DIM,h_mltri) = a74;

  
  klf(5,4,DIM,h_ml) -= (a54/a44)*a44; klf(5,5,DIM,h_ml) -= (a54/a44)*a45; klf(5,6,DIM,h_ml) -= (a54/a44)*a46; klf(5,7,DIM,h_ml) -= (a54/a44)*a47; //klf(5,8,DIM,h_ml) -= (a54/a44)*b4; 
  klf(6,4,DIM,h_ml) -= (a64/a44)*a44; klf(6,5,DIM,h_ml) -= (a64/a44)*a45; klf(6,6,DIM,h_ml) -= (a64/a44)*a46; klf(6,7,DIM,h_ml) -= (a64/a44)*a47; //klf(6,8,DIM,h_ml) -= (a64/a44)*b4; 
  klf(7,4,DIM,h_ml) -= (a74/a44)*a44; klf(7,5,DIM,h_ml) -= (a74/a44)*a45; klf(7,6,DIM,h_ml) -= (a74/a44)*a46; klf(7,7,DIM,h_ml) -= (a74/a44)*a47; //klf(7,8,DIM,h_ml) -= (a74/a44)*b4; 

  
  a54 = klf(5,4,DIM,h_ml); a55 = klf(5,5,DIM,h_ml); a56 = klf(5,6,DIM,h_ml); a57 = klf(5,7,DIM,h_ml); 
  a64 = klf(6,4,DIM,h_ml); a65 = klf(6,5,DIM,h_ml); a66 = klf(6,6,DIM,h_ml); a67 = klf(6,7,DIM,h_ml); 
  a74 = klf(7,4,DIM,h_ml); a75 = klf(7,5,DIM,h_ml); a76 = klf(7,6,DIM,h_ml); a77 = klf(7,7,DIM,h_ml); 


  klf(5,5,DIM,h_mltri) = a55;
  klf(6,5,DIM,h_mltri) = a65;
  klf(7,5,DIM,h_mltri) = a75;

  klf(6,5,DIM,h_ml) -= (a65/a55)*a55; klf(6,6,DIM,h_ml) -= (a65/a55)*a56; klf(6,7,DIM,h_ml) -= (a65/a55)*a57; 
  klf(7,5,DIM,h_ml) -= (a75/a55)*a55; klf(7,6,DIM,h_ml) -= (a75/a55)*a56; klf(7,7,DIM,h_ml) -= (a75/a55)*a57; 


  a65 = klf(6,5,DIM,h_ml); a66 = klf(6,6,DIM,h_ml); a67 = klf(6,7,DIM,h_ml);
  a75 = klf(7,5,DIM,h_ml); a76 = klf(7,6,DIM,h_ml); a77 = klf(7,7,DIM,h_ml); 

  klf(6,6,DIM,h_mltri) = a66;
  klf(7,6,DIM,h_mltri) = a76;

  klf(7,6,DIM,h_ml) -= (a76/a66)*a66; klf(7,7,DIM,h_ml) -= (a76/a66)*a67; 
  
  klf(7,7,DIM,h_mltri) = a77;


  // mllR = {0,  0,  0, 0, 0, 0,  0, 0, 
  //         0,  0,  0, 0, 0, 0,  0, 0,
  //         0,  0,  (dy*dz)/9,  (dy*dz)/18,  0,  0,  (dy*dz)/18,   (dy*dz)/36,
  //         0,  0,  (dy*dz)/18, (dy*dz)/9,   0,  0,  (dy*dz)/36,   (dy*dz)/18,
  //         0,  0,  0,    0,     0,  0,  0,      0,
  //         0,  0,  0,    0,     0,  0,  0,      0,
  //         0,  0,  (dy*dz)/18, (dy*dz)/36,  0,  0,  (dy*dz)/9,    (dy*dz)/18,
  //         0,  0,  (dy*dz)/36, (dy*dz)/18,  0,  0,  (dy*dz)/18,   (dy*dz)/9};


  
  h_mllR[0]=0;   h_mllR[1]=0;   h_mllR[2]=0;            h_mllR[3]=0;            h_mllR[4]=0;   h_mllR[5]=0;   h_mllR[6]=0;             h_mllR[7]=0; 
  h_mllR[8]=0;   h_mllR[9]=0;   h_mllR[10]=0;           h_mllR[11]=0;           h_mllR[12]=0;  h_mllR[13]=0;  h_mllR[14]=0;            h_mllR[15]=0;
  h_mllR[16]=0;  h_mllR[17]=0;  h_mllR[18]=(dy*dz)/9;   h_mllR[19]=(dy*dz)/18;  h_mllR[20]=0;  h_mllR[21]=0;  h_mllR[22]=(dy*dz)/18;   h_mllR[23]=(dy*dz)/36;
  h_mllR[24]=0;  h_mllR[25]=0;  h_mllR[26]=(dy*dz)/18;  h_mllR[27]=(dy*dz)/9;   h_mllR[28]=0;  h_mllR[29]=0;  h_mllR[30]=(dy*dz)/36;   h_mllR[31]=(dy*dz)/18;
  h_mllR[32]=0;  h_mllR[33]=0;  h_mllR[34]=0;           h_mllR[35]=0;           h_mllR[36]=0;  h_mllR[37]=0;  h_mllR[38]=0;            h_mllR[39]=0;
  h_mllR[40]=0;  h_mllR[41]=0;  h_mllR[42]=0;           h_mllR[43]=0;           h_mllR[44]=0;  h_mllR[45]=0;  h_mllR[46]=0;            h_mllR[47]=0;
  h_mllR[48]=0;  h_mllR[49]=0;  h_mllR[50]=(dy*dz)/18;  h_mllR[51]=(dy*dz)/36;  h_mllR[52]=0;  h_mllR[53]=0;  h_mllR[54]=(dy*dz)/9;    h_mllR[55]=(dy*dz)/18;
  h_mllR[56]=0;  h_mllR[57]=0;  h_mllR[58]=(dy*dz)/36;  h_mllR[59]=(dy*dz)/18;  h_mllR[60]=0;  h_mllR[61]=0;  h_mllR[62]=(dy*dz)/18;   h_mllR[63]=(dy*dz)/9;



  //   float mplR[64] = {0,     0,     0,   0,   0,     0,     0,   0,
  //                      0,     0,     0,   0,   0,     0,     0,   0,
  //                      (dy*dz)/18,  (dy*dz)/9,   0,   0,   (dy*dz)/36,  (dy*dz)/18,  0,   0,
  //                      (dy*dz)/9,   (dy*dz)/18,  0,   0,   (dy*dz)/18,  (dy*dz)/36,  0,   0,
  //                      0,     0,     0,   0,   0,     0,     0,   0,
  //                      0,     0,     0,   0,   0,     0,     0,   0,
  //                      (dy*dz)/36,  (dy*dz)/18,  0,   0,   (dy*dz)/18,  (dy*dz)/9,   0,   0,
  //                      (dy*dz)/18,  (dy*dz)/36,  0,   0,   (dy*dz)/9,   (dy*dz)/18,  0,   0};
  
  
  h_mplR[0]=0;            h_mplR[1]=0;            h_mplR[2]=0;            h_mplR[3]=0;            h_mplR[4]=0;            h_mplR[5]=0;            h_mplR[6]=0;              h_mplR[7]=0; 
  h_mplR[8]=0;            h_mplR[9]=0;            h_mplR[10]=0;           h_mplR[11]=0;           h_mplR[12]=0;           h_mplR[13]=0;           h_mplR[14]=0;            h_mplR[15]=0;
  h_mplR[16]=(dy*dz)/18;  h_mplR[17]=(dy*dz)/9;   h_mplR[18]=0;           h_mplR[19]=0;           h_mplR[20]=(dy*dz)/36;  h_mplR[21]=(dy*dz)/18;  h_mplR[22]=0;           h_mplR[23]=0;
  h_mplR[24]=(dy*dz)/9;   h_mplR[25]=(dy*dz)/18;  h_mplR[26]=0;           h_mplR[27]=0;           h_mplR[28]=(dy*dz)/18;  h_mplR[29]=(dy*dz)/36;  h_mplR[30]=0;           h_mplR[31]=0;
  h_mplR[32]=0;           h_mplR[33]=0;           h_mplR[34]=0;           h_mplR[35]=0;           h_mplR[36]=0;           h_mplR[37]=0;           h_mplR[38]=0;            h_mplR[39]=0;
  h_mplR[40]=0;           h_mplR[41]=0;           h_mplR[42]=0;           h_mplR[43]=0;           h_mplR[44]=0;           h_mplR[45]=0;           h_mplR[46]=0;            h_mplR[47]=0;
  h_mplR[48]=(dy*dz)/36;  h_mplR[49]=(dy*dz)/18;  h_mplR[50]=0;           h_mplR[51]=0;           h_mplR[52]=(dy*dz)/18;  h_mplR[53]=(dy*dz)/9;   h_mplR[54]=0;            h_mplR[55]=0;
  h_mplR[56]=(dy*dz)/18;  h_mplR[57]=(dy*dz)/36;  h_mplR[58]=0;           h_mplR[59]=0;           h_mplR[60]=(dy*dz)/9;   h_mplR[61]=(dy*dz)/18;  h_mplR[62]=0;            h_mplR[63]=0;

  
//   float mllL[64] = {(dy*dz)/9,   (dy*dz)/18,  0,  0,  (dy*dz)/18,  (dy*dz)/36,  0,  0,
//                      (dy*dz)/18,  (dy*dz)/9,   0,  0,  (dy*dz)/36,  (dy*dz)/18,  0,  0,
//                      0,     0,     0,  0,  0,     0,     0,  0,
//                      0,     0,     0,  0,  0,     0,     0,  0,
//                      (dy*dz)/18,  (dy*dz)/36,  0,  0,  (dy*dz)/9,   (dy*dz)/18,  0,  0,
//                      (dy*dz)/36,  (dy*dz)/18,  0,  0,  (dy*dz)/18,  (dy*dz)/9,   0,  0,
//                      0,     0,     0,  0,  0,     0,     0,  0,
//                      0,     0,     0,  0,  0,     0,     0,  0};



  h_mllL[0]=(dy*dz)/9;    h_mllL[1]=(dy*dz)/18;      h_mllL[2]=0;            h_mllL[3]=0;            h_mllL[4]=(dy*dz)/18;   h_mllL[5]=(dy*dz)/36;    h_mllL[6]=0;             h_mllL[7]=0; 
  h_mllL[8]=(dy*dz)/18;   h_mllL[9]=(dy*dz)/9;       h_mllL[10]=0;           h_mllL[11]=0;           h_mllL[12]=(dy*dz)/36;  h_mllL[13]=(dy*dz)/18;   h_mllL[14]=0;            h_mllL[15]=0;
  h_mllL[16]=0;           h_mllL[17]=0;              h_mllL[18]=0;           h_mllL[19]=0;           h_mllL[20]=0;           h_mllL[21]=0;            h_mllL[22]=0;            h_mllL[23]=0;
  h_mllL[24]=0;           h_mllL[25]=0;              h_mllL[26]=0;           h_mllL[27]=0;           h_mllL[28]=0;           h_mllL[29]=0;            h_mllL[30]=0;            h_mllL[31]=0;
  h_mllL[32]=(dy*dz)/18;  h_mllL[33]=(dy*dz)/36;     h_mllL[34]=0;           h_mllL[35]=0;           h_mllL[36]=(dy*dz)/9;   h_mllL[37]=(dy*dz)/18;   h_mllL[38]=0;            h_mllL[39]=0;
  h_mllL[40]=(dy*dz)/36;  h_mllL[41]=(dy*dz)/18;     h_mllL[42]=0;           h_mllL[43]=0;           h_mllL[44]=(dy*dz)/18;  h_mllL[45]=(dy*dz)/9;    h_mllL[46]=0;            h_mllL[47]=0;
  h_mllL[48]=0;           h_mllL[49]=0;              h_mllL[50]=0;           h_mllL[51]=0;           h_mllL[52]=0;           h_mllL[53]=0;            h_mllL[54]=0;            h_mllL[55]=0;
  h_mllL[56]=0;           h_mllL[57]=0;              h_mllL[58]=0;           h_mllL[59]=0;           h_mllL[60]=0;           h_mllL[61]=0;            h_mllL[62]=0;            h_mllL[63]=0;

  
//   float mplL[64] = {0,  0,  (dy*dz)/18,  (dy*dz)/9,   0,  0,  (dy*dz)/36,  (dy*dz)/18,
//                      0,  0,  (dy*dz)/9,   (dy*dz)/18,  0,  0,  (dy*dz)/18,  (dy*dz)/36,
//                      0,  0,  0,     0,     0,  0,  0,     0,
//                      0,  0,  0,     0,     0,  0,  0,     0,
//                      0,  0,  (dy*dz)/36,  (dy*dz)/18,  0,  0,  (dy*dz)/18,  (dy*dz)/9,
//                      0,  0,  (dy*dz)/18,  (dy*dz)/36,  0,  0,  (dy*dz)/9,   (dy*dz)/18,
//                      0,  0,  0,     0,     0,  0,  0,     0,
//                      0,  0,  0,     0,     0,  0,  0,     0};


  h_mplL[0]=0;   h_mplL[1]=0;   h_mplL[2]=(dy*dz)/18;           h_mplL[3]=(dy*dz)/9;             h_mplL[4]=0;   h_mplL[5]=0;   h_mplL[6]=(dy*dz)/36;             h_mplL[7]=(dy*dz)/18; 
  h_mplL[8]=0;   h_mplL[9]=0;   h_mplL[10]=(dy*dz)/9;           h_mplL[11]=(dy*dz)/18;           h_mplL[12]=0;  h_mplL[13]=0;  h_mplL[14]=(dy*dz)/18;            h_mplL[15]=(dy*dz)/36;
  h_mplL[16]=0;  h_mplL[17]=0;  h_mplL[18]=0;                   h_mplL[19]=0;                    h_mplL[20]=0;  h_mplL[21]=0;  h_mplL[22]=0;                     h_mplL[23]=0;
  h_mplL[24]=0;  h_mplL[25]=0;  h_mplL[26]=0;                   h_mplL[27]=0;                    h_mplL[28]=0;  h_mplL[29]=0;  h_mplL[30]=0;                     h_mplL[31]=0;
  h_mplL[32]=0;  h_mplL[33]=0;  h_mplL[34]=(dy*dz)/36;          h_mplL[35]=(dy*dz)/18;           h_mplL[36]=0;  h_mplL[37]=0;  h_mplL[38]=(dy*dz)/18;            h_mplL[39]=(dy*dz)/9;
  h_mplL[40]=0;  h_mplL[41]=0;  h_mplL[42]=(dy*dz)/18;          h_mplL[43]=(dy*dz)/36;           h_mplL[44]=0;  h_mplL[45]=0;  h_mplL[46]=(dy*dz)/9;             h_mplL[47]=(dy*dz)/18;
  h_mplL[48]=0;  h_mplL[49]=0;  h_mplL[50]=0;                   h_mplL[51]=0;                    h_mplL[52]=0;  h_mplL[53]=0;  h_mplL[54]=0;                     h_mplL[55]=0;
  h_mplL[56]=0;  h_mplL[57]=0;  h_mplL[58]=0;                   h_mplL[59]=0;                    h_mplL[60]=0;  h_mplL[61]=0;  h_mplL[62]=0;                     h_mplL[63]=0;


//   float mx[64] = {-(dy*dz)/18, -(dy*dz)/36, -(dy*dz)/36, -(dy*dz)/18, -(dy*dz)/36, -(dy*dz)/72, -(dy*dz)/72, -(dy*dz)/36,
//                    -(dy*dz)/36, -(dy*dz)/18, -(dy*dz)/18, -(dy*dz)/36, -(dy*dz)/72, -(dy*dz)/36, -(dy*dz)/36, -(dy*dz)/72,
//                    (dy*dz)/36,  (dy*dz)/18,  (dy*dz)/18,  (dy*dz)/36,  (dy*dz)/72,  (dy*dz)/36,  (dy*dz)/36,  (dy*dz)/72,
//                    (dy*dz)/18,  (dy*dz)/36,  (dy*dz)/36,  (dy*dz)/18,  (dy*dz)/36,  (dy*dz)/72,  (dy*dz)/72,  (dy*dz)/36,
//                    -(dy*dz)/36, -(dy*dz)/72, -(dy*dz)/72, -(dy*dz)/36, -(dy*dz)/18, -(dy*dz)/36, -(dy*dz)/36, -(dy*dz)/18,
//                    -(dy*dz)/72, -(dy*dz)/36, -(dy*dz)/36, -(dy*dz)/72, -(dy*dz)/36, -(dy*dz)/18, -(dy*dz)/18, -(dy*dz)/36,
//                    (dy*dz)/72, (dy*dz)/36, (dy*dz)/36, (dy*dz)/72, (dy*dz)/36, (dy*dz)/18, (dy*dz)/18, (dy*dz)/36,
//                    (dy*dz)/36,  (dy*dz)/72,  (dy*dz)/72,  (dy*dz)/36,  (dy*dz)/18,  (dy*dz)/36, (dy*dz)/36, (dy*dz)/18};


h_mx[0] = -(dy*dz)/18;  h_mx[1] = -(dy*dz)/36;  h_mx[2] = -(dy*dz)/36;  h_mx[3] = -(dy*dz)/18;  h_mx[4] = -(dy*dz)/36;  h_mx[5] = -(dy*dz)/72;  h_mx[6] = -(dy*dz)/72;  h_mx[7] = -(dy*dz)/36;
h_mx[8] = -(dy*dz)/36;  h_mx[9] = -(dy*dz)/18;  h_mx[10] = -(dy*dz)/18; h_mx[11] = -(dy*dz)/36; h_mx[12] = -(dy*dz)/72; h_mx[13] = -(dy*dz)/36; h_mx[14] = -(dy*dz)/36; h_mx[15] = -(dy*dz)/72;
h_mx[16] = (dy*dz)/36;  h_mx[17] = (dy*dz)/18;  h_mx[18] = (dy*dz)/18;  h_mx[19] = (dy*dz)/36;  h_mx[20] = (dy*dz)/72;  h_mx[21] = (dy*dz)/36;  h_mx[22] = (dy*dz)/36;  h_mx[23] = (dy*dz)/72;
h_mx[24] = (dy*dz)/18;  h_mx[25] = (dy*dz)/36;  h_mx[26] = (dy*dz)/36;  h_mx[27] = (dy*dz)/18;  h_mx[28] = (dy*dz)/36;  h_mx[29] = (dy*dz)/72;  h_mx[30] = (dy*dz)/72;  h_mx[31] = (dy*dz)/36;
h_mx[32] = -(dy*dz)/36; h_mx[33] = -(dy*dz)/72; h_mx[34] = -(dy*dz)/72; h_mx[35] = -(dy*dz)/36; h_mx[36] = -(dy*dz)/18; h_mx[37] = -(dy*dz)/36; h_mx[38] = -(dy*dz)/36; h_mx[39] = -(dy*dz)/18;
h_mx[40] = -(dy*dz)/72; h_mx[41] = -(dy*dz)/36; h_mx[42] = -(dy*dz)/36; h_mx[43] = -(dy*dz)/72; h_mx[44] = -(dy*dz)/36; h_mx[45] = -(dy*dz)/18; h_mx[46] = -(dy*dz)/18; h_mx[47] = -(dy*dz)/36;
h_mx[48] = (dy*dz)/72;  h_mx[49] = (dy*dz)/36;  h_mx[50] = (dy*dz)/36;  h_mx[51] = (dy*dz)/72;  h_mx[52] = (dy*dz)/36;  h_mx[53] = (dy*dz)/18;  h_mx[54] = (dy*dz)/18;  h_mx[55] = (dy*dz)/36;
h_mx[56] = (dy*dz)/36;  h_mx[57] = (dy*dz)/72;  h_mx[58] = (dy*dz)/72;  h_mx[59] = (dy*dz)/36;  h_mx[60] = (dy*dz)/18;  h_mx[61] = (dy*dz)/36;  h_mx[62] = (dy*dz)/36;  h_mx[63] = (dy*dz)/18;





//   float mllT[64] = {0, 0, 0, 0, 0, 0, 0, 0,
//                      0, (dx*dz)/9, (dx*dz)/18, 0, 0, (dx*dz)/18, (dx*dz)/36, 0,
//                      0, (dx*dz)/18, (dx*dz)/9, 0, 0, (dx*dz)/36, (dx*dz)/18, 0,
//                      0, 0,    0,   0, 0, 0,    0,   0,
//                      0, 0,    0,   0, 0, 0,    0,   0,
//                      0, (dx*dz)/18, (dx*dz)/36, 0, 0, (dx*dz)/9, (dx*dz)/18, 0,
//                      0, (dx*dz)/36, (dx*dz)/18, 0, 0, (dx*dz)/18, (dx*dz)/9, 0,
//                      0, 0,    0,    0, 0, 0,    0,   0};


h_mllT[0]=0;   h_mllT[1]=0;            h_mllT[2]=0;            h_mllT[3]=0;            h_mllT[4]=0;            h_mllT[5]=0;             h_mllT[6]=0;             h_mllT[7]=0; 
h_mllT[8]=0;   h_mllT[9]=(dx*dz)/9;    h_mllT[10]=(dx*dz)/18;  h_mllT[11]=0;           h_mllT[12]=0;           h_mllT[13]=(dx*dz)/18;   h_mllT[14]=(dx*dz)/36;   h_mllT[15]=0;
h_mllT[16]=0;  h_mllT[17]=(dx*dz)/18;  h_mllT[18]=(dx*dz)/9;   h_mllT[19]=0;           h_mllT[20]=0;           h_mllT[21]=(dx*dz)/36;   h_mllT[22]=(dx*dz)/18;   h_mllT[23]=0;
h_mllT[24]=0;  h_mllT[25]=0;           h_mllT[26]=0;           h_mllT[27]=0;           h_mllT[28]=0;           h_mllT[29]=0;            h_mllT[30]=0;            h_mllT[31]=0;
h_mllT[32]=0;  h_mllT[33]=0;           h_mllT[34]=0;           h_mllT[35]=0;           h_mllT[36]=0;           h_mllT[37]=0;            h_mllT[38]=0;            h_mllT[39]=0;
h_mllT[40]=0;  h_mllT[41]=(dx*dz)/18;  h_mllT[42]=(dx*dz)/36;  h_mllT[43]=0;           h_mllT[44]=0;           h_mllT[45]=(dx*dz)/9;    h_mllT[46]=(dx*dz)/18;   h_mllT[47]=0;
h_mllT[48]=0;  h_mllT[49]=(dx*dz)/36;  h_mllT[50]=(dx*dz)/18;  h_mllT[51]=0;           h_mllT[52]=0;           h_mllT[53]=(dx*dz)/18;   h_mllT[54]=(dx*dz)/9;    h_mllT[55]=0;
h_mllT[56]=0;  h_mllT[57]=0;           h_mllT[58]=0;           h_mllT[59]=0;           h_mllT[60]=0;           h_mllT[61]=0;            h_mllT[62]=0;            h_mllT[63]=0;




//   float mplT[64] = {0, 0, 0, 0, 0, 0, 0, 0,
//                      (dx*dz)/9, 0, 0, (dx*dz)/18, (dx*dz)/18, 0, 0, (dx*dz)/36,
//                      (dx*dz)/18, 0, 0, (dx*dz)/9, (dx*dz)/36, 0, 0, (dx*dz)/18,
//                      0, 0, 0, 0, 0, 0, 0, 0,
//                      0, 0, 0, 0, 0, 0, 0, 0,
//                      (dx*dz)/18, 0, 0, (dx*dz)/36, (dx*dz)/9, 0, 0, (dx*dz)/18,
//                      (dx*dz)/36, 0, 0, (dx*dz)/18, (dx*dz)/18, 0, 0, (dx*dz)/9,
//                      0,    0, 0, 0,    0,    0, 0, 0};

  h_mplT[0]=0;            h_mplT[1]=0;   h_mplT[2]=0;            h_mplT[3]=0;            h_mplT[4]=0;            h_mplT[5]=0;  h_mplT[6]=0;              h_mplT[7]=0; 
  h_mplT[8]=(dx*dz)/9;    h_mplT[9]=0;   h_mplT[10]=0;           h_mplT[11]=(dx*dz)/18;  h_mplT[12]=(dx*dz)/18;  h_mplT[13]=0;  h_mplT[14]=0;            h_mplT[15]=(dx*dz)/36;
  h_mplT[16]=(dx*dz)/18;  h_mplT[17]=0;  h_mplT[18]=0;           h_mplT[19]=(dx*dz)/9;   h_mplT[20]=(dx*dz)/36;  h_mplT[21]=0;  h_mplT[22]=0;            h_mplT[23]=(dx*dz)/18;
  h_mplT[24]=0;           h_mplT[25]=0;  h_mplT[26]=0;           h_mplT[27]=0;           h_mplT[28]=0;           h_mplT[29]=0;  h_mplT[30]=0;            h_mplT[31]=0;
  h_mplT[32]=0;           h_mplT[33]=0;  h_mplT[34]=0;           h_mplT[35]=0;           h_mplT[36]=0;           h_mplT[37]=0;  h_mplT[38]=0;            h_mplT[39]=0;
  h_mplT[40]=(dx*dz)/18;  h_mplT[41]=0;  h_mplT[42]=0;           h_mplT[43]=(dx*dz)/36;  h_mplT[44]=(dx*dz)/9;   h_mplT[45]=0;  h_mplT[46]=0;            h_mplT[47]=(dx*dz)/18;
  h_mplT[48]=(dx*dz)/36;  h_mplT[49]=0;  h_mplT[50]=0;           h_mplT[51]=(dx*dz)/18;  h_mplT[52]=(dx*dz)/18;  h_mplT[53]=0;  h_mplT[54]=0;            h_mplT[55]=(dx*dz)/9;
  h_mplT[56]=0;           h_mplT[57]=0;  h_mplT[58]=0;           h_mplT[59]=0;           h_mplT[60]=0;           h_mplT[61]=0;  h_mplT[62]=0;            h_mplT[63]=0;


  
//   float mllBo[64] = {(dx*dz)/9,   0,   0,   (dx*dz)/18,  (dx*dz)/18,  0,  0,  (dx*dz)/36,
//                       0,     0,   0,   0,     0,     0,  0,  0,
//                       0,     0,   0,   0,     0,     0,  0,  0,
//                       (dx*dz)/18,  0,   0,   (dx*dz)/9,   (dx*dz)/36,  0,  0,  (dx*dz)/18,
//                       (dx*dz)/18,  0,   0,   (dx*dz)/36,  (dx*dz)/9,   0,  0,  (dx*dz)/18,
//                       0,     0,   0,   0,     0,     0,  0,  0,
//                       0,     0,   0,   0,     0,     0,  0,  0,
//                       (dx*dz)/36,  0,   0,   (dx*dz)/18,  (dx*dz)/18,  0,  0, (dx*dz)/9};



  h_mllBo[0]=(dx*dz)/9;     h_mllBo[1]=0;   h_mllBo[2]=0;            h_mllBo[3]=(dx*dz)/18;            h_mllBo[4]=(dx*dz)/18;       h_mllBo[5]=0;   h_mllBo[6]=0;            h_mllBo[7]=(dx*dz)/36; 
  h_mllBo[8]=0;             h_mllBo[9]=0;   h_mllBo[10]=0;           h_mllBo[11]=0;                    h_mllBo[12]=0;               h_mllBo[13]=0;  h_mllBo[14]=0;           h_mllBo[15]=0;
  h_mllBo[16]=0;            h_mllBo[17]=0;  h_mllBo[18]=0;           h_mllBo[19]=0;                    h_mllBo[20]=0;               h_mllBo[21]=0;  h_mllBo[22]=0;           h_mllBo[23]=0;
  h_mllBo[24]= (dx*dz)/18;  h_mllBo[25]=0;  h_mllBo[26]=0;           h_mllBo[27]=(dx*dz)/9;            h_mllBo[28]=(dx*dz)/36;      h_mllBo[29]=0;  h_mllBo[30]=0;           h_mllBo[31]=(dx*dz)/18;
  h_mllBo[32]= (dx*dz)/18;  h_mllBo[33]=0;  h_mllBo[34]=0;           h_mllBo[35]=(dx*dz)/36;           h_mllBo[36]=(dx*dz)/9;       h_mllBo[37]=0;  h_mllBo[38]=0;           h_mllBo[39]=(dx*dz)/18;
  h_mllBo[40]=0;            h_mllBo[41]=0;  h_mllBo[42]=0;           h_mllBo[43]=0;                    h_mllBo[44]=0;               h_mllBo[45]=0;  h_mllBo[46]=0;           h_mllBo[47]=0;
  h_mllBo[48]=0;            h_mllBo[49]=0;  h_mllBo[50]=0;           h_mllBo[51]=0;                    h_mllBo[52]=0;               h_mllBo[53]=0;  h_mllBo[54]=0;           h_mllBo[55]=0;
  h_mllBo[56]=(dx*dz)/36;   h_mllBo[57]=0;  h_mllBo[58]=0;           h_mllBo[59]=(dx*dz)/18;           h_mllBo[60]=(dx*dz)/18;      h_mllBo[61]=0;  h_mllBo[62]=0;           h_mllBo[63]=(dx*dz)/9;





//   float mplBo[64] = {0, (dx*dz)/9,  (dx*dz)/18,  0,   0,  (dx*dz)/18,  (dx*dz)/36,  0,
//                       0, 0,    0,     0,   0,  0,     0,     0,
//                       0, 0,    0,     0,   0,  0,     0,     0,
//                       0, (dx*dz)/18, (dx*dz)/9,   0,   0,  (dx*dz)/36,  (dx*dz)/18,  0,
//                       0, (dx*dz)/18, (dx*dz)/36,  0,   0,  (dx*dz)/9,   (dx*dz)/18,  0,
//                       0, 0,    0,     0,   0,  0,     0,     0,
//                       0, 0,    0,     0,   0,  0,     0,     0,
//                       0, (dx*dz)/36, (dx*dz)/18,  0,   0,  (dx*dz)/18,  (dx*dz)/9,   0};



  h_mplBo[0]=0;   h_mplBo[1]=(dx*dz)/9;    h_mplBo[2]=(dx*dz)/18;   h_mplBo[3]=0;            h_mplBo[4]=0;   h_mplBo[5]=(dx*dz)/18;   h_mplBo[6]=(dx*dz)/36;   h_mplBo[7]=0; 
  h_mplBo[8]=0;   h_mplBo[9]=0;            h_mplBo[10]=0;           h_mplBo[11]=0;           h_mplBo[12]=0;  h_mplBo[13]=0;           h_mplBo[14]=0;           h_mplBo[15]=0;
  h_mplBo[16]=0;  h_mplBo[17]=0;           h_mplBo[18]=0;           h_mplBo[19]=0;           h_mplBo[20]=0;  h_mplBo[21]=0;           h_mplBo[22]=0;           h_mplBo[23]=0;
  h_mplBo[24]=0;  h_mplBo[25]=(dx*dz)/18;  h_mplBo[26]=(dx*dz)/9;   h_mplBo[27]=0;           h_mplBo[28]=0;  h_mplBo[29]=(dx*dz)/36;  h_mplBo[30]=(dx*dz)/18;  h_mplBo[31]=0;
  h_mplBo[32]=0;  h_mplBo[33]=(dx*dz)/18;  h_mplBo[34]=(dx*dz)/36;  h_mplBo[35]=0;           h_mplBo[36]=0;  h_mplBo[37]=(dx*dz)/9;   h_mplBo[38]=(dx*dz)/18;  h_mplBo[39]=0;
  h_mplBo[40]=0;  h_mplBo[41]=0;           h_mplBo[42]=0;           h_mplBo[43]=0;           h_mplBo[44]=0;  h_mplBo[45]=0;           h_mplBo[46]=0;           h_mplBo[47]=0;
  h_mplBo[48]=0;  h_mplBo[49]=0;           h_mplBo[50]=0;           h_mplBo[51]=0;           h_mplBo[52]=0;  h_mplBo[53]=0;           h_mplBo[54]=0;           h_mplBo[55]=0;
  h_mplBo[56]=0;  h_mplBo[57]=(dx*dz)/36;  h_mplBo[58]=(dx*dz)/18;  h_mplBo[59]=0;           h_mplBo[60]=0;  h_mplBo[61]=(dx*dz)/18;  h_mplBo[62]=(dx*dz)/9;   h_mplBo[63]=0;


  
//   float my[64] = {-(dx*dz)/18, -(dx*dz)/18, -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/72, -(dx*dz)/72,
//                    (dx*dz)/18,  (dx*dz)/18,  (dx*dz)/36,  (dx*dz)/36,  (dx*dz)/36,  (dx*dz)/36,  (dx*dz)/72,  (dx*dz)/72,
//                    (dx*dz)/36,  (dx*dz)/36,  (dx*dz)/18,  (dx*dz)/18,  (dx*dz)/72,  (dx*dz)/72,  (dx*dz)/36,  (dx*dz)/36,
//                    -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/18, -(dx*dz)/18, -(dx*dz)/72, -(dx*dz)/72, -(dx*dz)/36, -(dx*dz)/36,
//                    -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/72, -(dx*dz)/72, -(dx*dz)/18, -(dx*dz)/18, -(dx*dz)/36, -(dx*dz)/36,
//                    (dx*dz)/36, (dx*dz)/36, (dx*dz)/72, (dx*dz)/72, (dx*dz)/18, (dx*dz)/18, (dx*dz)/36, (dx*dz)/36,
//                    (dx*dz)/72, (dx*dz)/72, (dx*dz)/36, (dx*dz)/36, (dx*dz)/36, (dx*dz)/36, (dx*dz)/18, (dx*dz)/18,
//                    -(dx*dz)/72, -(dx*dz)/72, -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/18, -(dx*dz)/18};


h_my[0] = -(dx*dz)/18;  h_my[1] = -(dx*dz)/18; h_my[2] = -(dx*dz)/36;   h_my[3] = -(dx*dz)/36;  h_my[4] = -(dx*dz)/36;  h_my[5] = -(dx*dz)/36;  h_my[6] = -(dx*dz)/72;  h_my[7] = -(dx*dz)/72;
h_my[8] = (dx*dz)/18;   h_my[9] = (dx*dz)/18;  h_my[10] = (dx*dz)/36;   h_my[11] = (dx*dz)/36;  h_my[12] = (dx*dz)/36;  h_my[13] = (dx*dz)/36;  h_my[14] = (dx*dz)/72;  h_my[15] = (dx*dz)/72;
h_my[16] = (dx*dz)/36;  h_my[17] = (dx*dz)/36;  h_my[18] = (dx*dz)/18;  h_my[19] = (dx*dz)/18;  h_my[20] = (dx*dz)/72;  h_my[21] = (dx*dz)/72;  h_my[22] = (dx*dz)/36;  h_my[23] = (dx*dz)/36;
h_my[24] = -(dx*dz)/36; h_my[25] = -(dx*dz)/36; h_my[26] = -(dx*dz)/18; h_my[27] = -(dx*dz)/18; h_my[28] = -(dx*dz)/72; h_my[29] = -(dx*dz)/72; h_my[30] = -(dx*dz)/36; h_my[31] = -(dx*dz)/36;
h_my[32] = -(dx*dz)/36; h_my[33] = -(dx*dz)/36; h_my[34] = -(dx*dz)/72; h_my[35] = -(dx*dz)/72; h_my[36] = -(dx*dz)/18; h_my[37] = -(dx*dz)/18; h_my[38] = -(dx*dz)/36; h_my[39] = -(dx*dz)/36;
h_my[40] = (dx*dz)/36;  h_my[41] = (dx*dz)/36;  h_my[42] = (dx*dz)/72;  h_my[43] = (dx*dz)/72;  h_my[44] = (dx*dz)/18;  h_my[45] = (dx*dz)/18;  h_my[46] = (dx*dz)/36;  h_my[47] = (dx*dz)/36;
h_my[48] = (dx*dz)/72;  h_my[49] = (dx*dz)/72;  h_my[50] = (dx*dz)/36;  h_my[51] = (dx*dz)/36;  h_my[52] = (dx*dz)/36;  h_my[53] = (dx*dz)/36;  h_my[54] = (dx*dz)/18;  h_my[55] = (dx*dz)/18;
h_my[56] = -(dx*dz)/72; h_my[57] = -(dx*dz)/72; h_my[58] = -(dx*dz)/36; h_my[59] = -(dx*dz)/36; h_my[60] = -(dx*dz)/36; h_my[61] = -(dx*dz)/36; h_my[62] = -(dx*dz)/18; h_my[63] = -(dx*dz)/18;



//   float mllF[64] = {0, 0, 0, 0, 0, 0, 0, 0,
//                      0, 0, 0, 0, 0, 0, 0, 0,
//                      0, 0, 0, 0, 0, 0, 0, 0,
//                      0, 0, 0, 0, 0, 0, 0, 0,
//                      0, 0, 0, 0, (dx*dy)/9, (dx*dy)/18, (dx*dy)/36, (dx*dy)/18,
//                      0, 0, 0, 0, (dx*dy)/18, (dx*dy)/9, (dx*dy)/18, (dx*dy)/36,
//                      0, 0, 0, 0, (dx*dy)/36, (dx*dy)/18, (dx*dy)/9, (dx*dy)/18,
//                      0, 0, 0, 0, (dx*dy)/18, (dx*dy)/36, (dx*dy)/18, (dx*dy)/9};


  h_mllF[0]=0;   h_mllF[1]=0;   h_mllF[2]=0;            h_mllF[3]=0;            h_mllF[4]=0;            h_mllF[5]=0;            h_mllF[6]=0;                      h_mllF[7]=0; 
  h_mllF[8]=0;   h_mllF[9]=0;   h_mllF[10]=0;           h_mllF[11]=0;           h_mllF[12]=0;           h_mllF[13]=0;           h_mllF[14]=0;                     h_mllF[15]=0;
  h_mllF[16]=0;  h_mllF[17]=0;  h_mllF[18]=0;           h_mllF[19]=0;           h_mllF[20]=0;           h_mllF[21]=0;           h_mllF[22]=0;                     h_mllF[23]=0;
  h_mllF[24]=0;  h_mllF[25]=0;  h_mllF[26]=0;           h_mllF[27]=0;           h_mllF[28]=0;           h_mllF[29]=0;           h_mllF[30]=0;                     h_mllF[31]=0;
  h_mllF[32]=0;  h_mllF[33]=0;  h_mllF[34]=0;           h_mllF[35]=0;           h_mllF[36]=(dx*dy)/9;   h_mllF[37]=(dx*dy)/18;  h_mllF[38]=(dx*dy)/36;            h_mllF[39]=(dx*dy)/18;
  h_mllF[40]=0;  h_mllF[41]=0;  h_mllF[42]=0;           h_mllF[43]=0;           h_mllF[44]=(dx*dy)/18;  h_mllF[45]=(dx*dy)/9;   h_mllF[46]=(dx*dy)/18;            h_mllF[47]=(dx*dy)/36;
  h_mllF[48]=0;  h_mllF[49]=0;  h_mllF[50]=0;           h_mllF[51]=0;           h_mllF[52]=(dx*dy)/36;  h_mllF[53]=(dx*dy)/18;  h_mllF[54]=(dx*dy)/9;             h_mllF[55]=(dx*dy)/18;
  h_mllF[56]=0;  h_mllF[57]=0;  h_mllF[58]=0;           h_mllF[59]=0;           h_mllF[60]=(dx*dy)/18;  h_mllF[61]=(dx*dy)/36;  h_mllF[62]=(dx*dy)/18;            h_mllF[63]=(dx*dy)/9;




//   float mplF[64] = {0, 0, 0, 0, 0, 0, 0, 0,
//                      0, 0, 0, 0, 0, 0, 0, 0,
//                      0, 0, 0, 0, 0, 0, 0, 0,
//                      0, 0, 0, 0, 0, 0, 0, 0,
//                      (dx*dy)/9, (dx*dy)/18, (dx*dy)/36, (dx*dy)/18, 0, 0, 0, 0,
//                      (dx*dy)/18, (dx*dy)/9, (dx*dy)/18, (dx*dy)/36, 0, 0, 0, 0,
//                      (dx*dy)/36, (dx*dy)/18, (dx*dy)/9, (dx*dy)/18, 0, 0, 0, 0,
//                      (dx*dy)/18, (dx*dy)/36, (dx*dy)/18, (dx*dy)/9, 0, 0, 0, 0};



  h_mplF[0]=0;            h_mplF[1]=0;            h_mplF[2]=0;                h_mplF[3]=0;                    h_mplF[4]=0;   h_mplF[5]=0;  h_mplF[6]=0;              h_mplF[7]=0; 
  h_mplF[8]=0;            h_mplF[9]=0;            h_mplF[10]=0;               h_mplF[11]=0;                   h_mplF[12]=0;  h_mplF[13]=0;  h_mplF[14]=0;            h_mplF[15]=0;
  h_mplF[16]=0;           h_mplF[17]=0;           h_mplF[18]=0;               h_mplF[19]=0;                   h_mplF[20]=0;  h_mplF[21]=0;  h_mplF[22]=0;            h_mplF[23]=0;
  h_mplF[24]=0;           h_mplF[25]=0;           h_mplF[26]=0;               h_mplF[27]=0;                   h_mplF[28]=0;  h_mplF[29]=0;  h_mplF[30]=0;            h_mplF[31]=0;
  h_mplF[32]=(dx*dy)/9;   h_mplF[33]=(dx*dy)/18;  h_mplF[34]=(dx*dy)/36;      h_mplF[35]=(dx*dy)/18;          h_mplF[36]=0;  h_mplF[37]=0;  h_mplF[38]=0;            h_mplF[39]=0;
  h_mplF[40]=(dx*dy)/18;  h_mplF[41]=(dx*dy)/9;   h_mplF[42]=(dx*dy)/18;      h_mplF[43]=(dx*dy)/36;          h_mplF[44]=0;  h_mplF[45]=0;  h_mplF[46]=0;            h_mplF[47]=0;
  h_mplF[48]=(dx*dy)/36;  h_mplF[49]=(dx*dy)/18;  h_mplF[50]=(dx*dy)/9;       h_mplF[51]=(dx*dy)/18;          h_mplF[52]=0;  h_mplF[53]=0;  h_mplF[54]=0;            h_mplF[55]=0;
  h_mplF[56]=(dx*dy)/18;  h_mplF[57]=(dx*dy)/36;  h_mplF[58]=(dx*dy)/18;      h_mplF[59]=(dx*dy)/9;           h_mplF[60]=0;  h_mplF[61]=0;  h_mplF[62]=0;            h_mplF[63]=0;



  
//   float mllBa[64] = {(dx*dy)/9, (dx*dy)/18, (dx*dy)/36, (dx*dy)/18, 0, 0, 0, 0,
//                       (dx*dy)/18, (dx*dy)/9, (dx*dy)/18, (dx*dy)/36, 0, 0, 0, 0,
//                       (dx*dy)/36, (dx*dy)/18, (dx*dy)/9, (dx*dy)/18, 0, 0, 0, 0,
//                       (dx*dy)/18, (dx*dy)/36, (dx*dy)/18, (dx*dy)/9, 0, 0, 0, 0,
//                       0,    0,    0,    0,   0, 0, 0, 0,
//                       0,    0,    0,    0,   0, 0, 0, 0,
//                       0,    0,    0,    0,   0, 0, 0, 0,
//                       0,    0,    0,    0,   0, 0, 0, 0};


  h_mllBa[0]=(dx*dy)/9;    h_mllBa[1]=(dx*dy)/18;   h_mllBa[2]=(dx*dy)/36;        h_mllBa[3]=(dx*dy)/18;            h_mllBa[4]=0;   h_mllBa[5]=0;   h_mllBa[6]=0;             h_mllBa[7]=0; 
  h_mllBa[8]=(dx*dy)/18;   h_mllBa[9]=(dx*dy)/9;    h_mllBa[10]=(dx*dy)/18;       h_mllBa[11]=(dx*dy)/36;           h_mllBa[12]=0;  h_mllBa[13]=0;  h_mllBa[14]=0;            h_mllBa[15]=0;
  h_mllBa[16]=(dx*dy)/36;  h_mllBa[17]=(dx*dy)/18;  h_mllBa[18]=(dx*dy)/9;        h_mllBa[19]=(dx*dy)/18;           h_mllBa[20]=0;  h_mllBa[21]=0;  h_mllBa[22]=0;            h_mllBa[23]=0;
  h_mllBa[24]=(dx*dy)/18;  h_mllBa[25]=(dx*dy)/36;  h_mllBa[26]=(dx*dy)/18;       h_mllBa[27]=(dx*dy)/9;            h_mllBa[28]=0;  h_mllBa[29]=0;  h_mllBa[30]=0;            h_mllBa[31]=0;
  h_mllBa[32]=0;           h_mllBa[33]=0;           h_mllBa[34]=0;                h_mllBa[35]=0;                    h_mllBa[36]=0;  h_mllBa[37]=0;  h_mllBa[38]=0;            h_mllBa[39]=0;
  h_mllBa[40]=0;           h_mllBa[41]=0;           h_mllBa[42]=0;                h_mllBa[43]=0;                    h_mllBa[44]=0;  h_mllBa[45]=0;  h_mllBa[46]=0;            h_mllBa[47]=0;
  h_mllBa[48]=0;           h_mllBa[49]=0;           h_mllBa[50]=0;                h_mllBa[51]=0;                    h_mllBa[52]=0;  h_mllBa[53]=0;  h_mllBa[54]=0;            h_mllBa[55]=0;
  h_mllBa[56]=0;           h_mllBa[57]=0;           h_mllBa[58]=0;                h_mllBa[59]=0;                    h_mllBa[60]=0;  h_mllBa[61]=0;  h_mllBa[62]=0;            h_mllBa[63]=0;




//   float mplBa[64] = {0, 0, 0, 0, (dx*dy)/9, (dx*dy)/18, (dx*dy)/36, (dx*dy)/18,
//                       0, 0, 0, 0, (dx*dy)/18, (dx*dy)/9, (dx*dy)/18, (dx*dy)/36,
//                       0, 0, 0, 0, (dx*dy)/36, (dx*dy)/18, (dx*dy)/9, (dx*dy)/18,
//                       0, 0, 0, 0, (dx*dy)/18, (dx*dy)/36, (dx*dy)/18, (dx*dy)/9,
//                       0, 0, 0, 0, 0,    0,    0,    0,
//                       0, 0, 0, 0, 0,    0,    0,    0,
//                       0, 0, 0, 0, 0,    0,    0,    0,
//                       0, 0, 0, 0, 0,    0,    0,    0};

  h_mplBa[0]=0;   h_mplBa[1]=0;   h_mplBa[2]=0;            h_mplBa[3]=0;            h_mplBa[4]=(dx*dy)/9;    h_mplBa[5]=(dx*dy)/18;   h_mplBa[6]=(dx*dy)/36;     h_mplBa[7]=(dx*dy)/18; 
  h_mplBa[8]=0;   h_mplBa[9]=0;   h_mplBa[10]=0;           h_mplBa[11]=0;           h_mplBa[12]=(dx*dy)/18;  h_mplBa[13]=(dx*dy)/9;   h_mplBa[14]=(dx*dy)/18;    h_mplBa[15]=(dx*dy)/36;
  h_mplBa[16]=0;  h_mplBa[17]=0;  h_mplBa[18]=0;           h_mplBa[19]=0;           h_mplBa[20]=(dx*dy)/36;  h_mplBa[21]=(dx*dy)/18;  h_mplBa[22]=(dx*dy)/9;    h_mplBa[23]=(dx*dy)/18;
  h_mplBa[24]=0;  h_mplBa[25]=0;  h_mplBa[26]=0;           h_mplBa[27]=0;           h_mplBa[28]=(dx*dy)/18;  h_mplBa[29]=(dx*dy)/36;  h_mplBa[30]=(dx*dy)/18;   h_mplBa[31]=(dx*dy)/9;
  h_mplBa[32]=0;  h_mplBa[33]=0;  h_mplBa[34]=0;           h_mplBa[35]=0;           h_mplBa[36]=0;           h_mplBa[37]=0;           h_mplBa[38]=0;            h_mplBa[39]=0;
  h_mplBa[40]=0;  h_mplBa[41]=0;  h_mplBa[42]=0;           h_mplBa[43]=0;           h_mplBa[44]=0;           h_mplBa[45]=0;           h_mplBa[46]=0;            h_mplBa[47]=0;
  h_mplBa[48]=0;  h_mplBa[49]=0;  h_mplBa[50]=0;           h_mplBa[51]=0;           h_mplBa[52]=0;           h_mplBa[53]=0;           h_mplBa[54]=0;            h_mplBa[55]=0;
  h_mplBa[56]=0;  h_mplBa[57]=0;  h_mplBa[58]=0;           h_mplBa[59]=0;           h_mplBa[60]=0;           h_mplBa[61]=0;           h_mplBa[62]=0;            h_mplBa[63]=0;




//   float mz[64] = {-(dx*dy)/18, -(dx*dy)/36, -(dx*dy)/72, -(dx*dy)/36, -(dx*dy)/18, -(dx*dy)/36, -(dx*dy)/72, -(dx*dy)/36,
//                    -(dx*dy)/36, -(dx*dy)/18, -(dx*dy)/36, -(dx*dy)/72, -(dx*dy)/36, -(dx*dy)/18, -(dx*dy)/36, -(dx*dy)/72,
//                    -(dx*dy)/72, -(dx*dy)/36, -(dx*dy)/18, -(dx*dy)/36, -(dx*dy)/72, -(dx*dy)/36, -(dx*dy)/18, -(dx*dy)/36,
//                    -(dx*dy)/36, -(dx*dy)/72, -(dx*dy)/36, -(dx*dy)/18, -(dx*dy)/36, -(dx*dy)/72, -(dx*dy)/36, -(dx*dy)/18,
//                    (dx*dy)/18, (dx*dy)/36, (dx*dy)/72, (dx*dy)/36, (dx*dy)/18, (dx*dy)/36, (dx*dy)/72, (dx*dy)/36,
//                    (dx*dy)/36, (dx*dy)/18, (dx*dy)/36, (dx*dy)/72, (dx*dy)/36, (dx*dy)/18, (dx*dy)/36, (dx*dy)/72,
//                    (dx*dy)/72, (dx*dy)/36, (dx*dy)/18, (dx*dy)/36, (dx*dy)/72, (dx*dy)/36, (dx*dy)/18, (dx*dy)/36,
//                    (dx*dy)/36, (dx*dy)/72, (dx*dy)/36, (dx*dy)/18, (dx*dy)/36, (dx*dy)/72, (dx*dy)/36, (dx*dy)/18};



h_mz[0] = -(dx*dy)/18;  h_mz[1] = -(dx*dy)/36;  h_mz[2] = -(dx*dy)/72;  h_mz[3] = -(dx*dy)/36;  h_mz[4] = -(dx*dy)/18;  h_mz[5] = -(dx*dy)/36;  h_mz[6] = -(dx*dy)/72;  h_mz[7] = -(dx*dy)/36;
h_mz[8] = -(dx*dy)/36;  h_mz[9] = -(dx*dy)/18;  h_mz[10] = -(dx*dy)/36; h_mz[11] = -(dx*dy)/72; h_mz[12] = -(dx*dy)/36; h_mz[13] = -(dx*dy)/18; h_mz[14] = -(dx*dy)/36; h_mz[15] = -(dx*dy)/72;
h_mz[16] = -(dx*dy)/72; h_mz[17] = -(dx*dy)/36; h_mz[18] = -(dx*dy)/18; h_mz[19] = -(dx*dy)/36; h_mz[20] = -(dx*dy)/72; h_mz[21] = -(dx*dy)/36; h_mz[22] = -(dx*dy)/18; h_mz[23] = -(dx*dy)/36;
h_mz[24] = -(dx*dy)/36; h_mz[25] = -(dx*dy)/72; h_mz[26] = -(dx*dy)/36; h_mz[27] = -(dx*dy)/18; h_mz[28] = -(dx*dy)/36; h_mz[29] = -(dx*dy)/72; h_mz[30] = -(dx*dy)/36; h_mz[31] = -(dx*dy)/18;
h_mz[32] = (dx*dy)/18;  h_mz[33] = (dx*dy)/36;  h_mz[34] = (dx*dy)/72;  h_mz[35] = (dx*dy)/36;  h_mz[36] = (dx*dy)/18;  h_mz[37] = (dx*dy)/36;  h_mz[38] = (dx*dy)/72;  h_mz[39] = (dx*dy)/36;
h_mz[40] = (dx*dy)/36;  h_mz[41] = (dx*dy)/18;  h_mz[42] = (dx*dy)/36;  h_mz[43] = (dx*dy)/72;  h_mz[44] = (dx*dy)/36;  h_mz[45] = (dx*dy)/18;  h_mz[46] = (dx*dy)/36;  h_mz[47] = (dx*dy)/72;
h_mz[48] = (dx*dy)/72;  h_mz[49] = (dx*dy)/36;  h_mz[50] = (dx*dy)/18;  h_mz[51] = (dx*dy)/36;  h_mz[52] = (dx*dy)/72;  h_mz[53] = (dx*dy)/36;  h_mz[54] = (dx*dy)/18;  h_mz[55] = (dx*dy)/36;
h_mz[56] = (dx*dy)/36;  h_mz[57] = (dx*dy)/72;  h_mz[58] = (dx*dy)/36;  h_mz[59] = (dx*dy)/18;  h_mz[60] = (dx*dy)/36;  h_mz[61] = (dx*dy)/72;  h_mz[62] = (dx*dy)/36;  h_mz[63] = (dx*dy)/18;



  
  
//   float b[8] = {(dx*dy*dz)/8,
//                  (dx*dy*dz)/8,
//                  (dx*dy*dz)/8,
//                  (dx*dy*dz)/8,
//                  (dx*dy*dz)/8,
//                  (dx*dy*dz)/8,
//                  (dx*dy*dz)/8,
//                  (dx*dy*dz)/8};


h_b[0] = (dx*dy*dz)/8;
h_b[1] = (dx*dy*dz)/8;
h_b[2] = (dx*dy*dz)/8;
h_b[3] = (dx*dy*dz)/8;
h_b[4] = (dx*dy*dz)/8;
h_b[5] = (dx*dy*dz)/8;
h_b[6] = (dx*dy*dz)/8;
h_b[7] = (dx*dy*dz)/8;

 
}


void setDeltas(int skip_k, int skip_l, int skip_m, int K, int L, int M, vector<DIType> &indices){

  for(int k=1;k < K+1; k+=skip_k)
    for(int l=1; l < L+1; l+=skip_l)
      for(int m=1; m < M+1; m+=skip_m)
        indices.push_back({k,l,m});
}


void setDiffCoeff(int K, int L, int M, float* h_k, float diffCoeff){for(int i=0; i < (K+2)*(L+2)*(M+2); ++i)  h_k[i] = diffCoeff;}


__global__ void d_initUklm(int Ks, int Ls, int Ms, float *d_ml, float * d_mltri, float *d_b, float *d_deltas, float *d_uklm){
  
  int k = blockIdx.x * blockDim.x + threadIdx.x;
  int l = blockIdx.y * blockDim.y + threadIdx.y;
  int m = blockIdx.z * blockDim.z + threadIdx.z;

  if(  0 < k  && k < Ks+1  && 0 < l && l < Ls+1 && 0 < m &&  m < Ms+1 ){
  
        
    float b0,b1,b2,b3,b4,b5,b6,b7;
    float x0,x1,x2,x3,x4,x5,x6,x7;
    float delta;
    
    delta = klmf(k,l,m,Ks,Ms,Ls,d_deltas);    
    b0 = delta*d_b[0]; b1 = delta*d_b[1]; b2 = delta*d_b[2]; b3 = delta*d_b[3];  b4 = delta*d_b[4]; b5 = delta*d_b[5]; b6 = delta*d_b[6]; b7 = delta*d_b[7];


    b1 = b1 - ( klf(1,0,d_mltri)/klf(0,0,d_mltri) ) * b0;
    b2 = b2 - ( klf(2,0,d_mltri)/klf(0,0,d_mltri) ) * b0;
    b3 = b3 - ( klf(3,0,d_mltri)/klf(0,0,d_mltri) ) * b0;
    b4 = b4 - ( klf(4,0,d_mltri)/klf(0,0,d_mltri) ) * b0;
    b5 = b5 - ( klf(5,0,d_mltri)/klf(0,0,d_mltri) ) * b0;
    b6 = b6 - ( klf(6,0,d_mltri)/klf(0,0,d_mltri) ) * b0;
    b7 = b7 - ( klf(7,0,d_mltri)/klf(0,0,d_mltri) ) * b0;    
    
    b2 = b2 - ( klf(2,1,d_mltri)/klf(1,1,d_mltri) ) * b1;
    b3 = b3 - ( klf(3,1,d_mltri)/klf(1,1,d_mltri) ) * b1;
    b4 = b4 - ( klf(4,1,d_mltri)/klf(1,1,d_mltri) ) * b1;
    b5 = b5 - ( klf(5,1,d_mltri)/klf(1,1,d_mltri) ) * b1;
    b6 = b6 - ( klf(6,1,d_mltri)/klf(1,1,d_mltri) ) * b1;
    b7 = b7 - ( klf(7,1,d_mltri)/klf(1,1,d_mltri) ) * b1;
    
    b3 = b3 - ( klf(3,2,d_mltri)/klf(2,2,d_mltri) ) * b2;
    b4 = b4 - ( klf(4,2,d_mltri)/klf(2,2,d_mltri) ) * b2;
    b5 = b5 - ( klf(5,2,d_mltri)/klf(2,2,d_mltri) ) * b2;
    b6 = b6 - ( klf(6,2,d_mltri)/klf(2,2,d_mltri) ) * b2;
    b7 = b7 - ( klf(7,2,d_mltri)/klf(2,2,d_mltri) ) * b2;
    
    b4 = b4 - ( klf(4,3,d_mltri)/klf(3,3,d_mltri) ) * b3;
    b5 = b5 - ( klf(5,3,d_mltri)/klf(3,3,d_mltri) ) * b3;
    b6 = b6 - ( klf(6,3,d_mltri)/klf(3,3,d_mltri) ) * b3;
    b7 = b7 - ( klf(7,3,d_mltri)/klf(3,3,d_mltri) ) * b3;
    
    b5 = b5 - ( klf(5,4,d_mltri)/klf(4,4,d_mltri) ) * b4;
    b6 = b6 - ( klf(6,4,d_mltri)/klf(4,4,d_mltri) ) * b4;
    b7 = b7 - ( klf(7,4,d_mltri)/klf(4,4,d_mltri) ) * b4;    
    
    b6 = b6 - ( klf(6,5,d_mltri)/klf(5,5,d_mltri) ) * b5;
    b7 = b7 - ( klf(7,5,d_mltri)/klf(5,5,d_mltri) ) * b5;
        
    b7 = b7 - ( klf(7,6,d_mltri)/klf(6,6,d_mltri) ) * b6;



    x7 = (b7/klf(7,7,DIM,d_ml));
    x6 = (b6-klf(6,7,DIM,d_ml)*x7)/klf(6,6,DIM,d_ml);
    x5 = (b5-klf(5,6,DIM,d_ml)*x6-klf(5,7,DIM,d_ml)*x7)/klf(5,5,DIM,d_ml);
    x4 = (b4-klf(4,5,DIM,d_ml)*x5-klf(4,6,DIM,d_ml)*x6-klf(4,7,DIM,d_ml)*x7)/klf(4,4,DIM,d_ml);
    x3 = (b3-klf(3,4,DIM,d_ml)*x4-klf(3,5,DIM,d_ml)*x5-klf(3,6,DIM,d_ml)*x6-klf(3,7,DIM,d_ml)*x7)/klf(3,3,DIM,d_ml);
    x2 = (b2-klf(2,3,DIM,d_ml)*x3-klf(2,4,DIM,d_ml)*x4-klf(2,5,DIM,d_ml)*x5-klf(2,6,DIM,d_ml)*x6-klf(2,7,DIM,d_ml)*x7)/klf(2,2,DIM,d_ml);
    x1 = (b1-klf(1,2,DIM,d_ml)*x2-klf(1,3,DIM,d_ml)*x3-klf(1,4,DIM,d_ml)*x4-klf(1,5,DIM,d_ml)*x5-klf(1,6,DIM,d_ml)*x6-klf(1,7,DIM,d_ml)*x7)/klf(1,1,DIM,d_ml);
    x0 = (b0-klf(0,1,DIM,d_ml)*x1-klf(0,2,DIM,d_ml)*x2-klf(0,3,DIM,d_ml)*x3-klf(0,4,DIM,d_ml)*x4-klf(0,5,DIM,d_ml)*x5-klf(0,6,DIM,d_ml)*x6-klf(0,7,DIM,d_ml)*x7)/klf(0,0,DIM,d_ml);
    

    //printf("%d %d %d %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f \n",k,l,m,b7,b6,b5,b4,b3,b2,b1,b0);
    //printf("%d %d %d %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f \n",k,l,m,x7,x6,x5,x4,x3,x2,x1,x0);
    

    
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,0,x7);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,1,x6);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,2,x5);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,3,x4);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,4,x3);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,5,x2);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,6,x1);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,7,x0);

    // printf("-->%d %d %d %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f\n",k,l,m,
    //        klmdf(k,l,m,Ks,Ms,Ls,d_uklm,0), klmdf(k,l,m,Ks,Ms,Ls,d_uklm,1), klmdf(k,l,m,Ks,Ms,Ls,d_uklm,2), klmdf(k,l,m,Ks,Ms,Ls,d_uklm,3),
    //        klmdf(k,l,m,Ks,Ms,Ls,d_uklm,4), klmdf(k,l,m,Ks,Ms,Ls,d_uklm,5), klmdf(k,l,m,Ks,Ms,Ls,d_uklm,6), klmdf(k,l,m,Ks,Ms,Ls,d_uklm,7));
        

  }
}




inline __device__ __host__ float phi0(float x,float y,float z,float xkl12,float xkp12,float yll12,float ylp12,float zml12,float zmp12){
  return ((x-xkp12)/(xkl12-xkp12))*((y-ylp12)/(yll12-ylp12))*((z-zmp12)/(zml12-zmp12));
}
inline __device__ __host__ float phi1(float x,float y,float z,float xkl12,float xkp12,float yll12,float ylp12,float zml12,float zmp12){
  return ((x-xkp12)/(xkl12-xkp12))*((y-yll12)/(ylp12-yll12))*((z-zmp12)/(zml12-zmp12));
}
inline __device__ __host__ float phi2(float x,float y,float z,float xkl12,float xkp12,float yll12,float ylp12,float zml12,float zmp12){
  return ((x-xkl12)/(xkp12-xkl12))*((y-yll12)/(ylp12-yll12))*((z-zmp12)/(zml12-zmp12));
}
inline __device__ __host__ float phi3(float x,float y,float z,float xkl12,float xkp12,float yll12,float ylp12,float zml12,float zmp12){
  return ((x-xkl12)/(xkp12-xkl12))*((y-ylp12)/(yll12-ylp12))*((z-zmp12)/(zml12-zmp12));
}
inline  __device__ __host__ float phi4(float x,float y,float z,float xkl12,float xkp12,float yll12,float ylp12,float zml12,float zmp12){
  return ((x-xkp12)/(xkl12-xkp12))*((y-ylp12)/(yll12-ylp12))*((z-zml12)/(zmp12-zml12));
}
inline __device__ __host__ float phi5(float x,float y,float z,float xkl12,float xkp12,float yll12,float ylp12,float zml12,float zmp12){
  return ((x-xkp12)/(xkl12-xkp12))*((y-yll12)/(ylp12-yll12))*((z-zml12)/(zmp12-zml12));
}
inline __device__ __host__ float phi6(float x,float y,float z,float xkl12,float xkp12,float yll12,float ylp12,float zml12,float zmp12){
  return ((x-xkl12)/(xkp12-xkl12))*((y-yll12)/(ylp12-yll12))*((z-zml12)/(zmp12-zml12));
}
inline __device__ __host__ float phi7(float x,float y,float z,float xkl12,float xkp12,float yll12,float ylp12,float zml12,float zmp12){
  return ((x-xkl12)/(xkp12-xkl12))*((y-ylp12)/(yll12-ylp12))*((z-zml12)/(zmp12-zml12));
}

// void knots(float a, float b, float c, float d, float e, float f, int K, int L, int M, float* h_xIk, float* h_yIl, float* h_zIm){
  
//   float dx = (b-a)/K;
//   float dy = (d-c)/L;
//   float dz = (f-e)/M;

//   h_xIk[0] = a - dx;
//   for(int i=1; i < K+2; i++)
//     h_xIk[i] = a + ((b-a)/K)*(i-1);
//   h_xIk[K+2] = b + dx;
  
//   h_yIl[0] = c - dy;
//   for(int i=1; i < L+2; i++)
//     h_yIl[i] = c + ((d-c)/L)*(i-1);
//   h_yIl[L+2] = d + dy;

//   h_zIm[0] = e - dz;
//   for(int i=1; i < M+2; i++)
//     h_zIm[i] = e + ((f-e)/M)*(i-1);
//   h_zIm[M+2] = f + dz;
// }



__global__ void d_centerUklm(int Ks, int Ls, int Ms, float dx, float dy, float dz, float* d_uklm, float* d_uklmc){ 

  int k = blockIdx.x * blockDim.x + threadIdx.x;
  int l = blockIdx.y * blockDim.y + threadIdx.y;
  int m = blockIdx.z * blockDim.z + threadIdx.z;

  

  if(  0 < k  && k < Ks+1  && 0 < l && l < Ls+1 && 0 < m &&  m < Ms+1 ){
    
    float xkl12, xkp12, yll12, ylp12, zml12, zmp12;
    float xc,yc,zc;
    float temp;    
    
    // xkl12 = d_xIk[k];
    // xkp12 = d_xIk[k+1];
    // yll12 = d_yIl[l];
    // ylp12 = d_yIl[l+1];
    // zml12 = d_zIm[m];
    // zmp12 = d_zIm[m+1];

    xkl12 =  dx*k;
    xkp12 =  dx*(k+1);
    yll12 =  dy*l;
    ylp12 =  dy*(l+1);
    zml12 =  dz*m;
    zmp12 =  dz*(m+1);

    
    xc = 0.5*(xkl12+xkp12);
    yc = 0.5*(yll12+ylp12);
    zc = 0.5*(zml12+zmp12);


    //printf("%d %d %d %.3f %.3f %.3f %.3f %.3f %.3f  \n",k,l,m,xkl12,xkp12,yll12,ylp12,zml12,zmp12);

    temp = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,0)*phi0(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12)
          +klmdf(k,l,m,Ks,Ms,Ls,d_uklm,1)*phi1(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12)
          +klmdf(k,l,m,Ks,Ms,Ls,d_uklm,2)*phi2(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12)
          +klmdf(k,l,m,Ks,Ms,Ls,d_uklm,3)*phi3(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12)
          +klmdf(k,l,m,Ks,Ms,Ls,d_uklm,4)*phi4(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12)
          +klmdf(k,l,m,Ks,Ms,Ls,d_uklm,5)*phi5(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12)
          +klmdf(k,l,m,Ks,Ms,Ls,d_uklm,6)*phi6(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12)
          +klmdf(k,l,m,Ks,Ms,Ls,d_uklm,7)*phi7(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12);                              

    klmf(k-1,l-1,m-1,Ks,Ms,Ls,d_uklmc,temp);
    
    //printf("%d %d %d %.3f \n",k,l,m,klmf(k,l,m,K+2,L+2,M+2,d_uklmc));

  }
  
}


void saveUklm(int K, int L, int M, float * h_uklmc, float * h_xIk, float * h_yIl, float * h_zIm, string nfile, int skip_k, int skip_l, int skip_m){

  float xkl12, xkp12, yll12, ylp12, zml12, zmp12;
  float xc,yc,zc;
  float t;


  ofstream file;
  ofstream file2;
  ofstream file3;
  ofstream file4;
  ofstream file5;
  //ofstream file6;
  
  file.open(nfile.c_str());  
  std::string token = nfile.substr(0, nfile.find(".txt"));
  //file2.open ( (token+"_reduced.txt").c_str());
  file3.open ( (token+"_x.txt").c_str());
  file4.open ( (token+"_y.txt").c_str());
  file5.open ( (token+"_z.txt").c_str());
  //file6.open ( (token+".raw").c_str());
  
  
  //t = 0.0;
  
  for(int k=1; k < K+1; ++k){
    for(int l=1; l < L+1; ++l){
      for(int m=1; m < M+1; ++m){
        
        xkl12 = h_xIk[k];
        xkp12 = h_xIk[k+1];
        yll12 = h_yIl[l];
        ylp12 = h_yIl[l+1];
        zml12 = h_zIm[m];
        zmp12 = h_zIm[m+1];
        
        xc = 0.5*(xkl12+xkp12);
        yc = 0.5*(yll12+ylp12);
        zc = 0.5*(zml12+zmp12);
        
        
        if( klmf(k-1, l-1, m-1, K+2, L+2, M+2, h_uklmc) < 0 ){
          file <<  xc <<" " << yc << " " << zc <<" " << 0 << " " << endl;
        }
        else{
          file <<  xc <<" " << yc << " " << zc <<" " << klmf(k-1,l-1,m-1,K+2,L+2,M+2,h_uklmc) << " " << endl;
        }
        
      }
    }
    file << endl;
  } 
  file.close();


  // for(int k=1; k < K+1; k+=skip_k){
  //   for(int l=1; l < L+1; l+=skip_l){
  //     for(int m=1; m < M+1; m+=skip_m){
        
  //       xkl12 = h_xIk[k];
  //       xkp12 = h_xIk[k+1];
  //       yll12 = h_yIl[l];
  //       ylp12 = h_yIl[l+1];
  //       zml12 = h_zIm[m];
  //       zmp12 = h_zIm[m+1];

  //       xc = 0.5*(xkl12+xkp12);
  //       yc = 0.5*(yll12+ylp12);
  //       zc = 0.5*(zml12+zmp12);
        
        
  //       if( klmf(k-1, l-1, m-1, K+2, L+2, M+2, h_uklmc) < 0 ){
  //         file2 <<  xc <<" " << yc << " " << zc <<" " << 0 << " " << endl;          
  //       }
  //       else{
  //         file2 <<  xc <<" " << yc << " " << zc <<" " << klmf(k-1,l-1,m-1,K+2,L+2,M+2,h_uklmc) << " " << endl;          
  //       }
      
  //     }
  //   }
  //   file2 << endl;
  // } 
  // file2.close();

  

  
  for(int k=1; k < K+1; ++k){
    for(int l=1; l < L+1; ++l){
      for(int m=1; m < M+1; ++m){
        
        xkl12 = h_xIk[k];
        xkp12 = h_xIk[k+1];
        yll12 = h_yIl[l];
        ylp12 = h_yIl[l+1];
        zml12 = h_zIm[m];
        zmp12 = h_zIm[m+1];
        
        xc = 0.5*(xkl12+xkp12);
        yc = 0.5*(yll12+ylp12);
        zc = 0.5*(zml12+zmp12);
        
        
        if( k == K/2){          
          if(klmf(k-1, l-1, m-1, K+2, L+2, M+2, h_uklmc) < 0 ){
            file3 <<  yc <<" " << zc << " "  << 0 << " " << endl;         
          }
          else{              
            file3 <<  yc <<" " << zc << " " << klmf(k-1,l-1,m-1,K+2,L+2,M+2,h_uklmc) << " " << endl;        
          }
        }


        if(l == L/2){
          if(klmf(k-1,l-1,m-1,K+2,L+2,M+2,h_uklmc) < 0 ){
            file4 <<  xc <<" " << zc  << " " << 0 << endl;         
          }
          else{              
            file4 <<  xc <<" " << zc << " " << klmf(k-1,l-1,m-1,K+2,L+2,M+2,h_uklmc) << " " << endl;        
          }
        }
        
        
        if( m == M/2){            
          if(klmf(k-1,l-1,m-1,K+2,L+2,M+2,h_uklmc) < 0 ){
            file5 <<  xc <<" " << yc << " "  << 0 << " " << endl;         
          }
          else{              
            file5 <<  xc <<" " << yc << " " << klmf(k-1,l-1,m-1,K+2,L+2,M+2,h_uklmc) << " " << endl;        
          }            
        }
        
        
      }
    }    
  }
  
  file3.close();
  file4.close();
  file5.close();

  
  // for(int k=1; k < K+1; ++k){
  //   for(int l=1; l < L+1; ++l){
  //     for(int m=1; m < M+1; ++m){                

  //       if( klmf(k-1, l-1, m-1, K+2, L+2, M+2, h_uklmc) < 0 )
  //         file6 << (unsigned char) (0);        
  //       else
  //         file6 <<  (unsigned char) (255*klmf(k-1,l-1,m-1,K+2,L+2,M+2,h_uklmc)) ;                
  //     }
  //   }
  // } 

  // file6.close();


  
}



void saveUklm(int Ks, int Ls, int Ms, float * h_uklmSum, string nfile, int skip_k, int skip_l, int skip_m){

  float xkl12, xkp12, yll12, ylp12, zml12, zmp12;
  float xc,yc,zc;

  ofstream file;
  ofstream file2;
  ofstream file3;
  ofstream file4;
  ofstream file5;
  ofstream file6;
  
  file.open(nfile.c_str());  
  std::string token = nfile.substr(0, nfile.find(".txt"));
  //file2.open ( (token+"_reduced.txt").c_str());
  file3.open ( (token+"_x.txt").c_str());
  file4.open ( (token+"_y.txt").c_str());
  file5.open ( (token+"_z.txt").c_str());
  // file6.open ( (token+".raw").c_str());

  
  for(int k=1; k < Ks+1; ++k){
    for(int l=1; l < Ls+1; ++l){
      for(int m=1; m < Ms+1; ++m){

        xkl12 = (1.0/Ks)*(k-1);        
        xkp12 = (1.0/Ks)*k;
        
        yll12 = (1.0/Ls)*(l-1);
        ylp12 = (1.0/Ls)*l;
        
        zml12 = (1.0/Ms)*(m-1);
        zmp12 = (1/Ms)*m;
        
        xc = 0.5*(xkl12+xkp12);
        yc = 0.5*(yll12+ylp12);
        zc = 0.5*(zml12+zmp12);
        
        
        if( klmf(k-1, l-1, m-1, Ks, Ls, Ms, h_uklmSum) < 0 ){
          file <<  xc <<" " << yc << " " << zc <<" " << 0 << " " << endl;
        }
        else{
          file <<  xc <<" " << yc << " " << zc <<" " << klmf(k-1,l-1,m-1,Ks,Ls,Ms,h_uklmSum) << " " << endl;
        }
        
      }
    }
    file << endl;
  } 
  file.close();
  
  for(int k=1; k < Ks+1; ++k){
    for(int l=1; l < Ls+1; ++l){
      for(int m=1; m < Ms+1; ++m){

        xkl12 = (1.0/Ks)*(k-1);        
        xkp12 = (1.0/Ks)*k;
        
        yll12 = (1.0/Ls)*(l-1);
        ylp12 = (1.0/Ls)*l;
        
        zml12 = (1.0/Ms)*(m-1);
        zmp12 = (1/Ms)*m;
        
        xc = 0.5*(xkl12+xkp12);
        yc = 0.5*(yll12+ylp12);
        zc = 0.5*(zml12+zmp12);
        
        
        if( k == Ks/2){          
          if(klmf(k-1, l-1, m-1, Ks, Ls, Ms, h_uklmSum) < 0 ){
            file3 <<  yc <<" " << zc << " "  << 0 << " " << endl;         
          }
          else{              
            file3 <<  yc <<" " << zc << " " << klmf(k-1,l-1,m-1,Ks,Ls,Ms,h_uklmSum) << " " << endl;        
          }
        }


        if(l == Ls/2){
          if(klmf(k-1,l-1,m-1,Ks,Ls,Ms,h_uklmSum) < 0 ){
            file4 <<  xc <<" " << zc  << " " << 0 << endl;         
          }
          else{              
            file4 <<  xc <<" " << zc << " " << klmf(k-1,l-1,m-1,Ks,Ls,Ms,h_uklmSum) << " " << endl;        
          }
        }
        
        
        if( m == Ms/2){            
          if(klmf(k-1,l-1,m-1,Ks,Ls,Ms,h_uklmSum) < 0 ){
            file5 <<  xc <<" " << yc << " "  << 0 << " " << endl;         
          }
          else{              
            file5 <<  xc <<" " << yc << " " << klmf(k-1,l-1,m-1,Ks,Ls,Ms,h_uklmSum) << " " << endl;        
          }            
        }
        
      }
    }    
  }
  
  file3.close();
  file4.close();
  file5.close();
  
}


__global__ void bdyConditions(int K, int L, int M, int Ks, int Ms, int Ls, int kd, int ld, int md, float* d_uklm, float* d_qklm0, float* d_qklm1, float* d_qklm2){


  int k = blockIdx.x * blockDim.x + threadIdx.x;
  int l = blockIdx.y * blockDim.y + threadIdx.y;
  int m = blockIdx.z * blockDim.z + threadIdx.z;
  
  if( k < Ks &&  l < Ls && m < Ms ){

    k = k + kd - Ks/2;
    l = l + ld - Ls/2;
    m = m + md - Ms/2;

    
    //Left band   
    // d_uklm(k,0,m,3) = -d_uklm(k,1,m,0);
    // d_uklm(k,0,m,2) = -d_uklm(k,1,m,1);
    // d_uklm(k,0,m,7) = -d_uklm(k,1,m,4);
    // d_uklm(k,0,m,6) = -d_uklm(k,1,m,5);

    klmdf(k,0,m,K+2,L+2,M+2,d_uklm,3,-klmdf(k,1,m,K+2,L+2,M+2,d_uklm,0));
    klmdf(k,0,m,K+2,L+2,M+2,d_uklm,2,-klmdf(k,1,m,K+2,L+2,M+2,d_uklm,1));
    klmdf(k,0,m,K+2,L+2,M+2,d_uklm,7,-klmdf(k,1,m,K+2,L+2,M+2,d_uklm,4));
    klmdf(k,0,m,K+2,L+2,M+2,d_uklm,6,-klmdf(k,1,m,K+2,L+2,M+2,d_uklm,5));
    
    klmdf(k,0,m,K+2,L+2,M+2,d_qklm0,3,klmdf(k,1,m,K+2,L+2,M+2,d_qklm0,0));    
    klmdf(k,0,m,K+2,L+2,M+2,d_qklm0,2,klmdf(k,1,m,K+2,L+2,M+2,d_qklm0,1));   
    klmdf(k,0,m,K+2,L+2,M+2,d_qklm0,7,klmdf(k,1,m,K+2,L+2,M+2,d_qklm0,4));
    klmdf(k,0,m,K+2,L+2,M+2,d_qklm0,6,klmdf(k,1,m,K+2,L+2,M+2,d_qklm0,5));

    klmdf(k,0,m,K+2,L+2,M+2,d_qklm1,3,klmdf(k,1,m,K+2,L+2,M+2,d_qklm1,0));    
    klmdf(k,0,m,K+2,L+2,M+2,d_qklm1,2,klmdf(k,1,m,K+2,L+2,M+2,d_qklm1,1));   
    klmdf(k,0,m,K+2,L+2,M+2,d_qklm1,7,klmdf(k,1,m,K+2,L+2,M+2,d_qklm1,4));
    klmdf(k,0,m,K+2,L+2,M+2,d_qklm1,6,klmdf(k,1,m,K+2,L+2,M+2,d_qklm1,5));

    klmdf(k,0,m,K+2,L+2,M+2,d_qklm2,3,klmdf(k,1,m,K+2,L+2,M+2,d_qklm2,0));    
    klmdf(k,0,m,K+2,L+2,M+2,d_qklm2,2,klmdf(k,1,m,K+2,L+2,M+2,d_qklm2,1));   
    klmdf(k,0,m,K+2,L+2,M+2,d_qklm2,7,klmdf(k,1,m,K+2,L+2,M+2,d_qklm2,4));
    klmdf(k,0,m,K+2,L+2,M+2,d_qklm2,6,klmdf(k,1,m,K+2,L+2,M+2,d_qklm2,5));


    // //Right band
    // d_uklm(k,L+1,m,0) = -d_uklm(k,L,m,3);
    // d_uklm(k,L+1,m,1) = -d_uklm(k,L,m,2);
    // d_uklm(k,L+1,m,4) = -d_uklm(k,L,m,7);
    // d_uklm(k,L+1,m,5) = -d_uklm(k,L,m,6);

    klmdf(k,L+1,m,K+2,L+2,M+2,d_uklm,0,-klmdf(k,L,m,K+2,L+2,M+2,d_uklm,3));
    klmdf(k,L+1,m,K+2,L+2,M+2,d_uklm,1,-klmdf(k,L,m,K+2,L+2,M+2,d_uklm,2));
    klmdf(k,L+1,m,K+2,L+2,M+2,d_uklm,4,-klmdf(k,L,m,K+2,L+2,M+2,d_uklm,7));
    klmdf(k,L+1,m,K+2,L+2,M+2,d_uklm,5,-klmdf(k,L,m,K+2,L+2,M+2,d_uklm,6));

    klmdf(k,L+1,m,K+2,L+2,M+2,d_qklm0,0,klmdf(k,L,m,K+2,L+2,M+2,d_qklm0,3));
    klmdf(k,L+1,m,K+2,L+2,M+2,d_qklm0,1,klmdf(k,L,m,K+2,L+2,M+2,d_qklm0,2));
    klmdf(k,L+1,m,K+2,L+2,M+2,d_qklm0,4,klmdf(k,L,m,K+2,L+2,M+2,d_qklm0,7));
    klmdf(k,L+1,m,K+2,L+2,M+2,d_qklm0,5,klmdf(k,L,m,K+2,L+2,M+2,d_qklm0,6));

    klmdf(k,L+1,m,K+2,L+2,M+2,d_qklm1,0,klmdf(k,L,m,K+2,L+2,M+2,d_qklm1,3));
    klmdf(k,L+1,m,K+2,L+2,M+2,d_qklm1,1,klmdf(k,L,m,K+2,L+2,M+2,d_qklm1,2));
    klmdf(k,L+1,m,K+2,L+2,M+2,d_qklm1,4,klmdf(k,L,m,K+2,L+2,M+2,d_qklm1,7));
    klmdf(k,L+1,m,K+2,L+2,M+2,d_qklm1,5,klmdf(k,L,m,K+2,L+2,M+2,d_qklm1,6));

    klmdf(k,L+1,m,K+2,L+2,M+2,d_qklm2,0,klmdf(k,L,m,K+2,L+2,M+2,d_qklm2,3));
    klmdf(k,L+1,m,K+2,L+2,M+2,d_qklm2,1,klmdf(k,L,m,K+2,L+2,M+2,d_qklm2,2));
    klmdf(k,L+1,m,K+2,L+2,M+2,d_qklm2,4,klmdf(k,L,m,K+2,L+2,M+2,d_qklm2,7));
    klmdf(k,L+1,m,K+2,L+2,M+2,d_qklm2,5,klmdf(k,L,m,K+2,L+2,M+2,d_qklm2,6));
    
    
    // //Top band
    // d_uklm(k,l,M+1,4) = -d_uklm(k,l,L,5);
    // d_uklm(k,l,M+1,0) = -d_uklm(k,l,L,1);
    // d_uklm(k,l,M+1,7) = -d_uklm(k,l,L,6);
    // d_uklm(k,l,M+1,3) = -d_uklm(k,l,L,2);

    klmdf(k,l,M+1,K+2,L+2,M+2,d_uklm,4,-klmdf(k,l,L,K+2,L+2,M+2,d_uklm,5));
    klmdf(k,l,M+1,K+2,L+2,M+2,d_uklm,0,-klmdf(k,l,L,K+2,L+2,M+2,d_uklm,1));
    klmdf(k,l,M+1,K+2,L+2,M+2,d_uklm,7,-klmdf(k,l,L,K+2,L+2,M+2,d_uklm,6));
    klmdf(k,l,M+1,K+2,L+2,M+2,d_uklm,3,-klmdf(k,l,L,K+2,L+2,M+2,d_uklm,2));
    
    klmdf(k,l,M+1,K+2,L+2,M+2,d_qklm0,4,klmdf(k,l,L,K+2,L+2,M+2,d_qklm0,5));
    klmdf(k,l,M+1,K+2,L+2,M+2,d_qklm0,0,klmdf(k,l,L,K+2,L+2,M+2,d_qklm0,1));
    klmdf(k,l,M+1,K+2,L+2,M+2,d_qklm0,7,klmdf(k,l,L,K+2,L+2,M+2,d_qklm0,6));
    klmdf(k,l,M+1,K+2,L+2,M+2,d_qklm0,3,klmdf(k,l,L,K+2,L+2,M+2,d_qklm0,2));
    
    klmdf(k,l,M+1,K+2,L+2,M+2,d_qklm1,4,klmdf(k,l,L,K+2,L+2,M+2,d_qklm1,5));
    klmdf(k,l,M+1,K+2,L+2,M+2,d_qklm1,0,klmdf(k,l,L,K+2,L+2,M+2,d_qklm1,1));
    klmdf(k,l,M+1,K+2,L+2,M+2,d_qklm1,7,klmdf(k,l,L,K+2,L+2,M+2,d_qklm1,6));
    klmdf(k,l,M+1,K+2,L+2,M+2,d_qklm1,3,klmdf(k,l,L,K+2,L+2,M+2,d_qklm1,2));

    klmdf(k,l,M+1,K+2,L+2,M+2,d_qklm2,4,klmdf(k,l,L,K+2,L+2,M+2,d_qklm2,5));
    klmdf(k,l,M+1,K+2,L+2,M+2,d_qklm2,0,klmdf(k,l,L,K+2,L+2,M+2,d_qklm2,1));
    klmdf(k,l,M+1,K+2,L+2,M+2,d_qklm2,7,klmdf(k,l,L,K+2,L+2,M+2,d_qklm2,6));
    klmdf(k,l,M+1,K+2,L+2,M+2,d_qklm2,3,klmdf(k,l,L,K+2,L+2,M+2,d_qklm2,2));


        
    // //Bottom band
    // d_uklm(k,l,0,5) = -d_uklm(k,l,1,4);
    // d_uklm(k,l,0,1) = -d_uklm(k,l,1,0);
    // d_uklm(k,l,0,6) = -d_uklm(k,l,1,7);
    // d_uklm(k,l,0,2) = -d_uklm(k,l,1,3);

    klmdf(k,l,0,K+2,L+2,M+2,d_uklm,5,-klmdf(k,l,1,K+2,L+2,M+2,d_uklm,4));
    klmdf(k,l,0,K+2,L+2,M+2,d_uklm,1,-klmdf(k,l,1,K+2,L+2,M+2,d_uklm,0));
    klmdf(k,l,0,K+2,L+2,M+2,d_uklm,6,-klmdf(k,l,1,K+2,L+2,M+2,d_uklm,7));
    klmdf(k,l,0,K+2,L+2,M+2,d_uklm,2,-klmdf(k,l,1,K+2,L+2,M+2,d_uklm,3));
    
    klmdf(k,l,0,K+2,L+2,M+2,d_qklm0,5,klmdf(k,l,1,K+2,L+2,M+2,d_qklm0,4));
    klmdf(k,l,0,K+2,L+2,M+2,d_qklm0,1,klmdf(k,l,1,K+2,L+2,M+2,d_qklm0,0));
    klmdf(k,l,0,K+2,L+2,M+2,d_qklm0,6,klmdf(k,l,1,K+2,L+2,M+2,d_qklm0,7));
    klmdf(k,l,0,K+2,L+2,M+2,d_qklm0,2,klmdf(k,l,1,K+2,L+2,M+2,d_qklm0,3));


    klmdf(k,l,0,K+2,L+2,M+2,d_qklm1,5,klmdf(k,l,1,K+2,L+2,M+2,d_qklm1,4));
    klmdf(k,l,0,K+2,L+2,M+2,d_qklm1,1,klmdf(k,l,1,K+2,L+2,M+2,d_qklm1,0));
    klmdf(k,l,0,K+2,L+2,M+2,d_qklm1,6,klmdf(k,l,1,K+2,L+2,M+2,d_qklm1,7));
    klmdf(k,l,0,K+2,L+2,M+2,d_qklm1,2,klmdf(k,l,1,K+2,L+2,M+2,d_qklm1,3));


    klmdf(k,l,0,K+2,L+2,M+2,d_qklm2,5,klmdf(k,l,1,K+2,L+2,M+2,d_qklm2,4));
    klmdf(k,l,0,K+2,L+2,M+2,d_qklm2,1,klmdf(k,l,1,K+2,L+2,M+2,d_qklm2,0));
    klmdf(k,l,0,K+2,L+2,M+2,d_qklm2,6,klmdf(k,l,1,K+2,L+2,M+2,d_qklm2,7));
    klmdf(k,l,0,K+2,L+2,M+2,d_qklm2,2,klmdf(k,l,1,K+2,L+2,M+2,d_qklm2,3));

    
    
    
    // //Front band
    // d_uklm(K+1,l,m,0) = -d_uklm(L,l,m,4);
    // d_uklm(K+1,l,m,1) = -d_uklm(L,l,m,5);
    // d_uklm(K+1,l,m,3) = -d_uklm(L,l,m,7);
    // d_uklm(K+1,l,m,2) = -d_uklm(L,l,m,6);

    klmdf(K+1,l,m,K+2,L+2,M+2,d_uklm,0,-klmdf(L,l,m,K+2,L+2,M+2,d_uklm,4));
    klmdf(K+1,l,m,K+2,L+2,M+2,d_uklm,1,-klmdf(L,l,m,K+2,L+2,M+2,d_uklm,5));
    klmdf(K+1,l,m,K+2,L+2,M+2,d_uklm,3,-klmdf(L,l,m,K+2,L+2,M+2,d_uklm,7));
    klmdf(K+1,l,m,K+2,L+2,M+2,d_uklm,2,-klmdf(L,l,m,K+2,L+2,M+2,d_uklm,6));

    klmdf(K+1,l,m,K+2,L+2,M+2,d_qklm0,0,-klmdf(L,l,m,K+2,L+2,M+2,d_qklm0,4));
    klmdf(K+1,l,m,K+2,L+2,M+2,d_qklm0,1,-klmdf(L,l,m,K+2,L+2,M+2,d_qklm0,5));
    klmdf(K+1,l,m,K+2,L+2,M+2,d_qklm0,3,-klmdf(L,l,m,K+2,L+2,M+2,d_qklm0,7));
    klmdf(K+1,l,m,K+2,L+2,M+2,d_qklm0,2,-klmdf(L,l,m,K+2,L+2,M+2,d_qklm0,6));

    klmdf(K+1,l,m,K+2,L+2,M+2,d_qklm1,0,-klmdf(L,l,m,K+2,L+2,M+2,d_qklm1,4));
    klmdf(K+1,l,m,K+2,L+2,M+2,d_qklm1,1,-klmdf(L,l,m,K+2,L+2,M+2,d_qklm1,5));
    klmdf(K+1,l,m,K+2,L+2,M+2,d_qklm1,3,-klmdf(L,l,m,K+2,L+2,M+2,d_qklm1,7));
    klmdf(K+1,l,m,K+2,L+2,M+2,d_qklm1,2,-klmdf(L,l,m,K+2,L+2,M+2,d_qklm1,6));


    klmdf(K+1,l,m,K+2,L+2,M+2,d_qklm2,0,-klmdf(L,l,m,K+2,L+2,M+2,d_qklm2,4));
    klmdf(K+1,l,m,K+2,L+2,M+2,d_qklm2,1,-klmdf(L,l,m,K+2,L+2,M+2,d_qklm2,5));
    klmdf(K+1,l,m,K+2,L+2,M+2,d_qklm2,3,-klmdf(L,l,m,K+2,L+2,M+2,d_qklm2,7));
    klmdf(K+1,l,m,K+2,L+2,M+2,d_qklm2,2,-klmdf(L,l,m,K+2,L+2,M+2,d_qklm2,6));


        
    // //Back band
    // d_uklm(0,l,m,4) = -d_uklm(1,l,m,0);
    // d_uklm(0,l,m,5) = -d_uklm(1,l,m,1);
    // d_uklm(0,l,m,7) = -d_uklm(1,l,m,3);
    // d_uklm(0,l,m,6) = -d_uklm(1,l,m,2);

    klmdf(0,l,m,K+2,L+2,M+2,d_uklm,4,-klmdf(1,l,m,K+2,L+2,M+2,d_uklm,0));
    klmdf(0,l,m,K+2,L+2,M+2,d_uklm,5,-klmdf(1,l,m,K+2,L+2,M+2,d_uklm,1));
    klmdf(0,l,m,K+2,L+2,M+2,d_uklm,7,-klmdf(1,l,m,K+2,L+2,M+2,d_uklm,3));
    klmdf(0,l,m,K+2,L+2,M+2,d_uklm,6,-klmdf(1,l,m,K+2,L+2,M+2,d_uklm,2));

    klmdf(0,l,m,K+2,L+2,M+2,d_qklm0,4,klmdf(1,l,m,K+2,L+2,M+2,d_qklm0,0));
    klmdf(0,l,m,K+2,L+2,M+2,d_qklm0,5,klmdf(1,l,m,K+2,L+2,M+2,d_qklm0,1));
    klmdf(0,l,m,K+2,L+2,M+2,d_qklm0,7,klmdf(1,l,m,K+2,L+2,M+2,d_qklm0,3));
    klmdf(0,l,m,K+2,L+2,M+2,d_qklm0,6,klmdf(1,l,m,K+2,L+2,M+2,d_qklm0,2));

    klmdf(0,l,m,K+2,L+2,M+2,d_qklm1,4,klmdf(1,l,m,K+2,L+2,M+2,d_qklm1,0));
    klmdf(0,l,m,K+2,L+2,M+2,d_qklm1,5,klmdf(1,l,m,K+2,L+2,M+2,d_qklm1,1));
    klmdf(0,l,m,K+2,L+2,M+2,d_qklm1,7,klmdf(1,l,m,K+2,L+2,M+2,d_qklm1,3));
    klmdf(0,l,m,K+2,L+2,M+2,d_qklm1,6,klmdf(1,l,m,K+2,L+2,M+2,d_qklm1,2));

    klmdf(0,l,m,K+2,L+2,M+2,d_qklm2,4,klmdf(1,l,m,K+2,L+2,M+2,d_qklm2,0));
    klmdf(0,l,m,K+2,L+2,M+2,d_qklm2,5,klmdf(1,l,m,K+2,L+2,M+2,d_qklm2,1));
    klmdf(0,l,m,K+2,L+2,M+2,d_qklm2,7,klmdf(1,l,m,K+2,L+2,M+2,d_qklm2,3));
    klmdf(0,l,m,K+2,L+2,M+2,d_qklm2,6,klmdf(1,l,m,K+2,L+2,M+2,d_qklm2,2));

    
  }
  
}


__global__ void d_qklm0f(int Ks, int Ms, int Ls, float* d_uklm, float* d_ml, float* d_mltri,
                         float* d_mllR, float* d_mplR, float* d_mllL, float* d_mplL, float* d_mx,float* d_qklm0){

  int k = blockIdx.x * blockDim.x + threadIdx.x;
  int l = blockIdx.y * blockDim.y + threadIdx.y;
  int m = blockIdx.z * blockDim.z + threadIdx.z;

  

  if(  0 < k  && k < Ks+1  && 0 < l && l < Ls+1 && 0 < m &&  m < Ms+1 ){
  
    float uklm0,uklm1,uklm2,uklm3,uklm4,uklm5,uklm6,uklm7;
    float uklm0R, uklm1R, uklm2R, uklm3R, uklm4R, uklm5R, uklm6R, uklm7R;
    float uklm0L, uklm1L, uklm2L, uklm3L, uklm4L, uklm5L, uklm6L, uklm7L;
    float qklm0,qklm1,qklm2,qklm3,qklm4,qklm5,qklm6,qklm7;

    float v00,v01,v02,v03,v04,v05,v06,v07;
    float v10,v11,v12,v13,v14,v15,v16,v17;
    float v20,v21,v22,v23,v24,v25,v26,v27;
    float v30,v31,v32,v33,v34,v35,v36,v37;
    float v40,v41,v42,v43,v44,v45,v46,v47;

    float x0,x1,x2,x3,x4,x5,x6,x7;

    
    uklm0  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,0); uklm1  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,1); uklm2  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,2); uklm3  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,3);
    uklm4  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,4); uklm5  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,5); uklm6  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,6); uklm7  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,7);
    //printf("%d %d %d %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",k,l,m,uklm0,uklm1,uklm2,uklm3R,uklm4,uklm5,uklm6,uklm7);

    uklm0R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_uklm,0); uklm1R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_uklm,1); uklm2R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_uklm,2); uklm3R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_uklm,3);
    uklm4R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_uklm,4); uklm5R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_uklm,5); uklm6R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_uklm,6); uklm7R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_uklm,7);    
    //printf("%d %d %d %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",k,l,m,uklm0R,uklm1R,uklm2R,uklm3R,uklm4R,uklm5R,uklm6R,uklm7R);

    uklm0L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_uklm,0); uklm1L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_uklm,1); uklm2L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_uklm,2); uklm3L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_uklm,3);
    uklm4L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_uklm,4); uklm5L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_uklm,5); uklm6L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_uklm,6); uklm7L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_uklm,7);
    //printf("%d %d %d %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",k,l,m,uklm0L,uklm1L,uklm2L,uklm3L,uklm4L,uklm5L,uklm6L,uklm7L);
    
    
    // v00 = klf(0,0,d_mllR)*uklm0+klf(0,1,d_mllR)*uklm1+klf(0,2,d_mllR)*uklm2+klf(0,3,d_mllR)*uklm3+klf(0,4,d_mllR)*uklm4+klf(0,5,d_mllR)*uklm5+klf(0,6,d_mllR)*uklm6+klf(0,7,d_mllR)*uklm7;
    // v01 = klf(1,0,d_mllR)*uklm0+klf(1,1,d_mllR)*uklm1+klf(1,2,d_mllR)*uklm2+klf(1,3,d_mllR)*uklm3+klf(1,4,d_mllR)*uklm4+klf(1,5,d_mllR)*uklm5+klf(1,6,d_mllR)*uklm6+klf(1,7,d_mllR)*uklm7;
    // v02 = klf(2,0,d_mllR)*uklm0+klf(2,1,d_mllR)*uklm1+klf(2,2,d_mllR)*uklm2+klf(2,3,d_mllR)*uklm3+klf(2,4,d_mllR)*uklm4+klf(2,5,d_mllR)*uklm5+klf(2,6,d_mllR)*uklm6+klf(2,7,d_mllR)*uklm7;
    // v03 = klf(3,0,d_mllR)*uklm0+klf(3,1,d_mllR)*uklm1+klf(3,2,d_mllR)*uklm2+klf(3,3,d_mllR)*uklm3+klf(3,4,d_mllR)*uklm4+klf(3,5,d_mllR)*uklm5+klf(3,6,d_mllR)*uklm6+klf(3,7,d_mllR)*uklm7;
    // v04 = klf(4,0,d_mllR)*uklm0+klf(4,1,d_mllR)*uklm1+klf(4,2,d_mllR)*uklm2+klf(4,3,d_mllR)*uklm3+klf(4,4,d_mllR)*uklm4+klf(4,5,d_mllR)*uklm5+klf(4,6,d_mllR)*uklm6+klf(4,7,d_mllR)*uklm7;
    // v05 = klf(5,0,d_mllR)*uklm0+klf(5,1,d_mllR)*uklm1+klf(5,2,d_mllR)*uklm2+klf(5,3,d_mllR)*uklm3+klf(5,4,d_mllR)*uklm4+klf(5,5,d_mllR)*uklm5+klf(5,6,d_mllR)*uklm6+klf(5,7,d_mllR)*uklm7;
    // v06 = klf(6,0,d_mllR)*uklm0+klf(6,1,d_mllR)*uklm1+klf(6,2,d_mllR)*uklm2+klf(6,3,d_mllR)*uklm3+klf(6,4,d_mllR)*uklm4+klf(6,5,d_mllR)*uklm5+klf(6,6,d_mllR)*uklm6+klf(6,7,d_mllR)*uklm7;
    // v07 = klf(7,0,d_mllR)*uklm0+klf(7,1,d_mllR)*uklm1+klf(7,2,d_mllR)*uklm2+klf(7,3,d_mllR)*uklm3+klf(7,4,d_mllR)*uklm4+klf(7,5,d_mllR)*uklm5+klf(7,6,d_mllR)*uklm6+klf(7,7,d_mllR)*uklm7;


    
    // v10 = klf(0,0,d_mplR)*uklm0R+klf(0,1,d_mplR)*uklm1R+klf(0,2,d_mplR)*uklm2R+klf(0,3,d_mplR)*uklm3R+klf(0,4,d_mplR)*uklm4R+klf(0,5,d_mplR)*uklm5R+klf(0,6,d_mplR)*uklm6R+klf(0,7,d_mplR)*uklm7R;
    // v11 = klf(1,0,d_mplR)*uklm0R+klf(1,1,d_mplR)*uklm1R+klf(1,2,d_mplR)*uklm2R+klf(1,3,d_mplR)*uklm3R+klf(1,4,d_mplR)*uklm4R+klf(1,5,d_mplR)*uklm5R+klf(1,6,d_mplR)*uklm6R+klf(1,7,d_mplR)*uklm7R;
    // v12 = klf(2,0,d_mplR)*uklm0R+klf(2,1,d_mplR)*uklm1R+klf(2,2,d_mplR)*uklm2R+klf(2,3,d_mplR)*uklm3R+klf(2,4,d_mplR)*uklm4R+klf(2,5,d_mplR)*uklm5R+klf(2,6,d_mplR)*uklm6R+klf(2,7,d_mplR)*uklm7R;
    // v13 = klf(3,0,d_mplR)*uklm0R+klf(3,1,d_mplR)*uklm1R+klf(3,2,d_mplR)*uklm2R+klf(3,3,d_mplR)*uklm3R+klf(3,4,d_mplR)*uklm4R+klf(3,5,d_mplR)*uklm5R+klf(3,6,d_mplR)*uklm6R+klf(3,7,d_mplR)*uklm7R;
    // v14 = klf(4,0,d_mplR)*uklm0R+klf(4,1,d_mplR)*uklm1R+klf(4,2,d_mplR)*uklm2R+klf(4,3,d_mplR)*uklm3R+klf(4,4,d_mplR)*uklm4R+klf(4,5,d_mplR)*uklm5R+klf(4,6,d_mplR)*uklm6R+klf(4,7,d_mplR)*uklm7R;
    // v15 = klf(5,0,d_mplR)*uklm0R+klf(5,1,d_mplR)*uklm1R+klf(5,2,d_mplR)*uklm2R+klf(5,3,d_mplR)*uklm3R+klf(5,4,d_mplR)*uklm4R+klf(5,5,d_mplR)*uklm5R+klf(5,6,d_mplR)*uklm6R+klf(5,7,d_mplR)*uklm7R;
    // v16 = klf(6,0,d_mplR)*uklm0R+klf(6,1,d_mplR)*uklm1R+klf(6,2,d_mplR)*uklm2R+klf(6,3,d_mplR)*uklm3R+klf(6,4,d_mplR)*uklm4R+klf(6,5,d_mplR)*uklm5R+klf(6,6,d_mplR)*uklm6R+klf(6,7,d_mplR)*uklm7R;
    // v17 = klf(7,0,d_mplR)*uklm0R+klf(7,1,d_mplR)*uklm1R+klf(7,2,d_mplR)*uklm2R+klf(7,3,d_mplR)*uklm3R+klf(7,4,d_mplR)*uklm4R+klf(7,5,d_mplR)*uklm5R+klf(7,6,d_mplR)*uklm6R+klf(7,7,d_mplR)*uklm7R;



    // v20 = klf(0,0,d_mllL)*uklm0+klf(0,1,d_mllL)*uklm1+klf(0,2,d_mllL)*uklm2+klf(0,3,d_mllL)*uklm3+klf(0,4,d_mllL)*uklm4+klf(0,5,d_mllL)*uklm5+klf(0,6,d_mllL)*uklm6+klf(0,7,d_mllL)*uklm7;
    // v21 = klf(1,0,d_mllL)*uklm0+klf(1,1,d_mllL)*uklm1+klf(1,2,d_mllL)*uklm2+klf(1,3,d_mllL)*uklm3+klf(1,4,d_mllL)*uklm4+klf(1,5,d_mllL)*uklm5+klf(1,6,d_mllL)*uklm6+klf(1,7,d_mllL)*uklm7;
    // v22 = klf(2,0,d_mllL)*uklm0+klf(2,1,d_mllL)*uklm1+klf(2,2,d_mllL)*uklm2+klf(2,3,d_mllL)*uklm3+klf(2,4,d_mllL)*uklm4+klf(2,5,d_mllL)*uklm5+klf(2,6,d_mllL)*uklm6+klf(2,7,d_mllL)*uklm7;
    // v23 = klf(3,0,d_mllL)*uklm0+klf(3,1,d_mllL)*uklm1+klf(3,2,d_mllL)*uklm2+klf(3,3,d_mllL)*uklm3+klf(3,4,d_mllL)*uklm4+klf(3,5,d_mllL)*uklm5+klf(3,6,d_mllL)*uklm6+klf(3,7,d_mllL)*uklm7;
    // v24 = klf(4,0,d_mllL)*uklm0+klf(4,1,d_mllL)*uklm1+klf(4,2,d_mllL)*uklm2+klf(4,3,d_mllL)*uklm3+klf(4,4,d_mllL)*uklm4+klf(4,5,d_mllL)*uklm5+klf(4,6,d_mllL)*uklm6+klf(4,7,d_mllL)*uklm7;
    // v25 = klf(5,0,d_mllL)*uklm0+klf(5,1,d_mllL)*uklm1+klf(5,2,d_mllL)*uklm2+klf(5,3,d_mllL)*uklm3+klf(5,4,d_mllL)*uklm4+klf(5,5,d_mllL)*uklm5+klf(5,6,d_mllL)*uklm6+klf(5,7,d_mllL)*uklm7;
    // v26 = klf(6,0,d_mllL)*uklm0+klf(6,1,d_mllL)*uklm1+klf(6,2,d_mllL)*uklm2+klf(6,3,d_mllL)*uklm3+klf(6,4,d_mllL)*uklm4+klf(6,5,d_mllL)*uklm5+klf(6,6,d_mllL)*uklm6+klf(6,7,d_mllL)*uklm7;
    // v27 = klf(7,0,d_mllL)*uklm0+klf(7,1,d_mllL)*uklm1+klf(7,2,d_mllL)*uklm2+klf(7,3,d_mllL)*uklm3+klf(7,4,d_mllL)*uklm4+klf(7,5,d_mllL)*uklm5+klf(7,6,d_mllL)*uklm6+klf(7,7,d_mllL)*uklm7;


    // v30 = klf(0,0,d_mplL)*uklm0L+klf(0,1,d_mplL)*uklm1L+klf(0,2,d_mplL)*uklm2L+klf(0,3,d_mplL)*uklm3L+klf(0,4,d_mplL)*uklm4L+klf(0,5,d_mplL)*uklm5L+klf(0,6,d_mplL)*uklm6L+klf(0,7,d_mplL)*uklm7L;
    // v31 = klf(1,0,d_mplL)*uklm0L+klf(1,1,d_mplL)*uklm1L+klf(1,2,d_mplL)*uklm2L+klf(1,3,d_mplL)*uklm3L+klf(1,4,d_mplL)*uklm4L+klf(1,5,d_mplL)*uklm5L+klf(1,6,d_mplL)*uklm6L+klf(1,7,d_mplL)*uklm7L;
    // v32 = klf(2,0,d_mplL)*uklm0L+klf(2,1,d_mplL)*uklm1L+klf(2,2,d_mplL)*uklm2L+klf(2,3,d_mplL)*uklm3L+klf(2,4,d_mplL)*uklm4L+klf(2,5,d_mplL)*uklm5L+klf(2,6,d_mplL)*uklm6L+klf(2,7,d_mplL)*uklm7L;
    // v33 = klf(3,0,d_mplL)*uklm0L+klf(3,1,d_mplL)*uklm1L+klf(3,2,d_mplL)*uklm2L+klf(3,3,d_mplL)*uklm3L+klf(3,4,d_mplL)*uklm4L+klf(3,5,d_mplL)*uklm5L+klf(3,6,d_mplL)*uklm6L+klf(3,7,d_mplL)*uklm7L;
    // v34 = klf(4,0,d_mplL)*uklm0L+klf(4,1,d_mplL)*uklm1L+klf(4,2,d_mplL)*uklm2L+klf(4,3,d_mplL)*uklm3L+klf(4,4,d_mplL)*uklm4L+klf(4,5,d_mplL)*uklm5L+klf(4,6,d_mplL)*uklm6L+klf(4,7,d_mplL)*uklm7L;
    // v35 = klf(5,0,d_mplL)*uklm0L+klf(5,1,d_mplL)*uklm1L+klf(5,2,d_mplL)*uklm2L+klf(5,3,d_mplL)*uklm3L+klf(5,4,d_mplL)*uklm4L+klf(5,5,d_mplL)*uklm5L+klf(5,6,d_mplL)*uklm6L+klf(5,7,d_mplL)*uklm7L;
    // v36 = klf(6,0,d_mplL)*uklm0L+klf(6,1,d_mplL)*uklm1L+klf(6,2,d_mplL)*uklm2L+klf(6,3,d_mplL)*uklm3L+klf(6,4,d_mplL)*uklm4L+klf(6,5,d_mplL)*uklm5L+klf(6,6,d_mplL)*uklm6L+klf(6,7,d_mplL)*uklm7L;
    // v37 = klf(7,0,d_mplL)*uklm0L+klf(7,1,d_mplL)*uklm1L+klf(7,2,d_mplL)*uklm2L+klf(7,3,d_mplL)*uklm3L+klf(7,4,d_mplL)*uklm4L+klf(7,5,d_mplL)*uklm5L+klf(7,6,d_mplL)*uklm6L+klf(7,7,d_mplL)*uklm7L;


    // v40 = klf(0,0,d_mx)*uklm0+klf(0,1,d_mx)*uklm1+klf(0,2,d_mx)*uklm2+klf(0,3,d_mx)*uklm3+klf(0,4,d_mx)*uklm4+klf(0,5,d_mx)*uklm5+klf(0,6,d_mx)*uklm6+klf(0,7,d_mx)*uklm7;
    // v41 = klf(1,0,d_mx)*uklm0+klf(1,1,d_mx)*uklm1+klf(1,2,d_mx)*uklm2+klf(1,3,d_mx)*uklm3+klf(1,4,d_mx)*uklm4+klf(1,5,d_mx)*uklm5+klf(1,6,d_mx)*uklm6+klf(1,7,d_mx)*uklm7;
    // v42 = klf(2,0,d_mx)*uklm0+klf(2,1,d_mx)*uklm1+klf(2,2,d_mx)*uklm2+klf(2,3,d_mx)*uklm3+klf(2,4,d_mx)*uklm4+klf(2,5,d_mx)*uklm5+klf(2,6,d_mx)*uklm6+klf(2,7,d_mx)*uklm7;
    // v43 = klf(3,0,d_mx)*uklm0+klf(3,1,d_mx)*uklm1+klf(3,2,d_mx)*uklm2+klf(3,3,d_mx)*uklm3+klf(3,4,d_mx)*uklm4+klf(3,5,d_mx)*uklm5+klf(3,6,d_mx)*uklm6+klf(3,7,d_mx)*uklm7;
    // v44 = klf(4,0,d_mx)*uklm0+klf(4,1,d_mx)*uklm1+klf(4,2,d_mx)*uklm2+klf(4,3,d_mx)*uklm3+klf(4,4,d_mx)*uklm4+klf(4,5,d_mx)*uklm5+klf(4,6,d_mx)*uklm6+klf(4,7,d_mx)*uklm7;
    // v45 = klf(5,0,d_mx)*uklm0+klf(5,1,d_mx)*uklm1+klf(5,2,d_mx)*uklm2+klf(5,3,d_mx)*uklm3+klf(5,4,d_mx)*uklm4+klf(5,5,d_mx)*uklm5+klf(5,6,d_mx)*uklm6+klf(5,7,d_mx)*uklm7;
    // v46 = klf(6,0,d_mx)*uklm0+klf(6,1,d_mx)*uklm1+klf(6,2,d_mx)*uklm2+klf(6,3,d_mx)*uklm3+klf(6,4,d_mx)*uklm4+klf(6,5,d_mx)*uklm5+klf(6,6,d_mx)*uklm6+klf(6,7,d_mx)*uklm7;
    // v47 = klf(7,0,d_mx)*uklm0+klf(7,1,d_mx)*uklm1+klf(7,2,d_mx)*uklm2+klf(7,3,d_mx)*uklm3+klf(7,4,d_mx)*uklm4+klf(7,5,d_mx)*uklm5+klf(7,6,d_mx)*uklm6+klf(7,7,d_mx)*uklm7;

    
    // qklm0 = -0.5*v00 -0.5*v10 + 0.5*v20 + 0.5*v30 + v40;
    // qklm1 = -0.5*v01 -0.5*v11 + 0.5*v21 + 0.5*v31 + v41;
    // qklm2 = -0.5*v02 -0.5*v12 + 0.5*v22 + 0.5*v32 + v42;
    // qklm3 = -0.5*v03 -0.5*v13 + 0.5*v23 + 0.5*v33 + v43;
    // qklm4 = -0.5*v04 -0.5*v14 + 0.5*v24 + 0.5*v34 + v44;
    // qklm5 = -0.5*v05 -0.5*v15 + 0.5*v25 + 0.5*v35 + v45;
    // qklm6 = -0.5*v06 -0.5*v16 + 0.5*v26 + 0.5*v36 + v46;
    // qklm7 = -0.5*v07 -0.5*v17 + 0.5*v27 + 0.5*v37 + v47;


     qklm0 = -0.5*(klf(0,0,d_mllR)*uklm0+klf(0,1,d_mllR)*uklm1+klf(0,2,d_mllR)*uklm2+klf(0,3,d_mllR)*uklm3+klf(0,4,d_mllR)*uklm4+klf(0,5,d_mllR)*uklm5+klf(0,6,d_mllR)*uklm6+klf(0,7,d_mllR)*uklm7)-0.5*(klf(0,0,d_mplR)*uklm0R+klf(0,1,d_mplR)*uklm1R+klf(0,2,d_mplR)*uklm2R+klf(0,3,d_mplR)*uklm3R+klf(0,4,d_mplR)*uklm4R+klf(0,5,d_mplR)*uklm5R+klf(0,6,d_mplR)*uklm6R+klf(0,7,d_mplR)*uklm7R)+0.5*(klf(0,0,d_mllL)*uklm0+klf(0,1,d_mllL)*uklm1+klf(0,2,d_mllL)*uklm2+klf(0,3,d_mllL)*uklm3+klf(0,4,d_mllL)*uklm4+klf(0,5,d_mllL)*uklm5+klf(0,6,d_mllL)*uklm6+klf(0,7,d_mllL)*uklm7)+0.5*(klf(0,0,d_mplL)*uklm0L+klf(0,1,d_mplL)*uklm1L+klf(0,2,d_mplL)*uklm2L+klf(0,3,d_mplL)*uklm3L+klf(0,4,d_mplL)*uklm4L+klf(0,5,d_mplL)*uklm5L+klf(0,6,d_mplL)*uklm6L+klf(0,7,d_mplL)*uklm7L)+(klf(0,0,d_mx)*uklm0+klf(0,1,d_mx)*uklm1+klf(0,2,d_mx)*uklm2+klf(0,3,d_mx)*uklm3+klf(0,4,d_mx)*uklm4+klf(0,5,d_mx)*uklm5+klf(0,6,d_mx)*uklm6+klf(0,7,d_mx)*uklm7);

    qklm1 = -0.5*(klf(1,0,d_mllR)*uklm0+klf(1,1,d_mllR)*uklm1+klf(1,2,d_mllR)*uklm2+klf(1,3,d_mllR)*uklm3+klf(1,4,d_mllR)*uklm4+klf(1,5,d_mllR)*uklm5+klf(1,6,d_mllR)*uklm6+klf(1,7,d_mllR)*uklm7)-0.5*(klf(1,0,d_mplR)*uklm0R+klf(1,1,d_mplR)*uklm1R+klf(1,2,d_mplR)*uklm2R+klf(1,3,d_mplR)*uklm3R+klf(1,4,d_mplR)*uklm4R+klf(1,5,d_mplR)*uklm5R+klf(1,6,d_mplR)*uklm6R+klf(1,7,d_mplR)*uklm7R)+0.5*(klf(1,0,d_mllL)*uklm0+klf(1,1,d_mllL)*uklm1+klf(1,2,d_mllL)*uklm2+klf(1,3,d_mllL)*uklm3+klf(1,4,d_mllL)*uklm4+klf(1,5,d_mllL)*uklm5+klf(1,6,d_mllL)*uklm6+klf(1,7,d_mllL)*uklm7)+0.5*(klf(1,0,d_mplL)*uklm0L+klf(1,1,d_mplL)*uklm1L+klf(1,2,d_mplL)*uklm2L+klf(1,3,d_mplL)*uklm3L+klf(1,4,d_mplL)*uklm4L+klf(1,5,d_mplL)*uklm5L+klf(1,6,d_mplL)*uklm6L+klf(1,7,d_mplL)*uklm7L)+(klf(1,0,d_mx)*uklm0+klf(1,1,d_mx)*uklm1+klf(1,2,d_mx)*uklm2+klf(1,3,d_mx)*uklm3+klf(1,4,d_mx)*uklm4+klf(1,5,d_mx)*uklm5+klf(1,6,d_mx)*uklm6+klf(1,7,d_mx)*uklm7);

    qklm2 = -0.5*(klf(2,0,d_mllR)*uklm0+klf(2,1,d_mllR)*uklm1+klf(2,2,d_mllR)*uklm2+klf(2,3,d_mllR)*uklm3+klf(2,4,d_mllR)*uklm4+klf(2,5,d_mllR)*uklm5+klf(2,6,d_mllR)*uklm6+klf(2,7,d_mllR)*uklm7)-0.5*(klf(2,0,d_mplR)*uklm0R+klf(2,1,d_mplR)*uklm1R+klf(2,2,d_mplR)*uklm2R+klf(2,3,d_mplR)*uklm3R+klf(2,4,d_mplR)*uklm4R+klf(2,5,d_mplR)*uklm5R+klf(2,6,d_mplR)*uklm6R+klf(2,7,d_mplR)*uklm7R)+0.5*(klf(2,0,d_mllL)*uklm0+klf(2,1,d_mllL)*uklm1+klf(2,2,d_mllL)*uklm2+klf(2,3,d_mllL)*uklm3+klf(2,4,d_mllL)*uklm4+klf(2,5,d_mllL)*uklm5+klf(2,6,d_mllL)*uklm6+klf(2,7,d_mllL)*uklm7)+0.5*(klf(2,0,d_mplL)*uklm0L+klf(2,1,d_mplL)*uklm1L+klf(2,2,d_mplL)*uklm2L+klf(2,3,d_mplL)*uklm3L+klf(2,4,d_mplL)*uklm4L+klf(2,5,d_mplL)*uklm5L+klf(2,6,d_mplL)*uklm6L+klf(2,7,d_mplL)*uklm7L)+(klf(2,0,d_mx)*uklm0+klf(2,1,d_mx)*uklm1+klf(2,2,d_mx)*uklm2+klf(2,3,d_mx)*uklm3+klf(2,4,d_mx)*uklm4+klf(2,5,d_mx)*uklm5+klf(2,6,d_mx)*uklm6+klf(2,7,d_mx)*uklm7);

    qklm3 = -0.5*(klf(3,0,d_mllR)*uklm0+klf(3,1,d_mllR)*uklm1+klf(3,2,d_mllR)*uklm2+klf(3,3,d_mllR)*uklm3+klf(3,4,d_mllR)*uklm4+klf(3,5,d_mllR)*uklm5+klf(3,6,d_mllR)*uklm6+klf(3,7,d_mllR)*uklm7)-0.5*(klf(3,0,d_mplR)*uklm0R+klf(3,1,d_mplR)*uklm1R+klf(3,2,d_mplR)*uklm2R+klf(3,3,d_mplR)*uklm3R+klf(3,4,d_mplR)*uklm4R+klf(3,5,d_mplR)*uklm5R+klf(3,6,d_mplR)*uklm6R+klf(3,7,d_mplR)*uklm7R)+0.5*(klf(3,0,d_mllL)*uklm0+klf(3,1,d_mllL)*uklm1+klf(3,2,d_mllL)*uklm2+klf(3,3,d_mllL)*uklm3+klf(3,4,d_mllL)*uklm4+klf(3,5,d_mllL)*uklm5+klf(3,6,d_mllL)*uklm6+klf(3,7,d_mllL)*uklm7)+0.5*(klf(3,0,d_mplL)*uklm0L+klf(3,1,d_mplL)*uklm1L+klf(3,2,d_mplL)*uklm2L+klf(3,3,d_mplL)*uklm3L+klf(3,4,d_mplL)*uklm4L+klf(3,5,d_mplL)*uklm5L+klf(3,6,d_mplL)*uklm6L+klf(3,7,d_mplL)*uklm7L)+(klf(3,0,d_mx)*uklm0+klf(3,1,d_mx)*uklm1+klf(3,2,d_mx)*uklm2+klf(3,3,d_mx)*uklm3+klf(3,4,d_mx)*uklm4+klf(3,5,d_mx)*uklm5+klf(3,6,d_mx)*uklm6+klf(3,7,d_mx)*uklm7);

    qklm4 = -0.5*( klf(4,0,d_mllR)*uklm0+klf(4,1,d_mllR)*uklm1+klf(4,2,d_mllR)*uklm2+klf(4,3,d_mllR)*uklm3+klf(4,4,d_mllR)*uklm4+klf(4,5,d_mllR)*uklm5+klf(4,6,d_mllR)*uklm6+klf(4,7,d_mllR)*uklm7)-0.5*(klf(4,0,d_mplR)*uklm0R+klf(4,1,d_mplR)*uklm1R+klf(4,2,d_mplR)*uklm2R+klf(4,3,d_mplR)*uklm3R+klf(4,4,d_mplR)*uklm4R+klf(4,5,d_mplR)*uklm5R+klf(4,6,d_mplR)*uklm6R+klf(4,7,d_mplR)*uklm7R)+0.5*(klf(4,0,d_mllL)*uklm0+klf(4,1,d_mllL)*uklm1+klf(4,2,d_mllL)*uklm2+klf(4,3,d_mllL)*uklm3+klf(4,4,d_mllL)*uklm4+klf(4,5,d_mllL)*uklm5+klf(4,6,d_mllL)*uklm6+klf(4,7,d_mllL)*uklm7)+0.5*(klf(4,0,d_mplL)*uklm0L+klf(4,1,d_mplL)*uklm1L+klf(4,2,d_mplL)*uklm2L+klf(4,3,d_mplL)*uklm3L+klf(4,4,d_mplL)*uklm4L+klf(4,5,d_mplL)*uklm5L+klf(4,6,d_mplL)*uklm6L+klf(4,7,d_mplL)*uklm7L)+(klf(4,0,d_mx)*uklm0+klf(4,1,d_mx)*uklm1+klf(4,2,d_mx)*uklm2+klf(4,3,d_mx)*uklm3+klf(4,4,d_mx)*uklm4+klf(4,5,d_mx)*uklm5+klf(4,6,d_mx)*uklm6+klf(4,7,d_mx)*uklm7);

    qklm5 = -0.5*(klf(5,0,d_mllR)*uklm0+klf(5,1,d_mllR)*uklm1+klf(5,2,d_mllR)*uklm2+klf(5,3,d_mllR)*uklm3+klf(5,4,d_mllR)*uklm4+klf(5,5,d_mllR)*uklm5+klf(5,6,d_mllR)*uklm6+klf(5,7,d_mllR)*uklm7)-0.5*(klf(5,0,d_mplR)*uklm0R+klf(5,1,d_mplR)*uklm1R+klf(5,2,d_mplR)*uklm2R+klf(5,3,d_mplR)*uklm3R+klf(5,4,d_mplR)*uklm4R+klf(5,5,d_mplR)*uklm5R+klf(5,6,d_mplR)*uklm6R+klf(5,7,d_mplR)*uklm7R)+0.5*(klf(5,0,d_mllL)*uklm0+klf(5,1,d_mllL)*uklm1+klf(5,2,d_mllL)*uklm2+klf(5,3,d_mllL)*uklm3+klf(5,4,d_mllL)*uklm4+klf(5,5,d_mllL)*uklm5+klf(5,6,d_mllL)*uklm6+klf(5,7,d_mllL)*uklm7)+0.5*(klf(5,0,d_mplL)*uklm0L+klf(5,1,d_mplL)*uklm1L+klf(5,2,d_mplL)*uklm2L+klf(5,3,d_mplL)*uklm3L+klf(5,4,d_mplL)*uklm4L+klf(5,5,d_mplL)*uklm5L+klf(5,6,d_mplL)*uklm6L+klf(5,7,d_mplL)*uklm7L)+(klf(5,0,d_mx)*uklm0+klf(5,1,d_mx)*uklm1+klf(5,2,d_mx)*uklm2+klf(5,3,d_mx)*uklm3+klf(5,4,d_mx)*uklm4+klf(5,5,d_mx)*uklm5+klf(5,6,d_mx)*uklm6+klf(5,7,d_mx)*uklm7);

    qklm6 = -0.5*(klf(6,0,d_mllR)*uklm0+klf(6,1,d_mllR)*uklm1+klf(6,2,d_mllR)*uklm2+klf(6,3,d_mllR)*uklm3+klf(6,4,d_mllR)*uklm4+klf(6,5,d_mllR)*uklm5+klf(6,6,d_mllR)*uklm6+klf(6,7,d_mllR)*uklm7)-0.5*(klf(6,0,d_mplR)*uklm0R+klf(6,1,d_mplR)*uklm1R+klf(6,2,d_mplR)*uklm2R+klf(6,3,d_mplR)*uklm3R+klf(6,4,d_mplR)*uklm4R+klf(6,5,d_mplR)*uklm5R+klf(6,6,d_mplR)*uklm6R+klf(6,7,d_mplR)*uklm7R)+0.5*(klf(6,0,d_mllL)*uklm0+klf(6,1,d_mllL)*uklm1+klf(6,2,d_mllL)*uklm2+klf(6,3,d_mllL)*uklm3+klf(6,4,d_mllL)*uklm4+klf(6,5,d_mllL)*uklm5+klf(6,6,d_mllL)*uklm6+klf(6,7,d_mllL)*uklm7)+0.5*(klf(6,0,d_mplL)*uklm0L+klf(6,1,d_mplL)*uklm1L+klf(6,2,d_mplL)*uklm2L+klf(6,3,d_mplL)*uklm3L+klf(6,4,d_mplL)*uklm4L+klf(6,5,d_mplL)*uklm5L+klf(6,6,d_mplL)*uklm6L+klf(6,7,d_mplL)*uklm7L)+(klf(6,0,d_mx)*uklm0+klf(6,1,d_mx)*uklm1+klf(6,2,d_mx)*uklm2+klf(6,3,d_mx)*uklm3+klf(6,4,d_mx)*uklm4+klf(6,5,d_mx)*uklm5+klf(6,6,d_mx)*uklm6+klf(6,7,d_mx)*uklm7);

    qklm7 = -0.5*(klf(7,0,d_mllR)*uklm0+klf(7,1,d_mllR)*uklm1+klf(7,2,d_mllR)*uklm2+klf(7,3,d_mllR)*uklm3+klf(7,4,d_mllR)*uklm4+klf(7,5,d_mllR)*uklm5+klf(7,6,d_mllR)*uklm6+klf(7,7,d_mllR)*uklm7)-0.5*(klf(7,0,d_mplR)*uklm0R+klf(7,1,d_mplR)*uklm1R+klf(7,2,d_mplR)*uklm2R+klf(7,3,d_mplR)*uklm3R+klf(7,4,d_mplR)*uklm4R+klf(7,5,d_mplR)*uklm5R+klf(7,6,d_mplR)*uklm6R+klf(7,7,d_mplR)*uklm7R)+0.5*(klf(7,0,d_mllL)*uklm0+klf(7,1,d_mllL)*uklm1+klf(7,2,d_mllL)*uklm2+klf(7,3,d_mllL)*uklm3+klf(7,4,d_mllL)*uklm4+klf(7,5,d_mllL)*uklm5+klf(7,6,d_mllL)*uklm6+klf(7,7,d_mllL)*uklm7)+0.5*(klf(7,0,d_mplL)*uklm0L+klf(7,1,d_mplL)*uklm1L+klf(7,2,d_mplL)*uklm2L+klf(7,3,d_mplL)*uklm3L+klf(7,4,d_mplL)*uklm4L+klf(7,5,d_mplL)*uklm5L+klf(7,6,d_mplL)*uklm6L+klf(7,7,d_mplL)*uklm7L)+(klf(7,0,d_mx)*uklm0+klf(7,1,d_mx)*uklm1+klf(7,2,d_mx)*uklm2+klf(7,3,d_mx)*uklm3+klf(7,4,d_mx)*uklm4+klf(7,5,d_mx)*uklm5+klf(7,6,d_mx)*uklm6+klf(7,7,d_mx)*uklm7);
    

    
    qklm1 = qklm1 - ( klf(1,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm2 = qklm2 - ( klf(2,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm3 = qklm3 - ( klf(3,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm4 = qklm4 - ( klf(4,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm5 = qklm5 - ( klf(5,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm6 = qklm6 - ( klf(6,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm7 = qklm7 - ( klf(7,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;    
    
    qklm2 = qklm2 - ( klf(2,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm3 = qklm3 - ( klf(3,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm4 = qklm4 - ( klf(4,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm5 = qklm5 - ( klf(5,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm6 = qklm6 - ( klf(6,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm7 = qklm7 - ( klf(7,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    
    qklm3 = qklm3 - ( klf(3,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    qklm4 = qklm4 - ( klf(4,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    qklm5 = qklm5 - ( klf(5,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    qklm6 = qklm6 - ( klf(6,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    qklm7 = qklm7 - ( klf(7,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    
    qklm4 = qklm4 - ( klf(4,3,d_mltri)/klf(3,3,d_mltri) ) * qklm3;
    qklm5 = qklm5 - ( klf(5,3,d_mltri)/klf(3,3,d_mltri) ) * qklm3;
    qklm6 = qklm6 - ( klf(6,3,d_mltri)/klf(3,3,d_mltri) ) * qklm3;
    qklm7 = qklm7 - ( klf(7,3,d_mltri)/klf(3,3,d_mltri) ) * qklm3;
    
    qklm5 = qklm5 - ( klf(5,4,d_mltri)/klf(4,4,d_mltri) ) * qklm4;
    qklm6 = qklm6 - ( klf(6,4,d_mltri)/klf(4,4,d_mltri) ) * qklm4;
    qklm7 = qklm7 - ( klf(7,4,d_mltri)/klf(4,4,d_mltri) ) * qklm4;    
    
    qklm6 = qklm6 - ( klf(6,5,d_mltri)/klf(5,5,d_mltri) ) * qklm5;
    qklm7 = qklm7 - ( klf(7,5,d_mltri)/klf(5,5,d_mltri) ) * qklm5;
        
    qklm7 = qklm7 - ( klf(7,6,d_mltri)/klf(6,6,d_mltri) ) * qklm6;


    x7 = (qklm7/klf(7,7,DIM,d_ml));
    x6 = (qklm6-klf(6,7,DIM,d_ml)*x7)/klf(6,6,DIM,d_ml);
    x5 = (qklm5-klf(5,6,DIM,d_ml)*x6-klf(5,7,DIM,d_ml)*x7)/klf(5,5,DIM,d_ml);
    x4 = (qklm4-klf(4,5,DIM,d_ml)*x5-klf(4,6,DIM,d_ml)*x6-klf(4,7,DIM,d_ml)*x7)/klf(4,4,DIM,d_ml);
    x3 = (qklm3-klf(3,4,DIM,d_ml)*x4-klf(3,5,DIM,d_ml)*x5-klf(3,6,DIM,d_ml)*x6-klf(3,7,DIM,d_ml)*x7)/klf(3,3,DIM,d_ml);
    x2 = (qklm2-klf(2,3,DIM,d_ml)*x3-klf(2,4,DIM,d_ml)*x4-klf(2,5,DIM,d_ml)*x5-klf(2,6,DIM,d_ml)*x6-klf(2,7,DIM,d_ml)*x7)/klf(2,2,DIM,d_ml);
    x1 = (qklm1-klf(1,2,DIM,d_ml)*x2-klf(1,3,DIM,d_ml)*x3-klf(1,4,DIM,d_ml)*x4-klf(1,5,DIM,d_ml)*x5-klf(1,6,DIM,d_ml)*x6-klf(1,7,DIM,d_ml)*x7)/klf(1,1,DIM,d_ml);
    x0 = (qklm0-klf(0,1,DIM,d_ml)*x1-klf(0,2,DIM,d_ml)*x2-klf(0,3,DIM,d_ml)*x3-klf(0,4,DIM,d_ml)*x4-klf(0,5,DIM,d_ml)*x5-klf(0,6,DIM,d_ml)*x6-klf(0,7,DIM,d_ml)*x7)/klf(0,0,DIM,d_ml);


    
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,0,x0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,1,x1);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,2,x2);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,3,x3);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,4,x4);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,5,x5);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,6,x6);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,7,x7);
    
    //printf("b qklm0: %d %d %d %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",k,l,m,x0,x1,x2,x3,x4,x5,x6,x7);
  }

}


__global__ void d_qklm1f(int Ks, int Ls, int Ms, float* d_uklm,  float* d_ml, float * d_mltri, 
                         float* d_mllT, float* d_mplT, float* d_mllBo, float* d_mplBo, float* d_my,
                         float* d_qklm1){
  
  int k = blockIdx.x * blockDim.x + threadIdx.x;
  int l = blockIdx.y * blockDim.y + threadIdx.y;
  int m = blockIdx.z * blockDim.z + threadIdx.z;
  

  if(  0 < k  && k < Ks+1  && 0 < l && l < Ls+1 && 0 < m &&  m < Ms+1 ){
    
    float uklm0,uklm1,uklm2,uklm3,uklm4,uklm5,uklm6,uklm7;
    float uklm0T,uklm1T,uklm2T,uklm3T,uklm4T,uklm5T,uklm6T,uklm7T;
    float uklm0Bo,uklm1Bo,uklm2Bo,uklm3Bo,uklm4Bo,uklm5Bo,uklm6Bo,uklm7Bo;
    float qklm0,qklm1,qklm2,qklm3,qklm4,qklm5,qklm6,qklm7;

    float v00,v01,v02,v03,v04,v05,v06,v07;
    float v10,v11,v12,v13,v14,v15,v16,v17;
    float v20,v21,v22,v23,v24,v25,v26,v27;
    float v30,v31,v32,v33,v34,v35,v36,v37;
    float v40,v41,v42,v43,v44,v45,v46,v47;
    float x0,x1,x2,x3,x4,x5,x6,x7;

    
    // uklm0 = d_uklm(k,l,m,0); uklm1 = d_uklm(k,l,m,1); uklm2 = d_uklm(k,l,m,2); uklm3 = d_uklm(k,l,m,3); 
    // uklm4 = d_uklm(k,l,m,4); uklm5 = d_uklm(k,l,m,5); uklm6 = d_uklm(k,l,m,6); uklm7 = d_uklm(k,l,m,7);
    uklm0  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,0); uklm1  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,1); uklm2  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,2); uklm3  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,3);
    uklm4  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,4); uklm5  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,5); uklm6  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,6); uklm7  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,7);
            
    
    // uklm0T = d_uklm(k-1,l,m,0); uklm1T = d_uklm(k-1,l,m,1); uklm2T = d_uklm(k-1,l,m,2); uklm3T = d_uklm(k-1,l,m,3); 
    // uklm4T = d_uklm(k-1,l,m,4); uklm5T = d_uklm(k-1,l,m,5); uklm6T = d_uklm(k-1,l,m,6); uklm7T = d_uklm(k-1,l,m,7);
    uklm0T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_uklm,0); uklm1T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_uklm,1); uklm2T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_uklm,2); uklm3T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_uklm,3);
    uklm4T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_uklm,4); uklm5T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_uklm,5); uklm6T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_uklm,6); uklm7T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_uklm,7);
   
    
    // uklm0Bo = d_uklm(k+1,l,m,0); uklm1Bo = d_uklm(k+1,l,m,1); uklm2Bo = d_uklm(k+1,l,m,2); uklm3Bo = d_uklm(k+1,l,m,3);
    // uklm4Bo = d_uklm(k+1,l,m,4); uklm5Bo = d_uklm(k+1,l,m,5); uklm6Bo = d_uklm(k+1,l,m,6); uklm7Bo = d_uklm(k+1,l,m,7);
    uklm0Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_uklm,0); uklm1Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_uklm,1); uklm2Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_uklm,2); uklm3Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_uklm,3);
    uklm4Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_uklm,4); uklm5Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_uklm,5); uklm6Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_uklm,6); uklm7Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_uklm,7);
   


    // v00 = klf(0,0,d_mllT)*uklm0+klf(0,1,d_mllT)*uklm1+klf(0,2,d_mllT)*uklm2+klf(0,3,d_mllT)*uklm3+klf(0,4,d_mllT)*uklm4+klf(0,5,d_mllT)*uklm5+klf(0,6,d_mllT)*uklm6+klf(0,7,d_mllT)*uklm7;
    // v01 = klf(1,0,d_mllT)*uklm0+klf(1,1,d_mllT)*uklm1+klf(1,2,d_mllT)*uklm2+klf(1,3,d_mllT)*uklm3+klf(1,4,d_mllT)*uklm4+klf(1,5,d_mllT)*uklm5+klf(1,6,d_mllT)*uklm6+klf(1,7,d_mllT)*uklm7;
    // v02 = klf(2,0,d_mllT)*uklm0+klf(2,1,d_mllT)*uklm1+klf(2,2,d_mllT)*uklm2+klf(2,3,d_mllT)*uklm3+klf(2,4,d_mllT)*uklm4+klf(2,5,d_mllT)*uklm5+klf(2,6,d_mllT)*uklm6+klf(2,7,d_mllT)*uklm7;
    // v03 = klf(3,0,d_mllT)*uklm0+klf(3,1,d_mllT)*uklm1+klf(3,2,d_mllT)*uklm2+klf(3,3,d_mllT)*uklm3+klf(3,4,d_mllT)*uklm4+klf(3,5,d_mllT)*uklm5+klf(3,6,d_mllT)*uklm6+klf(3,7,d_mllT)*uklm7;
    // v04 = klf(4,0,d_mllT)*uklm0+klf(4,1,d_mllT)*uklm1+klf(4,2,d_mllT)*uklm2+klf(4,3,d_mllT)*uklm3+klf(4,4,d_mllT)*uklm4+klf(4,5,d_mllT)*uklm5+klf(4,6,d_mllT)*uklm6+klf(4,7,d_mllT)*uklm7;
    // v05 = klf(5,0,d_mllT)*uklm0+klf(5,1,d_mllT)*uklm1+klf(5,2,d_mllT)*uklm2+klf(5,3,d_mllT)*uklm3+klf(5,4,d_mllT)*uklm4+klf(5,5,d_mllT)*uklm5+klf(5,6,d_mllT)*uklm6+klf(5,7,d_mllT)*uklm7;
    // v06 = klf(6,0,d_mllT)*uklm0+klf(6,1,d_mllT)*uklm1+klf(6,2,d_mllT)*uklm2+klf(6,3,d_mllT)*uklm3+klf(6,4,d_mllT)*uklm4+klf(6,5,d_mllT)*uklm5+klf(6,6,d_mllT)*uklm6+klf(6,7,d_mllT)*uklm7;
    // v07 = klf(7,0,d_mllT)*uklm0+klf(7,1,d_mllT)*uklm1+klf(7,2,d_mllT)*uklm2+klf(7,3,d_mllT)*uklm3+klf(7,4,d_mllT)*uklm4+klf(7,5,d_mllT)*uklm5+klf(7,6,d_mllT)*uklm6+klf(7,7,d_mllT)*uklm7;


    // v10 = klf(0,0,d_mplT)*uklm0T+klf(0,1,d_mplT)*uklm1T+klf(0,2,d_mplT)*uklm2T+klf(0,3,d_mplT)*uklm3T+klf(0,4,d_mplT)*uklm4T+klf(0,5,d_mplT)*uklm5T+klf(0,6,d_mplT)*uklm6T+klf(0,7,d_mplT)*uklm7T;
    // v11 = klf(1,0,d_mplT)*uklm0T+klf(1,1,d_mplT)*uklm1T+klf(1,2,d_mplT)*uklm2T+klf(1,3,d_mplT)*uklm3T+klf(1,4,d_mplT)*uklm4T+klf(1,5,d_mplT)*uklm5T+klf(1,6,d_mplT)*uklm6T+klf(1,7,d_mplT)*uklm7T;
    // v12 = klf(2,0,d_mplT)*uklm0T+klf(2,1,d_mplT)*uklm1T+klf(2,2,d_mplT)*uklm2T+klf(2,3,d_mplT)*uklm3T+klf(2,4,d_mplT)*uklm4T+klf(2,5,d_mplT)*uklm5T+klf(2,6,d_mplT)*uklm6T+klf(2,7,d_mplT)*uklm7T;
    // v13 = klf(3,0,d_mplT)*uklm0T+klf(3,1,d_mplT)*uklm1T+klf(3,2,d_mplT)*uklm2T+klf(3,3,d_mplT)*uklm3T+klf(3,4,d_mplT)*uklm4T+klf(3,5,d_mplT)*uklm5T+klf(3,6,d_mplT)*uklm6T+klf(3,7,d_mplT)*uklm7T;
    // v14 = klf(4,0,d_mplT)*uklm0T+klf(4,1,d_mplT)*uklm1T+klf(4,2,d_mplT)*uklm2T+klf(4,3,d_mplT)*uklm3T+klf(4,4,d_mplT)*uklm4T+klf(4,5,d_mplT)*uklm5T+klf(4,6,d_mplT)*uklm6T+klf(4,7,d_mplT)*uklm7T;
    // v15 = klf(5,0,d_mplT)*uklm0T+klf(5,1,d_mplT)*uklm1T+klf(5,2,d_mplT)*uklm2T+klf(5,3,d_mplT)*uklm3T+klf(5,4,d_mplT)*uklm4T+klf(5,5,d_mplT)*uklm5T+klf(5,6,d_mplT)*uklm6T+klf(5,7,d_mplT)*uklm7T;
    // v16 = klf(6,0,d_mplT)*uklm0T+klf(6,1,d_mplT)*uklm1T+klf(6,2,d_mplT)*uklm2T+klf(6,3,d_mplT)*uklm3T+klf(6,4,d_mplT)*uklm4T+klf(6,5,d_mplT)*uklm5T+klf(6,6,d_mplT)*uklm6T+klf(6,7,d_mplT)*uklm7T;
    // v17 = klf(7,0,d_mplT)*uklm0T+klf(7,1,d_mplT)*uklm1T+klf(7,2,d_mplT)*uklm2T+klf(7,3,d_mplT)*uklm3T+klf(7,4,d_mplT)*uklm4T+klf(7,5,d_mplT)*uklm5T+klf(7,6,d_mplT)*uklm6T+klf(7,7,d_mplT)*uklm7T;


    // v20 = klf(0,0,d_mllBo)*uklm0+klf(0,1,d_mllBo)*uklm1+klf(0,2,d_mllBo)*uklm2+klf(0,3,d_mllBo)*uklm3+klf(0,4,d_mllBo)*uklm4+klf(0,5,d_mllBo)*uklm5+klf(0,6,d_mllBo)*uklm6+klf(0,7,d_mllBo)*uklm7;
    // v21 = klf(1,0,d_mllBo)*uklm0+klf(1,1,d_mllBo)*uklm1+klf(1,2,d_mllBo)*uklm2+klf(1,3,d_mllBo)*uklm3+klf(1,4,d_mllBo)*uklm4+klf(1,5,d_mllBo)*uklm5+klf(1,6,d_mllBo)*uklm6+klf(1,7,d_mllBo)*uklm7;
    // v22 = klf(2,0,d_mllBo)*uklm0+klf(2,1,d_mllBo)*uklm1+klf(2,2,d_mllBo)*uklm2+klf(2,3,d_mllBo)*uklm3+klf(2,4,d_mllBo)*uklm4+klf(2,5,d_mllBo)*uklm5+klf(2,6,d_mllBo)*uklm6+klf(2,7,d_mllBo)*uklm7;
    // v23 = klf(3,0,d_mllBo)*uklm0+klf(3,1,d_mllBo)*uklm1+klf(3,2,d_mllBo)*uklm2+klf(3,3,d_mllBo)*uklm3+klf(3,4,d_mllBo)*uklm4+klf(3,5,d_mllBo)*uklm5+klf(3,6,d_mllBo)*uklm6+klf(3,7,d_mllBo)*uklm7;
    // v24 = klf(4,0,d_mllBo)*uklm0+klf(4,1,d_mllBo)*uklm1+klf(4,2,d_mllBo)*uklm2+klf(4,3,d_mllBo)*uklm3+klf(4,4,d_mllBo)*uklm4+klf(4,5,d_mllBo)*uklm5+klf(4,6,d_mllBo)*uklm6+klf(4,7,d_mllBo)*uklm7;
    // v25 = klf(5,0,d_mllBo)*uklm0+klf(5,1,d_mllBo)*uklm1+klf(5,2,d_mllBo)*uklm2+klf(5,3,d_mllBo)*uklm3+klf(5,4,d_mllBo)*uklm4+klf(5,5,d_mllBo)*uklm5+klf(5,6,d_mllBo)*uklm6+klf(5,7,d_mllBo)*uklm7;
    // v26 = klf(6,0,d_mllBo)*uklm0+klf(6,1,d_mllBo)*uklm1+klf(6,2,d_mllBo)*uklm2+klf(6,3,d_mllBo)*uklm3+klf(6,4,d_mllBo)*uklm4+klf(6,5,d_mllBo)*uklm5+klf(6,6,d_mllBo)*uklm6+klf(6,7,d_mllBo)*uklm7;
    // v27 = klf(7,0,d_mllBo)*uklm0+klf(7,1,d_mllBo)*uklm1+klf(7,2,d_mllBo)*uklm2+klf(7,3,d_mllBo)*uklm3+klf(7,4,d_mllBo)*uklm4+klf(7,5,d_mllBo)*uklm5+klf(7,6,d_mllBo)*uklm6+klf(7,7,d_mllBo)*uklm7;

    
    // v30 = klf(0,0,d_mplBo)*uklm0Bo+klf(0,1,d_mplBo)*uklm1Bo+klf(0,2,d_mplBo)*uklm2Bo+klf(0,3,d_mplBo)*uklm3Bo+klf(0,4,d_mplBo)*uklm4Bo+klf(0,5,d_mplBo)*uklm5Bo+klf(0,6,d_mplBo)*uklm6Bo+klf(0,7,d_mplBo)*uklm7Bo;
    // v31 = klf(1,0,d_mplBo)*uklm0Bo+klf(1,1,d_mplBo)*uklm1Bo+klf(1,2,d_mplBo)*uklm2Bo+klf(1,3,d_mplBo)*uklm3Bo+klf(1,4,d_mplBo)*uklm4Bo+klf(1,5,d_mplBo)*uklm5Bo+klf(1,6,d_mplBo)*uklm6Bo+klf(1,7,d_mplBo)*uklm7Bo;
    // v32 = klf(2,0,d_mplBo)*uklm0Bo+klf(2,1,d_mplBo)*uklm1Bo+klf(2,2,d_mplBo)*uklm2Bo+klf(2,3,d_mplBo)*uklm3Bo+klf(2,4,d_mplBo)*uklm4Bo+klf(2,5,d_mplBo)*uklm5Bo+klf(2,6,d_mplBo)*uklm6Bo+klf(2,7,d_mplBo)*uklm7Bo;
    // v33 = klf(3,0,d_mplBo)*uklm0Bo+klf(3,1,d_mplBo)*uklm1Bo+klf(3,2,d_mplBo)*uklm2Bo+klf(3,3,d_mplBo)*uklm3Bo+klf(3,4,d_mplBo)*uklm4Bo+klf(3,5,d_mplBo)*uklm5Bo+klf(3,6,d_mplBo)*uklm6Bo+klf(3,7,d_mplBo)*uklm7Bo;
    // v34 = klf(4,0,d_mplBo)*uklm0Bo+klf(4,1,d_mplBo)*uklm1Bo+klf(4,2,d_mplBo)*uklm2Bo+klf(4,3,d_mplBo)*uklm3Bo+klf(4,4,d_mplBo)*uklm4Bo+klf(4,5,d_mplBo)*uklm5Bo+klf(4,6,d_mplBo)*uklm6Bo+klf(4,7,d_mplBo)*uklm7Bo;
    // v35 = klf(5,0,d_mplBo)*uklm0Bo+klf(5,1,d_mplBo)*uklm1Bo+klf(5,2,d_mplBo)*uklm2Bo+klf(5,3,d_mplBo)*uklm3Bo+klf(5,4,d_mplBo)*uklm4Bo+klf(5,5,d_mplBo)*uklm5Bo+klf(5,6,d_mplBo)*uklm6Bo+klf(5,7,d_mplBo)*uklm7Bo;
    // v36 = klf(6,0,d_mplBo)*uklm0Bo+klf(6,1,d_mplBo)*uklm1Bo+klf(6,2,d_mplBo)*uklm2Bo+klf(6,3,d_mplBo)*uklm3Bo+klf(6,4,d_mplBo)*uklm4Bo+klf(6,5,d_mplBo)*uklm5Bo+klf(6,6,d_mplBo)*uklm6Bo+klf(6,7,d_mplBo)*uklm7Bo;
    // v37 = klf(7,0,d_mplBo)*uklm0Bo+klf(7,1,d_mplBo)*uklm1Bo+klf(7,2,d_mplBo)*uklm2Bo+klf(7,3,d_mplBo)*uklm3Bo+klf(7,4,d_mplBo)*uklm4Bo+klf(7,5,d_mplBo)*uklm5Bo+klf(7,6,d_mplBo)*uklm6Bo+klf(7,7,d_mplBo)*uklm7Bo;


    // v40 = klf(0,0,d_my)*uklm0+klf(0,1,d_my)*uklm1+klf(0,2,d_my)*uklm2+klf(0,3,d_my)*uklm3+klf(0,4,d_my)*uklm4+klf(0,5,d_my)*uklm5+klf(0,6,d_my)*uklm6+klf(0,7,d_my)*uklm7;
    // v41 = klf(1,0,d_my)*uklm0+klf(1,1,d_my)*uklm1+klf(1,2,d_my)*uklm2+klf(1,3,d_my)*uklm3+klf(1,4,d_my)*uklm4+klf(1,5,d_my)*uklm5+klf(1,6,d_my)*uklm6+klf(1,7,d_my)*uklm7;
    // v42 = klf(2,0,d_my)*uklm0+klf(2,1,d_my)*uklm1+klf(2,2,d_my)*uklm2+klf(2,3,d_my)*uklm3+klf(2,4,d_my)*uklm4+klf(2,5,d_my)*uklm5+klf(2,6,d_my)*uklm6+klf(2,7,d_my)*uklm7;
    // v43 = klf(3,0,d_my)*uklm0+klf(3,1,d_my)*uklm1+klf(3,2,d_my)*uklm2+klf(3,3,d_my)*uklm3+klf(3,4,d_my)*uklm4+klf(3,5,d_my)*uklm5+klf(3,6,d_my)*uklm6+klf(3,7,d_my)*uklm7;
    // v44 = klf(4,0,d_my)*uklm0+klf(4,1,d_my)*uklm1+klf(4,2,d_my)*uklm2+klf(4,3,d_my)*uklm3+klf(4,4,d_my)*uklm4+klf(4,5,d_my)*uklm5+klf(4,6,d_my)*uklm6+klf(4,7,d_my)*uklm7;
    // v45 = klf(5,0,d_my)*uklm0+klf(5,1,d_my)*uklm1+klf(5,2,d_my)*uklm2+klf(5,3,d_my)*uklm3+klf(5,4,d_my)*uklm4+klf(5,5,d_my)*uklm5+klf(5,6,d_my)*uklm6+klf(5,7,d_my)*uklm7;
    // v46 = klf(6,0,d_my)*uklm0+klf(6,1,d_my)*uklm1+klf(6,2,d_my)*uklm2+klf(6,3,d_my)*uklm3+klf(6,4,d_my)*uklm4+klf(6,5,d_my)*uklm5+klf(6,6,d_my)*uklm6+klf(6,7,d_my)*uklm7;
    // v47 = klf(7,0,d_my)*uklm0+klf(7,1,d_my)*uklm1+klf(7,2,d_my)*uklm2+klf(7,3,d_my)*uklm3+klf(7,4,d_my)*uklm4+klf(7,5,d_my)*uklm5+klf(7,6,d_my)*uklm6+klf(7,7,d_my)*uklm7;

    
    
    // qklm0 = -0.5*v00 -0.5*v10 + 0.5*v20 + 0.5*v30 + v40;
    // qklm1 = -0.5*v01 -0.5*v11 + 0.5*v21 + 0.5*v31 + v41;
    // qklm2 = -0.5*v02 -0.5*v12 + 0.5*v22 + 0.5*v32 + v42;
    // qklm3 = -0.5*v03 -0.5*v13 + 0.5*v23 + 0.5*v33 + v43;
    // qklm4 = -0.5*v04 -0.5*v14 + 0.5*v24 + 0.5*v34 + v44;
    // qklm5 = -0.5*v05 -0.5*v15 + 0.5*v25 + 0.5*v35 + v45;
    // qklm6 = -0.5*v06 -0.5*v16 + 0.5*v26 + 0.5*v36 + v46;
    // qklm7 = -0.5*v07 -0.5*v17 + 0.5*v27 + 0.5*v37 + v47;

        qklm0 = -0.5*(klf(0,0,d_mllT)*uklm0+klf(0,1,d_mllT)*uklm1+klf(0,2,d_mllT)*uklm2+klf(0,3,d_mllT)*uklm3+klf(0,4,d_mllT)*uklm4+klf(0,5,d_mllT)*uklm5+klf(0,6,d_mllT)*uklm6+klf(0,7,d_mllT)*uklm7)-0.5*(klf(0,0,d_mplT)*uklm0T+klf(0,1,d_mplT)*uklm1T+klf(0,2,d_mplT)*uklm2T+klf(0,3,d_mplT)*uklm3T+klf(0,4,d_mplT)*uklm4T+klf(0,5,d_mplT)*uklm5T+klf(0,6,d_mplT)*uklm6T+klf(0,7,d_mplT)*uklm7T)+0.5*(klf(0,0,d_mllBo)*uklm0+klf(0,1,d_mllBo)*uklm1+klf(0,2,d_mllBo)*uklm2+klf(0,3,d_mllBo)*uklm3+klf(0,4,d_mllBo)*uklm4+klf(0,5,d_mllBo)*uklm5+klf(0,6,d_mllBo)*uklm6+klf(0,7,d_mllBo)*uklm7)+0.5*(klf(0,0,d_mplBo)*uklm0Bo+klf(0,1,d_mplBo)*uklm1Bo+klf(0,2,d_mplBo)*uklm2Bo+klf(0,3,d_mplBo)*uklm3Bo+klf(0,4,d_mplBo)*uklm4Bo+klf(0,5,d_mplBo)*uklm5Bo+klf(0,6,d_mplBo)*uklm6Bo+klf(0,7,d_mplBo)*uklm7Bo)+(klf(0,0,d_my)*uklm0+klf(0,1,d_my)*uklm1+klf(0,2,d_my)*uklm2+klf(0,3,d_my)*uklm3+klf(0,4,d_my)*uklm4+klf(0,5,d_my)*uklm5+klf(0,6,d_my)*uklm6+klf(0,7,d_my)*uklm7);
    
    qklm1 = -0.5*(klf(1,0,d_mllT)*uklm0+klf(1,1,d_mllT)*uklm1+klf(1,2,d_mllT)*uklm2+klf(1,3,d_mllT)*uklm3+klf(1,4,d_mllT)*uklm4+klf(1,5,d_mllT)*uklm5+klf(1,6,d_mllT)*uklm6+klf(1,7,d_mllT)*uklm7)-0.5*(klf(1,0,d_mplT)*uklm0T+klf(1,1,d_mplT)*uklm1T+klf(1,2,d_mplT)*uklm2T+klf(1,3,d_mplT)*uklm3T+klf(1,4,d_mplT)*uklm4T+klf(1,5,d_mplT)*uklm5T+klf(1,6,d_mplT)*uklm6T+klf(1,7,d_mplT)*uklm7T)+0.5*(klf(1,0,d_mllBo)*uklm0+klf(1,1,d_mllBo)*uklm1+klf(1,2,d_mllBo)*uklm2+klf(1,3,d_mllBo)*uklm3+klf(1,4,d_mllBo)*uklm4+klf(1,5,d_mllBo)*uklm5+klf(1,6,d_mllBo)*uklm6+klf(1,7,d_mllBo)*uklm7)+0.5*(klf(1,0,d_mplBo)*uklm0Bo+klf(1,1,d_mplBo)*uklm1Bo+klf(1,2,d_mplBo)*uklm2Bo+klf(1,3,d_mplBo)*uklm3Bo+klf(1,4,d_mplBo)*uklm4Bo+klf(1,5,d_mplBo)*uklm5Bo+klf(1,6,d_mplBo)*uklm6Bo+klf(1,7,d_mplBo)*uklm7Bo)+(klf(1,0,d_my)*uklm0+klf(1,1,d_my)*uklm1+klf(1,2,d_my)*uklm2+klf(1,3,d_my)*uklm3+klf(1,4,d_my)*uklm4+klf(1,5,d_my)*uklm5+klf(1,6,d_my)*uklm6+klf(1,7,d_my)*uklm7);

    qklm2 = -0.5*(klf(2,0,d_mllT)*uklm0+klf(2,1,d_mllT)*uklm1+klf(2,2,d_mllT)*uklm2+klf(2,3,d_mllT)*uklm3+klf(2,4,d_mllT)*uklm4+klf(2,5,d_mllT)*uklm5+klf(2,6,d_mllT)*uklm6+klf(2,7,d_mllT)*uklm7)-0.5*(klf(2,0,d_mplT)*uklm0T+klf(2,1,d_mplT)*uklm1T+klf(2,2,d_mplT)*uklm2T+klf(2,3,d_mplT)*uklm3T+klf(2,4,d_mplT)*uklm4T+klf(2,5,d_mplT)*uklm5T+klf(2,6,d_mplT)*uklm6T+klf(2,7,d_mplT)*uklm7T)+0.5*(klf(2,0,d_mllBo)*uklm0+klf(2,1,d_mllBo)*uklm1+klf(2,2,d_mllBo)*uklm2+klf(2,3,d_mllBo)*uklm3+klf(2,4,d_mllBo)*uklm4+klf(2,5,d_mllBo)*uklm5+klf(2,6,d_mllBo)*uklm6+klf(2,7,d_mllBo)*uklm7)+0.5*(klf(2,0,d_mplBo)*uklm0Bo+klf(2,1,d_mplBo)*uklm1Bo+klf(2,2,d_mplBo)*uklm2Bo+klf(2,3,d_mplBo)*uklm3Bo+klf(2,4,d_mplBo)*uklm4Bo+klf(2,5,d_mplBo)*uklm5Bo+klf(2,6,d_mplBo)*uklm6Bo+klf(2,7,d_mplBo)*uklm7Bo)+(klf(2,0,d_my)*uklm0+klf(2,1,d_my)*uklm1+klf(2,2,d_my)*uklm2+klf(2,3,d_my)*uklm3+klf(2,4,d_my)*uklm4+klf(2,5,d_my)*uklm5+klf(2,6,d_my)*uklm6+klf(2,7,d_my)*uklm7);

    qklm3 = -0.5*(klf(3,0,d_mllT)*uklm0+klf(3,1,d_mllT)*uklm1+klf(3,2,d_mllT)*uklm2+klf(3,3,d_mllT)*uklm3+klf(3,4,d_mllT)*uklm4+klf(3,5,d_mllT)*uklm5+klf(3,6,d_mllT)*uklm6+klf(3,7,d_mllT)*uklm7)-0.5*(klf(3,0,d_mplT)*uklm0T+klf(3,1,d_mplT)*uklm1T+klf(3,2,d_mplT)*uklm2T+klf(3,3,d_mplT)*uklm3T+klf(3,4,d_mplT)*uklm4T+klf(3,5,d_mplT)*uklm5T+klf(3,6,d_mplT)*uklm6T+klf(3,7,d_mplT)*uklm7T)+0.5*(klf(3,0,d_mllBo)*uklm0+klf(3,1,d_mllBo)*uklm1+klf(3,2,d_mllBo)*uklm2+klf(3,3,d_mllBo)*uklm3+klf(3,4,d_mllBo)*uklm4+klf(3,5,d_mllBo)*uklm5+klf(3,6,d_mllBo)*uklm6+klf(3,7,d_mllBo)*uklm7)+0.5*(klf(3,0,d_mplBo)*uklm0Bo+klf(3,1,d_mplBo)*uklm1Bo+klf(3,2,d_mplBo)*uklm2Bo+klf(3,3,d_mplBo)*uklm3Bo+klf(3,4,d_mplBo)*uklm4Bo+klf(3,5,d_mplBo)*uklm5Bo+klf(3,6,d_mplBo)*uklm6Bo+klf(3,7,d_mplBo)*uklm7Bo)+(klf(3,0,d_my)*uklm0+klf(3,1,d_my)*uklm1+klf(3,2,d_my)*uklm2+klf(3,3,d_my)*uklm3+klf(3,4,d_my)*uklm4+klf(3,5,d_my)*uklm5+klf(3,6,d_my)*uklm6+klf(3,7,d_my)*uklm7);

    qklm4 = -0.5*( klf(4,0,d_mllT)*uklm0+klf(4,1,d_mllT)*uklm1+klf(4,2,d_mllT)*uklm2+klf(4,3,d_mllT)*uklm3+klf(4,4,d_mllT)*uklm4+klf(4,5,d_mllT)*uklm5+klf(4,6,d_mllT)*uklm6+klf(4,7,d_mllT)*uklm7)-0.5*(klf(4,0,d_mplT)*uklm0T+klf(4,1,d_mplT)*uklm1T+klf(4,2,d_mplT)*uklm2T+klf(4,3,d_mplT)*uklm3T+klf(4,4,d_mplT)*uklm4T+klf(4,5,d_mplT)*uklm5T+klf(4,6,d_mplT)*uklm6T+klf(4,7,d_mplT)*uklm7T)+0.5*(klf(4,0,d_mllBo)*uklm0+klf(4,1,d_mllBo)*uklm1+klf(4,2,d_mllBo)*uklm2+klf(4,3,d_mllBo)*uklm3+klf(4,4,d_mllBo)*uklm4+klf(4,5,d_mllBo)*uklm5+klf(4,6,d_mllBo)*uklm6+klf(4,7,d_mllBo)*uklm7)+0.5*(klf(4,0,d_mplBo)*uklm0Bo+klf(4,1,d_mplBo)*uklm1Bo+klf(4,2,d_mplBo)*uklm2Bo+klf(4,3,d_mplBo)*uklm3Bo+klf(4,4,d_mplBo)*uklm4Bo+klf(4,5,d_mplBo)*uklm5Bo+klf(4,6,d_mplBo)*uklm6Bo+klf(4,7,d_mplBo)*uklm7Bo)+(klf(4,0,d_my)*uklm0+klf(4,1,d_my)*uklm1+klf(4,2,d_my)*uklm2+klf(4,3,d_my)*uklm3+klf(4,4,d_my)*uklm4+klf(4,5,d_my)*uklm5+klf(4,6,d_my)*uklm6+klf(4,7,d_my)*uklm7);

    qklm5 = -0.5*(klf(5,0,d_mllT)*uklm0+klf(5,1,d_mllT)*uklm1+klf(5,2,d_mllT)*uklm2+klf(5,3,d_mllT)*uklm3+klf(5,4,d_mllT)*uklm4+klf(5,5,d_mllT)*uklm5+klf(5,6,d_mllT)*uklm6+klf(5,7,d_mllT)*uklm7)-0.5*(klf(5,0,d_mplT)*uklm0T+klf(5,1,d_mplT)*uklm1T+klf(5,2,d_mplT)*uklm2T+klf(5,3,d_mplT)*uklm3T+klf(5,4,d_mplT)*uklm4T+klf(5,5,d_mplT)*uklm5T+klf(5,6,d_mplT)*uklm6T+klf(5,7,d_mplT)*uklm7T)+0.5*(klf(5,0,d_mllBo)*uklm0+klf(5,1,d_mllBo)*uklm1+klf(5,2,d_mllBo)*uklm2+klf(5,3,d_mllBo)*uklm3+klf(5,4,d_mllBo)*uklm4+klf(5,5,d_mllBo)*uklm5+klf(5,6,d_mllBo)*uklm6+klf(5,7,d_mllBo)*uklm7)+0.5*(klf(5,0,d_mplBo)*uklm0Bo+klf(5,1,d_mplBo)*uklm1Bo+klf(5,2,d_mplBo)*uklm2Bo+klf(5,3,d_mplBo)*uklm3Bo+klf(5,4,d_mplBo)*uklm4Bo+klf(5,5,d_mplBo)*uklm5Bo+klf(5,6,d_mplBo)*uklm6Bo+klf(5,7,d_mplBo)*uklm7Bo)+(klf(5,0,d_my)*uklm0+klf(5,1,d_my)*uklm1+klf(5,2,d_my)*uklm2+klf(5,3,d_my)*uklm3+klf(5,4,d_my)*uklm4+klf(5,5,d_my)*uklm5+klf(5,6,d_my)*uklm6+klf(5,7,d_my)*uklm7);

    qklm6 = -0.5*(klf(6,0,d_mllT)*uklm0+klf(6,1,d_mllT)*uklm1+klf(6,2,d_mllT)*uklm2+klf(6,3,d_mllT)*uklm3+klf(6,4,d_mllT)*uklm4+klf(6,5,d_mllT)*uklm5+klf(6,6,d_mllT)*uklm6+klf(6,7,d_mllT)*uklm7)-0.5*(klf(6,0,d_mplT)*uklm0T+klf(6,1,d_mplT)*uklm1T+klf(6,2,d_mplT)*uklm2T+klf(6,3,d_mplT)*uklm3T+klf(6,4,d_mplT)*uklm4T+klf(6,5,d_mplT)*uklm5T+klf(6,6,d_mplT)*uklm6T+klf(6,7,d_mplT)*uklm7T)+0.5*(klf(6,0,d_mllBo)*uklm0+klf(6,1,d_mllBo)*uklm1+klf(6,2,d_mllBo)*uklm2+klf(6,3,d_mllBo)*uklm3+klf(6,4,d_mllBo)*uklm4+klf(6,5,d_mllBo)*uklm5+klf(6,6,d_mllBo)*uklm6+klf(6,7,d_mllBo)*uklm7)+0.5*(klf(6,0,d_mplBo)*uklm0Bo+klf(6,1,d_mplBo)*uklm1Bo+klf(6,2,d_mplBo)*uklm2Bo+klf(6,3,d_mplBo)*uklm3Bo+klf(6,4,d_mplBo)*uklm4Bo+klf(6,5,d_mplBo)*uklm5Bo+klf(6,6,d_mplBo)*uklm6Bo+klf(6,7,d_mplBo)*uklm7Bo)+(klf(6,0,d_my)*uklm0+klf(6,1,d_my)*uklm1+klf(6,2,d_my)*uklm2+klf(6,3,d_my)*uklm3+klf(6,4,d_my)*uklm4+klf(6,5,d_my)*uklm5+klf(6,6,d_my)*uklm6+klf(6,7,d_my)*uklm7);

    qklm7 = -0.5*(klf(7,0,d_mllT)*uklm0+klf(7,1,d_mllT)*uklm1+klf(7,2,d_mllT)*uklm2+klf(7,3,d_mllT)*uklm3+klf(7,4,d_mllT)*uklm4+klf(7,5,d_mllT)*uklm5+klf(7,6,d_mllT)*uklm6+klf(7,7,d_mllT)*uklm7)-0.5*(klf(7,0,d_mplT)*uklm0T+klf(7,1,d_mplT)*uklm1T+klf(7,2,d_mplT)*uklm2T+klf(7,3,d_mplT)*uklm3T+klf(7,4,d_mplT)*uklm4T+klf(7,5,d_mplT)*uklm5T+klf(7,6,d_mplT)*uklm6T+klf(7,7,d_mplT)*uklm7T)+0.5*(klf(7,0,d_mllBo)*uklm0+klf(7,1,d_mllBo)*uklm1+klf(7,2,d_mllBo)*uklm2+klf(7,3,d_mllBo)*uklm3+klf(7,4,d_mllBo)*uklm4+klf(7,5,d_mllBo)*uklm5+klf(7,6,d_mllBo)*uklm6+klf(7,7,d_mllBo)*uklm7)+0.5*(klf(7,0,d_mplBo)*uklm0Bo+klf(7,1,d_mplBo)*uklm1Bo+klf(7,2,d_mplBo)*uklm2Bo+klf(7,3,d_mplBo)*uklm3Bo+klf(7,4,d_mplBo)*uklm4Bo+klf(7,5,d_mplBo)*uklm5Bo+klf(7,6,d_mplBo)*uklm6Bo+klf(7,7,d_mplBo)*uklm7Bo)+(klf(7,0,d_my)*uklm0+klf(7,1,d_my)*uklm1+klf(7,2,d_my)*uklm2+klf(7,3,d_my)*uklm3+klf(7,4,d_my)*uklm4+klf(7,5,d_my)*uklm5+klf(7,6,d_my)*uklm6+klf(7,7,d_my)*uklm7);


    
    qklm1 = qklm1 - ( klf(1,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm2 = qklm2 - ( klf(2,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm3 = qklm3 - ( klf(3,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm4 = qklm4 - ( klf(4,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm5 = qklm5 - ( klf(5,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm6 = qklm6 - ( klf(6,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm7 = qklm7 - ( klf(7,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;    
    
    qklm2 = qklm2 - ( klf(2,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm3 = qklm3 - ( klf(3,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm4 = qklm4 - ( klf(4,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm5 = qklm5 - ( klf(5,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm6 = qklm6 - ( klf(6,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm7 = qklm7 - ( klf(7,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    
    qklm3 = qklm3 - ( klf(3,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    qklm4 = qklm4 - ( klf(4,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    qklm5 = qklm5 - ( klf(5,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    qklm6 = qklm6 - ( klf(6,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    qklm7 = qklm7 - ( klf(7,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    
    qklm4 = qklm4 - ( klf(4,3,d_mltri)/klf(3,3,d_mltri) ) * qklm3;
    qklm5 = qklm5 - ( klf(5,3,d_mltri)/klf(3,3,d_mltri) ) * qklm3;
    qklm6 = qklm6 - ( klf(6,3,d_mltri)/klf(3,3,d_mltri) ) * qklm3;
    qklm7 = qklm7 - ( klf(7,3,d_mltri)/klf(3,3,d_mltri) ) * qklm3;
    
    qklm5 = qklm5 - ( klf(5,4,d_mltri)/klf(4,4,d_mltri) ) * qklm4;
    qklm6 = qklm6 - ( klf(6,4,d_mltri)/klf(4,4,d_mltri) ) * qklm4;
    qklm7 = qklm7 - ( klf(7,4,d_mltri)/klf(4,4,d_mltri) ) * qklm4;    
    
    qklm6 = qklm6 - ( klf(6,5,d_mltri)/klf(5,5,d_mltri) ) * qklm5;
    qklm7 = qklm7 - ( klf(7,5,d_mltri)/klf(5,5,d_mltri) ) * qklm5;
        
    qklm7 = qklm7 - ( klf(7,6,d_mltri)/klf(6,6,d_mltri) ) * qklm6;


    x7 = (qklm7/klf(7,7,DIM,d_ml));
    x6 = (qklm6-klf(6,7,DIM,d_ml)*x7)/klf(6,6,DIM,d_ml);
    x5 = (qklm5-klf(5,6,DIM,d_ml)*x6-klf(5,7,DIM,d_ml)*x7)/klf(5,5,DIM,d_ml);
    x4 = (qklm4-klf(4,5,DIM,d_ml)*x5-klf(4,6,DIM,d_ml)*x6-klf(4,7,DIM,d_ml)*x7)/klf(4,4,DIM,d_ml);
    x3 = (qklm3-klf(3,4,DIM,d_ml)*x4-klf(3,5,DIM,d_ml)*x5-klf(3,6,DIM,d_ml)*x6-klf(3,7,DIM,d_ml)*x7)/klf(3,3,DIM,d_ml);
    x2 = (qklm2-klf(2,3,DIM,d_ml)*x3-klf(2,4,DIM,d_ml)*x4-klf(2,5,DIM,d_ml)*x5-klf(2,6,DIM,d_ml)*x6-klf(2,7,DIM,d_ml)*x7)/klf(2,2,DIM,d_ml);
    x1 = (qklm1-klf(1,2,DIM,d_ml)*x2-klf(1,3,DIM,d_ml)*x3-klf(1,4,DIM,d_ml)*x4-klf(1,5,DIM,d_ml)*x5-klf(1,6,DIM,d_ml)*x6-klf(1,7,DIM,d_ml)*x7)/klf(1,1,DIM,d_ml);
    x0 = (qklm0-klf(0,1,DIM,d_ml)*x1-klf(0,2,DIM,d_ml)*x2-klf(0,3,DIM,d_ml)*x3-klf(0,4,DIM,d_ml)*x4-klf(0,5,DIM,d_ml)*x5-klf(0,6,DIM,d_ml)*x6-klf(0,7,DIM,d_ml)*x7)/klf(0,0,DIM,d_ml);




    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,0,x0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,1,x1);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,2,x2);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,3,x3);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,4,x4);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,5,x5);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,6,x6);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,7,x7);
    

  }
}



__global__ void d_qklm2f(int Ks, int Ls, int Ms, float* d_uklm, float* d_ml, float * d_mltri, 
                         float* d_mllF, float* d_mplF, float* d_mllBa, float* d_mplBa, float* d_mz,
                         float* d_qklm2){

  int k = blockIdx.x * blockDim.x + threadIdx.x;
  int l = blockIdx.y * blockDim.y + threadIdx.y;
  int m = blockIdx.z * blockDim.z + threadIdx.z;


  if(  0 < k  && k < Ks+1  && 0 < l && l < Ls+1 && 0 < m &&  m < Ms+1 ){

    float uklm0,uklm1,uklm2,uklm3,uklm4,uklm5,uklm6,uklm7;
    float uklm0F,uklm1F,uklm2F,uklm3F,uklm4F,uklm5F,uklm6F,uklm7F;
    float uklm0Ba,uklm1Ba,uklm2Ba,uklm3Ba,uklm4Ba,uklm5Ba,uklm6Ba,uklm7Ba;
    float qklm0,qklm1,qklm2,qklm3,qklm4,qklm5,qklm6,qklm7;

    float v00,v01,v02,v03,v04,v05,v06,v07;
    float v10,v11,v12,v13,v14,v15,v16,v17;
    float v20,v21,v22,v23,v24,v25,v26,v27;
    float v30,v31,v32,v33,v34,v35,v36,v37;
    float v40,v41,v42,v43,v44,v45,v46,v47;
    float x0,x1,x2,x3,x4,x5,x6,x7;
    
    
    // uklm0 = d_uklm(k,l,m,0); uklm1 = d_uklm(k,l,m,1); uklm2 = d_uklm(k,l,m,2); uklm3 = d_uklm(k,l,m,3); 
    // uklm4 = d_uklm(k,l,m,4); uklm5 = d_uklm(k,l,m,5); uklm6 = d_uklm(k,l,m,6); uklm7 = d_uklm(k,l,m,7);
    uklm0  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,0); uklm1  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,1); uklm2  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,2); uklm3  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,3);
    uklm4  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,4); uklm5  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,5); uklm6  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,6); uklm7  = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,7);

    
    // uklm0F = d_uklm(k,l,m+1,0); uklm1F = d_uklm(k,l,m+1,1); uklm2F = d_uklm(k,l,m+1,2); uklm3F = d_uklm(k,l,m+1,3); 
    // uklm4F = d_uklm(k,l,m+1,4); uklm5F = d_uklm(k,l,m+1,5); uklm6F = d_uklm(k,l,m+1,6); uklm7F = d_uklm(k,l,m+1,7);
    uklm0F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_uklm,0); uklm1F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_uklm,1); uklm2F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_uklm,2); uklm3F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_uklm,3);
    uklm4F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_uklm,4); uklm5F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_uklm,5); uklm6F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_uklm,6); uklm7F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_uklm,7);
   
    
    // uklm0Ba = d_uklm(k,l,m-1,0); uklm1Ba = d_uklm(k,l,m-1,1); uklm2Ba = d_uklm(k,l,m-1,2); uklm3Ba = d_uklm(k,l,m-1,3); 
    // uklm4Ba = d_uklm(k,l,m-1,4); uklm5Ba = d_uklm(k,l,m-1,5); uklm6Ba = d_uklm(k,l,m-1,6); uklm7Ba = d_uklm(k,l,m-1,7);
    uklm0Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_uklm,0); uklm1Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_uklm,1); uklm2Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_uklm,2); uklm3Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_uklm,3);
    uklm4Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_uklm,4); uklm5Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_uklm,5); uklm6Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_uklm,6); uklm7Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_uklm,7);
   


    // v00 = klf(0,0,d_mllF)*uklm0+klf(0,1,d_mllF)*uklm1+klf(0,2,d_mllF)*uklm2+klf(0,3,d_mllF)*uklm3+klf(0,4,d_mllF)*uklm4+klf(0,5,d_mllF)*uklm5+klf(0,6,d_mllF)*uklm6+klf(0,7,d_mllF)*uklm7;
    // v01 = klf(1,0,d_mllF)*uklm0+klf(1,1,d_mllF)*uklm1+klf(1,2,d_mllF)*uklm2+klf(1,3,d_mllF)*uklm3+klf(1,4,d_mllF)*uklm4+klf(1,5,d_mllF)*uklm5+klf(1,6,d_mllF)*uklm6+klf(1,7,d_mllF)*uklm7;
    // v02 = klf(2,0,d_mllF)*uklm0+klf(2,1,d_mllF)*uklm1+klf(2,2,d_mllF)*uklm2+klf(2,3,d_mllF)*uklm3+klf(2,4,d_mllF)*uklm4+klf(2,5,d_mllF)*uklm5+klf(2,6,d_mllF)*uklm6+klf(2,7,d_mllF)*uklm7;
    // v03 = klf(3,0,d_mllF)*uklm0+klf(3,1,d_mllF)*uklm1+klf(3,2,d_mllF)*uklm2+klf(3,3,d_mllF)*uklm3+klf(3,4,d_mllF)*uklm4+klf(3,5,d_mllF)*uklm5+klf(3,6,d_mllF)*uklm6+klf(3,7,d_mllF)*uklm7;
    // v04 = klf(4,0,d_mllF)*uklm0+klf(4,1,d_mllF)*uklm1+klf(4,2,d_mllF)*uklm2+klf(4,3,d_mllF)*uklm3+klf(4,4,d_mllF)*uklm4+klf(4,5,d_mllF)*uklm5+klf(4,6,d_mllF)*uklm6+klf(4,7,d_mllF)*uklm7;
    // v05 = klf(5,0,d_mllF)*uklm0+klf(5,1,d_mllF)*uklm1+klf(5,2,d_mllF)*uklm2+klf(5,3,d_mllF)*uklm3+klf(5,4,d_mllF)*uklm4+klf(5,5,d_mllF)*uklm5+klf(5,6,d_mllF)*uklm6+klf(5,7,d_mllF)*uklm7;
    // v06 = klf(6,0,d_mllF)*uklm0+klf(6,1,d_mllF)*uklm1+klf(6,2,d_mllF)*uklm2+klf(6,3,d_mllF)*uklm3+klf(6,4,d_mllF)*uklm4+klf(6,5,d_mllF)*uklm5+klf(6,6,d_mllF)*uklm6+klf(6,7,d_mllF)*uklm7;
    // v07 = klf(7,0,d_mllF)*uklm0+klf(7,1,d_mllF)*uklm1+klf(7,2,d_mllF)*uklm2+klf(7,3,d_mllF)*uklm3+klf(7,4,d_mllF)*uklm4+klf(7,5,d_mllF)*uklm5+klf(7,6,d_mllF)*uklm6+klf(7,7,d_mllF)*uklm7;


    // v10 = klf(0,0,d_mplF)*uklm0F+klf(0,1,d_mplF)*uklm1F+klf(0,2,d_mplF)*uklm2F+klf(0,3,d_mplF)*uklm3F+klf(0,4,d_mplF)*uklm4F+klf(0,5,d_mplF)*uklm5F+klf(0,6,d_mplF)*uklm6F+klf(0,7,d_mplF)*uklm7F;
    // v11 = klf(1,0,d_mplF)*uklm0F+klf(1,1,d_mplF)*uklm1F+klf(1,2,d_mplF)*uklm2F+klf(1,3,d_mplF)*uklm3F+klf(1,4,d_mplF)*uklm4F+klf(1,5,d_mplF)*uklm5F+klf(1,6,d_mplF)*uklm6F+klf(1,7,d_mplF)*uklm7F;
    // v12 = klf(2,0,d_mplF)*uklm0F+klf(2,1,d_mplF)*uklm1F+klf(2,2,d_mplF)*uklm2F+klf(2,3,d_mplF)*uklm3F+klf(2,4,d_mplF)*uklm4F+klf(2,5,d_mplF)*uklm5F+klf(2,6,d_mplF)*uklm6F+klf(2,7,d_mplF)*uklm7F;
    // v13 = klf(3,0,d_mplF)*uklm0F+klf(3,1,d_mplF)*uklm1F+klf(3,2,d_mplF)*uklm2F+klf(3,3,d_mplF)*uklm3F+klf(3,4,d_mplF)*uklm4F+klf(3,5,d_mplF)*uklm5F+klf(3,6,d_mplF)*uklm6F+klf(3,7,d_mplF)*uklm7F;
    // v14 = klf(4,0,d_mplF)*uklm0F+klf(4,1,d_mplF)*uklm1F+klf(4,2,d_mplF)*uklm2F+klf(4,3,d_mplF)*uklm3F+klf(4,4,d_mplF)*uklm4F+klf(4,5,d_mplF)*uklm5F+klf(4,6,d_mplF)*uklm6F+klf(4,7,d_mplF)*uklm7F;
    // v15 = klf(5,0,d_mplF)*uklm0F+klf(5,1,d_mplF)*uklm1F+klf(5,2,d_mplF)*uklm2F+klf(5,3,d_mplF)*uklm3F+klf(5,4,d_mplF)*uklm4F+klf(5,5,d_mplF)*uklm5F+klf(5,6,d_mplF)*uklm6F+klf(5,7,d_mplF)*uklm7F;
    // v16 = klf(6,0,d_mplF)*uklm0F+klf(6,1,d_mplF)*uklm1F+klf(6,2,d_mplF)*uklm2F+klf(6,3,d_mplF)*uklm3F+klf(6,4,d_mplF)*uklm4F+klf(6,5,d_mplF)*uklm5F+klf(6,6,d_mplF)*uklm6F+klf(6,7,d_mplF)*uklm7F;
    // v17 = klf(7,0,d_mplF)*uklm0F+klf(7,1,d_mplF)*uklm1F+klf(7,2,d_mplF)*uklm2F+klf(7,3,d_mplF)*uklm3F+klf(7,4,d_mplF)*uklm4F+klf(7,5,d_mplF)*uklm5F+klf(7,6,d_mplF)*uklm6F+klf(7,7,d_mplF)*uklm7F;

    

    // v20 = klf(0,0,d_mllBa)*uklm0+klf(0,1,d_mllBa)*uklm1+klf(0,2,d_mllBa)*uklm2+klf(0,3,d_mllBa)*uklm3+klf(0,4,d_mllBa)*uklm4+klf(0,5,d_mllBa)*uklm5+klf(0,6,d_mllBa)*uklm6+klf(0,7,d_mllBa)*uklm7;
    // v21 = klf(1,0,d_mllBa)*uklm0+klf(1,1,d_mllBa)*uklm1+klf(1,2,d_mllBa)*uklm2+klf(1,3,d_mllBa)*uklm3+klf(1,4,d_mllBa)*uklm4+klf(1,5,d_mllBa)*uklm5+klf(1,6,d_mllBa)*uklm6+klf(1,7,d_mllBa)*uklm7;
    // v22 = klf(2,0,d_mllBa)*uklm0+klf(2,1,d_mllBa)*uklm1+klf(2,2,d_mllBa)*uklm2+klf(2,3,d_mllBa)*uklm3+klf(2,4,d_mllBa)*uklm4+klf(2,5,d_mllBa)*uklm5+klf(2,6,d_mllBa)*uklm6+klf(2,7,d_mllBa)*uklm7;
    // v23 = klf(3,0,d_mllBa)*uklm0+klf(3,1,d_mllBa)*uklm1+klf(3,2,d_mllBa)*uklm2+klf(3,3,d_mllBa)*uklm3+klf(3,4,d_mllBa)*uklm4+klf(3,5,d_mllBa)*uklm5+klf(3,6,d_mllBa)*uklm6+klf(3,7,d_mllBa)*uklm7;
    // v24 = klf(4,0,d_mllBa)*uklm0+klf(4,1,d_mllBa)*uklm1+klf(4,2,d_mllBa)*uklm2+klf(4,3,d_mllBa)*uklm3+klf(4,4,d_mllBa)*uklm4+klf(4,5,d_mllBa)*uklm5+klf(4,6,d_mllBa)*uklm6+klf(4,7,d_mllBa)*uklm7;
    // v25 = klf(5,0,d_mllBa)*uklm0+klf(5,1,d_mllBa)*uklm1+klf(5,2,d_mllBa)*uklm2+klf(5,3,d_mllBa)*uklm3+klf(5,4,d_mllBa)*uklm4+klf(5,5,d_mllBa)*uklm5+klf(5,6,d_mllBa)*uklm6+klf(5,7,d_mllBa)*uklm7;
    // v26 = klf(6,0,d_mllBa)*uklm0+klf(6,1,d_mllBa)*uklm1+klf(6,2,d_mllBa)*uklm2+klf(6,3,d_mllBa)*uklm3+klf(6,4,d_mllBa)*uklm4+klf(6,5,d_mllBa)*uklm5+klf(6,6,d_mllBa)*uklm6+klf(6,7,d_mllBa)*uklm7;
    // v27 = klf(7,0,d_mllBa)*uklm0+klf(7,1,d_mllBa)*uklm1+klf(7,2,d_mllBa)*uklm2+klf(7,3,d_mllBa)*uklm3+klf(7,4,d_mllBa)*uklm4+klf(7,5,d_mllBa)*uklm5+klf(7,6,d_mllBa)*uklm6+klf(7,7,d_mllBa)*uklm7;



    // v30 = klf(0,0,d_mplBa)*uklm0Ba+klf(0,1,d_mplBa)*uklm1Ba+klf(0,2,d_mplBa)*uklm2Ba+klf(0,3,d_mplBa)*uklm3Ba+klf(0,4,d_mplBa)*uklm4Ba+klf(0,5,d_mplBa)*uklm5Ba+klf(0,6,d_mplBa)*uklm6Ba+klf(0,7,d_mplBa)*uklm7Ba;
    // v31 = klf(1,0,d_mplBa)*uklm0Ba+klf(1,1,d_mplBa)*uklm1Ba+klf(1,2,d_mplBa)*uklm2Ba+klf(1,3,d_mplBa)*uklm3Ba+klf(1,4,d_mplBa)*uklm4Ba+klf(1,5,d_mplBa)*uklm5Ba+klf(1,6,d_mplBa)*uklm6Ba+klf(1,7,d_mplBa)*uklm7Ba;
    // v32 = klf(2,0,d_mplBa)*uklm0Ba+klf(2,1,d_mplBa)*uklm1Ba+klf(2,2,d_mplBa)*uklm2Ba+klf(2,3,d_mplBa)*uklm3Ba+klf(2,4,d_mplBa)*uklm4Ba+klf(2,5,d_mplBa)*uklm5Ba+klf(2,6,d_mplBa)*uklm6Ba+klf(2,7,d_mplBa)*uklm7Ba;
    // v33 = klf(3,0,d_mplBa)*uklm0Ba+klf(3,1,d_mplBa)*uklm1Ba+klf(3,2,d_mplBa)*uklm2Ba+klf(3,3,d_mplBa)*uklm3Ba+klf(3,4,d_mplBa)*uklm4Ba+klf(3,5,d_mplBa)*uklm5Ba+klf(3,6,d_mplBa)*uklm6Ba+klf(3,7,d_mplBa)*uklm7Ba;
    // v34 = klf(4,0,d_mplBa)*uklm0Ba+klf(4,1,d_mplBa)*uklm1Ba+klf(4,2,d_mplBa)*uklm2Ba+klf(4,3,d_mplBa)*uklm3Ba+klf(4,4,d_mplBa)*uklm4Ba+klf(4,5,d_mplBa)*uklm5Ba+klf(4,6,d_mplBa)*uklm6Ba+klf(4,7,d_mplBa)*uklm7Ba;
    // v35 = klf(5,0,d_mplBa)*uklm0Ba+klf(5,1,d_mplBa)*uklm1Ba+klf(5,2,d_mplBa)*uklm2Ba+klf(5,3,d_mplBa)*uklm3Ba+klf(5,4,d_mplBa)*uklm4Ba+klf(5,5,d_mplBa)*uklm5Ba+klf(5,6,d_mplBa)*uklm6Ba+klf(5,7,d_mplBa)*uklm7Ba;
    // v36 = klf(6,0,d_mplBa)*uklm0Ba+klf(6,1,d_mplBa)*uklm1Ba+klf(6,2,d_mplBa)*uklm2Ba+klf(6,3,d_mplBa)*uklm3Ba+klf(6,4,d_mplBa)*uklm4Ba+klf(6,5,d_mplBa)*uklm5Ba+klf(6,6,d_mplBa)*uklm6Ba+klf(6,7,d_mplBa)*uklm7Ba;
    // v37 = klf(7,0,d_mplBa)*uklm0Ba+klf(7,1,d_mplBa)*uklm1Ba+klf(7,2,d_mplBa)*uklm2Ba+klf(7,3,d_mplBa)*uklm3Ba+klf(7,4,d_mplBa)*uklm4Ba+klf(7,5,d_mplBa)*uklm5Ba+klf(7,6,d_mplBa)*uklm6Ba+klf(7,7,d_mplBa)*uklm7Ba;



    // v40 = klf(0,0,d_mz)*uklm0+klf(0,1,d_mz)*uklm1+klf(0,2,d_mz)*uklm2+klf(0,3,d_mz)*uklm3+klf(0,4,d_mz)*uklm4+klf(0,5,d_mz)*uklm5+klf(0,6,d_mz)*uklm6+klf(0,7,d_mz)*uklm7;
    // v41 = klf(1,0,d_mz)*uklm0+klf(1,1,d_mz)*uklm1+klf(1,2,d_mz)*uklm2+klf(1,3,d_mz)*uklm3+klf(1,4,d_mz)*uklm4+klf(1,5,d_mz)*uklm5+klf(1,6,d_mz)*uklm6+klf(1,7,d_mz)*uklm7;
    // v42 = klf(2,0,d_mz)*uklm0+klf(2,1,d_mz)*uklm1+klf(2,2,d_mz)*uklm2+klf(2,3,d_mz)*uklm3+klf(2,4,d_mz)*uklm4+klf(2,5,d_mz)*uklm5+klf(2,6,d_mz)*uklm6+klf(2,7,d_mz)*uklm7;
    // v43 = klf(3,0,d_mz)*uklm0+klf(3,1,d_mz)*uklm1+klf(3,2,d_mz)*uklm2+klf(3,3,d_mz)*uklm3+klf(3,4,d_mz)*uklm4+klf(3,5,d_mz)*uklm5+klf(3,6,d_mz)*uklm6+klf(3,7,d_mz)*uklm7;
    // v44 = klf(4,0,d_mz)*uklm0+klf(4,1,d_mz)*uklm1+klf(4,2,d_mz)*uklm2+klf(4,3,d_mz)*uklm3+klf(4,4,d_mz)*uklm4+klf(4,5,d_mz)*uklm5+klf(4,6,d_mz)*uklm6+klf(4,7,d_mz)*uklm7;
    // v45 = klf(5,0,d_mz)*uklm0+klf(5,1,d_mz)*uklm1+klf(5,2,d_mz)*uklm2+klf(5,3,d_mz)*uklm3+klf(5,4,d_mz)*uklm4+klf(5,5,d_mz)*uklm5+klf(5,6,d_mz)*uklm6+klf(5,7,d_mz)*uklm7;
    // v46 = klf(6,0,d_mz)*uklm0+klf(6,1,d_mz)*uklm1+klf(6,2,d_mz)*uklm2+klf(6,3,d_mz)*uklm3+klf(6,4,d_mz)*uklm4+klf(6,5,d_mz)*uklm5+klf(6,6,d_mz)*uklm6+klf(6,7,d_mz)*uklm7;
    // v47 = klf(7,0,d_mz)*uklm0+klf(7,1,d_mz)*uklm1+klf(7,2,d_mz)*uklm2+klf(7,3,d_mz)*uklm3+klf(7,4,d_mz)*uklm4+klf(7,5,d_mz)*uklm5+klf(7,6,d_mz)*uklm6+klf(7,7,d_mz)*uklm7;

    
    // qklm0 = -0.5*v00 -0.5*v10 + 0.5*v20 + 0.5*v30 + v40;
    // qklm1 = -0.5*v01 -0.5*v11 + 0.5*v21 + 0.5*v31 + v41;
    // qklm2 = -0.5*v02 -0.5*v12 + 0.5*v22 + 0.5*v32 + v42;
    // qklm3 = -0.5*v03 -0.5*v13 + 0.5*v23 + 0.5*v33 + v43;
    // qklm4 = -0.5*v04 -0.5*v14 + 0.5*v24 + 0.5*v34 + v44;
    // qklm5 = -0.5*v05 -0.5*v15 + 0.5*v25 + 0.5*v35 + v45;
    // qklm6 = -0.5*v06 -0.5*v16 + 0.5*v26 + 0.5*v36 + v46;
    // qklm7 = -0.5*v07 -0.5*v17 + 0.5*v27 + 0.5*v37 + v47;
    

    qklm0 = -0.5*(klf(0,0,d_mllF)*uklm0+klf(0,1,d_mllF)*uklm1+klf(0,2,d_mllF)*uklm2+klf(0,3,d_mllF)*uklm3+klf(0,4,d_mllF)*uklm4+klf(0,5,d_mllF)*uklm5+klf(0,6,d_mllF)*uklm6+klf(0,7,d_mllF)*uklm7)-0.5*(klf(0,0,d_mplF)*uklm0F+klf(0,1,d_mplF)*uklm1F+klf(0,2,d_mplF)*uklm2F+klf(0,3,d_mplF)*uklm3F+klf(0,4,d_mplF)*uklm4F+klf(0,5,d_mplF)*uklm5F+klf(0,6,d_mplF)*uklm6F+klf(0,7,d_mplF)*uklm7F)+0.5*(klf(0,0,d_mllBa)*uklm0+klf(0,1,d_mllBa)*uklm1+klf(0,2,d_mllBa)*uklm2+klf(0,3,d_mllBa)*uklm3+klf(0,4,d_mllBa)*uklm4+klf(0,5,d_mllBa)*uklm5+klf(0,6,d_mllBa)*uklm6+klf(0,7,d_mllBa)*uklm7)+0.5*(klf(0,0,d_mplBa)*uklm0Ba+klf(0,1,d_mplBa)*uklm1Ba+klf(0,2,d_mplBa)*uklm2Ba+klf(0,3,d_mplBa)*uklm3Ba+klf(0,4,d_mplBa)*uklm4Ba+klf(0,5,d_mplBa)*uklm5Ba+klf(0,6,d_mplBa)*uklm6Ba+klf(0,7,d_mplBa)*uklm7Ba)+(klf(0,0,d_mz)*uklm0+klf(0,1,d_mz)*uklm1+klf(0,2,d_mz)*uklm2+klf(0,3,d_mz)*uklm3+klf(0,4,d_mz)*uklm4+klf(0,5,d_mz)*uklm5+klf(0,6,d_mz)*uklm6+klf(0,7,d_mz)*uklm7);
    
    qklm1 = -0.5*(klf(1,0,d_mllF)*uklm0+klf(1,1,d_mllF)*uklm1+klf(1,2,d_mllF)*uklm2+klf(1,3,d_mllF)*uklm3+klf(1,4,d_mllF)*uklm4+klf(1,5,d_mllF)*uklm5+klf(1,6,d_mllF)*uklm6+klf(1,7,d_mllF)*uklm7)-0.5*(klf(1,0,d_mplF)*uklm0F+klf(1,1,d_mplF)*uklm1F+klf(1,2,d_mplF)*uklm2F+klf(1,3,d_mplF)*uklm3F+klf(1,4,d_mplF)*uklm4F+klf(1,5,d_mplF)*uklm5F+klf(1,6,d_mplF)*uklm6F+klf(1,7,d_mplF)*uklm7F)+0.5*(klf(1,0,d_mllBa)*uklm0+klf(1,1,d_mllBa)*uklm1+klf(1,2,d_mllBa)*uklm2+klf(1,3,d_mllBa)*uklm3+klf(1,4,d_mllBa)*uklm4+klf(1,5,d_mllBa)*uklm5+klf(1,6,d_mllBa)*uklm6+klf(1,7,d_mllBa)*uklm7)+0.5*(klf(1,0,d_mplBa)*uklm0Ba+klf(1,1,d_mplBa)*uklm1Ba+klf(1,2,d_mplBa)*uklm2Ba+klf(1,3,d_mplBa)*uklm3Ba+klf(1,4,d_mplBa)*uklm4Ba+klf(1,5,d_mplBa)*uklm5Ba+klf(1,6,d_mplBa)*uklm6Ba+klf(1,7,d_mplBa)*uklm7Ba)+(klf(1,0,d_mz)*uklm0+klf(1,1,d_mz)*uklm1+klf(1,2,d_mz)*uklm2+klf(1,3,d_mz)*uklm3+klf(1,4,d_mz)*uklm4+klf(1,5,d_mz)*uklm5+klf(1,6,d_mz)*uklm6+klf(1,7,d_mz)*uklm7);

    qklm2 = -0.5*(klf(2,0,d_mllF)*uklm0+klf(2,1,d_mllF)*uklm1+klf(2,2,d_mllF)*uklm2+klf(2,3,d_mllF)*uklm3+klf(2,4,d_mllF)*uklm4+klf(2,5,d_mllF)*uklm5+klf(2,6,d_mllF)*uklm6+klf(2,7,d_mllF)*uklm7)-0.5*(klf(2,0,d_mplF)*uklm0F+klf(2,1,d_mplF)*uklm1F+klf(2,2,d_mplF)*uklm2F+klf(2,3,d_mplF)*uklm3F+klf(2,4,d_mplF)*uklm4F+klf(2,5,d_mplF)*uklm5F+klf(2,6,d_mplF)*uklm6F+klf(2,7,d_mplF)*uklm7F)+0.5*(klf(2,0,d_mllBa)*uklm0+klf(2,1,d_mllBa)*uklm1+klf(2,2,d_mllBa)*uklm2+klf(2,3,d_mllBa)*uklm3+klf(2,4,d_mllBa)*uklm4+klf(2,5,d_mllBa)*uklm5+klf(2,6,d_mllBa)*uklm6+klf(2,7,d_mllBa)*uklm7)+0.5*(klf(2,0,d_mplBa)*uklm0Ba+klf(2,1,d_mplBa)*uklm1Ba+klf(2,2,d_mplBa)*uklm2Ba+klf(2,3,d_mplBa)*uklm3Ba+klf(2,4,d_mplBa)*uklm4Ba+klf(2,5,d_mplBa)*uklm5Ba+klf(2,6,d_mplBa)*uklm6Ba+klf(2,7,d_mplBa)*uklm7Ba)+(klf(2,0,d_mz)*uklm0+klf(2,1,d_mz)*uklm1+klf(2,2,d_mz)*uklm2+klf(2,3,d_mz)*uklm3+klf(2,4,d_mz)*uklm4+klf(2,5,d_mz)*uklm5+klf(2,6,d_mz)*uklm6+klf(2,7,d_mz)*uklm7);

    qklm3 = -0.5*(klf(3,0,d_mllF)*uklm0+klf(3,1,d_mllF)*uklm1+klf(3,2,d_mllF)*uklm2+klf(3,3,d_mllF)*uklm3+klf(3,4,d_mllF)*uklm4+klf(3,5,d_mllF)*uklm5+klf(3,6,d_mllF)*uklm6+klf(3,7,d_mllF)*uklm7)-0.5*(klf(3,0,d_mplF)*uklm0F+klf(3,1,d_mplF)*uklm1F+klf(3,2,d_mplF)*uklm2F+klf(3,3,d_mplF)*uklm3F+klf(3,4,d_mplF)*uklm4F+klf(3,5,d_mplF)*uklm5F+klf(3,6,d_mplF)*uklm6F+klf(3,7,d_mplF)*uklm7F)+0.5*(klf(3,0,d_mllBa)*uklm0+klf(3,1,d_mllBa)*uklm1+klf(3,2,d_mllBa)*uklm2+klf(3,3,d_mllBa)*uklm3+klf(3,4,d_mllBa)*uklm4+klf(3,5,d_mllBa)*uklm5+klf(3,6,d_mllBa)*uklm6+klf(3,7,d_mllBa)*uklm7)+0.5*(klf(3,0,d_mplBa)*uklm0Ba+klf(3,1,d_mplBa)*uklm1Ba+klf(3,2,d_mplBa)*uklm2Ba+klf(3,3,d_mplBa)*uklm3Ba+klf(3,4,d_mplBa)*uklm4Ba+klf(3,5,d_mplBa)*uklm5Ba+klf(3,6,d_mplBa)*uklm6Ba+klf(3,7,d_mplBa)*uklm7Ba)+(klf(3,0,d_mz)*uklm0+klf(3,1,d_mz)*uklm1+klf(3,2,d_mz)*uklm2+klf(3,3,d_mz)*uklm3+klf(3,4,d_mz)*uklm4+klf(3,5,d_mz)*uklm5+klf(3,6,d_mz)*uklm6+klf(3,7,d_mz)*uklm7);

    qklm4 = -0.5*( klf(4,0,d_mllF)*uklm0+klf(4,1,d_mllF)*uklm1+klf(4,2,d_mllF)*uklm2+klf(4,3,d_mllF)*uklm3+klf(4,4,d_mllF)*uklm4+klf(4,5,d_mllF)*uklm5+klf(4,6,d_mllF)*uklm6+klf(4,7,d_mllF)*uklm7)-0.5*(klf(4,0,d_mplF)*uklm0F+klf(4,1,d_mplF)*uklm1F+klf(4,2,d_mplF)*uklm2F+klf(4,3,d_mplF)*uklm3F+klf(4,4,d_mplF)*uklm4F+klf(4,5,d_mplF)*uklm5F+klf(4,6,d_mplF)*uklm6F+klf(4,7,d_mplF)*uklm7F)+0.5*(klf(4,0,d_mllBa)*uklm0+klf(4,1,d_mllBa)*uklm1+klf(4,2,d_mllBa)*uklm2+klf(4,3,d_mllBa)*uklm3+klf(4,4,d_mllBa)*uklm4+klf(4,5,d_mllBa)*uklm5+klf(4,6,d_mllBa)*uklm6+klf(4,7,d_mllBa)*uklm7)+0.5*(klf(4,0,d_mplBa)*uklm0Ba+klf(4,1,d_mplBa)*uklm1Ba+klf(4,2,d_mplBa)*uklm2Ba+klf(4,3,d_mplBa)*uklm3Ba+klf(4,4,d_mplBa)*uklm4Ba+klf(4,5,d_mplBa)*uklm5Ba+klf(4,6,d_mplBa)*uklm6Ba+klf(4,7,d_mplBa)*uklm7Ba)+(klf(4,0,d_mz)*uklm0+klf(4,1,d_mz)*uklm1+klf(4,2,d_mz)*uklm2+klf(4,3,d_mz)*uklm3+klf(4,4,d_mz)*uklm4+klf(4,5,d_mz)*uklm5+klf(4,6,d_mz)*uklm6+klf(4,7,d_mz)*uklm7);

    qklm5 = -0.5*(klf(5,0,d_mllF)*uklm0+klf(5,1,d_mllF)*uklm1+klf(5,2,d_mllF)*uklm2+klf(5,3,d_mllF)*uklm3+klf(5,4,d_mllF)*uklm4+klf(5,5,d_mllF)*uklm5+klf(5,6,d_mllF)*uklm6+klf(5,7,d_mllF)*uklm7)-0.5*(klf(5,0,d_mplF)*uklm0F+klf(5,1,d_mplF)*uklm1F+klf(5,2,d_mplF)*uklm2F+klf(5,3,d_mplF)*uklm3F+klf(5,4,d_mplF)*uklm4F+klf(5,5,d_mplF)*uklm5F+klf(5,6,d_mplF)*uklm6F+klf(5,7,d_mplF)*uklm7F)+0.5*(klf(5,0,d_mllBa)*uklm0+klf(5,1,d_mllBa)*uklm1+klf(5,2,d_mllBa)*uklm2+klf(5,3,d_mllBa)*uklm3+klf(5,4,d_mllBa)*uklm4+klf(5,5,d_mllBa)*uklm5+klf(5,6,d_mllBa)*uklm6+klf(5,7,d_mllBa)*uklm7)+0.5*(klf(5,0,d_mplBa)*uklm0Ba+klf(5,1,d_mplBa)*uklm1Ba+klf(5,2,d_mplBa)*uklm2Ba+klf(5,3,d_mplBa)*uklm3Ba+klf(5,4,d_mplBa)*uklm4Ba+klf(5,5,d_mplBa)*uklm5Ba+klf(5,6,d_mplBa)*uklm6Ba+klf(5,7,d_mplBa)*uklm7Ba)+(klf(5,0,d_mz)*uklm0+klf(5,1,d_mz)*uklm1+klf(5,2,d_mz)*uklm2+klf(5,3,d_mz)*uklm3+klf(5,4,d_mz)*uklm4+klf(5,5,d_mz)*uklm5+klf(5,6,d_mz)*uklm6+klf(5,7,d_mz)*uklm7);

    qklm6 = -0.5*(klf(6,0,d_mllF)*uklm0+klf(6,1,d_mllF)*uklm1+klf(6,2,d_mllF)*uklm2+klf(6,3,d_mllF)*uklm3+klf(6,4,d_mllF)*uklm4+klf(6,5,d_mllF)*uklm5+klf(6,6,d_mllF)*uklm6+klf(6,7,d_mllF)*uklm7)-0.5*(klf(6,0,d_mplF)*uklm0F+klf(6,1,d_mplF)*uklm1F+klf(6,2,d_mplF)*uklm2F+klf(6,3,d_mplF)*uklm3F+klf(6,4,d_mplF)*uklm4F+klf(6,5,d_mplF)*uklm5F+klf(6,6,d_mplF)*uklm6F+klf(6,7,d_mplF)*uklm7F)+0.5*(klf(6,0,d_mllBa)*uklm0+klf(6,1,d_mllBa)*uklm1+klf(6,2,d_mllBa)*uklm2+klf(6,3,d_mllBa)*uklm3+klf(6,4,d_mllBa)*uklm4+klf(6,5,d_mllBa)*uklm5+klf(6,6,d_mllBa)*uklm6+klf(6,7,d_mllBa)*uklm7)+0.5*(klf(6,0,d_mplBa)*uklm0Ba+klf(6,1,d_mplBa)*uklm1Ba+klf(6,2,d_mplBa)*uklm2Ba+klf(6,3,d_mplBa)*uklm3Ba+klf(6,4,d_mplBa)*uklm4Ba+klf(6,5,d_mplBa)*uklm5Ba+klf(6,6,d_mplBa)*uklm6Ba+klf(6,7,d_mplBa)*uklm7Ba)+(klf(6,0,d_mz)*uklm0+klf(6,1,d_mz)*uklm1+klf(6,2,d_mz)*uklm2+klf(6,3,d_mz)*uklm3+klf(6,4,d_mz)*uklm4+klf(6,5,d_mz)*uklm5+klf(6,6,d_mz)*uklm6+klf(6,7,d_mz)*uklm7);

    qklm7 = -0.5*(klf(7,0,d_mllF)*uklm0+klf(7,1,d_mllF)*uklm1+klf(7,2,d_mllF)*uklm2+klf(7,3,d_mllF)*uklm3+klf(7,4,d_mllF)*uklm4+klf(7,5,d_mllF)*uklm5+klf(7,6,d_mllF)*uklm6+klf(7,7,d_mllF)*uklm7)-0.5*(klf(7,0,d_mplF)*uklm0F+klf(7,1,d_mplF)*uklm1F+klf(7,2,d_mplF)*uklm2F+klf(7,3,d_mplF)*uklm3F+klf(7,4,d_mplF)*uklm4F+klf(7,5,d_mplF)*uklm5F+klf(7,6,d_mplF)*uklm6F+klf(7,7,d_mplF)*uklm7F)+0.5*(klf(7,0,d_mllBa)*uklm0+klf(7,1,d_mllBa)*uklm1+klf(7,2,d_mllBa)*uklm2+klf(7,3,d_mllBa)*uklm3+klf(7,4,d_mllBa)*uklm4+klf(7,5,d_mllBa)*uklm5+klf(7,6,d_mllBa)*uklm6+klf(7,7,d_mllBa)*uklm7)+0.5*(klf(7,0,d_mplBa)*uklm0Ba+klf(7,1,d_mplBa)*uklm1Ba+klf(7,2,d_mplBa)*uklm2Ba+klf(7,3,d_mplBa)*uklm3Ba+klf(7,4,d_mplBa)*uklm4Ba+klf(7,5,d_mplBa)*uklm5Ba+klf(7,6,d_mplBa)*uklm6Ba+klf(7,7,d_mplBa)*uklm7Ba)+(klf(7,0,d_mz)*uklm0+klf(7,1,d_mz)*uklm1+klf(7,2,d_mz)*uklm2+klf(7,3,d_mz)*uklm3+klf(7,4,d_mz)*uklm4+klf(7,5,d_mz)*uklm5+klf(7,6,d_mz)*uklm6+klf(7,7,d_mz)*uklm7);

   
    
    
    // mm[0]   = d_ml[0];    mm[1]  = d_ml[1];    mm[2]  = d_ml[2];    mm[3]  = d_ml[3];    mm[4]  = d_ml[4];    mm[5]  = d_ml[5];    mm[6]  = d_ml[6];     mm[7]  = d_ml[7];    mm[8]  = qklm0;
    // mm[9]   = d_ml[8];    mm[10] = d_ml[9];    mm[11] = d_ml[10];   mm[12] = d_ml[11];   mm[13] = d_ml[12];   mm[14] = d_ml[13];   mm[15] = d_ml[14];    mm[16] = d_ml[15];   mm[17] = qklm1;
    // mm[18]  = d_ml[16];  mm[19]  = d_ml[17];  mm[20]  = d_ml[18];  mm[21]  = d_ml[19];  mm[22]  = d_ml[20];  mm[23]  = d_ml[21];  mm[24]  = d_ml[22];   mm[25]  = d_ml[23];   mm[26] = qklm2;
    // mm[27]  = d_ml[24];  mm[28]  = d_ml[25];  mm[29]  = d_ml[26];  mm[30]  = d_ml[27];  mm[31]  = d_ml[28];  mm[32]  = d_ml[29];  mm[33]  = d_ml[30];   mm[34]  = d_ml[31];   mm[35]  = qklm3;
    // mm[36]  = d_ml[32];  mm[37]  = d_ml[33];  mm[38]  = d_ml[34];  mm[39]  = d_ml[35];  mm[40]  = d_ml[36];  mm[41]  = d_ml[37];  mm[42]  = d_ml[38];   mm[43]  = d_ml[39];   mm[44]  = qklm4;
    // mm[45]  = d_ml[40];  mm[46]  = d_ml[41];  mm[47]  = d_ml[42];  mm[48]  = d_ml[43];  mm[49]  = d_ml[44];  mm[50]  = d_ml[45];  mm[51]  = d_ml[46];   mm[52]  = d_ml[47];   mm[53]  = qklm5;
    // mm[54]  = d_ml[48];  mm[55]  = d_ml[49];  mm[56]  = d_ml[50];  mm[57]  = d_ml[51];  mm[58]  = d_ml[52];  mm[59]  = d_ml[53];  mm[60]  = d_ml[54];   mm[61]  = d_ml[55];   mm[62]  = qklm6;
    // mm[63]  = d_ml[56];  mm[64]  = d_ml[57];  mm[65]  = d_ml[58];  mm[66]  = d_ml[59];  mm[67]  = d_ml[60];  mm[68]  = d_ml[61];  mm[69]  = d_ml[62];   mm[70]  = d_ml[63];   mm[71]  = qklm7;


    
    
    // for(int j=0; j < 8; ++j){
    //   for(int i=j; i < 8; ++i){
    //     if(i!=j){
    //       c = mm[i*(C+1)+j]/mm[j*(C+1)+j];
    //       for(int kk=0; kk < C+1; ++kk)
    //         mm[i*(C+1)+kk] = mm[i*(C+1)+kk] - c*mm[j*(C+1)+kk];
    //     }
    //   }
    // }

    // x7 = (mm[71]/mm[70]);
    // x6 = (mm[62]-mm[61]*(x7))/mm[60];
    // x5 = (mm[53]-mm[51]*(x6)-mm[52]*(x7))/mm[50];
    // x4 = (mm[44]-mm[41]*(x5)-mm[42]*(x6)-mm[43]*(x7))/mm[40];
    // x3 = (mm[35]-mm[31]*(x4)-mm[32]*(x5)-mm[33]*(x6)-mm[34]*(x7))/mm[30];
    // x2 = (mm[26]-mm[21]*(x3)-mm[22]*(x4)-mm[23]*(x5)-mm[24]*(x6)-mm[25]*(x7))/mm[20];
    // x1 = (mm[17]-mm[11]*(x2)-mm[12]*(x3)-mm[13]*(x4)-mm[14]*(x5)-mm[15]*(x6)-mm[16]*(x7))/mm[10];
    // x0 = (mm[8]-mm[1]*(x1)-mm[2]*(x2)-mm[3]*(x3)-mm[4]*(x4)-mm[5]*(x5)-mm[6]*(x6)-mm[7]*(x7))/mm[0];


    qklm1 = qklm1 - ( klf(1,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm2 = qklm2 - ( klf(2,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm3 = qklm3 - ( klf(3,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm4 = qklm4 - ( klf(4,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm5 = qklm5 - ( klf(5,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm6 = qklm6 - ( klf(6,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;
    qklm7 = qklm7 - ( klf(7,0,d_mltri)/klf(0,0,d_mltri) ) * qklm0;    
    
    qklm2 = qklm2 - ( klf(2,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm3 = qklm3 - ( klf(3,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm4 = qklm4 - ( klf(4,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm5 = qklm5 - ( klf(5,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm6 = qklm6 - ( klf(6,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    qklm7 = qklm7 - ( klf(7,1,d_mltri)/klf(1,1,d_mltri) ) * qklm1;
    
    qklm3 = qklm3 - ( klf(3,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    qklm4 = qklm4 - ( klf(4,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    qklm5 = qklm5 - ( klf(5,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    qklm6 = qklm6 - ( klf(6,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    qklm7 = qklm7 - ( klf(7,2,d_mltri)/klf(2,2,d_mltri) ) * qklm2;
    
    qklm4 = qklm4 - ( klf(4,3,d_mltri)/klf(3,3,d_mltri) ) * qklm3;
    qklm5 = qklm5 - ( klf(5,3,d_mltri)/klf(3,3,d_mltri) ) * qklm3;
    qklm6 = qklm6 - ( klf(6,3,d_mltri)/klf(3,3,d_mltri) ) * qklm3;
    qklm7 = qklm7 - ( klf(7,3,d_mltri)/klf(3,3,d_mltri) ) * qklm3;
    
    qklm5 = qklm5 - ( klf(5,4,d_mltri)/klf(4,4,d_mltri) ) * qklm4;
    qklm6 = qklm6 - ( klf(6,4,d_mltri)/klf(4,4,d_mltri) ) * qklm4;
    qklm7 = qklm7 - ( klf(7,4,d_mltri)/klf(4,4,d_mltri) ) * qklm4;    
    
    qklm6 = qklm6 - ( klf(6,5,d_mltri)/klf(5,5,d_mltri) ) * qklm5;
    qklm7 = qklm7 - ( klf(7,5,d_mltri)/klf(5,5,d_mltri) ) * qklm5;
        
    qklm7 = qklm7 - ( klf(7,6,d_mltri)/klf(6,6,d_mltri) ) * qklm6;


    x7 = (qklm7/klf(7,7,DIM,d_ml));
    x6 = (qklm6-klf(6,7,DIM,d_ml)*x7)/klf(6,6,DIM,d_ml);
    x5 = (qklm5-klf(5,6,DIM,d_ml)*x6-klf(5,7,DIM,d_ml)*x7)/klf(5,5,DIM,d_ml);
    x4 = (qklm4-klf(4,5,DIM,d_ml)*x5-klf(4,6,DIM,d_ml)*x6-klf(4,7,DIM,d_ml)*x7)/klf(4,4,DIM,d_ml);
    x3 = (qklm3-klf(3,4,DIM,d_ml)*x4-klf(3,5,DIM,d_ml)*x5-klf(3,6,DIM,d_ml)*x6-klf(3,7,DIM,d_ml)*x7)/klf(3,3,DIM,d_ml);
    x2 = (qklm2-klf(2,3,DIM,d_ml)*x3-klf(2,4,DIM,d_ml)*x4-klf(2,5,DIM,d_ml)*x5-klf(2,6,DIM,d_ml)*x6-klf(2,7,DIM,d_ml)*x7)/klf(2,2,DIM,d_ml);
    x1 = (qklm1-klf(1,2,DIM,d_ml)*x2-klf(1,3,DIM,d_ml)*x3-klf(1,4,DIM,d_ml)*x4-klf(1,5,DIM,d_ml)*x5-klf(1,6,DIM,d_ml)*x6-klf(1,7,DIM,d_ml)*x7)/klf(1,1,DIM,d_ml);
    x0 = (qklm0-klf(0,1,DIM,d_ml)*x1-klf(0,2,DIM,d_ml)*x2-klf(0,3,DIM,d_ml)*x3-klf(0,4,DIM,d_ml)*x4-klf(0,5,DIM,d_ml)*x5-klf(0,6,DIM,d_ml)*x6-klf(0,7,DIM,d_ml)*x7)/klf(0,0,DIM,d_ml);


    
    
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,0,x0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,1,x1);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,2,x2);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,3,x3);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,4,x4);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,5,x5);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,6,x6);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,7,x7);
    
  }
}


__global__ void d_rhsf(int K, int L, int M, int Ks, int Ls, int Ms, int kd, int ld, int md, 
                       float* d_kM, float* d_ml, float * d_mltri,  
                       float* d_mllR, float* d_mplR, float* d_mllL,   float* d_mplL, float* d_mx,
                       float* d_mllT, float* d_mplT, float* d_mllBo,  float* d_mplBo, float* d_my,
                       float* d_mllF, float* d_mplF, float* d_mllBa,   float* d_mplBa, float* d_mz,
                       float* d_qklm0, float* d_qklm1, float* d_qklm2, float * d_rhs){
  
  int k = blockIdx.x * blockDim.x + threadIdx.x;
  int l = blockIdx.y * blockDim.y + threadIdx.y;
  int m = blockIdx.z * blockDim.z + threadIdx.z;


  if(  0 < k  && k < Ks+1  && 0 < l && l < Ls+1 && 0 < m &&  m < Ms+1 ){

    float qklm0,qklm1,qklm2,qklm3,qklm4,qklm5,qklm6,qklm7;
    float qklm0R,qklm1R,qklm2R,qklm3R,qklm4R,qklm5R,qklm6R,qklm7R;
    float qklm0L,qklm1L,qklm2L,qklm3L,qklm4L,qklm5L,qklm6L,qklm7L;
    float qklm0T,qklm1T,qklm2T,qklm3T,qklm4T,qklm5T,qklm6T,qklm7T;
    float qklm0Bo,qklm1Bo,qklm2Bo,qklm3Bo,qklm4Bo,qklm5Bo,qklm6Bo,qklm7Bo;
    float qklm0F,qklm1F,qklm2F,qklm3F,qklm4F,qklm5F,qklm6F,qklm7F;
    float qklm0Ba,qklm1Ba,qklm2Ba,qklm3Ba,qklm4Ba,qklm5Ba,qklm6Ba,qklm7Ba;
    float rhs0,rhs1,rhs2,rhs3,rhs4,rhs5,rhs6,rhs7;

    float v00,v01,v02,v03,v04,v05,v06,v07;
    float v10,v11,v12,v13,v14,v15,v16,v17;
    float v20,v21,v22,v23,v24,v25,v26,v27;
    float v30,v31,v32,v33,v34,v35,v36,v37;
    float v40,v41,v42,v43,v44,v45,v46,v47;
    
    
    float q0v0,q0v1,q0v2,q0v3,q0v4,q0v5,q0v6,q0v7;
    float q1v0,q1v1,q1v2,q1v3,q1v4,q1v5,q1v6,q1v7;
    float q2v0,q2v1,q2v2,q2v3,q2v4,q2v5,q2v6,q2v7;
   

    float x0,x1,x2,x3,x4,x5,x6,x7;

    int ks = k + kd - Ks/2;
    int ls = l + ld - Ls/2;
    int ms = m + md - Ms/2;

    
    
    float kInkR  = 1.0;
    float kInkL  = 1.0;

    if( klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)+klmf(ks,ls+1,ms,K+2,M+2,L+2,d_kM) > 0 )
      kInkR = 2*klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)*klmf(ks,ls+1,ms,K+2,M+2,L+2,d_kM)/(klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)+klmf(ks,ls+1,ms,K+2,M+2,L+2,d_kM));
    else
      kInkR = 0;        
    
    if( klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)+klmf(ks,ls-1,ms,K+2,M+2,L+2,d_kM) > 0 )
      kInkL = 2*klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)*klmf(ks,ls-1,ms,K+2,M+2,L+2,d_kM)/(klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)+klmf(ks,ls-1,ms,K+2,M+2,L+2,d_kM));
    else
      kInkL = 0;


    qklm0  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,0); qklm1  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,1); qklm2  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,2); qklm3  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,3);
    qklm4  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,4); qklm5  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,5); qklm6  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,6); qklm7  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,7);
    
    qklm0R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_qklm0,0); qklm1R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_qklm0,1); qklm2R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_qklm0,2); qklm3R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_qklm0,3);
    qklm4R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_qklm0,4); qklm5R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_qklm0,5); qklm6R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_qklm0,6); qklm7R  = klmdf(k,l+1,m,Ks,Ms,Ls,d_qklm0,7);

    qklm0L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_qklm0,0); qklm1L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_qklm0,1); qklm2L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_qklm0,2); qklm3L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_qklm0,3);
    qklm4L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_qklm0,4); qklm5L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_qklm0,5); qklm6L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_qklm0,6); qklm7L  = klmdf(k,l-1,m,Ks,Ms,Ls,d_qklm0,7);



    // v00 = klf(0,0,d_mllR)*qklm0+klf(0,1,d_mllR)*qklm1+klf(0,2,d_mllR)*qklm2+klf(0,3,d_mllR)*qklm3+klf(0,4,d_mllR)*qklm4+klf(0,5,d_mllR)*qklm5+klf(0,6,d_mllR)*qklm6+klf(0,7,d_mllR)*qklm7;
    // v01 = klf(1,0,d_mllR)*qklm0+klf(1,1,d_mllR)*qklm1+klf(1,2,d_mllR)*qklm2+klf(1,3,d_mllR)*qklm3+klf(1,4,d_mllR)*qklm4+klf(1,5,d_mllR)*qklm5+klf(1,6,d_mllR)*qklm6+klf(1,7,d_mllR)*qklm7;
    // v02 = klf(2,0,d_mllR)*qklm0+klf(2,1,d_mllR)*qklm1+klf(2,2,d_mllR)*qklm2+klf(2,3,d_mllR)*qklm3+klf(2,4,d_mllR)*qklm4+klf(2,5,d_mllR)*qklm5+klf(2,6,d_mllR)*qklm6+klf(2,7,d_mllR)*qklm7;
    // v03 = klf(3,0,d_mllR)*qklm0+klf(3,1,d_mllR)*qklm1+klf(3,2,d_mllR)*qklm2+klf(3,3,d_mllR)*qklm3+klf(3,4,d_mllR)*qklm4+klf(3,5,d_mllR)*qklm5+klf(3,6,d_mllR)*qklm6+klf(3,7,d_mllR)*qklm7;
    // v04 = klf(4,0,d_mllR)*qklm0+klf(4,1,d_mllR)*qklm1+klf(4,2,d_mllR)*qklm2+klf(4,3,d_mllR)*qklm3+klf(4,4,d_mllR)*qklm4+klf(4,5,d_mllR)*qklm5+klf(4,6,d_mllR)*qklm6+klf(4,7,d_mllR)*qklm7;
    // v05 = klf(5,0,d_mllR)*qklm0+klf(5,1,d_mllR)*qklm1+klf(5,2,d_mllR)*qklm2+klf(5,3,d_mllR)*qklm3+klf(5,4,d_mllR)*qklm4+klf(5,5,d_mllR)*qklm5+klf(5,6,d_mllR)*qklm6+klf(5,7,d_mllR)*qklm7;
    // v06 = klf(6,0,d_mllR)*qklm0+klf(6,1,d_mllR)*qklm1+klf(6,2,d_mllR)*qklm2+klf(6,3,d_mllR)*qklm3+klf(6,4,d_mllR)*qklm4+klf(6,5,d_mllR)*qklm5+klf(6,6,d_mllR)*qklm6+klf(6,7,d_mllR)*qklm7;
    // v07 = klf(7,0,d_mllR)*qklm0+klf(7,1,d_mllR)*qklm1+klf(7,2,d_mllR)*qklm2+klf(7,3,d_mllR)*qklm3+klf(7,4,d_mllR)*qklm4+klf(7,5,d_mllR)*qklm5+klf(7,6,d_mllR)*qklm6+klf(7,7,d_mllR)*qklm7;

    // v10 = klf(0,0,d_mplR)*qklm0R+klf(0,1,d_mplR)*qklm1R+klf(0,2,d_mplR)*qklm2R+klf(0,3,d_mplR)*qklm3R+klf(0,4,d_mplR)*qklm4R+klf(0,5,d_mplR)*qklm5R+klf(0,6,d_mplR)*qklm6R+klf(0,7,d_mplR)*qklm7R;
    // v11 = klf(1,0,d_mplR)*qklm0R+klf(1,1,d_mplR)*qklm1R+klf(1,2,d_mplR)*qklm2R+klf(1,3,d_mplR)*qklm3R+klf(1,4,d_mplR)*qklm4R+klf(1,5,d_mplR)*qklm5R+klf(1,6,d_mplR)*qklm6R+klf(1,7,d_mplR)*qklm7R;
    // v12 = klf(2,0,d_mplR)*qklm0R+klf(2,1,d_mplR)*qklm1R+klf(2,2,d_mplR)*qklm2R+klf(2,3,d_mplR)*qklm3R+klf(2,4,d_mplR)*qklm4R+klf(2,5,d_mplR)*qklm5R+klf(2,6,d_mplR)*qklm6R+klf(2,7,d_mplR)*qklm7R;
    // v13 = klf(3,0,d_mplR)*qklm0R+klf(3,1,d_mplR)*qklm1R+klf(3,2,d_mplR)*qklm2R+klf(3,3,d_mplR)*qklm3R+klf(3,4,d_mplR)*qklm4R+klf(3,5,d_mplR)*qklm5R+klf(3,6,d_mplR)*qklm6R+klf(3,7,d_mplR)*qklm7R;
    // v14 = klf(4,0,d_mplR)*qklm0R+klf(4,1,d_mplR)*qklm1R+klf(4,2,d_mplR)*qklm2R+klf(4,3,d_mplR)*qklm3R+klf(4,4,d_mplR)*qklm4R+klf(4,5,d_mplR)*qklm5R+klf(4,6,d_mplR)*qklm6R+klf(4,7,d_mplR)*qklm7R;
    // v15 = klf(5,0,d_mplR)*qklm0R+klf(5,1,d_mplR)*qklm1R+klf(5,2,d_mplR)*qklm2R+klf(5,3,d_mplR)*qklm3R+klf(5,4,d_mplR)*qklm4R+klf(5,5,d_mplR)*qklm5R+klf(5,6,d_mplR)*qklm6R+klf(5,7,d_mplR)*qklm7R;
    // v16 = klf(6,0,d_mplR)*qklm0R+klf(6,1,d_mplR)*qklm1R+klf(6,2,d_mplR)*qklm2R+klf(6,3,d_mplR)*qklm3R+klf(6,4,d_mplR)*qklm4R+klf(6,5,d_mplR)*qklm5R+klf(6,6,d_mplR)*qklm6R+klf(6,7,d_mplR)*qklm7R;
    // v17 = klf(7,0,d_mplR)*qklm0R+klf(7,1,d_mplR)*qklm1R+klf(7,2,d_mplR)*qklm2R+klf(7,3,d_mplR)*qklm3R+klf(7,4,d_mplR)*qklm4R+klf(7,5,d_mplR)*qklm5R+klf(7,6,d_mplR)*qklm6R+klf(7,7,d_mplR)*qklm7R;

    // v20 = klf(0,0,d_mllL)*qklm0+klf(0,1,d_mllL)*qklm1+klf(0,2,d_mllL)*qklm2+klf(0,3,d_mllL)*qklm3+klf(0,4,d_mllL)*qklm4+klf(0,5,d_mllL)*qklm5+klf(0,6,d_mllL)*qklm6+klf(0,7,d_mllL)*qklm7;
    // v21 = klf(1,0,d_mllL)*qklm0+klf(1,1,d_mllL)*qklm1+klf(1,2,d_mllL)*qklm2+klf(1,3,d_mllL)*qklm3+klf(1,4,d_mllL)*qklm4+klf(1,5,d_mllL)*qklm5+klf(1,6,d_mllL)*qklm6+klf(1,7,d_mllL)*qklm7;
    // v22 = klf(2,0,d_mllL)*qklm0+klf(2,1,d_mllL)*qklm1+klf(2,2,d_mllL)*qklm2+klf(2,3,d_mllL)*qklm3+klf(2,4,d_mllL)*qklm4+klf(2,5,d_mllL)*qklm5+klf(2,6,d_mllL)*qklm6+klf(2,7,d_mllL)*qklm7;
    // v23 = klf(3,0,d_mllL)*qklm0+klf(3,1,d_mllL)*qklm1+klf(3,2,d_mllL)*qklm2+klf(3,3,d_mllL)*qklm3+klf(3,4,d_mllL)*qklm4+klf(3,5,d_mllL)*qklm5+klf(3,6,d_mllL)*qklm6+klf(3,7,d_mllL)*qklm7;
    // v24 = klf(4,0,d_mllL)*qklm0+klf(4,1,d_mllL)*qklm1+klf(4,2,d_mllL)*qklm2+klf(4,3,d_mllL)*qklm3+klf(4,4,d_mllL)*qklm4+klf(4,5,d_mllL)*qklm5+klf(4,6,d_mllL)*qklm6+klf(4,7,d_mllL)*qklm7;
    // v25 = klf(5,0,d_mllL)*qklm0+klf(5,1,d_mllL)*qklm1+klf(5,2,d_mllL)*qklm2+klf(5,3,d_mllL)*qklm3+klf(5,4,d_mllL)*qklm4+klf(5,5,d_mllL)*qklm5+klf(5,6,d_mllL)*qklm6+klf(5,7,d_mllL)*qklm7;
    // v26 = klf(6,0,d_mllL)*qklm0+klf(6,1,d_mllL)*qklm1+klf(6,2,d_mllL)*qklm2+klf(6,3,d_mllL)*qklm3+klf(6,4,d_mllL)*qklm4+klf(6,5,d_mllL)*qklm5+klf(6,6,d_mllL)*qklm6+klf(6,7,d_mllL)*qklm7;
    // v27 = klf(7,0,d_mllL)*qklm0+klf(7,1,d_mllL)*qklm1+klf(7,2,d_mllL)*qklm2+klf(7,3,d_mllL)*qklm3+klf(7,4,d_mllL)*qklm4+klf(7,5,d_mllL)*qklm5+klf(7,6,d_mllL)*qklm6+klf(7,7,d_mllL)*qklm7;

    // v30 = klf(0,0,d_mplL)*qklm0L+klf(0,1,d_mplL)*qklm1L+klf(0,2,d_mplL)*qklm2L+klf(0,3,d_mplL)*qklm3L+klf(0,4,d_mplL)*qklm4L+klf(0,5,d_mplL)*qklm5L+klf(0,6,d_mplL)*qklm6L+klf(0,7,d_mplL)*qklm7L;
    // v31 = klf(1,0,d_mplL)*qklm0L+klf(1,1,d_mplL)*qklm1L+klf(1,2,d_mplL)*qklm2L+klf(1,3,d_mplL)*qklm3L+klf(1,4,d_mplL)*qklm4L+klf(1,5,d_mplL)*qklm5L+klf(1,6,d_mplL)*qklm6L+klf(1,7,d_mplL)*qklm7L;
    // v32 = klf(2,0,d_mplL)*qklm0L+klf(2,1,d_mplL)*qklm1L+klf(2,2,d_mplL)*qklm2L+klf(2,3,d_mplL)*qklm3L+klf(2,4,d_mplL)*qklm4L+klf(2,5,d_mplL)*qklm5L+klf(2,6,d_mplL)*qklm6L+klf(2,7,d_mplL)*qklm7L;
    // v33 = klf(3,0,d_mplL)*qklm0L+klf(3,1,d_mplL)*qklm1L+klf(3,2,d_mplL)*qklm2L+klf(3,3,d_mplL)*qklm3L+klf(3,4,d_mplL)*qklm4L+klf(3,5,d_mplL)*qklm5L+klf(3,6,d_mplL)*qklm6L+klf(3,7,d_mplL)*qklm7L;
    // v34 = klf(4,0,d_mplL)*qklm0L+klf(4,1,d_mplL)*qklm1L+klf(4,2,d_mplL)*qklm2L+klf(4,3,d_mplL)*qklm3L+klf(4,4,d_mplL)*qklm4L+klf(4,5,d_mplL)*qklm5L+klf(4,6,d_mplL)*qklm6L+klf(4,7,d_mplL)*qklm7L;
    // v35 = klf(5,0,d_mplL)*qklm0L+klf(5,1,d_mplL)*qklm1L+klf(5,2,d_mplL)*qklm2L+klf(5,3,d_mplL)*qklm3L+klf(5,4,d_mplL)*qklm4L+klf(5,5,d_mplL)*qklm5L+klf(5,6,d_mplL)*qklm6L+klf(5,7,d_mplL)*qklm7L;
    // v36 = klf(6,0,d_mplL)*qklm0L+klf(6,1,d_mplL)*qklm1L+klf(6,2,d_mplL)*qklm2L+klf(6,3,d_mplL)*qklm3L+klf(6,4,d_mplL)*qklm4L+klf(6,5,d_mplL)*qklm5L+klf(6,6,d_mplL)*qklm6L+klf(6,7,d_mplL)*qklm7L;
    // v37 = klf(7,0,d_mplL)*qklm0L+klf(7,1,d_mplL)*qklm1L+klf(7,2,d_mplL)*qklm2L+klf(7,3,d_mplL)*qklm3L+klf(7,4,d_mplL)*qklm4L+klf(7,5,d_mplL)*qklm5L+klf(7,6,d_mplL)*qklm6L+klf(7,7,d_mplL)*qklm7L;

    // v40 = klf(0,0,d_mx)*qklm0+klf(0,1,d_mx)*qklm1+klf(0,2,d_mx)*qklm2+klf(0,3,d_mx)*qklm3+klf(0,4,d_mx)*qklm4+klf(0,5,d_mx)*qklm5+klf(0,6,d_mx)*qklm6+klf(0,7,d_mx)*qklm7;
    // v41 = klf(1,0,d_mx)*qklm0+klf(1,1,d_mx)*qklm1+klf(1,2,d_mx)*qklm2+klf(1,3,d_mx)*qklm3+klf(1,4,d_mx)*qklm4+klf(1,5,d_mx)*qklm5+klf(1,6,d_mx)*qklm6+klf(1,7,d_mx)*qklm7;
    // v42 = klf(2,0,d_mx)*qklm0+klf(2,1,d_mx)*qklm1+klf(2,2,d_mx)*qklm2+klf(2,3,d_mx)*qklm3+klf(2,4,d_mx)*qklm4+klf(2,5,d_mx)*qklm5+klf(2,6,d_mx)*qklm6+klf(2,7,d_mx)*qklm7;
    // v43 = klf(3,0,d_mx)*qklm0+klf(3,1,d_mx)*qklm1+klf(3,2,d_mx)*qklm2+klf(3,3,d_mx)*qklm3+klf(3,4,d_mx)*qklm4+klf(3,5,d_mx)*qklm5+klf(3,6,d_mx)*qklm6+klf(3,7,d_mx)*qklm7;
    // v44 = klf(4,0,d_mx)*qklm0+klf(4,1,d_mx)*qklm1+klf(4,2,d_mx)*qklm2+klf(4,3,d_mx)*qklm3+klf(4,4,d_mx)*qklm4+klf(4,5,d_mx)*qklm5+klf(4,6,d_mx)*qklm6+klf(4,7,d_mx)*qklm7;
    // v45 = klf(5,0,d_mx)*qklm0+klf(5,1,d_mx)*qklm1+klf(5,2,d_mx)*qklm2+klf(5,3,d_mx)*qklm3+klf(5,4,d_mx)*qklm4+klf(5,5,d_mx)*qklm5+klf(5,6,d_mx)*qklm6+klf(5,7,d_mx)*qklm7;
    // v46 = klf(6,0,d_mx)*qklm0+klf(6,1,d_mx)*qklm1+klf(6,2,d_mx)*qklm2+klf(6,3,d_mx)*qklm3+klf(6,4,d_mx)*qklm4+klf(6,5,d_mx)*qklm5+klf(6,6,d_mx)*qklm6+klf(6,7,d_mx)*qklm7;
    // v47 = klf(7,0,d_mx)*qklm0+klf(7,1,d_mx)*qklm1+klf(7,2,d_mx)*qklm2+klf(7,3,d_mx)*qklm3+klf(7,4,d_mx)*qklm4+klf(7,5,d_mx)*qklm5+klf(7,6,d_mx)*qklm6+klf(7,7,d_mx)*qklm7;

    // q0v0 = -0.5*kInkR*v00 - 0.5*kInkR*v10 + 0.5*kInkL*v20 + 0.5*kInkL*v30 + v40;
    // q0v1 = -0.5*kInkR*v01 - 0.5*kInkR*v11 + 0.5*kInkL*v21 + 0.5*kInkL*v31 + v41;
    // q0v2 = -0.5*kInkR*v02 - 0.5*kInkR*v12 + 0.5*kInkL*v22 + 0.5*kInkL*v32 + v42;
    // q0v3 = -0.5*kInkR*v03 - 0.5*kInkR*v13 + 0.5*kInkL*v23 + 0.5*kInkL*v33 + v43;
    // q0v4 = -0.5*kInkR*v04 - 0.5*kInkR*v14 + 0.5*kInkL*v24 + 0.5*kInkL*v34 + v44;
    // q0v5 = -0.5*kInkR*v05 - 0.5*kInkR*v15 + 0.5*kInkL*v25 + 0.5*kInkL*v35 + v45;
    // q0v6 = -0.5*kInkR*v06 - 0.5*kInkR*v16 + 0.5*kInkL*v26 + 0.5*kInkL*v36 + v46;
    // q0v7 = -0.5*kInkR*v07 - 0.5*kInkR*v17 + 0.5*kInkL*v27 + 0.5*kInkL*v37 + v47;
    


 q0v0 = -0.5*kInkR*(klf(0,0,d_mllR)*qklm0+klf(0,1,d_mllR)*qklm1+klf(0,2,d_mllR)*qklm2+klf(0,3,d_mllR)*qklm3+klf(0,4,d_mllR)*qklm4+klf(0,5,d_mllR)*qklm5+klf(0,6,d_mllR)*qklm6+klf(0,7,d_mllR)*qklm7)-0.5*kInkR*(klf(0,0,d_mplR)*qklm0R+klf(0,1,d_mplR)*qklm1R+klf(0,2,d_mplR)*qklm2R+klf(0,3,d_mplR)*qklm3R+klf(0,4,d_mplR)*qklm4R+klf(0,5,d_mplR)*qklm5R+klf(0,6,d_mplR)*qklm6R+klf(0,7,d_mplR)*qklm7R)+0.5*kInkL*(klf(0,0,d_mllL)*qklm0+klf(0,1,d_mllL)*qklm1+klf(0,2,d_mllL)*qklm2+klf(0,3,d_mllL)*qklm3+klf(0,4,d_mllL)*qklm4+klf(0,5,d_mllL)*qklm5+klf(0,6,d_mllL)*qklm6+klf(0,7,d_mllL)*qklm7)+0.5*kInkL*(klf(0,0,d_mplL)*qklm0L+klf(0,1,d_mplL)*qklm1L+klf(0,2,d_mplL)*qklm2L+klf(0,3,d_mplL)*qklm3L+klf(0,4,d_mplL)*qklm4L+klf(0,5,d_mplL)*qklm5L+klf(0,6,d_mplL)*qklm6L+klf(0,7,d_mplL)*qklm7L)+(klf(0,0,d_mx)*qklm0+klf(0,1,d_mx)*qklm1+klf(0,2,d_mx)*qklm2+klf(0,3,d_mx)*qklm3+klf(0,4,d_mx)*qklm4+klf(0,5,d_mx)*qklm5+klf(0,6,d_mx)*qklm6+klf(0,7,d_mx)*qklm7);

    q0v1 = -0.5*kInkR*(klf(1,0,d_mllR)*qklm0+klf(1,1,d_mllR)*qklm1+klf(1,2,d_mllR)*qklm2+klf(1,3,d_mllR)*qklm3+klf(1,4,d_mllR)*qklm4+klf(1,5,d_mllR)*qklm5+klf(1,6,d_mllR)*qklm6+klf(1,7,d_mllR)*qklm7)-0.5*kInkR*(klf(1,0,d_mplR)*qklm0R+klf(1,1,d_mplR)*qklm1R+klf(1,2,d_mplR)*qklm2R+klf(1,3,d_mplR)*qklm3R+klf(1,4,d_mplR)*qklm4R+klf(1,5,d_mplR)*qklm5R+klf(1,6,d_mplR)*qklm6R+klf(1,7,d_mplR)*qklm7R)+0.5*kInkL*(klf(1,0,d_mllL)*qklm0+klf(1,1,d_mllL)*qklm1+klf(1,2,d_mllL)*qklm2+klf(1,3,d_mllL)*qklm3+klf(1,4,d_mllL)*qklm4+klf(1,5,d_mllL)*qklm5+klf(1,6,d_mllL)*qklm6+klf(1,7,d_mllL)*qklm7)+0.5*kInkL*(klf(1,0,d_mplL)*qklm0L+klf(1,1,d_mplL)*qklm1L+klf(1,2,d_mplL)*qklm2L+klf(1,3,d_mplL)*qklm3L+klf(1,4,d_mplL)*qklm4L+klf(1,5,d_mplL)*qklm5L+klf(1,6,d_mplL)*qklm6L+klf(1,7,d_mplL)*qklm7L)+(klf(1,0,d_mx)*qklm0+klf(1,1,d_mx)*qklm1+klf(1,2,d_mx)*qklm2+klf(1,3,d_mx)*qklm3+klf(1,4,d_mx)*qklm4+klf(1,5,d_mx)*qklm5+klf(1,6,d_mx)*qklm6+klf(1,7,d_mx)*qklm7);

    q0v2 = -0.5*kInkR*(klf(2,0,d_mllR)*qklm0+klf(2,1,d_mllR)*qklm1+klf(2,2,d_mllR)*qklm2+klf(2,3,d_mllR)*qklm3+klf(2,4,d_mllR)*qklm4+klf(2,5,d_mllR)*qklm5+klf(2,6,d_mllR)*qklm6+klf(2,7,d_mllR)*qklm7)-0.5*kInkR*(klf(2,0,d_mplR)*qklm0R+klf(2,1,d_mplR)*qklm1R+klf(2,2,d_mplR)*qklm2R+klf(2,3,d_mplR)*qklm3R+klf(2,4,d_mplR)*qklm4R+klf(2,5,d_mplR)*qklm5R+klf(2,6,d_mplR)*qklm6R+klf(2,7,d_mplR)*qklm7R)+0.5*kInkL*(klf(2,0,d_mllL)*qklm0+klf(2,1,d_mllL)*qklm1+klf(2,2,d_mllL)*qklm2+klf(2,3,d_mllL)*qklm3+klf(2,4,d_mllL)*qklm4+klf(2,5,d_mllL)*qklm5+klf(2,6,d_mllL)*qklm6+klf(2,7,d_mllL)*qklm7)+0.5*kInkL*(klf(2,0,d_mplL)*qklm0L+klf(2,1,d_mplL)*qklm1L+klf(2,2,d_mplL)*qklm2L+klf(2,3,d_mplL)*qklm3L+klf(2,4,d_mplL)*qklm4L+klf(2,5,d_mplL)*qklm5L+klf(2,6,d_mplL)*qklm6L+klf(2,7,d_mplL)*qklm7L)+(klf(2,0,d_mx)*qklm0+klf(2,1,d_mx)*qklm1+klf(2,2,d_mx)*qklm2+klf(2,3,d_mx)*qklm3+klf(2,4,d_mx)*qklm4+klf(2,5,d_mx)*qklm5+klf(2,6,d_mx)*qklm6+klf(2,7,d_mx)*qklm7);

    q0v3 = -0.5*kInkR*(klf(3,0,d_mllR)*qklm0+klf(3,1,d_mllR)*qklm1+klf(3,2,d_mllR)*qklm2+klf(3,3,d_mllR)*qklm3+klf(3,4,d_mllR)*qklm4+klf(3,5,d_mllR)*qklm5+klf(3,6,d_mllR)*qklm6+klf(3,7,d_mllR)*qklm7)-0.5*kInkR*(klf(3,0,d_mplR)*qklm0R+klf(3,1,d_mplR)*qklm1R+klf(3,2,d_mplR)*qklm2R+klf(3,3,d_mplR)*qklm3R+klf(3,4,d_mplR)*qklm4R+klf(3,5,d_mplR)*qklm5R+klf(3,6,d_mplR)*qklm6R+klf(3,7,d_mplR)*qklm7R)+0.5*kInkL*(klf(3,0,d_mllL)*qklm0+klf(3,1,d_mllL)*qklm1+klf(3,2,d_mllL)*qklm2+klf(3,3,d_mllL)*qklm3+klf(3,4,d_mllL)*qklm4+klf(3,5,d_mllL)*qklm5+klf(3,6,d_mllL)*qklm6+klf(3,7,d_mllL)*qklm7)+0.5*kInkL*(klf(3,0,d_mplL)*qklm0L+klf(3,1,d_mplL)*qklm1L+klf(3,2,d_mplL)*qklm2L+klf(3,3,d_mplL)*qklm3L+klf(3,4,d_mplL)*qklm4L+klf(3,5,d_mplL)*qklm5L+klf(3,6,d_mplL)*qklm6L+klf(3,7,d_mplL)*qklm7L)+(klf(3,0,d_mx)*qklm0+klf(3,1,d_mx)*qklm1+klf(3,2,d_mx)*qklm2+klf(3,3,d_mx)*qklm3+klf(3,4,d_mx)*qklm4+klf(3,5,d_mx)*qklm5+klf(3,6,d_mx)*qklm6+klf(3,7,d_mx)*qklm7);

    q0v4 = -0.5*kInkR*( klf(4,0,d_mllR)*qklm0+klf(4,1,d_mllR)*qklm1+klf(4,2,d_mllR)*qklm2+klf(4,3,d_mllR)*qklm3+klf(4,4,d_mllR)*qklm4+klf(4,5,d_mllR)*qklm5+klf(4,6,d_mllR)*qklm6+klf(4,7,d_mllR)*qklm7)-0.5*kInkR*(klf(4,0,d_mplR)*qklm0R+klf(4,1,d_mplR)*qklm1R+klf(4,2,d_mplR)*qklm2R+klf(4,3,d_mplR)*qklm3R+klf(4,4,d_mplR)*qklm4R+klf(4,5,d_mplR)*qklm5R+klf(4,6,d_mplR)*qklm6R+klf(4,7,d_mplR)*qklm7R)+0.5*kInkL*(klf(4,0,d_mllL)*qklm0+klf(4,1,d_mllL)*qklm1+klf(4,2,d_mllL)*qklm2+klf(4,3,d_mllL)*qklm3+klf(4,4,d_mllL)*qklm4+klf(4,5,d_mllL)*qklm5+klf(4,6,d_mllL)*qklm6+klf(4,7,d_mllL)*qklm7)+0.5*kInkL*(klf(4,0,d_mplL)*qklm0L+klf(4,1,d_mplL)*qklm1L+klf(4,2,d_mplL)*qklm2L+klf(4,3,d_mplL)*qklm3L+klf(4,4,d_mplL)*qklm4L+klf(4,5,d_mplL)*qklm5L+klf(4,6,d_mplL)*qklm6L+klf(4,7,d_mplL)*qklm7L)+(klf(4,0,d_mx)*qklm0+klf(4,1,d_mx)*qklm1+klf(4,2,d_mx)*qklm2+klf(4,3,d_mx)*qklm3+klf(4,4,d_mx)*qklm4+klf(4,5,d_mx)*qklm5+klf(4,6,d_mx)*qklm6+klf(4,7,d_mx)*qklm7);

    q0v5 = -0.5*kInkR*(klf(5,0,d_mllR)*qklm0+klf(5,1,d_mllR)*qklm1+klf(5,2,d_mllR)*qklm2+klf(5,3,d_mllR)*qklm3+klf(5,4,d_mllR)*qklm4+klf(5,5,d_mllR)*qklm5+klf(5,6,d_mllR)*qklm6+klf(5,7,d_mllR)*qklm7)-0.5*kInkR*(klf(5,0,d_mplR)*qklm0R+klf(5,1,d_mplR)*qklm1R+klf(5,2,d_mplR)*qklm2R+klf(5,3,d_mplR)*qklm3R+klf(5,4,d_mplR)*qklm4R+klf(5,5,d_mplR)*qklm5R+klf(5,6,d_mplR)*qklm6R+klf(5,7,d_mplR)*qklm7R)+0.5*kInkL*(klf(5,0,d_mllL)*qklm0+klf(5,1,d_mllL)*qklm1+klf(5,2,d_mllL)*qklm2+klf(5,3,d_mllL)*qklm3+klf(5,4,d_mllL)*qklm4+klf(5,5,d_mllL)*qklm5+klf(5,6,d_mllL)*qklm6+klf(5,7,d_mllL)*qklm7)+0.5*kInkL*(klf(5,0,d_mplL)*qklm0L+klf(5,1,d_mplL)*qklm1L+klf(5,2,d_mplL)*qklm2L+klf(5,3,d_mplL)*qklm3L+klf(5,4,d_mplL)*qklm4L+klf(5,5,d_mplL)*qklm5L+klf(5,6,d_mplL)*qklm6L+klf(5,7,d_mplL)*qklm7L)+(klf(5,0,d_mx)*qklm0+klf(5,1,d_mx)*qklm1+klf(5,2,d_mx)*qklm2+klf(5,3,d_mx)*qklm3+klf(5,4,d_mx)*qklm4+klf(5,5,d_mx)*qklm5+klf(5,6,d_mx)*qklm6+klf(5,7,d_mx)*qklm7);

    q0v6 = -0.5*kInkR*(klf(6,0,d_mllR)*qklm0+klf(6,1,d_mllR)*qklm1+klf(6,2,d_mllR)*qklm2+klf(6,3,d_mllR)*qklm3+klf(6,4,d_mllR)*qklm4+klf(6,5,d_mllR)*qklm5+klf(6,6,d_mllR)*qklm6+klf(6,7,d_mllR)*qklm7)-0.5*kInkR*(klf(6,0,d_mplR)*qklm0R+klf(6,1,d_mplR)*qklm1R+klf(6,2,d_mplR)*qklm2R+klf(6,3,d_mplR)*qklm3R+klf(6,4,d_mplR)*qklm4R+klf(6,5,d_mplR)*qklm5R+klf(6,6,d_mplR)*qklm6R+klf(6,7,d_mplR)*qklm7R)+0.5*kInkL*(klf(6,0,d_mllL)*qklm0+klf(6,1,d_mllL)*qklm1+klf(6,2,d_mllL)*qklm2+klf(6,3,d_mllL)*qklm3+klf(6,4,d_mllL)*qklm4+klf(6,5,d_mllL)*qklm5+klf(6,6,d_mllL)*qklm6+klf(6,7,d_mllL)*qklm7)+0.5*kInkL*(klf(6,0,d_mplL)*qklm0L+klf(6,1,d_mplL)*qklm1L+klf(6,2,d_mplL)*qklm2L+klf(6,3,d_mplL)*qklm3L+klf(6,4,d_mplL)*qklm4L+klf(6,5,d_mplL)*qklm5L+klf(6,6,d_mplL)*qklm6L+klf(6,7,d_mplL)*qklm7L)+(klf(6,0,d_mx)*qklm0+klf(6,1,d_mx)*qklm1+klf(6,2,d_mx)*qklm2+klf(6,3,d_mx)*qklm3+klf(6,4,d_mx)*qklm4+klf(6,5,d_mx)*qklm5+klf(6,6,d_mx)*qklm6+klf(6,7,d_mx)*qklm7);

    q0v7 = -0.5*kInkR*(klf(7,0,d_mllR)*qklm0+klf(7,1,d_mllR)*qklm1+klf(7,2,d_mllR)*qklm2+klf(7,3,d_mllR)*qklm3+klf(7,4,d_mllR)*qklm4+klf(7,5,d_mllR)*qklm5+klf(7,6,d_mllR)*qklm6+klf(7,7,d_mllR)*qklm7)-0.5*kInkR*(klf(7,0,d_mplR)*qklm0R+klf(7,1,d_mplR)*qklm1R+klf(7,2,d_mplR)*qklm2R+klf(7,3,d_mplR)*qklm3R+klf(7,4,d_mplR)*qklm4R+klf(7,5,d_mplR)*qklm5R+klf(7,6,d_mplR)*qklm6R+klf(7,7,d_mplR)*qklm7R)+0.5*kInkL*(klf(7,0,d_mllL)*qklm0+klf(7,1,d_mllL)*qklm1+klf(7,2,d_mllL)*qklm2+klf(7,3,d_mllL)*qklm3+klf(7,4,d_mllL)*qklm4+klf(7,5,d_mllL)*qklm5+klf(7,6,d_mllL)*qklm6+klf(7,7,d_mllL)*qklm7)+0.5*kInkL*(klf(7,0,d_mplL)*qklm0L+klf(7,1,d_mplL)*qklm1L+klf(7,2,d_mplL)*qklm2L+klf(7,3,d_mplL)*qklm3L+klf(7,4,d_mplL)*qklm4L+klf(7,5,d_mplL)*qklm5L+klf(7,6,d_mplL)*qklm6L+klf(7,7,d_mplL)*qklm7L)+(klf(7,0,d_mx)*qklm0+klf(7,1,d_mx)*qklm1+klf(7,2,d_mx)*qklm2+klf(7,3,d_mx)*qklm3+klf(7,4,d_mx)*qklm4+klf(7,5,d_mx)*qklm5+klf(7,6,d_mx)*qklm6+klf(7,7,d_mx)*qklm7);
    

    


    float kInkT  = 1.0;
    float kInkBo = 1.0;
    
    
    if( klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)+klmf(ks-1,ls,ms,K+2,M+2,L+2,d_kM) > 0 )
      kInkT = 2*klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)*klmf(ks-1,ls,ms,K+2,M+2,L+2,d_kM)/(klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)+klmf(ks-1,ls,ms,K+2,M+2,L+2,d_kM));
    else
      kInkT = 0;
    
    if( klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)+klmf(ks+1,ls,ms,K+2,M+2,L+2,d_kM) > 0 )
      kInkBo = 2*klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)*klmf(ks+1,ls,ms,K+2,M+2,L+2,d_kM)/(klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)+klmf(ks+1,ls,ms,K+2,M+2,L+2,d_kM));
    else
      kInkBo = 0;

    qklm0  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,0); qklm1  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,1); qklm2  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,2); qklm3  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,3);
    qklm4  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,4); qklm5  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,5); qklm6  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,6); qklm7  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,7);

    qklm0T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_qklm1,0); qklm1T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_qklm1,1); qklm2T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_qklm1,2); qklm3T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_qklm1,3);
    qklm4T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_qklm1,4); qklm5T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_qklm1,5); qklm6T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_qklm1,6); qklm7T  = klmdf(k-1,l,m,Ks,Ms,Ls,d_qklm1,7);

    qklm0Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_qklm1,0); qklm1Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_qklm1,1); qklm2Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_qklm1,2); qklm3Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_qklm1,3);
    qklm4Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_qklm1,4); qklm5Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_qklm1,5); qklm6Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_qklm1,6); qklm7Bo  = klmdf(k+1,l,m,Ks,Ms,Ls,d_qklm1,7);



    // v00 = klf(0,0,d_mllT)*qklm0+klf(0,1,d_mllT)*qklm1+klf(0,2,d_mllT)*qklm2+klf(0,3,d_mllT)*qklm3+klf(0,4,d_mllT)*qklm4+klf(0,5,d_mllT)*qklm5+klf(0,6,d_mllT)*qklm6+klf(0,7,d_mllT)*qklm7;
    // v01 = klf(1,0,d_mllT)*qklm0+klf(1,1,d_mllT)*qklm1+klf(1,2,d_mllT)*qklm2+klf(1,3,d_mllT)*qklm3+klf(1,4,d_mllT)*qklm4+klf(1,5,d_mllT)*qklm5+klf(1,6,d_mllT)*qklm6+klf(1,7,d_mllT)*qklm7;
    // v02 = klf(2,0,d_mllT)*qklm0+klf(2,1,d_mllT)*qklm1+klf(2,2,d_mllT)*qklm2+klf(2,3,d_mllT)*qklm3+klf(2,4,d_mllT)*qklm4+klf(2,5,d_mllT)*qklm5+klf(2,6,d_mllT)*qklm6+klf(2,7,d_mllT)*qklm7;
    // v03 = klf(3,0,d_mllT)*qklm0+klf(3,1,d_mllT)*qklm1+klf(3,2,d_mllT)*qklm2+klf(3,3,d_mllT)*qklm3+klf(3,4,d_mllT)*qklm4+klf(3,5,d_mllT)*qklm5+klf(3,6,d_mllT)*qklm6+klf(3,7,d_mllT)*qklm7;
    // v04 = klf(4,0,d_mllT)*qklm0+klf(4,1,d_mllT)*qklm1+klf(4,2,d_mllT)*qklm2+klf(4,3,d_mllT)*qklm3+klf(4,4,d_mllT)*qklm4+klf(4,5,d_mllT)*qklm5+klf(4,6,d_mllT)*qklm6+klf(4,7,d_mllT)*qklm7;
    // v05 = klf(5,0,d_mllT)*qklm0+klf(5,1,d_mllT)*qklm1+klf(5,2,d_mllT)*qklm2+klf(5,3,d_mllT)*qklm3+klf(5,4,d_mllT)*qklm4+klf(5,5,d_mllT)*qklm5+klf(5,6,d_mllT)*qklm6+klf(5,7,d_mllT)*qklm7;
    // v06 = klf(6,0,d_mllT)*qklm0+klf(6,1,d_mllT)*qklm1+klf(6,2,d_mllT)*qklm2+klf(6,3,d_mllT)*qklm3+klf(6,4,d_mllT)*qklm4+klf(6,5,d_mllT)*qklm5+klf(6,6,d_mllT)*qklm6+klf(6,7,d_mllT)*qklm7;
    // v07 = klf(7,0,d_mllT)*qklm0+klf(7,1,d_mllT)*qklm1+klf(7,2,d_mllT)*qklm2+klf(7,3,d_mllT)*qklm3+klf(7,4,d_mllT)*qklm4+klf(7,5,d_mllT)*qklm5+klf(7,6,d_mllT)*qklm6+klf(7,7,d_mllT)*qklm7;

    // v10 = klf(0,0,d_mplT)*qklm0T+klf(0,1,d_mplT)*qklm1T+klf(0,2,d_mplT)*qklm2T+klf(0,3,d_mplT)*qklm3T+klf(0,4,d_mplT)*qklm4T+klf(0,5,d_mplT)*qklm5T+klf(0,6,d_mplT)*qklm6T+klf(0,7,d_mplT)*qklm7T;
    // v11 = klf(1,0,d_mplT)*qklm0T+klf(1,1,d_mplT)*qklm1T+klf(1,2,d_mplT)*qklm2T+klf(1,3,d_mplT)*qklm3T+klf(1,4,d_mplT)*qklm4T+klf(1,5,d_mplT)*qklm5T+klf(1,6,d_mplT)*qklm6T+klf(1,7,d_mplT)*qklm7T;
    // v12 = klf(2,0,d_mplT)*qklm0T+klf(2,1,d_mplT)*qklm1T+klf(2,2,d_mplT)*qklm2T+klf(2,3,d_mplT)*qklm3T+klf(2,4,d_mplT)*qklm4T+klf(2,5,d_mplT)*qklm5T+klf(2,6,d_mplT)*qklm6T+klf(2,7,d_mplT)*qklm7T;
    // v13 = klf(3,0,d_mplT)*qklm0T+klf(3,1,d_mplT)*qklm1T+klf(3,2,d_mplT)*qklm2T+klf(3,3,d_mplT)*qklm3T+klf(3,4,d_mplT)*qklm4T+klf(3,5,d_mplT)*qklm5T+klf(3,6,d_mplT)*qklm6T+klf(3,7,d_mplT)*qklm7T;
    // v14 = klf(4,0,d_mplT)*qklm0T+klf(4,1,d_mplT)*qklm1T+klf(4,2,d_mplT)*qklm2T+klf(4,3,d_mplT)*qklm3T+klf(4,4,d_mplT)*qklm4T+klf(4,5,d_mplT)*qklm5T+klf(4,6,d_mplT)*qklm6T+klf(4,7,d_mplT)*qklm7T;
    // v15 = klf(5,0,d_mplT)*qklm0T+klf(5,1,d_mplT)*qklm1T+klf(5,2,d_mplT)*qklm2T+klf(5,3,d_mplT)*qklm3T+klf(5,4,d_mplT)*qklm4T+klf(5,5,d_mplT)*qklm5T+klf(5,6,d_mplT)*qklm6T+klf(5,7,d_mplT)*qklm7T;
    // v16 = klf(6,0,d_mplT)*qklm0T+klf(6,1,d_mplT)*qklm1T+klf(6,2,d_mplT)*qklm2T+klf(6,3,d_mplT)*qklm3T+klf(6,4,d_mplT)*qklm4T+klf(6,5,d_mplT)*qklm5T+klf(6,6,d_mplT)*qklm6T+klf(6,7,d_mplT)*qklm7T;
    // v17 = klf(7,0,d_mplT)*qklm0T+klf(7,1,d_mplT)*qklm1T+klf(7,2,d_mplT)*qklm2T+klf(7,3,d_mplT)*qklm3T+klf(7,4,d_mplT)*qklm4T+klf(7,5,d_mplT)*qklm5T+klf(7,6,d_mplT)*qklm6T+klf(7,7,d_mplT)*qklm7T;
    
    // v20 = klf(0,0,d_mllBo)*qklm0+klf(0,1,d_mllBo)*qklm1+klf(0,2,d_mllBo)*qklm2+klf(0,3,d_mllBo)*qklm3+klf(0,4,d_mllBo)*qklm4+klf(0,5,d_mllBo)*qklm5+klf(0,6,d_mllBo)*qklm6+klf(0,7,d_mllBo)*qklm7;
    // v21 = klf(1,0,d_mllBo)*qklm0+klf(1,1,d_mllBo)*qklm1+klf(1,2,d_mllBo)*qklm2+klf(1,3,d_mllBo)*qklm3+klf(1,4,d_mllBo)*qklm4+klf(1,5,d_mllBo)*qklm5+klf(1,6,d_mllBo)*qklm6+klf(1,7,d_mllBo)*qklm7;
    // v22 = klf(2,0,d_mllBo)*qklm0+klf(2,1,d_mllBo)*qklm1+klf(2,2,d_mllBo)*qklm2+klf(2,3,d_mllBo)*qklm3+klf(2,4,d_mllBo)*qklm4+klf(2,5,d_mllBo)*qklm5+klf(2,6,d_mllBo)*qklm6+klf(2,7,d_mllBo)*qklm7;
    // v23 = klf(3,0,d_mllBo)*qklm0+klf(3,1,d_mllBo)*qklm1+klf(3,2,d_mllBo)*qklm2+klf(3,3,d_mllBo)*qklm3+klf(3,4,d_mllBo)*qklm4+klf(3,5,d_mllBo)*qklm5+klf(3,6,d_mllBo)*qklm6+klf(3,7,d_mllBo)*qklm7;
    // v24 = klf(4,0,d_mllBo)*qklm0+klf(4,1,d_mllBo)*qklm1+klf(4,2,d_mllBo)*qklm2+klf(4,3,d_mllBo)*qklm3+klf(4,4,d_mllBo)*qklm4+klf(4,5,d_mllBo)*qklm5+klf(4,6,d_mllBo)*qklm6+klf(4,7,d_mllBo)*qklm7;
    // v25 = klf(5,0,d_mllBo)*qklm0+klf(5,1,d_mllBo)*qklm1+klf(5,2,d_mllBo)*qklm2+klf(5,3,d_mllBo)*qklm3+klf(5,4,d_mllBo)*qklm4+klf(5,5,d_mllBo)*qklm5+klf(5,6,d_mllBo)*qklm6+klf(5,7,d_mllBo)*qklm7;
    // v26 = klf(6,0,d_mllBo)*qklm0+klf(6,1,d_mllBo)*qklm1+klf(6,2,d_mllBo)*qklm2+klf(6,3,d_mllBo)*qklm3+klf(6,4,d_mllBo)*qklm4+klf(6,5,d_mllBo)*qklm5+klf(6,6,d_mllBo)*qklm6+klf(6,7,d_mllBo)*qklm7;
    // v27 = klf(7,0,d_mllBo)*qklm0+klf(7,1,d_mllBo)*qklm1+klf(7,2,d_mllBo)*qklm2+klf(7,3,d_mllBo)*qklm3+klf(7,4,d_mllBo)*qklm4+klf(7,5,d_mllBo)*qklm5+klf(7,6,d_mllBo)*qklm6+klf(7,7,d_mllBo)*qklm7;

    // v30 = klf(0,0,d_mplBo)*qklm0Bo+klf(0,1,d_mplBo)*qklm1Bo+klf(0,2,d_mplBo)*qklm2Bo+klf(0,3,d_mplBo)*qklm3Bo+klf(0,4,d_mplBo)*qklm4Bo+klf(0,5,d_mplBo)*qklm5Bo+klf(0,6,d_mplBo)*qklm6Bo+klf(0,7,d_mplBo)*qklm7Bo;
    // v31 = klf(1,0,d_mplBo)*qklm0Bo+klf(1,1,d_mplBo)*qklm1Bo+klf(1,2,d_mplBo)*qklm2Bo+klf(1,3,d_mplBo)*qklm3Bo+klf(1,4,d_mplBo)*qklm4Bo+klf(1,5,d_mplBo)*qklm5Bo+klf(1,6,d_mplBo)*qklm6Bo+klf(1,7,d_mplBo)*qklm7Bo;
    // v32 = klf(2,0,d_mplBo)*qklm0Bo+klf(2,1,d_mplBo)*qklm1Bo+klf(2,2,d_mplBo)*qklm2Bo+klf(2,3,d_mplBo)*qklm3Bo+klf(2,4,d_mplBo)*qklm4Bo+klf(2,5,d_mplBo)*qklm5Bo+klf(2,6,d_mplBo)*qklm6Bo+klf(2,7,d_mplBo)*qklm7Bo;
    // v33 = klf(3,0,d_mplBo)*qklm0Bo+klf(3,1,d_mplBo)*qklm1Bo+klf(3,2,d_mplBo)*qklm2Bo+klf(3,3,d_mplBo)*qklm3Bo+klf(3,4,d_mplBo)*qklm4Bo+klf(3,5,d_mplBo)*qklm5Bo+klf(3,6,d_mplBo)*qklm6Bo+klf(3,7,d_mplBo)*qklm7Bo;
    // v34 = klf(4,0,d_mplBo)*qklm0Bo+klf(4,1,d_mplBo)*qklm1Bo+klf(4,2,d_mplBo)*qklm2Bo+klf(4,3,d_mplBo)*qklm3Bo+klf(4,4,d_mplBo)*qklm4Bo+klf(4,5,d_mplBo)*qklm5Bo+klf(4,6,d_mplBo)*qklm6Bo+klf(4,7,d_mplBo)*qklm7Bo;
    // v35 = klf(5,0,d_mplBo)*qklm0Bo+klf(5,1,d_mplBo)*qklm1Bo+klf(5,2,d_mplBo)*qklm2Bo+klf(5,3,d_mplBo)*qklm3Bo+klf(5,4,d_mplBo)*qklm4Bo+klf(5,5,d_mplBo)*qklm5Bo+klf(5,6,d_mplBo)*qklm6Bo+klf(5,7,d_mplBo)*qklm7Bo;
    // v36 = klf(6,0,d_mplBo)*qklm0Bo+klf(6,1,d_mplBo)*qklm1Bo+klf(6,2,d_mplBo)*qklm2Bo+klf(6,3,d_mplBo)*qklm3Bo+klf(6,4,d_mplBo)*qklm4Bo+klf(6,5,d_mplBo)*qklm5Bo+klf(6,6,d_mplBo)*qklm6Bo+klf(6,7,d_mplBo)*qklm7Bo;
    // v37 = klf(7,0,d_mplBo)*qklm0Bo+klf(7,1,d_mplBo)*qklm1Bo+klf(7,2,d_mplBo)*qklm2Bo+klf(7,3,d_mplBo)*qklm3Bo+klf(7,4,d_mplBo)*qklm4Bo+klf(7,5,d_mplBo)*qklm5Bo+klf(7,6,d_mplBo)*qklm6Bo+klf(7,7,d_mplBo)*qklm7Bo;

    // v40 = klf(0,0,d_my)*qklm0+klf(0,1,d_my)*qklm1+klf(0,2,d_my)*qklm2+klf(0,3,d_my)*qklm3+klf(0,4,d_my)*qklm4+klf(0,5,d_my)*qklm5+klf(0,6,d_my)*qklm6+klf(0,7,d_my)*qklm7;
    // v41 = klf(1,0,d_my)*qklm0+klf(1,1,d_my)*qklm1+klf(1,2,d_my)*qklm2+klf(1,3,d_my)*qklm3+klf(1,4,d_my)*qklm4+klf(1,5,d_my)*qklm5+klf(1,6,d_my)*qklm6+klf(1,7,d_my)*qklm7;
    // v42 = klf(2,0,d_my)*qklm0+klf(2,1,d_my)*qklm1+klf(2,2,d_my)*qklm2+klf(2,3,d_my)*qklm3+klf(2,4,d_my)*qklm4+klf(2,5,d_my)*qklm5+klf(2,6,d_my)*qklm6+klf(2,7,d_my)*qklm7;
    // v43 = klf(3,0,d_my)*qklm0+klf(3,1,d_my)*qklm1+klf(3,2,d_my)*qklm2+klf(3,3,d_my)*qklm3+klf(3,4,d_my)*qklm4+klf(3,5,d_my)*qklm5+klf(3,6,d_my)*qklm6+klf(3,7,d_my)*qklm7;
    // v44 = klf(4,0,d_my)*qklm0+klf(4,1,d_my)*qklm1+klf(4,2,d_my)*qklm2+klf(4,3,d_my)*qklm3+klf(4,4,d_my)*qklm4+klf(4,5,d_my)*qklm5+klf(4,6,d_my)*qklm6+klf(4,7,d_my)*qklm7;
    // v45 = klf(5,0,d_my)*qklm0+klf(5,1,d_my)*qklm1+klf(5,2,d_my)*qklm2+klf(5,3,d_my)*qklm3+klf(5,4,d_my)*qklm4+klf(5,5,d_my)*qklm5+klf(5,6,d_my)*qklm6+klf(5,7,d_my)*qklm7;
    // v46 = klf(6,0,d_my)*qklm0+klf(6,1,d_my)*qklm1+klf(6,2,d_my)*qklm2+klf(6,3,d_my)*qklm3+klf(6,4,d_my)*qklm4+klf(6,5,d_my)*qklm5+klf(6,6,d_my)*qklm6+klf(6,7,d_my)*qklm7;
    // v47 = klf(7,0,d_my)*qklm0+klf(7,1,d_my)*qklm1+klf(7,2,d_my)*qklm2+klf(7,3,d_my)*qklm3+klf(7,4,d_my)*qklm4+klf(7,5,d_my)*qklm5+klf(7,6,d_my)*qklm6+klf(7,7,d_my)*qklm7;


    // q1v0 = -0.5*kInkT*v00 - 0.5*kInkT*v10 + 0.5*kInkBo*v20 + 0.5*kInkBo*v30 + v40;
    // q1v1 = -0.5*kInkT*v01 - 0.5*kInkT*v11 + 0.5*kInkBo*v21 + 0.5*kInkBo*v31 + v41;
    // q1v2 = -0.5*kInkT*v02 - 0.5*kInkT*v12 + 0.5*kInkBo*v22 + 0.5*kInkBo*v32 + v42;
    // q1v3 = -0.5*kInkT*v03 - 0.5*kInkT*v13 + 0.5*kInkBo*v23 + 0.5*kInkBo*v33 + v43;
    // q1v4 = -0.5*kInkT*v04 - 0.5*kInkT*v14 + 0.5*kInkBo*v24 + 0.5*kInkBo*v34 + v44;
    // q1v5 = -0.5*kInkT*v05 - 0.5*kInkT*v15 + 0.5*kInkBo*v25 + 0.5*kInkBo*v35 + v45;
    // q1v6 = -0.5*kInkT*v06 - 0.5*kInkT*v16 + 0.5*kInkBo*v26 + 0.5*kInkBo*v36 + v46;
    // q1v7 = -0.5*kInkT*v07 - 0.5*kInkT*v17 + 0.5*kInkBo*v27 + 0.5*kInkBo*v37 + v47;



      q1v0 = -0.5*kInkT*(klf(0,0,d_mllT)*qklm0+klf(0,1,d_mllT)*qklm1+klf(0,2,d_mllT)*qklm2+klf(0,3,d_mllT)*qklm3+klf(0,4,d_mllT)*qklm4+klf(0,5,d_mllT)*qklm5+klf(0,6,d_mllT)*qklm6+klf(0,7,d_mllT)*qklm7)-0.5*kInkT*(klf(0,0,d_mplT)*qklm0T+klf(0,1,d_mplT)*qklm1T+klf(0,2,d_mplT)*qklm2T+klf(0,3,d_mplT)*qklm3T+klf(0,4,d_mplT)*qklm4T+klf(0,5,d_mplT)*qklm5T+klf(0,6,d_mplT)*qklm6T+klf(0,7,d_mplT)*qklm7T)+0.5*kInkBo*(klf(0,0,d_mllBo)*qklm0+klf(0,1,d_mllBo)*qklm1+klf(0,2,d_mllBo)*qklm2+klf(0,3,d_mllBo)*qklm3+klf(0,4,d_mllBo)*qklm4+klf(0,5,d_mllBo)*qklm5+klf(0,6,d_mllBo)*qklm6+klf(0,7,d_mllBo)*qklm7)+0.5*kInkBo*(klf(0,0,d_mplBo)*qklm0Bo+klf(0,1,d_mplBo)*qklm1Bo+klf(0,2,d_mplBo)*qklm2Bo+klf(0,3,d_mplBo)*qklm3Bo+klf(0,4,d_mplBo)*qklm4Bo+klf(0,5,d_mplBo)*qklm5Bo+klf(0,6,d_mplBo)*qklm6Bo+klf(0,7,d_mplBo)*qklm7Bo)+(klf(0,0,d_my)*qklm0+klf(0,1,d_my)*qklm1+klf(0,2,d_my)*qklm2+klf(0,3,d_my)*qklm3+klf(0,4,d_my)*qklm4+klf(0,5,d_my)*qklm5+klf(0,6,d_my)*qklm6+klf(0,7,d_my)*qklm7);
    
    q1v1 = -0.5*kInkT*(klf(1,0,d_mllT)*qklm0+klf(1,1,d_mllT)*qklm1+klf(1,2,d_mllT)*qklm2+klf(1,3,d_mllT)*qklm3+klf(1,4,d_mllT)*qklm4+klf(1,5,d_mllT)*qklm5+klf(1,6,d_mllT)*qklm6+klf(1,7,d_mllT)*qklm7)-0.5*kInkT*(klf(1,0,d_mplT)*qklm0T+klf(1,1,d_mplT)*qklm1T+klf(1,2,d_mplT)*qklm2T+klf(1,3,d_mplT)*qklm3T+klf(1,4,d_mplT)*qklm4T+klf(1,5,d_mplT)*qklm5T+klf(1,6,d_mplT)*qklm6T+klf(1,7,d_mplT)*qklm7T)+0.5*kInkBo*(klf(1,0,d_mllBo)*qklm0+klf(1,1,d_mllBo)*qklm1+klf(1,2,d_mllBo)*qklm2+klf(1,3,d_mllBo)*qklm3+klf(1,4,d_mllBo)*qklm4+klf(1,5,d_mllBo)*qklm5+klf(1,6,d_mllBo)*qklm6+klf(1,7,d_mllBo)*qklm7)+0.5*kInkBo*(klf(1,0,d_mplBo)*qklm0Bo+klf(1,1,d_mplBo)*qklm1Bo+klf(1,2,d_mplBo)*qklm2Bo+klf(1,3,d_mplBo)*qklm3Bo+klf(1,4,d_mplBo)*qklm4Bo+klf(1,5,d_mplBo)*qklm5Bo+klf(1,6,d_mplBo)*qklm6Bo+klf(1,7,d_mplBo)*qklm7Bo)+(klf(1,0,d_my)*qklm0+klf(1,1,d_my)*qklm1+klf(1,2,d_my)*qklm2+klf(1,3,d_my)*qklm3+klf(1,4,d_my)*qklm4+klf(1,5,d_my)*qklm5+klf(1,6,d_my)*qklm6+klf(1,7,d_my)*qklm7);

    q1v2 = -0.5*kInkT*(klf(2,0,d_mllT)*qklm0+klf(2,1,d_mllT)*qklm1+klf(2,2,d_mllT)*qklm2+klf(2,3,d_mllT)*qklm3+klf(2,4,d_mllT)*qklm4+klf(2,5,d_mllT)*qklm5+klf(2,6,d_mllT)*qklm6+klf(2,7,d_mllT)*qklm7)-0.5*kInkT*(klf(2,0,d_mplT)*qklm0T+klf(2,1,d_mplT)*qklm1T+klf(2,2,d_mplT)*qklm2T+klf(2,3,d_mplT)*qklm3T+klf(2,4,d_mplT)*qklm4T+klf(2,5,d_mplT)*qklm5T+klf(2,6,d_mplT)*qklm6T+klf(2,7,d_mplT)*qklm7T)+0.5*kInkBo*(klf(2,0,d_mllBo)*qklm0+klf(2,1,d_mllBo)*qklm1+klf(2,2,d_mllBo)*qklm2+klf(2,3,d_mllBo)*qklm3+klf(2,4,d_mllBo)*qklm4+klf(2,5,d_mllBo)*qklm5+klf(2,6,d_mllBo)*qklm6+klf(2,7,d_mllBo)*qklm7)+0.5*kInkBo*(klf(2,0,d_mplBo)*qklm0Bo+klf(2,1,d_mplBo)*qklm1Bo+klf(2,2,d_mplBo)*qklm2Bo+klf(2,3,d_mplBo)*qklm3Bo+klf(2,4,d_mplBo)*qklm4Bo+klf(2,5,d_mplBo)*qklm5Bo+klf(2,6,d_mplBo)*qklm6Bo+klf(2,7,d_mplBo)*qklm7Bo)+(klf(2,0,d_my)*qklm0+klf(2,1,d_my)*qklm1+klf(2,2,d_my)*qklm2+klf(2,3,d_my)*qklm3+klf(2,4,d_my)*qklm4+klf(2,5,d_my)*qklm5+klf(2,6,d_my)*qklm6+klf(2,7,d_my)*qklm7);

    q1v3 = -0.5*kInkT*(klf(3,0,d_mllT)*qklm0+klf(3,1,d_mllT)*qklm1+klf(3,2,d_mllT)*qklm2+klf(3,3,d_mllT)*qklm3+klf(3,4,d_mllT)*qklm4+klf(3,5,d_mllT)*qklm5+klf(3,6,d_mllT)*qklm6+klf(3,7,d_mllT)*qklm7)-0.5*kInkT*(klf(3,0,d_mplT)*qklm0T+klf(3,1,d_mplT)*qklm1T+klf(3,2,d_mplT)*qklm2T+klf(3,3,d_mplT)*qklm3T+klf(3,4,d_mplT)*qklm4T+klf(3,5,d_mplT)*qklm5T+klf(3,6,d_mplT)*qklm6T+klf(3,7,d_mplT)*qklm7T)+0.5*kInkBo*(klf(3,0,d_mllBo)*qklm0+klf(3,1,d_mllBo)*qklm1+klf(3,2,d_mllBo)*qklm2+klf(3,3,d_mllBo)*qklm3+klf(3,4,d_mllBo)*qklm4+klf(3,5,d_mllBo)*qklm5+klf(3,6,d_mllBo)*qklm6+klf(3,7,d_mllBo)*qklm7)+0.5*kInkBo*(klf(3,0,d_mplBo)*qklm0Bo+klf(3,1,d_mplBo)*qklm1Bo+klf(3,2,d_mplBo)*qklm2Bo+klf(3,3,d_mplBo)*qklm3Bo+klf(3,4,d_mplBo)*qklm4Bo+klf(3,5,d_mplBo)*qklm5Bo+klf(3,6,d_mplBo)*qklm6Bo+klf(3,7,d_mplBo)*qklm7Bo)+(klf(3,0,d_my)*qklm0+klf(3,1,d_my)*qklm1+klf(3,2,d_my)*qklm2+klf(3,3,d_my)*qklm3+klf(3,4,d_my)*qklm4+klf(3,5,d_my)*qklm5+klf(3,6,d_my)*qklm6+klf(3,7,d_my)*qklm7);

    q1v4 = -0.5*kInkT*( klf(4,0,d_mllT)*qklm0+klf(4,1,d_mllT)*qklm1+klf(4,2,d_mllT)*qklm2+klf(4,3,d_mllT)*qklm3+klf(4,4,d_mllT)*qklm4+klf(4,5,d_mllT)*qklm5+klf(4,6,d_mllT)*qklm6+klf(4,7,d_mllT)*qklm7)-0.5*kInkT*(klf(4,0,d_mplT)*qklm0T+klf(4,1,d_mplT)*qklm1T+klf(4,2,d_mplT)*qklm2T+klf(4,3,d_mplT)*qklm3T+klf(4,4,d_mplT)*qklm4T+klf(4,5,d_mplT)*qklm5T+klf(4,6,d_mplT)*qklm6T+klf(4,7,d_mplT)*qklm7T)+0.5*kInkBo*(klf(4,0,d_mllBo)*qklm0+klf(4,1,d_mllBo)*qklm1+klf(4,2,d_mllBo)*qklm2+klf(4,3,d_mllBo)*qklm3+klf(4,4,d_mllBo)*qklm4+klf(4,5,d_mllBo)*qklm5+klf(4,6,d_mllBo)*qklm6+klf(4,7,d_mllBo)*qklm7)+0.5*kInkBo*(klf(4,0,d_mplBo)*qklm0Bo+klf(4,1,d_mplBo)*qklm1Bo+klf(4,2,d_mplBo)*qklm2Bo+klf(4,3,d_mplBo)*qklm3Bo+klf(4,4,d_mplBo)*qklm4Bo+klf(4,5,d_mplBo)*qklm5Bo+klf(4,6,d_mplBo)*qklm6Bo+klf(4,7,d_mplBo)*qklm7Bo)+(klf(4,0,d_my)*qklm0+klf(4,1,d_my)*qklm1+klf(4,2,d_my)*qklm2+klf(4,3,d_my)*qklm3+klf(4,4,d_my)*qklm4+klf(4,5,d_my)*qklm5+klf(4,6,d_my)*qklm6+klf(4,7,d_my)*qklm7);

    q1v5 = -0.5*kInkT*(klf(5,0,d_mllT)*qklm0+klf(5,1,d_mllT)*qklm1+klf(5,2,d_mllT)*qklm2+klf(5,3,d_mllT)*qklm3+klf(5,4,d_mllT)*qklm4+klf(5,5,d_mllT)*qklm5+klf(5,6,d_mllT)*qklm6+klf(5,7,d_mllT)*qklm7)-0.5*kInkT*(klf(5,0,d_mplT)*qklm0T+klf(5,1,d_mplT)*qklm1T+klf(5,2,d_mplT)*qklm2T+klf(5,3,d_mplT)*qklm3T+klf(5,4,d_mplT)*qklm4T+klf(5,5,d_mplT)*qklm5T+klf(5,6,d_mplT)*qklm6T+klf(5,7,d_mplT)*qklm7T)+0.5*kInkBo*(klf(5,0,d_mllBo)*qklm0+klf(5,1,d_mllBo)*qklm1+klf(5,2,d_mllBo)*qklm2+klf(5,3,d_mllBo)*qklm3+klf(5,4,d_mllBo)*qklm4+klf(5,5,d_mllBo)*qklm5+klf(5,6,d_mllBo)*qklm6+klf(5,7,d_mllBo)*qklm7)+0.5*kInkBo*(klf(5,0,d_mplBo)*qklm0Bo+klf(5,1,d_mplBo)*qklm1Bo+klf(5,2,d_mplBo)*qklm2Bo+klf(5,3,d_mplBo)*qklm3Bo+klf(5,4,d_mplBo)*qklm4Bo+klf(5,5,d_mplBo)*qklm5Bo+klf(5,6,d_mplBo)*qklm6Bo+klf(5,7,d_mplBo)*qklm7Bo)+(klf(5,0,d_my)*qklm0+klf(5,1,d_my)*qklm1+klf(5,2,d_my)*qklm2+klf(5,3,d_my)*qklm3+klf(5,4,d_my)*qklm4+klf(5,5,d_my)*qklm5+klf(5,6,d_my)*qklm6+klf(5,7,d_my)*qklm7);

    q1v6 = -0.5*kInkT*(klf(6,0,d_mllT)*qklm0+klf(6,1,d_mllT)*qklm1+klf(6,2,d_mllT)*qklm2+klf(6,3,d_mllT)*qklm3+klf(6,4,d_mllT)*qklm4+klf(6,5,d_mllT)*qklm5+klf(6,6,d_mllT)*qklm6+klf(6,7,d_mllT)*qklm7)-0.5*kInkT*(klf(6,0,d_mplT)*qklm0T+klf(6,1,d_mplT)*qklm1T+klf(6,2,d_mplT)*qklm2T+klf(6,3,d_mplT)*qklm3T+klf(6,4,d_mplT)*qklm4T+klf(6,5,d_mplT)*qklm5T+klf(6,6,d_mplT)*qklm6T+klf(6,7,d_mplT)*qklm7T)+0.5*kInkBo*(klf(6,0,d_mllBo)*qklm0+klf(6,1,d_mllBo)*qklm1+klf(6,2,d_mllBo)*qklm2+klf(6,3,d_mllBo)*qklm3+klf(6,4,d_mllBo)*qklm4+klf(6,5,d_mllBo)*qklm5+klf(6,6,d_mllBo)*qklm6+klf(6,7,d_mllBo)*qklm7)+0.5*kInkBo*(klf(6,0,d_mplBo)*qklm0Bo+klf(6,1,d_mplBo)*qklm1Bo+klf(6,2,d_mplBo)*qklm2Bo+klf(6,3,d_mplBo)*qklm3Bo+klf(6,4,d_mplBo)*qklm4Bo+klf(6,5,d_mplBo)*qklm5Bo+klf(6,6,d_mplBo)*qklm6Bo+klf(6,7,d_mplBo)*qklm7Bo)+(klf(6,0,d_my)*qklm0+klf(6,1,d_my)*qklm1+klf(6,2,d_my)*qklm2+klf(6,3,d_my)*qklm3+klf(6,4,d_my)*qklm4+klf(6,5,d_my)*qklm5+klf(6,6,d_my)*qklm6+klf(6,7,d_my)*qklm7);

    q1v7 = -0.5*kInkT*(klf(7,0,d_mllT)*qklm0+klf(7,1,d_mllT)*qklm1+klf(7,2,d_mllT)*qklm2+klf(7,3,d_mllT)*qklm3+klf(7,4,d_mllT)*qklm4+klf(7,5,d_mllT)*qklm5+klf(7,6,d_mllT)*qklm6+klf(7,7,d_mllT)*qklm7)-0.5*kInkT*(klf(7,0,d_mplT)*qklm0T+klf(7,1,d_mplT)*qklm1T+klf(7,2,d_mplT)*qklm2T+klf(7,3,d_mplT)*qklm3T+klf(7,4,d_mplT)*qklm4T+klf(7,5,d_mplT)*qklm5T+klf(7,6,d_mplT)*qklm6T+klf(7,7,d_mplT)*qklm7T)+0.5*kInkBo*(klf(7,0,d_mllBo)*qklm0+klf(7,1,d_mllBo)*qklm1+klf(7,2,d_mllBo)*qklm2+klf(7,3,d_mllBo)*qklm3+klf(7,4,d_mllBo)*qklm4+klf(7,5,d_mllBo)*qklm5+klf(7,6,d_mllBo)*qklm6+klf(7,7,d_mllBo)*qklm7)+0.5*kInkBo*(klf(7,0,d_mplBo)*qklm0Bo+klf(7,1,d_mplBo)*qklm1Bo+klf(7,2,d_mplBo)*qklm2Bo+klf(7,3,d_mplBo)*qklm3Bo+klf(7,4,d_mplBo)*qklm4Bo+klf(7,5,d_mplBo)*qklm5Bo+klf(7,6,d_mplBo)*qklm6Bo+klf(7,7,d_mplBo)*qklm7Bo)+(klf(7,0,d_my)*qklm0+klf(7,1,d_my)*qklm1+klf(7,2,d_my)*qklm2+klf(7,3,d_my)*qklm3+klf(7,4,d_my)*qklm4+klf(7,5,d_my)*qklm5+klf(7,6,d_my)*qklm6+klf(7,7,d_my)*qklm7);


    
       
    float kInkF  = 1.0;
    float kInkBa = 1.0;
    
    
    if( klmf(ks,ls,ms+1,K+2,M+2,L+2,d_kM)+klmf(ks,ls,ms+1,K+2,M+2,L+2,d_kM) > 0 )
      kInkF = 2*klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)*klmf(ks,ls,ms+1,K+2,M+2,L+2,d_kM)/(klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)+klmf(ks,ls,ms+1,K+2,M+2,L+2,d_kM));
    else
      kInkF = 0;
    
    if( klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)+klmf(ks,ls,ms-1,K+2,M+2,L+2,d_kM) > 0 )
      kInkBa = 2*klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)*klmf(ks,ls,ms-1,K+2,M+2,L+2,d_kM)/(klmf(ks,ls,ms,K+2,M+2,L+2,d_kM)+klmf(ks,ls,ms-1,K+2,M+2,L+2,d_kM));
    else
      kInkBa = 0;


    qklm0  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,0); qklm1  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,1); qklm2  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,2); qklm3  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,3);
    qklm4  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,4); qklm5  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,5); qklm6  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,6); qklm7  = klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,7);

    qklm0F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_qklm2,0); qklm1F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_qklm2,1); qklm2F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_qklm2,2); qklm3F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_qklm2,3);
    qklm4F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_qklm2,4); qklm5F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_qklm2,5); qklm6F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_qklm2,6); qklm7F  = klmdf(k,l,m+1,Ks,Ms,Ls,d_qklm2,7);
    
    qklm0Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_qklm2,0); qklm1Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_qklm2,1); qklm2Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_qklm2,2); qklm3Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_qklm2,3);
    qklm4Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_qklm2,4); qklm5Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_qklm2,5); qklm6Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_qklm2,6); qklm7Ba  = klmdf(k,l,m-1,Ks,Ms,Ls,d_qklm2,7);

    

    // v00 = klf(0,0,d_mllF)*qklm0+klf(0,1,d_mllF)*qklm1+klf(0,2,d_mllF)*qklm2+klf(0,3,d_mllF)*qklm3+klf(0,4,d_mllF)*qklm4+klf(0,5,d_mllF)*qklm5+klf(0,6,d_mllF)*qklm6+klf(0,7,d_mllF)*qklm7;
    // v01 = klf(1,0,d_mllF)*qklm0+klf(1,1,d_mllF)*qklm1+klf(1,2,d_mllF)*qklm2+klf(1,3,d_mllF)*qklm3+klf(1,4,d_mllF)*qklm4+klf(1,5,d_mllF)*qklm5+klf(1,6,d_mllF)*qklm6+klf(1,7,d_mllF)*qklm7;
    // v02 = klf(2,0,d_mllF)*qklm0+klf(2,1,d_mllF)*qklm1+klf(2,2,d_mllF)*qklm2+klf(2,3,d_mllF)*qklm3+klf(2,4,d_mllF)*qklm4+klf(2,5,d_mllF)*qklm5+klf(2,6,d_mllF)*qklm6+klf(2,7,d_mllF)*qklm7;
    // v03 = klf(3,0,d_mllF)*qklm0+klf(3,1,d_mllF)*qklm1+klf(3,2,d_mllF)*qklm2+klf(3,3,d_mllF)*qklm3+klf(3,4,d_mllF)*qklm4+klf(3,5,d_mllF)*qklm5+klf(3,6,d_mllF)*qklm6+klf(3,7,d_mllF)*qklm7;
    // v04 = klf(4,0,d_mllF)*qklm0+klf(4,1,d_mllF)*qklm1+klf(4,2,d_mllF)*qklm2+klf(4,3,d_mllF)*qklm3+klf(4,4,d_mllF)*qklm4+klf(4,5,d_mllF)*qklm5+klf(4,6,d_mllF)*qklm6+klf(4,7,d_mllF)*qklm7;
    // v05 = klf(5,0,d_mllF)*qklm0+klf(5,1,d_mllF)*qklm1+klf(5,2,d_mllF)*qklm2+klf(5,3,d_mllF)*qklm3+klf(5,4,d_mllF)*qklm4+klf(5,5,d_mllF)*qklm5+klf(5,6,d_mllF)*qklm6+klf(5,7,d_mllF)*qklm7;
    // v06 = klf(6,0,d_mllF)*qklm0+klf(6,1,d_mllF)*qklm1+klf(6,2,d_mllF)*qklm2+klf(6,3,d_mllF)*qklm3+klf(6,4,d_mllF)*qklm4+klf(6,5,d_mllF)*qklm5+klf(6,6,d_mllF)*qklm6+klf(6,7,d_mllF)*qklm7;
    // v07 = klf(7,0,d_mllF)*qklm0+klf(7,1,d_mllF)*qklm1+klf(7,2,d_mllF)*qklm2+klf(7,3,d_mllF)*qklm3+klf(7,4,d_mllF)*qklm4+klf(7,5,d_mllF)*qklm5+klf(7,6,d_mllF)*qklm6+klf(7,7,d_mllF)*qklm7;


    // v10 = klf(0,0,d_mplF)*qklm0F+klf(0,1,d_mplF)*qklm1F+klf(0,2,d_mplF)*qklm2F+klf(0,3,d_mplF)*qklm3F+klf(0,4,d_mplF)*qklm4F+klf(0,5,d_mplF)*qklm5F+klf(0,6,d_mplF)*qklm6F+klf(0,7,d_mplF)*qklm7F;
    // v11 = klf(1,0,d_mplF)*qklm0F+klf(1,1,d_mplF)*qklm1F+klf(1,2,d_mplF)*qklm2F+klf(1,3,d_mplF)*qklm3F+klf(1,4,d_mplF)*qklm4F+klf(1,5,d_mplF)*qklm5F+klf(1,6,d_mplF)*qklm6F+klf(1,7,d_mplF)*qklm7F;
    // v12 = klf(2,0,d_mplF)*qklm0F+klf(2,1,d_mplF)*qklm1F+klf(2,2,d_mplF)*qklm2F+klf(2,3,d_mplF)*qklm3F+klf(2,4,d_mplF)*qklm4F+klf(2,5,d_mplF)*qklm5F+klf(2,6,d_mplF)*qklm6F+klf(2,7,d_mplF)*qklm7F;
    // v13 = klf(3,0,d_mplF)*qklm0F+klf(3,1,d_mplF)*qklm1F+klf(3,2,d_mplF)*qklm2F+klf(3,3,d_mplF)*qklm3F+klf(3,4,d_mplF)*qklm4F+klf(3,5,d_mplF)*qklm5F+klf(3,6,d_mplF)*qklm6F+klf(3,7,d_mplF)*qklm7F;
    // v14 = klf(4,0,d_mplF)*qklm0F+klf(4,1,d_mplF)*qklm1F+klf(4,2,d_mplF)*qklm2F+klf(4,3,d_mplF)*qklm3F+klf(4,4,d_mplF)*qklm4F+klf(4,5,d_mplF)*qklm5F+klf(4,6,d_mplF)*qklm6F+klf(4,7,d_mplF)*qklm7F;
    // v15 = klf(5,0,d_mplF)*qklm0F+klf(5,1,d_mplF)*qklm1F+klf(5,2,d_mplF)*qklm2F+klf(5,3,d_mplF)*qklm3F+klf(5,4,d_mplF)*qklm4F+klf(5,5,d_mplF)*qklm5F+klf(5,6,d_mplF)*qklm6F+klf(5,7,d_mplF)*qklm7F;
    // v16 = klf(6,0,d_mplF)*qklm0F+klf(6,1,d_mplF)*qklm1F+klf(6,2,d_mplF)*qklm2F+klf(6,3,d_mplF)*qklm3F+klf(6,4,d_mplF)*qklm4F+klf(6,5,d_mplF)*qklm5F+klf(6,6,d_mplF)*qklm6F+klf(6,7,d_mplF)*qklm7F;
    // v17 = klf(7,0,d_mplF)*qklm0F+klf(7,1,d_mplF)*qklm1F+klf(7,2,d_mplF)*qklm2F+klf(7,3,d_mplF)*qklm3F+klf(7,4,d_mplF)*qklm4F+klf(7,5,d_mplF)*qklm5F+klf(7,6,d_mplF)*qklm6F+klf(7,7,d_mplF)*qklm7F;

    
    // v20 = klf(0,0,d_mllBa)*qklm0+klf(0,1,d_mllBa)*qklm1+klf(0,2,d_mllBa)*qklm2+klf(0,3,d_mllBa)*qklm3+klf(0,4,d_mllBa)*qklm4+klf(0,5,d_mllBa)*qklm5+klf(0,6,d_mllBa)*qklm6+klf(0,7,d_mllBa)*qklm7;
    // v21 = klf(1,0,d_mllBa)*qklm0+klf(1,1,d_mllBa)*qklm1+klf(1,2,d_mllBa)*qklm2+klf(1,3,d_mllBa)*qklm3+klf(1,4,d_mllBa)*qklm4+klf(1,5,d_mllBa)*qklm5+klf(1,6,d_mllBa)*qklm6+klf(1,7,d_mllBa)*qklm7;
    // v22 = klf(2,0,d_mllBa)*qklm0+klf(2,1,d_mllBa)*qklm1+klf(2,2,d_mllBa)*qklm2+klf(2,3,d_mllBa)*qklm3+klf(2,4,d_mllBa)*qklm4+klf(2,5,d_mllBa)*qklm5+klf(2,6,d_mllBa)*qklm6+klf(2,7,d_mllBa)*qklm7;
    // v23 = klf(3,0,d_mllBa)*qklm0+klf(3,1,d_mllBa)*qklm1+klf(3,2,d_mllBa)*qklm2+klf(3,3,d_mllBa)*qklm3+klf(3,4,d_mllBa)*qklm4+klf(3,5,d_mllBa)*qklm5+klf(3,6,d_mllBa)*qklm6+klf(3,7,d_mllBa)*qklm7;
    // v24 = klf(4,0,d_mllBa)*qklm0+klf(4,1,d_mllBa)*qklm1+klf(4,2,d_mllBa)*qklm2+klf(4,3,d_mllBa)*qklm3+klf(4,4,d_mllBa)*qklm4+klf(4,5,d_mllBa)*qklm5+klf(4,6,d_mllBa)*qklm6+klf(4,7,d_mllBa)*qklm7;
    // v25 = klf(5,0,d_mllBa)*qklm0+klf(5,1,d_mllBa)*qklm1+klf(5,2,d_mllBa)*qklm2+klf(5,3,d_mllBa)*qklm3+klf(5,4,d_mllBa)*qklm4+klf(5,5,d_mllBa)*qklm5+klf(5,6,d_mllBa)*qklm6+klf(5,7,d_mllBa)*qklm7;
    // v26 = klf(6,0,d_mllBa)*qklm0+klf(6,1,d_mllBa)*qklm1+klf(6,2,d_mllBa)*qklm2+klf(6,3,d_mllBa)*qklm3+klf(6,4,d_mllBa)*qklm4+klf(6,5,d_mllBa)*qklm5+klf(6,6,d_mllBa)*qklm6+klf(6,7,d_mllBa)*qklm7;
    // v27 = klf(7,0,d_mllBa)*qklm0+klf(7,1,d_mllBa)*qklm1+klf(7,2,d_mllBa)*qklm2+klf(7,3,d_mllBa)*qklm3+klf(7,4,d_mllBa)*qklm4+klf(7,5,d_mllBa)*qklm5+klf(7,6,d_mllBa)*qklm6+klf(7,7,d_mllBa)*qklm7;


    // v30 = klf(0,0,d_mplBa)*qklm0Ba+klf(0,1,d_mplBa)*qklm1Ba+klf(0,2,d_mplBa)*qklm2Ba+klf(0,3,d_mplBa)*qklm3Ba+klf(0,4,d_mplBa)*qklm4Ba+klf(0,5,d_mplBa)*qklm5Ba+klf(0,6,d_mplBa)*qklm6Ba+klf(0,7,d_mplBa)*qklm7Ba;
    // v31 = klf(1,0,d_mplBa)*qklm0Ba+klf(1,1,d_mplBa)*qklm1Ba+klf(1,2,d_mplBa)*qklm2Ba+klf(1,3,d_mplBa)*qklm3Ba+klf(1,4,d_mplBa)*qklm4Ba+klf(1,5,d_mplBa)*qklm5Ba+klf(1,6,d_mplBa)*qklm6Ba+klf(1,7,d_mplBa)*qklm7Ba;
    // v32 = klf(2,0,d_mplBa)*qklm0Ba+klf(2,1,d_mplBa)*qklm1Ba+klf(2,2,d_mplBa)*qklm2Ba+klf(2,3,d_mplBa)*qklm3Ba+klf(2,4,d_mplBa)*qklm4Ba+klf(2,5,d_mplBa)*qklm5Ba+klf(2,6,d_mplBa)*qklm6Ba+klf(2,7,d_mplBa)*qklm7Ba;
    // v33 = klf(3,0,d_mplBa)*qklm0Ba+klf(3,1,d_mplBa)*qklm1Ba+klf(3,2,d_mplBa)*qklm2Ba+klf(3,3,d_mplBa)*qklm3Ba+klf(3,4,d_mplBa)*qklm4Ba+klf(3,5,d_mplBa)*qklm5Ba+klf(3,6,d_mplBa)*qklm6Ba+klf(3,7,d_mplBa)*qklm7Ba;
    // v34 = klf(4,0,d_mplBa)*qklm0Ba+klf(4,1,d_mplBa)*qklm1Ba+klf(4,2,d_mplBa)*qklm2Ba+klf(4,3,d_mplBa)*qklm3Ba+klf(4,4,d_mplBa)*qklm4Ba+klf(4,5,d_mplBa)*qklm5Ba+klf(4,6,d_mplBa)*qklm6Ba+klf(4,7,d_mplBa)*qklm7Ba;
    // v35 = klf(5,0,d_mplBa)*qklm0Ba+klf(5,1,d_mplBa)*qklm1Ba+klf(5,2,d_mplBa)*qklm2Ba+klf(5,3,d_mplBa)*qklm3Ba+klf(5,4,d_mplBa)*qklm4Ba+klf(5,5,d_mplBa)*qklm5Ba+klf(5,6,d_mplBa)*qklm6Ba+klf(5,7,d_mplBa)*qklm7Ba;
    // v36 = klf(6,0,d_mplBa)*qklm0Ba+klf(6,1,d_mplBa)*qklm1Ba+klf(6,2,d_mplBa)*qklm2Ba+klf(6,3,d_mplBa)*qklm3Ba+klf(6,4,d_mplBa)*qklm4Ba+klf(6,5,d_mplBa)*qklm5Ba+klf(6,6,d_mplBa)*qklm6Ba+klf(6,7,d_mplBa)*qklm7Ba;
    // v37 = klf(7,0,d_mplBa)*qklm0Ba+klf(7,1,d_mplBa)*qklm1Ba+klf(7,2,d_mplBa)*qklm2Ba+klf(7,3,d_mplBa)*qklm3Ba+klf(7,4,d_mplBa)*qklm4Ba+klf(7,5,d_mplBa)*qklm5Ba+klf(7,6,d_mplBa)*qklm6Ba+klf(7,7,d_mplBa)*qklm7Ba;

    // v40 = klf(0,0,d_mz)*qklm0+klf(0,1,d_mz)*qklm1+klf(0,2,d_mz)*qklm2+klf(0,3,d_mz)*qklm3+klf(0,4,d_mz)*qklm4+klf(0,5,d_mz)*qklm5+klf(0,6,d_mz)*qklm6+klf(0,7,d_mz)*qklm7;
    // v41 = klf(1,0,d_mz)*qklm0+klf(1,1,d_mz)*qklm1+klf(1,2,d_mz)*qklm2+klf(1,3,d_mz)*qklm3+klf(1,4,d_mz)*qklm4+klf(1,5,d_mz)*qklm5+klf(1,6,d_mz)*qklm6+klf(1,7,d_mz)*qklm7;
    // v42 = klf(2,0,d_mz)*qklm0+klf(2,1,d_mz)*qklm1+klf(2,2,d_mz)*qklm2+klf(2,3,d_mz)*qklm3+klf(2,4,d_mz)*qklm4+klf(2,5,d_mz)*qklm5+klf(2,6,d_mz)*qklm6+klf(2,7,d_mz)*qklm7;
    // v43 = klf(3,0,d_mz)*qklm0+klf(3,1,d_mz)*qklm1+klf(3,2,d_mz)*qklm2+klf(3,3,d_mz)*qklm3+klf(3,4,d_mz)*qklm4+klf(3,5,d_mz)*qklm5+klf(3,6,d_mz)*qklm6+klf(3,7,d_mz)*qklm7;
    // v44 = klf(4,0,d_mz)*qklm0+klf(4,1,d_mz)*qklm1+klf(4,2,d_mz)*qklm2+klf(4,3,d_mz)*qklm3+klf(4,4,d_mz)*qklm4+klf(4,5,d_mz)*qklm5+klf(4,6,d_mz)*qklm6+klf(4,7,d_mz)*qklm7;
    // v45 = klf(5,0,d_mz)*qklm0+klf(5,1,d_mz)*qklm1+klf(5,2,d_mz)*qklm2+klf(5,3,d_mz)*qklm3+klf(5,4,d_mz)*qklm4+klf(5,5,d_mz)*qklm5+klf(5,6,d_mz)*qklm6+klf(5,7,d_mz)*qklm7;
    // v46 = klf(6,0,d_mz)*qklm0+klf(6,1,d_mz)*qklm1+klf(6,2,d_mz)*qklm2+klf(6,3,d_mz)*qklm3+klf(6,4,d_mz)*qklm4+klf(6,5,d_mz)*qklm5+klf(6,6,d_mz)*qklm6+klf(6,7,d_mz)*qklm7;
    // v47 = klf(7,0,d_mz)*qklm0+klf(7,1,d_mz)*qklm1+klf(7,2,d_mz)*qklm2+klf(7,3,d_mz)*qklm3+klf(7,4,d_mz)*qklm4+klf(7,5,d_mz)*qklm5+klf(7,6,d_mz)*qklm6+klf(7,7,d_mz)*qklm7;
    
    // klmdf(k,l,m,K,L,M,d_qklm2,0,-0.5*kInkF*v00 - 0.5*kInkF*v10 + 0.5*kInkBa*v20 + 0.5*kInkBa*v30 + v40);
    // klmdf(k,l,m,K,L,M,d_qklm2,1,-0.5*kInkF*v01 - 0.5*kInkF*v11 + 0.5*kInkBa*v21 + 0.5*kInkBa*v31 + v41);
    // klmdf(k,l,m,K,L,M,d_qklm2,2,-0.5*kInkF*v02 - 0.5*kInkF*v12 + 0.5*kInkBa*v22 + 0.5*kInkBa*v32 + v42);
    // klmdf(k,l,m,K,L,M,d_qklm2,3,-0.5*kInkF*v03 - 0.5*kInkF*v13 + 0.5*kInkBa*v23 + 0.5*kInkBa*v33 + v43);
    // klmdf(k,l,m,K,L,M,d_qklm2,4,-0.5*kInkF*v04 - 0.5*kInkF*v14 + 0.5*kInkBa*v24 + 0.5*kInkBa*v34 + v44);
    // klmdf(k,l,m,K,L,M,d_qklm2,5,-0.5*kInkF*v05 - 0.5*kInkF*v15 + 0.5*kInkBa*v25 + 0.5*kInkBa*v35 + v45);
    // klmdf(k,l,m,K,L,M,d_qklm2,6,-0.5*kInkF*v06 - 0.5*kInkF*v16 + 0.5*kInkBa*v26 + 0.5*kInkBa*v36 + v46);
    // klmdf(k,l,m,K,L,M,d_qklm2,7,-0.5*kInkF*v07 - 0.5*kInkF*v17 + 0.5*kInkBa*v27 + 0.5*kInkBa*v37 + v47);
    
    // q2v0 = -0.5*kInkF*v00 - 0.5*kInkF*v10 + 0.5*kInkBa*v20 + 0.5*kInkBa*v30 + v40;
    // q2v1 = -0.5*kInkF*v01 - 0.5*kInkF*v11 + 0.5*kInkBa*v21 + 0.5*kInkBa*v31 + v41;
    // q2v2 = -0.5*kInkF*v02 - 0.5*kInkF*v12 + 0.5*kInkBa*v22 + 0.5*kInkBa*v32 + v42;
    // q2v3 = -0.5*kInkF*v03 - 0.5*kInkF*v13 + 0.5*kInkBa*v23 + 0.5*kInkBa*v33 + v43;
    // q2v4 = -0.5*kInkF*v04 - 0.5*kInkF*v14 + 0.5*kInkBa*v24 + 0.5*kInkBa*v34 + v44;
    // q2v5 = -0.5*kInkF*v05 - 0.5*kInkF*v15 + 0.5*kInkBa*v25 + 0.5*kInkBa*v35 + v45;
    // q2v6 = -0.5*kInkF*v06 - 0.5*kInkF*v16 + 0.5*kInkBa*v26 + 0.5*kInkBa*v36 + v46;
    // q2v7 = -0.5*kInkF*v07 - 0.5*kInkF*v17 + 0.5*kInkBa*v27 + 0.5*kInkBa*v37 + v47;

    
    q2v0 = -0.5*kInkF*(klf(0,0,d_mllF)*qklm0+klf(0,1,d_mllF)*qklm1+klf(0,2,d_mllF)*qklm2+klf(0,3,d_mllF)*qklm3+klf(0,4,d_mllF)*qklm4+klf(0,5,d_mllF)*qklm5+klf(0,6,d_mllF)*qklm6+klf(0,7,d_mllF)*qklm7)-0.5*kInkF*(klf(0,0,d_mplF)*qklm0F+klf(0,1,d_mplF)*qklm1F+klf(0,2,d_mplF)*qklm2F+klf(0,3,d_mplF)*qklm3F+klf(0,4,d_mplF)*qklm4F+klf(0,5,d_mplF)*qklm5F+klf(0,6,d_mplF)*qklm6F+klf(0,7,d_mplF)*qklm7F)+0.5*kInkBa*(klf(0,0,d_mllBa)*qklm0+klf(0,1,d_mllBa)*qklm1+klf(0,2,d_mllBa)*qklm2+klf(0,3,d_mllBa)*qklm3+klf(0,4,d_mllBa)*qklm4+klf(0,5,d_mllBa)*qklm5+klf(0,6,d_mllBa)*qklm6+klf(0,7,d_mllBa)*qklm7)+0.5*kInkBa*(klf(0,0,d_mplBa)*qklm0Ba+klf(0,1,d_mplBa)*qklm1Ba+klf(0,2,d_mplBa)*qklm2Ba+klf(0,3,d_mplBa)*qklm3Ba+klf(0,4,d_mplBa)*qklm4Ba+klf(0,5,d_mplBa)*qklm5Ba+klf(0,6,d_mplBa)*qklm6Ba+klf(0,7,d_mplBa)*qklm7Ba)+(klf(0,0,d_mz)*qklm0+klf(0,1,d_mz)*qklm1+klf(0,2,d_mz)*qklm2+klf(0,3,d_mz)*qklm3+klf(0,4,d_mz)*qklm4+klf(0,5,d_mz)*qklm5+klf(0,6,d_mz)*qklm6+klf(0,7,d_mz)*qklm7);
    
    q2v1 = -0.5*kInkF*(klf(1,0,d_mllF)*qklm0+klf(1,1,d_mllF)*qklm1+klf(1,2,d_mllF)*qklm2+klf(1,3,d_mllF)*qklm3+klf(1,4,d_mllF)*qklm4+klf(1,5,d_mllF)*qklm5+klf(1,6,d_mllF)*qklm6+klf(1,7,d_mllF)*qklm7)-0.5*kInkF*(klf(1,0,d_mplF)*qklm0F+klf(1,1,d_mplF)*qklm1F+klf(1,2,d_mplF)*qklm2F+klf(1,3,d_mplF)*qklm3F+klf(1,4,d_mplF)*qklm4F+klf(1,5,d_mplF)*qklm5F+klf(1,6,d_mplF)*qklm6F+klf(1,7,d_mplF)*qklm7F)+0.5*kInkBa*(klf(1,0,d_mllBa)*qklm0+klf(1,1,d_mllBa)*qklm1+klf(1,2,d_mllBa)*qklm2+klf(1,3,d_mllBa)*qklm3+klf(1,4,d_mllBa)*qklm4+klf(1,5,d_mllBa)*qklm5+klf(1,6,d_mllBa)*qklm6+klf(1,7,d_mllBa)*qklm7)+0.5*kInkBa*(klf(1,0,d_mplBa)*qklm0Ba+klf(1,1,d_mplBa)*qklm1Ba+klf(1,2,d_mplBa)*qklm2Ba+klf(1,3,d_mplBa)*qklm3Ba+klf(1,4,d_mplBa)*qklm4Ba+klf(1,5,d_mplBa)*qklm5Ba+klf(1,6,d_mplBa)*qklm6Ba+klf(1,7,d_mplBa)*qklm7Ba)+(klf(1,0,d_mz)*qklm0+klf(1,1,d_mz)*qklm1+klf(1,2,d_mz)*qklm2+klf(1,3,d_mz)*qklm3+klf(1,4,d_mz)*qklm4+klf(1,5,d_mz)*qklm5+klf(1,6,d_mz)*qklm6+klf(1,7,d_mz)*qklm7);

    q2v2 = -0.5*kInkF*(klf(2,0,d_mllF)*qklm0+klf(2,1,d_mllF)*qklm1+klf(2,2,d_mllF)*qklm2+klf(2,3,d_mllF)*qklm3+klf(2,4,d_mllF)*qklm4+klf(2,5,d_mllF)*qklm5+klf(2,6,d_mllF)*qklm6+klf(2,7,d_mllF)*qklm7)-0.5*kInkF*(klf(2,0,d_mplF)*qklm0F+klf(2,1,d_mplF)*qklm1F+klf(2,2,d_mplF)*qklm2F+klf(2,3,d_mplF)*qklm3F+klf(2,4,d_mplF)*qklm4F+klf(2,5,d_mplF)*qklm5F+klf(2,6,d_mplF)*qklm6F+klf(2,7,d_mplF)*qklm7F)+0.5*kInkBa*(klf(2,0,d_mllBa)*qklm0+klf(2,1,d_mllBa)*qklm1+klf(2,2,d_mllBa)*qklm2+klf(2,3,d_mllBa)*qklm3+klf(2,4,d_mllBa)*qklm4+klf(2,5,d_mllBa)*qklm5+klf(2,6,d_mllBa)*qklm6+klf(2,7,d_mllBa)*qklm7)+0.5*kInkBa*(klf(2,0,d_mplBa)*qklm0Ba+klf(2,1,d_mplBa)*qklm1Ba+klf(2,2,d_mplBa)*qklm2Ba+klf(2,3,d_mplBa)*qklm3Ba+klf(2,4,d_mplBa)*qklm4Ba+klf(2,5,d_mplBa)*qklm5Ba+klf(2,6,d_mplBa)*qklm6Ba+klf(2,7,d_mplBa)*qklm7Ba)+(klf(2,0,d_mz)*qklm0+klf(2,1,d_mz)*qklm1+klf(2,2,d_mz)*qklm2+klf(2,3,d_mz)*qklm3+klf(2,4,d_mz)*qklm4+klf(2,5,d_mz)*qklm5+klf(2,6,d_mz)*qklm6+klf(2,7,d_mz)*qklm7);

    q2v3 = -0.5*kInkF*(klf(3,0,d_mllF)*qklm0+klf(3,1,d_mllF)*qklm1+klf(3,2,d_mllF)*qklm2+klf(3,3,d_mllF)*qklm3+klf(3,4,d_mllF)*qklm4+klf(3,5,d_mllF)*qklm5+klf(3,6,d_mllF)*qklm6+klf(3,7,d_mllF)*qklm7)-0.5*kInkF*(klf(3,0,d_mplF)*qklm0F+klf(3,1,d_mplF)*qklm1F+klf(3,2,d_mplF)*qklm2F+klf(3,3,d_mplF)*qklm3F+klf(3,4,d_mplF)*qklm4F+klf(3,5,d_mplF)*qklm5F+klf(3,6,d_mplF)*qklm6F+klf(3,7,d_mplF)*qklm7F)+0.5*kInkBa*(klf(3,0,d_mllBa)*qklm0+klf(3,1,d_mllBa)*qklm1+klf(3,2,d_mllBa)*qklm2+klf(3,3,d_mllBa)*qklm3+klf(3,4,d_mllBa)*qklm4+klf(3,5,d_mllBa)*qklm5+klf(3,6,d_mllBa)*qklm6+klf(3,7,d_mllBa)*qklm7)+0.5*kInkBa*(klf(3,0,d_mplBa)*qklm0Ba+klf(3,1,d_mplBa)*qklm1Ba+klf(3,2,d_mplBa)*qklm2Ba+klf(3,3,d_mplBa)*qklm3Ba+klf(3,4,d_mplBa)*qklm4Ba+klf(3,5,d_mplBa)*qklm5Ba+klf(3,6,d_mplBa)*qklm6Ba+klf(3,7,d_mplBa)*qklm7Ba)+(klf(3,0,d_mz)*qklm0+klf(3,1,d_mz)*qklm1+klf(3,2,d_mz)*qklm2+klf(3,3,d_mz)*qklm3+klf(3,4,d_mz)*qklm4+klf(3,5,d_mz)*qklm5+klf(3,6,d_mz)*qklm6+klf(3,7,d_mz)*qklm7);


    q2v4 = -0.5*kInkF*( klf(4,0,d_mllF)*qklm0+klf(4,1,d_mllF)*qklm1+klf(4,2,d_mllF)*qklm2+klf(4,3,d_mllF)*qklm3+klf(4,4,d_mllF)*qklm4+klf(4,5,d_mllF)*qklm5+klf(4,6,d_mllF)*qklm6+klf(4,7,d_mllF)*qklm7)-0.5*kInkF*(klf(4,0,d_mplF)*qklm0F+klf(4,1,d_mplF)*qklm1F+klf(4,2,d_mplF)*qklm2F+klf(4,3,d_mplF)*qklm3F+klf(4,4,d_mplF)*qklm4F+klf(4,5,d_mplF)*qklm5F+klf(4,6,d_mplF)*qklm6F+klf(4,7,d_mplF)*qklm7F)+0.5*kInkBa*(klf(4,0,d_mllBa)*qklm0+klf(4,1,d_mllBa)*qklm1+klf(4,2,d_mllBa)*qklm2+klf(4,3,d_mllBa)*qklm3+klf(4,4,d_mllBa)*qklm4+klf(4,5,d_mllBa)*qklm5+klf(4,6,d_mllBa)*qklm6+klf(4,7,d_mllBa)*qklm7)+0.5*kInkBa*(klf(4,0,d_mplBa)*qklm0Ba+klf(4,1,d_mplBa)*qklm1Ba+klf(4,2,d_mplBa)*qklm2Ba+klf(4,3,d_mplBa)*qklm3Ba+klf(4,4,d_mplBa)*qklm4Ba+klf(4,5,d_mplBa)*qklm5Ba+klf(4,6,d_mplBa)*qklm6Ba+klf(4,7,d_mplBa)*qklm7Ba)+(klf(4,0,d_mz)*qklm0+klf(4,1,d_mz)*qklm1+klf(4,2,d_mz)*qklm2+klf(4,3,d_mz)*qklm3+klf(4,4,d_mz)*qklm4+klf(4,5,d_mz)*qklm5+klf(4,6,d_mz)*qklm6+klf(4,7,d_mz)*qklm7);

    q2v5 = -0.5*kInkF*(klf(5,0,d_mllF)*qklm0+klf(5,1,d_mllF)*qklm1+klf(5,2,d_mllF)*qklm2+klf(5,3,d_mllF)*qklm3+klf(5,4,d_mllF)*qklm4+klf(5,5,d_mllF)*qklm5+klf(5,6,d_mllF)*qklm6+klf(5,7,d_mllF)*qklm7)-0.5*kInkF*(klf(5,0,d_mplF)*qklm0F+klf(5,1,d_mplF)*qklm1F+klf(5,2,d_mplF)*qklm2F+klf(5,3,d_mplF)*qklm3F+klf(5,4,d_mplF)*qklm4F+klf(5,5,d_mplF)*qklm5F+klf(5,6,d_mplF)*qklm6F+klf(5,7,d_mplF)*qklm7F)+0.5*kInkBa*(klf(5,0,d_mllBa)*qklm0+klf(5,1,d_mllBa)*qklm1+klf(5,2,d_mllBa)*qklm2+klf(5,3,d_mllBa)*qklm3+klf(5,4,d_mllBa)*qklm4+klf(5,5,d_mllBa)*qklm5+klf(5,6,d_mllBa)*qklm6+klf(5,7,d_mllBa)*qklm7)+0.5*kInkBa*(klf(5,0,d_mplBa)*qklm0Ba+klf(5,1,d_mplBa)*qklm1Ba+klf(5,2,d_mplBa)*qklm2Ba+klf(5,3,d_mplBa)*qklm3Ba+klf(5,4,d_mplBa)*qklm4Ba+klf(5,5,d_mplBa)*qklm5Ba+klf(5,6,d_mplBa)*qklm6Ba+klf(5,7,d_mplBa)*qklm7Ba)+(klf(5,0,d_mz)*qklm0+klf(5,1,d_mz)*qklm1+klf(5,2,d_mz)*qklm2+klf(5,3,d_mz)*qklm3+klf(5,4,d_mz)*qklm4+klf(5,5,d_mz)*qklm5+klf(5,6,d_mz)*qklm6+klf(5,7,d_mz)*qklm7);

    q2v6 = -0.5*kInkF*(klf(6,0,d_mllF)*qklm0+klf(6,1,d_mllF)*qklm1+klf(6,2,d_mllF)*qklm2+klf(6,3,d_mllF)*qklm3+klf(6,4,d_mllF)*qklm4+klf(6,5,d_mllF)*qklm5+klf(6,6,d_mllF)*qklm6+klf(6,7,d_mllF)*qklm7)-0.5*kInkF*(klf(6,0,d_mplF)*qklm0F+klf(6,1,d_mplF)*qklm1F+klf(6,2,d_mplF)*qklm2F+klf(6,3,d_mplF)*qklm3F+klf(6,4,d_mplF)*qklm4F+klf(6,5,d_mplF)*qklm5F+klf(6,6,d_mplF)*qklm6F+klf(6,7,d_mplF)*qklm7F)+0.5*kInkBa*(klf(6,0,d_mllBa)*qklm0+klf(6,1,d_mllBa)*qklm1+klf(6,2,d_mllBa)*qklm2+klf(6,3,d_mllBa)*qklm3+klf(6,4,d_mllBa)*qklm4+klf(6,5,d_mllBa)*qklm5+klf(6,6,d_mllBa)*qklm6+klf(6,7,d_mllBa)*qklm7)+0.5*kInkBa*(klf(6,0,d_mplBa)*qklm0Ba+klf(6,1,d_mplBa)*qklm1Ba+klf(6,2,d_mplBa)*qklm2Ba+klf(6,3,d_mplBa)*qklm3Ba+klf(6,4,d_mplBa)*qklm4Ba+klf(6,5,d_mplBa)*qklm5Ba+klf(6,6,d_mplBa)*qklm6Ba+klf(6,7,d_mplBa)*qklm7Ba)+(klf(6,0,d_mz)*qklm0+klf(6,1,d_mz)*qklm1+klf(6,2,d_mz)*qklm2+klf(6,3,d_mz)*qklm3+klf(6,4,d_mz)*qklm4+klf(6,5,d_mz)*qklm5+klf(6,6,d_mz)*qklm6+klf(6,7,d_mz)*qklm7);

    q2v7 = -0.5*kInkF*(klf(7,0,d_mllF)*qklm0+klf(7,1,d_mllF)*qklm1+klf(7,2,d_mllF)*qklm2+klf(7,3,d_mllF)*qklm3+klf(7,4,d_mllF)*qklm4+klf(7,5,d_mllF)*qklm5+klf(7,6,d_mllF)*qklm6+klf(7,7,d_mllF)*qklm7)-0.5*kInkF*(klf(7,0,d_mplF)*qklm0F+klf(7,1,d_mplF)*qklm1F+klf(7,2,d_mplF)*qklm2F+klf(7,3,d_mplF)*qklm3F+klf(7,4,d_mplF)*qklm4F+klf(7,5,d_mplF)*qklm5F+klf(7,6,d_mplF)*qklm6F+klf(7,7,d_mplF)*qklm7F)+0.5*kInkBa*(klf(7,0,d_mllBa)*qklm0+klf(7,1,d_mllBa)*qklm1+klf(7,2,d_mllBa)*qklm2+klf(7,3,d_mllBa)*qklm3+klf(7,4,d_mllBa)*qklm4+klf(7,5,d_mllBa)*qklm5+klf(7,6,d_mllBa)*qklm6+klf(7,7,d_mllBa)*qklm7)+0.5*kInkBa*(klf(7,0,d_mplBa)*qklm0Ba+klf(7,1,d_mplBa)*qklm1Ba+klf(7,2,d_mplBa)*qklm2Ba+klf(7,3,d_mplBa)*qklm3Ba+klf(7,4,d_mplBa)*qklm4Ba+klf(7,5,d_mplBa)*qklm5Ba+klf(7,6,d_mplBa)*qklm6Ba+klf(7,7,d_mplBa)*qklm7Ba)+(klf(7,0,d_mz)*qklm0+klf(7,1,d_mz)*qklm1+klf(7,2,d_mz)*qklm2+klf(7,3,d_mz)*qklm3+klf(7,4,d_mz)*qklm4+klf(7,5,d_mz)*qklm5+klf(7,6,d_mz)*qklm6+klf(7,7,d_mz)*qklm7);


    
    // rhs0 = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,0) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,0) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,0);
    // rhs1 = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,1) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,1) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,1);    
    // rhs2 = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,2) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,2) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,2);
    // rhs3 = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,3) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,3) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,3);
    // rhs4 = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,4) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,4) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,4);    
    // rhs5 = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,5) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,5) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,5);
    // rhs6 = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,6) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,6) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,6);    
    // rhs7 = klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,7) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,7) + klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,7);


    rhs0 = q0v0+ q1v0 + q2v0;
    rhs1 = q0v1+ q1v1 + q2v1;
    rhs2 = q0v2+ q1v2 + q2v2;
    rhs3 = q0v3+ q1v3 + q2v3;
    rhs4 = q0v4+ q1v4 + q2v4;
    rhs5 = q0v5+ q1v5 + q2v5;
    rhs6 = q0v6+ q1v6 + q2v6;
    rhs7 = q0v7+ q1v7 + q2v7;



    rhs1 = rhs1 - ( klf(1,0,d_mltri)/klf(0,0,d_mltri) ) * rhs0;
    rhs2 = rhs2 - ( klf(2,0,d_mltri)/klf(0,0,d_mltri) ) * rhs0;
    rhs3 = rhs3 - ( klf(3,0,d_mltri)/klf(0,0,d_mltri) ) * rhs0;
    rhs4 = rhs4 - ( klf(4,0,d_mltri)/klf(0,0,d_mltri) ) * rhs0;
    rhs5 = rhs5 - ( klf(5,0,d_mltri)/klf(0,0,d_mltri) ) * rhs0;
    rhs6 = rhs6 - ( klf(6,0,d_mltri)/klf(0,0,d_mltri) ) * rhs0;
    rhs7 = rhs7 - ( klf(7,0,d_mltri)/klf(0,0,d_mltri) ) * rhs0;    
    
    rhs2 = rhs2 - ( klf(2,1,d_mltri)/klf(1,1,d_mltri) ) * rhs1;
    rhs3 = rhs3 - ( klf(3,1,d_mltri)/klf(1,1,d_mltri) ) * rhs1;
    rhs4 = rhs4 - ( klf(4,1,d_mltri)/klf(1,1,d_mltri) ) * rhs1;
    rhs5 = rhs5 - ( klf(5,1,d_mltri)/klf(1,1,d_mltri) ) * rhs1;
    rhs6 = rhs6 - ( klf(6,1,d_mltri)/klf(1,1,d_mltri) ) * rhs1;
    rhs7 = rhs7 - ( klf(7,1,d_mltri)/klf(1,1,d_mltri) ) * rhs1;
    
    rhs3 = rhs3 - ( klf(3,2,d_mltri)/klf(2,2,d_mltri) ) * rhs2;
    rhs4 = rhs4 - ( klf(4,2,d_mltri)/klf(2,2,d_mltri) ) * rhs2;
    rhs5 = rhs5 - ( klf(5,2,d_mltri)/klf(2,2,d_mltri) ) * rhs2;
    rhs6 = rhs6 - ( klf(6,2,d_mltri)/klf(2,2,d_mltri) ) * rhs2;
    rhs7 = rhs7 - ( klf(7,2,d_mltri)/klf(2,2,d_mltri) ) * rhs2;
    
    rhs4 = rhs4 - ( klf(4,3,d_mltri)/klf(3,3,d_mltri) ) * rhs3;
    rhs5 = rhs5 - ( klf(5,3,d_mltri)/klf(3,3,d_mltri) ) * rhs3;
    rhs6 = rhs6 - ( klf(6,3,d_mltri)/klf(3,3,d_mltri) ) * rhs3;
    rhs7 = rhs7 - ( klf(7,3,d_mltri)/klf(3,3,d_mltri) ) * rhs3;
    
    rhs5 = rhs5 - ( klf(5,4,d_mltri)/klf(4,4,d_mltri) ) * rhs4;
    rhs6 = rhs6 - ( klf(6,4,d_mltri)/klf(4,4,d_mltri) ) * rhs4;
    rhs7 = rhs7 - ( klf(7,4,d_mltri)/klf(4,4,d_mltri) ) * rhs4;    
    
    rhs6 = rhs6 - ( klf(6,5,d_mltri)/klf(5,5,d_mltri) ) * rhs5;
    rhs7 = rhs7 - ( klf(7,5,d_mltri)/klf(5,5,d_mltri) ) * rhs5;
        
    rhs7 = rhs7 - ( klf(7,6,d_mltri)/klf(6,6,d_mltri) ) * rhs6;


    x7 = (rhs7/klf(7,7,DIM,d_ml));
    x6 = (rhs6-klf(6,7,DIM,d_ml)*x7)/klf(6,6,DIM,d_ml);
    x5 = (rhs5-klf(5,6,DIM,d_ml)*x6-klf(5,7,DIM,d_ml)*x7)/klf(5,5,DIM,d_ml);
    x4 = (rhs4-klf(4,5,DIM,d_ml)*x5-klf(4,6,DIM,d_ml)*x6-klf(4,7,DIM,d_ml)*x7)/klf(4,4,DIM,d_ml);
    x3 = (rhs3-klf(3,4,DIM,d_ml)*x4-klf(3,5,DIM,d_ml)*x5-klf(3,6,DIM,d_ml)*x6-klf(3,7,DIM,d_ml)*x7)/klf(3,3,DIM,d_ml);
    x2 = (rhs2-klf(2,3,DIM,d_ml)*x3-klf(2,4,DIM,d_ml)*x4-klf(2,5,DIM,d_ml)*x5-klf(2,6,DIM,d_ml)*x6-klf(2,7,DIM,d_ml)*x7)/klf(2,2,DIM,d_ml);
    x1 = (rhs1-klf(1,2,DIM,d_ml)*x2-klf(1,3,DIM,d_ml)*x3-klf(1,4,DIM,d_ml)*x4-klf(1,5,DIM,d_ml)*x5-klf(1,6,DIM,d_ml)*x6-klf(1,7,DIM,d_ml)*x7)/klf(1,1,DIM,d_ml);
    x0 = (rhs0-klf(0,1,DIM,d_ml)*x1-klf(0,2,DIM,d_ml)*x2-klf(0,3,DIM,d_ml)*x3-klf(0,4,DIM,d_ml)*x4-klf(0,5,DIM,d_ml)*x5-klf(0,6,DIM,d_ml)*x6-klf(0,7,DIM,d_ml)*x7)/klf(0,0,DIM,d_ml);

    
    klmdf(k,l,m,Ks,Ms,Ls,d_rhs,0,x0);
    klmdf(k,l,m,Ks,Ms,Ls,d_rhs,1,x1);
    klmdf(k,l,m,Ks,Ms,Ls,d_rhs,2,x2);
    klmdf(k,l,m,Ks,Ms,Ls,d_rhs,3,x3);
    klmdf(k,l,m,Ks,Ms,Ls,d_rhs,4,x4);
    klmdf(k,l,m,Ks,Ms,Ls,d_rhs,5,x5);
    klmdf(k,l,m,Ks,Ms,Ls,d_rhs,6,x6);
    klmdf(k,l,m,Ks,Ms,Ls,d_rhs,7,x7);

  }

}



__global__ void d_zero_dArrays(int Ks, int Ls, int Ms,float* d_uklm, float * d_qklm0, float * d_qklm1, float * d_qklm2){


  int k = blockIdx.x * blockDim.x + threadIdx.x;
  int l = blockIdx.y * blockDim.y + threadIdx.y;
  int m = blockIdx.z * blockDim.z + threadIdx.z;

  
  if(  0 < k  && k < Ks+1  && 0 < l && l < Ls+1 && 0 < m &&  m < Ms+1 ){  

      
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,0,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,1,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,2,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,3,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,4,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,5,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,6,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,7,0);

    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,0,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,1,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,2,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,3,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,4,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,5,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,6,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm0,7,0);

    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,0,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,1,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,2,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,3,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,4,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,5,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,6,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm1,7,0);

    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,0,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,1,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,2,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,3,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,4,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,5,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,6,0);
    klmdf(k,l,m,Ks,Ms,Ls,d_qklm2,7,0);
    
    
  }
    
}


float integralf(int Ks, int Ls, int Ms, float dx, float dy, float dz, float *h_uklmc){

  float result = 0.0;
  
  
  for(int k=1; k < Ks+1; ++k)
    for(int l=1; l < Ls+1; ++l)
      for(int m=1; m < Ms+1; ++m){                
        if(klmf(k,l,m,Ks,Ms,Ls,h_uklmc) > 0){
          
          // if(max < klmf(k,l,m,Ks,Ms,Ls,h_uklmc))
          //   max = klmf(k,l,m,Ks,Ms,Ls,h_uklmc);
          
          
          result += klmf(k,l,m,Ks,Ms,Ls,h_uklmc)*dx*dy*dz;
        }        
      }
  
  //  bool nonUniform = false;
  //   if( klmf(k,l,m,Ks,Ms,Ls,h_uklmc)/result >= 10  ){
  //     cout << klmf(k,l,m,Ks,Ms,Ls,h_uklmc) << "  " << result << "  " << klmf(k,l,m,Ks,Ms,Ls,h_uklmc)/result << endl;
  //     nonUniform = true;
  //     goto theEnd;          
  //   }        
  // }
  //   theEnd: if(nonUniform)  result =  1.0;
  //   //return max;
    return result;
}


// //__global__ void d_center (int K, int L, int M, int kd, int ld, int md,  float* d_uklmc, float* d_uklmCent){
// __global__ void d_center (int K, int L, int M, int kd, int ld, int md, float integral, float* d_uklmc, float* d_uklmCent){
  
//   int k = blockIdx.x * blockDim.x + threadIdx.x;
//   int l = blockIdx.y * blockDim.y + threadIdx.y;
//   int m = blockIdx.z * blockDim.z + threadIdx.z;

//   int kc,lc,mc;
//   int alpha,beta,gamma;
//   int r,s,t;

//   kc = K/2;
//   lc = L/2;
//   mc = M/2;
  
//   if( (0 < k) && (k < K+1) &&  (0 < l) &&  (l < L+1) && (0 < m) && (m < M+1) ){

//     //printf("->%d %d %d\n",kd,ld,md);
//     r = kc - kd;
//     s = lc - ld;
//     t = mc - md;

//     alpha = k-1+r > K || k-1+r < 0 ? abs(K-abs(k-1+r)) : k-1+r;
//     beta  = l-1+s > L || l-1+s < 0 ? abs(L-abs(l-1+s)) : l-1+s;
//     gamma = m-1+t > M || m-1+t < 0 ? abs(M-abs(m-1+t)) : m-1+t;

//     //printf("->%d %d %d %d %d %d\n",kd,ld,md,alpha,beta,gamma);
//     //d_uklmCent(alpha,beta,gamma,0) = d_uklmc(k-1,l-1,m-1,0)/integral;
//     //klmf(alpha,beta,gamma,K+2,L+2,M+2,d_uklmc,klmf(k-1,l-1,m-1,K+2,L+2,M+2,d_uklmc)/integral);
//     klmf(alpha,beta,gamma,K+2,L+2,M+2,d_uklmCent,klmf(k-1,l-1,m-1,K+2,L+2,M+2,d_uklmc));



    
//   }
// }


__global__ void d_nonZeroSum (int Ks, int Ls, int Ms, float integral,  float * d_uklmc,  float* d_uklmNZSum, float* d_uklmSum){

  int k = blockIdx.x * blockDim.x + threadIdx.x;
  int l = blockIdx.y * blockDim.y + threadIdx.y;
  int m = blockIdx.z * blockDim.z + threadIdx.z;

  float nZero;
   
  if(  0 < k  && k < Ks+1  && 0 < l && l < Ls+1 && 0 < m &&  m < Ms+1 ){  
    if(klmf(k,l,m,Ks,Ms,Ls,d_uklmc) < 1e-10){
      nZero = 0.0;
      //klmf(k-1,l-1,m-1,Ks,Ms,Ls,d_uklmCent,0);
    }
    else{
      nZero = 1.0;
    }    
    
    klmf(k,l,m,Ks,Ls,Ms,d_uklmNZSum,  klmf(k,l,m,Ks,Ls,Ms,d_uklmNZSum) +  nZero  );
    klmf(k,l,m,Ks,Ls,Ms,d_uklmSum,    klmf(k,l,m,Ks,Ls,Ms,d_uklmSum)   +  klmf(k,l,m,Ks,Ms,Ls,d_uklmc)/integral  );    
  }
}





__global__ void d_average (int Ks, int Ls, int Ms, float * d_uklmNZSum, float * d_uklmSum){

  int k = blockIdx.x * blockDim.x + threadIdx.x;
  int l = blockIdx.y * blockDim.y + threadIdx.y;
  int m = blockIdx.z * blockDim.z + threadIdx.z;

  if(  0 < k  && k < Ks+1  && 0 < l && l < Ls+1 && 0 < m &&  m < Ms+1 ){  
    if( klmf(k,l,m,Ks,Ls,Ms,d_uklmNZSum)  > 0){      
      klmf(k,l,m,Ks,Ls,Ms,d_uklmSum,  (1.0/klmf(k,l,m,Ks,Ls,Ms,d_uklmNZSum))*klmf(k,l,m,Ks,Ls,Ms,d_uklmSum) );        
    }    
  }
}



__global__ void d_euler(int Ks, int Ls, int Ms, float dt, float* d_rhs, float* d_uklm){
  
  int k = blockIdx.x * blockDim.x + threadIdx.x;
  int l = blockIdx.y * blockDim.y + threadIdx.y;
  int m = blockIdx.z * blockDim.z + threadIdx.z;
  
  if(  0 < k  && k < Ks+1  && 0 < l && l < Ls+1 && 0 < m &&  m < Ms+1 ){  

    float u0,u1,u2,u3,u4,u5,u6,u7;
    
    u0 = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,0)  + dt*klmdf(k,l,m,Ks,Ms,Ls,d_rhs,0);
    u1 = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,1)  + dt*klmdf(k,l,m,Ks,Ms,Ls,d_rhs,1);
    u2 = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,2)  + dt*klmdf(k,l,m,Ks,Ms,Ls,d_rhs,2);
    u3 = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,3)  + dt*klmdf(k,l,m,Ks,Ms,Ls,d_rhs,3);
    u4 = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,4)  + dt*klmdf(k,l,m,Ks,Ms,Ls,d_rhs,4);
    u5 = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,5)  + dt*klmdf(k,l,m,Ks,Ms,Ls,d_rhs,5);
    u6 = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,6)  + dt*klmdf(k,l,m,Ks,Ms,Ls,d_rhs,6);
    u7 = klmdf(k,l,m,Ks,Ms,Ls,d_uklm,7)  + dt*klmdf(k,l,m,Ks,Ms,Ls,d_rhs,7);

    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,0,u0);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,1,u1);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,2,u2);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,3,u3);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,4,u4);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,5,u5);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,6,u6);
    klmdf(k,l,m,Ks,Ms,Ls,d_uklm,7,u7);


    // printf("uklm %d %d %d %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",k,l,m,
    //        klmdf(k,l,m,K+2,L+2,M+2,d_uklm,0),klmdf(k,l,m,K+2,L+2,M+2,d_uklm,1),klmdf(k,l,m,K+2,L+2,M+2,d_uklm,2),klmdf(k,l,m,K+2,L+2,M+2,d_uklm,3),
    //        klmdf(k,l,m,K+2,L+2,M+2,d_uklm,4),klmdf(k,l,m,K+2,L+2,M+2,d_uklm,5),klmdf(k,l,m,K+2,L+2,M+2,d_uklm,6),klmdf(k,l,m,K+2,L+2,M+2,d_uklm,7));

    
    //    printf(" euler \n");

    //printf("%d %d %d\n",k,l,m);
    // printf("k=%d l=%d m=%d dt=%.8f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",
    //        k,l,m,dt,d_uklm(k,l,m,0),d_uklm(k,l,m,1),d_uklm(k,l,m,2),d_uklm(k,l,m,3),
    //        d_uklm(k,l,m,4),d_uklm(k,l,m,5),d_uklm(k,l,m,6),d_uklm(k,l,m,7));

  }
  
}


void setDiffCoeff(int K, int L, int M, string nfile, float diffCoeff, float* h_k){

  ifstream file (nfile.c_str());
  int n,m;
  float t;
    
    if (file.is_open())
      {
        file >> n >> m;
        if(n == K && m == L){
          for(int i=1; i < K+1; i++){
            for(int j=1; j < L+1; j++){
              file >> t;
              klmf(i,j,1,K+2,L+2,M+2,h_k,diffCoeff*t);
              for(int m=2; m < M+1; ++m)
                klmf(i,j,m,K+2,L+2,M+2,h_k,klmf(i,j,1,K+2,L+2,M+2,h_k));
            }
          }          
        }
      } 
    file.close();
}

void setDeltas(int K, int L, int M, string nfile, float* h_deltas, int skip=1){
  
  ifstream file (nfile.c_str());
  int n,m;
  float t;
  
    if (file.is_open())
      {
        file >> n >> m;
        if(n == K && m == L){
          for(int i=1; i < K+1; i++){
            for(int j=1; j < L+1; j++){
              file >>  t;
              klmf(i,j,1,K+2,L+2,M+2,h_deltas,t);
              for(int m=2; m < M+1; m+=skip)
                klmf(i,j,m,K+2,L+2,M+2,h_deltas,t);
            }
          }          
        }
      } 
    file.close();

}



void read_myelinated_axons(float diffCoeff, float * h_k, float * h_deltas, vector<DIType> &indices){

  string line;
  vector <string> tokens; 
  float c;
  int K,L,M;

  float * temp;
  ifstream ifs ("./data/mat_myelinated_axons_small_binary_openning.mat", ifstream::in|ifstream::binary);
  getline(ifs, line);
  getline(ifs, line);
  getline(ifs, line);
  getline(ifs, line);
  getline(ifs, line);
  
  stringstream check1(line);    
  string intermediate; 
  
  while(getline(check1, intermediate, ' ')) 
    tokens.push_back(intermediate); 
  
  K = atoi(tokens[1].c_str());
  L = atoi(tokens[2].c_str());
  M = atoi(tokens[3].c_str());

  //cout << K << " " << L << " " << M << endl;
  
  int KK = 100;
  int LL = 100;
  int MM = 30;

  for(int i=0; i < (KK+2)*(LL+2)*(MM+2); ++i){
    h_k[i] = 1.0;
    //h_deltas[i] = 1.0;
  }

  
  for(int m=1; m < M+1; ++m)
    for(int l=1; l < L+1; ++l)
      for(int k=1; k < K+1; ++k){
        // klmf(k,l,m,KK+2,LL+2,MM+2,h_k,1.0);
        // klmf(k,l,m,KK+2,LL+2,MM+2,h_deltas,1.0);
        ifs >> c;
        if(c > 0){
          klmf(k,l,m,KK+2,LL+2,MM+2,h_k,0);
          //klmf(k,l,m,KK+2,LL+2,MM+2,h_deltas,0);
        }
        else{
          klmf(k,l,m,KK+2,LL+2,MM+2,h_k,diffCoeff);
          //klmf(k,l,m,KK+2,LL+2,MM+2,h_deltas,1.0);
        }
      }
  
  ifs.close();



  int skip_x = 50;
  int skip_y = 50;
  int skip_z = 10;
  
  for(int k=1; k < KK+1; k+=skip_x)
    for(int l=1; l < LL+1; l+=skip_y)
      for(int m=1; m < MM+1; m+=skip_z){        
        //if(m%2 == 0)
        if( klmf(k,l,m,KK+2,LL+2,MM+2,h_k) > 0 )
          indices.push_back({k,l,m});
          //klmf(k,l,m,KK+2,LL+2,MM+2,h_deltas,1);
      }



  //klmf((KK+2)/2,(LL+2)/2,(MM+2)/2,KK+2,LL+2,MM+2,h_deltas,1);
  
  

}



void read_axons(float diffCoeff, int Ks, int Ls, int Ms, int K, int L, int M, float * h_k,  vector<DIType> &indices){

  ifstream imyfile;
  string nfile = "./data/cylinders_gamma_voxelSize_50_1786-cylinders_0.45460819748_axons_Matrix_400x400_0.4.txt";
  cout << nfile << endl;
  imyfile.open(nfile.c_str());
  //imyfile.open("./data/substratesAT_axons_Matrix_100x100.txt");
  //int K, L;
  float c;

  imyfile >> K >> L;
  
  for(int k=0; k < K; ++k){
    for(int l=0; l < L; ++l){      
      imyfile >> c;
      klmf(k+1,l+1,1,K+2,L+2,M+2,h_k,diffCoeff*c);
      //klmf(k+1,l+1,1,K+2,L+2,M+2,h_deltas,c);
    }
  }
  
  imyfile.close();
  
  for(int k=0; k < K; ++k)
    for(int l=0; l < L; ++l)      
      for(int m=1; m < M; ++m){
        klmf(k+1,l+1,m+1,K+2,L+2,M+2,h_k,klmf(k+1,l+1,m,K+2,L+2,M+2,h_k));
        //klmf(k+1,l+1,m+1,K+2,L+2,M+2,h_deltas,klmf(k+1,l+1,m,K+2,L+2,M+2,h_deltas));
      }
  
  // //klmf((K+2)/2,(L+2)/2,(M+2)/2,K+2,L+2,M+2,h_deltas,1.0);

  int sOk = 138;
  int sOl = 138;
  
  int sNEX = 125;
  int sNEY = 125;
  
  // int zCI  = 137;  
  

  // int sOk = K/2-Ks/2;
  // int sOl = L/2-Ls/2;
  
  // int sNEX = Ks;
  // int sNEY = Ls;

  // int zCI  = M/2;  

  int skip_k = 15;
  int skip_l = 15;


  int zCI  = M/2;  
  
  for(int k=sOk; k < sOk+sNEX; k+=skip_k)
    for(int l=sOl; l < sOl+sNEY; l+=skip_l)      
      if(

         
         klmf(k-2,l-2,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k-1,l-2,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k,  l-2,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k+1,l-2,zCI,K+2,L+2,M+2,h_k) > 0 &&
         klmf(k+2,l-2,zCI,K+2,L+2,M+2,h_k) > 0 &&

         klmf(k-2,l-1,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k-1,l-1,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k,  l-1,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k+1,l-1,zCI,K+2,L+2,M+2,h_k) > 0 &&
         klmf(k+2,l-1,zCI,K+2,L+2,M+2,h_k) > 0 &&

         klmf(k-2,l,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k-1,l,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k,  l,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k+1,l,zCI,K+2,L+2,M+2,h_k) > 0 &&
         klmf(k+2,l,zCI,K+2,L+2,M+2,h_k) > 0 &&

         klmf(k-2,l+1,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k-1,l+1,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k,  l+1,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k+1,l+1,zCI,K+2,L+2,M+2,h_k) > 0 &&
         klmf(k+2,l+1,zCI,K+2,L+2,M+2,h_k) > 0 &&

         klmf(k-2,l+2,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k-1,l+2,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k,  l+2,zCI,K+2,L+2,M+2,h_k) > 0 &&         
         klmf(k+1,l+2,zCI,K+2,L+2,M+2,h_k) > 0 &&
         klmf(k+2,l+2,zCI,K+2,L+2,M+2,h_k) > 0 

         )
        {          
          indices.push_back({k,l,zCI});
        }

  
}


void setDelta(int k, int l, int m, int Ks, int Ls, int Ms, float* h_deltas){klmf(k+1,l+1,m+1,Ks,Ms,Ls,h_deltas,1.0);}
void initDeltas(int Ks, int Ls, int Ms, float *h_deltas){for(int i=0; i < Ks*Ls*Ms; ++i) h_deltas[i] = 0.0;}

int main(int argc, char* argv[]){

  

  if(argc < 15){
    cerr << "To run: ./aDiff3D a b c d e f K L M Ks Ls Ms alpha T" << endl;
    exit(0);
  }

  float a = atof(argv[1]);
  float b = atof(argv[2]);
  float c = atof(argv[3]);
  float d = atof(argv[4]);
  float e = atof(argv[5]);
  float f = atof(argv[6]);
  int   K = atoi(argv[7]);
  int   L = atoi(argv[8]);
  int   M = atoi(argv[9]);

  int   Ks = atoi(argv[10]);
  int   Ls = atoi(argv[11]);
  int   Ms = atoi(argv[12]);

  float alpha = atof(argv[13]);
  float T     = atof(argv[14]);

  float diffCoeff = 1.0;
  
  float dx = (b-a)/K;
  float dy = (d-c)/L;
  float dz = (f-e)/M;

  float mdxyz = dx;
  mdxyz = (mdxyz > dy) ? dy : mdxyz;  
  mdxyz = (mdxyz > dz) ? dz : mdxyz;
  

  // int Ks = 275;
  // int Ls = 275;
  // int Ms = 275;
  
  // int Ks = 275;
  // int Ls = 275;
  // int Ms = 275;




  
  //host variables
  float *h_ml,   *h_mltri;
  float *h_mllR, *h_mplR, *h_mllL,  *h_mplL,  *h_mx;
  float *h_mllT, *h_mplT, *h_mllBo, *h_mplBo, *h_my;
  float *h_mllF, *h_mplF, *h_mllBa, *h_mplBa, *h_mz;
  float *h_b;
  float *h_deltas, *h_k;
  //float *h_xIk,*h_yIl,*h_zIm;
  float *h_uklmc, *h_uklmSum;
  
  
  h_ml        = (float*) malloc(8*8*sizeof(float)); 
  h_mltri     = (float*) malloc(8*8*sizeof(float)); 
  h_mllR      = (float*) malloc(8*8*sizeof(float));
  h_mplR      = (float*) malloc(8*8*sizeof(float)); 
  h_mllL      = (float*) malloc(8*8*sizeof(float));
  h_mplL      = (float*) malloc(8*8*sizeof(float));
  h_mx        = (float*) malloc(8*8*sizeof(float));
  h_mllT      = (float*) malloc(8*8*sizeof(float));
  h_mplT      = (float*) malloc(8*8*sizeof(float));
  h_mllBo     = (float*) malloc(8*8*sizeof(float));
  h_mplBo     = (float*) malloc(8*8*sizeof(float));
  h_my        = (float*) malloc(8*8*sizeof(float));
  h_mllF      = (float*) malloc(8*8*sizeof(float));
  h_mplF      = (float*) malloc(8*8*sizeof(float));
  h_mllBa     = (float*) malloc(8*8*sizeof(float));
  h_mplBa     = (float*) malloc(8*8*sizeof(float));
  h_mz        = (float*) malloc(8*8*sizeof(float));
  h_b         = (float*) malloc(8*sizeof(float));


  // h_xIk       = (float*) malloc((K+3)*sizeof(float));
  // h_yIl       = (float*) malloc((L+3)*sizeof(float));
  // h_zIm       = (float*) malloc((M+3)*sizeof(float));

  //h_deltas    = (float*) malloc((K+2)*(L+2)*(M+2)*sizeof(float) ); //new float[(K+2)*(L+2)*(M+2)];
  // h_uklmc     = (float*) malloc((K+2)*(L+2)*(M+2)*sizeof(float));
  // h_uklmSum   = (float*) malloc(Ks*Ls*Ms*sizeof(float));



  h_k         = (float*) malloc((K+2)*(L+2)*(M+2)*sizeof(float) );   
  h_uklmc     = (float*) malloc((Ks+2)*(Ls+2)*(Ms+2)*sizeof(float));
  h_uklmSum   = (float*) malloc((Ks+2)*(Ls+2)*(Ms+2)*sizeof(float));

  h_deltas    = (float*) malloc((Ks+2)*(Ls+2)*(Ms+2)*sizeof(float));  
  klmf(Ks/2,Ls/2,Ms/2,Ks,Ls,Ms,h_deltas,1.0);

  init_host_Matrices(dx,dy,dz,h_ml,h_mltri,
                     h_mllR,h_mplR,h_mllL,h_mplL,h_mx,
                     h_mllT,h_mplT,h_mllBo,h_mplBo,h_my,
                     h_mllF,h_mplF,h_mllBa,h_mplBa,h_mz,h_b);  

  

  int nD;
  vector<DIType> indices;

  //Free diff
  // setDeltas(15,15,15,K,L,M,indices);  
  // nD = indices.size();
  // setDiffCoeff(K,L,M,h_k,diffCoeff);
  

  // read_myelinated_axons(diffCoeff,h_k,h_deltas,indices);
  // nD = indices.size();
  
  read_axons(diffCoeff,Ks,Ls,Ms,K,L,M,h_k,indices);
  nD = indices.size();
  
  // int zCI = 137;
  // for(int k=0; k < K+2; ++k){
  //   for(int l=0; l < L+2; ++l){
  //     cout << klmf(k,l,zCI,K+2,L+2,M+2,h_deltas) << " ";
  //   }
  //   cout << endl;
  // }
    
  //knots(a,b,c,d,e,f,K,L,M,h_xIk,h_yIl,h_zIm);


  //device variables
  float *d_ml, *d_mltri;
  float *d_mllR, *d_mplR, *d_mllL,  *d_mplL,  *d_mx;
  float *d_mllT, *d_mplT, *d_mllBo, *d_mplBo, *d_my;
  float *d_mllF, *d_mplF, *d_mllBa, *d_mplBa, *d_mz;
  float *d_b;
  float *d_deltas, *d_k;
  //float *d_xIk,*d_yIl,*d_zIm;
  float *d_uklm, *d_uklmc;
  float *d_qklm0, *d_qklm1,  *d_qklm2;
  //float *d_qklm0t, *d_qklm1t,  *d_qklm2t;
  float *d_rhs;
  float *d_uklmSum, *d_uklmNZSum,*d_uklmCent;
  
  cudaMalloc((void**)&d_ml,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mltri,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mllR,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mplR,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mllL,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mplL,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mx,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mllT,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mplT,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mllBo,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mplBo,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_my,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mllF,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mplF,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mllBa,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mplBa,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_mz,DIM*DIM*sizeof(float));
  cudaMalloc((void**)&d_b,DIM*sizeof(float));

  // cudaMalloc((void**)&d_xIk,(K+3)*sizeof(float));  
  // cudaMalloc((void**)&d_yIl,(K+3)*sizeof(float));
  // cudaMalloc((void**)&d_zIm,(K+3)*sizeof(float));



  cudaMalloc((void**)&d_deltas,(Ks+2)*(Ls+2)*(Ms+2)*sizeof(float));
  cudaMalloc((void**)&d_uklmc,(Ks+2)*(Ls+2)*(Ms+2)*sizeof(float));
  cudaMalloc((void**)&d_uklm,DIM*(Ks+2)*(Ls+2)*(Ms+2)*sizeof(float));  
  cudaMalloc((void**)&d_qklm0,DIM*(Ks+2)*(Ls+2)*(Ms+2)*sizeof(float));
  cudaMalloc((void**)&d_qklm1,DIM*(Ks+2)*(Ls+2)*(Ms+2)*sizeof(float));
  cudaMalloc((void**)&d_qklm2,DIM*(Ks+2)*(Ls+2)*(Ms+2)*sizeof(float));
  cudaMalloc((void**)&d_rhs,DIM*(Ks+2)*(Ls+2)*(Ms+2)*sizeof(float));
  cudaMalloc((void**)&d_uklmSum,(Ks+2)*(Ls+2)*(Ms+2)*sizeof(float));
  cudaMalloc((void**)&d_uklmNZSum,(Ks+2)*(Ls+2)*(Ms+2)*sizeof(float));
  cudaMalloc((void**)&d_uklmCent,(Ks+2)*(Ls+2)*(Ms+2)*sizeof(float));

  cudaMalloc((void**)&d_k,(K+2)*(L+2)*(M+2)*sizeof(float));


  // cudaMemcpy(d_xIk,h_xIk,(K+3)*sizeof(float),cudaMemcpyHostToDevice);
  // cudaMemcpy(d_yIl,h_yIl,(K+3)*sizeof(float),cudaMemcpyHostToDevice);
  // cudaMemcpy(d_zIm,h_zIm,(K+3)*sizeof(float),cudaMemcpyHostToDevice);  

  cudaMemcpy(d_ml,h_ml,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpy(d_mltri,h_mltri,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpy(d_mllR,h_mllR,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpy(d_mplR,h_mplR,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpy(d_mllL,h_mllL,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpy(d_mplL,h_mplL,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);  
  cudaMemcpy(d_mx,h_mx,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);  
  cudaMemcpy(d_mllT,h_mllT,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice); 
  cudaMemcpy(d_mplT,h_mplT,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);  
  cudaMemcpy(d_mllBo,h_mllBo,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpy(d_mplBo,h_mplBo,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);   
  cudaMemcpy(d_my,h_my,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpy(d_mllF,h_mllF,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);  
  cudaMemcpy(d_mplF,h_mplF,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpy(d_mllBa,h_mllBa,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpy(d_mplBa,h_mplBa,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpy(d_mz,h_mz,DIM*DIM*sizeof(float),cudaMemcpyHostToDevice);   
  cudaMemcpy(d_b,h_b,DIM*sizeof(float),cudaMemcpyHostToDevice);  
  

  cudaMemcpy(d_deltas,h_deltas,(Ks+2)*(Ls+2)*(Ms+2)*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpy(d_k,h_k,(K+2)*(L+2)*(M+2)*sizeof(float),cudaMemcpyHostToDevice);  

  
  //CUDA blocks and threads
  unsigned int grid_i = (Ks+2 + BLOCK_SIZE - 1) / BLOCK_SIZE;
  unsigned int grid_j = (Ls+2 + BLOCK_SIZE - 1) / BLOCK_SIZE;
  unsigned int grid_k = (Ms+2 + BLOCK_SIZE - 1) / BLOCK_SIZE;

  dim3 dimGrid(grid_i,grid_j,grid_k);
  dim3 dimBlock(BLOCK_SIZE,BLOCK_SIZE,BLOCK_SIZE);  
  // // cout << grid_i << " " << grid_j << " " << grid_k << endl;
  // // cout << BLOCK_SIZE << " " << BLOCK_SIZE << " " << BLOCK_SIZE << endl << endl;


  //float dt   = (alpha/diffCoeff)*(3*mdxyz*mdxyz);
  float dt   = (alpha/diffCoeff)*(dx*dx+dx*dx+dy*dy);
  
  int nSteps  = T/dt;
  float time = 0;
  cout << "nSteps: " << nSteps << endl;
  cout << "dt: " << dt << endl;
  cout << "dx " << dx << " dy " << dy << " dz " << dz << endl;
  cout << "Number of deltas " << nD << endl;
  
  cudaEvent_t start;
  cudaEvent_t stop;
  float msecTotal = 0;

  ofstream file;
  GNUplot plotter;
  GNUplot plotter1;
  GNUplot plotter2;
  GNUplot plotter3;
  string s;
  string nfile;
  int pause;
  int kd,ld,md;
  float integral;

  int skip_k = 1;
  int skip_l = 1;
  int skip_m = 1;

  
  cudaEventCreate(&start);
  cudaEventCreate(&stop);  
  cudaEventRecord(start);

  //nD = 1;
  for(int nd = 0; nd < nD; ++nd){
 
    d_zero_dArrays<<<dimGrid,dimBlock>>>(Ks,Ls,Ms,d_uklm,d_qklm0,d_qklm1,d_qklm2);


    //nd = 1;
    //cout << nd << " from " << nD << endl;

    kd = indices[nd].k;
    ld = indices[nd].l;
    md = indices[nd].m;
    

    //cout << kd << " " << ld << " " << md << endl;
    d_initUklm<<<dimGrid,dimBlock>>>(Ks,Ls,Ms,d_ml,d_mltri,d_b,d_deltas,d_uklm);
    cudaDeviceSynchronize();

    // // d_centerUklm<<<dimGrid,dimBlock>>>(Ks,Ls,Ms,dx,dy,dz,d_uklm,d_uklmc);
    // // cudaDeviceSynchronize();
    // // cudaMemcpy(h_uklmc,d_uklmc,Ks*Ls*Ms*sizeof(float),cudaMemcpyDeviceToHost);        
    // // string nfile =  "./data/uklm_0_subdomain_histo.txt";
    // // saveUklm(Ks,Ls,Ms,h_uklmc,nfile,skip_k,skip_l,skip_m);
    // // s = "set title "+string("\'")+"T_0"+string("\'")+";"+ " splot "+string("\'")+nfile+string("\'")+ " using 1:2:3:4 w p lt palette lw 10";
    // // //s = "set title "+string("\'")+"XY"+string("\'")+";"+ " unset key; set pm3d map; splot "+string("\'")+nfile+string("\'")+ " using 1:2:3 w p lt palette lw 10"; 
    // // //s = "unset key; set pm3d map; splot "+string("\'")+nfile+string("\'")+ " using 1:2:3 w p lt palette lw 10"; 
    // // plotter3(s.c_str());
    // // pause = cin.get();
    

    //nSteps = 1;
    for(int t=0; t < nSteps; t++)
      {
      
        //bdyConditions<<<dimGrid,dimBlock>>>(K,L,M,Ks,Ls,Ms,kd,ld,md,d_uklm,d_qklm0,d_qklm1,d_qklm2);
        //cudaDeviceSynchronize();
      
        d_qklm0f<<<dimGrid,dimBlock>>>(Ks,Ls,Ms,d_uklm,d_ml,d_mltri,d_mllR,d_mplR,d_mllL,d_mplL,d_mx,d_qklm0);
        d_qklm1f<<<dimGrid,dimBlock>>>(Ks,Ls,Ms,d_uklm,d_ml,d_mltri,d_mllT,d_mplT,d_mllBo,d_mplBo,d_my,d_qklm1);      
        d_qklm2f<<<dimGrid,dimBlock>>>(Ks,Ls,Ms,d_uklm,d_ml,d_mltri,d_mllF,d_mplF,d_mllBa,d_mplBa,d_mz,d_qklm2);      
        cudaDeviceSynchronize();

        d_rhsf<<<dimGrid,dimBlock>>>(K,L,M,Ks,Ls,Ms,kd,ld,md,
                                     d_k,d_ml,d_mltri,
                                     d_mllR,d_mplR,d_mllL,d_mplL,d_mx,
                                     d_mllT,d_mplT,d_mllBo,d_mplBo,d_my,
                                     d_mllF,d_mplF,d_mllBa,d_mplBa,d_mz,
                                     d_qklm0,d_qklm1,d_qklm2,d_rhs);

        cudaDeviceSynchronize();
        
        d_euler<<<dimGrid,dimBlock>>>(Ks,Ls,Ms,dt,d_rhs,d_uklm);      
        cudaDeviceSynchronize();

      
        // // for(int t=0; t < nSteps; ++t){
        // //   //RungeKutta 4 order
        // //   for(int i=0; i < 5; i++)
        // //     {
        // //       // ltime = time + rk4c[i]*dt;
        // //       rhsf(K,L,M);
        // //       resu = rk4a[i]*resu + dt*rhs;
        // //       uklm = uklm + rk4b[i]*resu;
        // //     }
        // //   time += dt;
        // // }

      
      }  
    
    d_centerUklm<<<dimGrid,dimBlock>>>(Ks,Ls,Ms,dx,dy,dz,d_uklm,d_uklmc);
    cudaDeviceSynchronize();

    cudaMemcpy(h_uklmc,d_uklmc,(Ks+2)*(Ls+2)*(Ms+2)*sizeof(float),cudaMemcpyDeviceToHost);        

    integral = integralf(Ks,Ls,Ms,dx,dy,dz,h_uklmc);      
    //cout << "integral "  <<integral << endl;
    // d_center<<<dimGrid,dimBlock>>>(K,L,M,kd,ld,md,integral,d_uklmc,d_uklmCent);
    // //d_center<<<dimGrid,dimBlock>>>(K,L,M,kd,ld,md,d_uklmc,d_uklmCent);
    // cudaDeviceSynchronize();

    //if(integral < 1)
    {
      d_nonZeroSum<<<dimGrid,dimBlock>>>(Ks,Ls,Ms,integral,d_uklmc,d_uklmNZSum,d_uklmSum);
      cudaDeviceSynchronize();
    }
  }  

  d_average<<<dimGrid,dimBlock>>>(Ks,Ls,Ms,d_uklmNZSum,d_uklmSum);
  cudaDeviceSynchronize();

  // //cudaMemcpy(h_uklmc,d_uklmc,(K+2)*(L+2)*(M+2)*sizeof(float),cudaMemcpyDeviceToHost);
  // //cudaMemcpy(h_uklmc,d_uklmCent,(K+2)*(L+2)*(M+2)*sizeof(float),cudaMemcpyDeviceToHost);
  // //cudaMemcpy(h_uklmc,d_uklmSum,(K+2)*(L+2)*(M+2)*sizeof(float),cudaMemcpyDeviceToHost);

  cudaMemcpy(h_uklmSum,d_uklmSum,(Ks+2)*(Ls+2)*(Ms+2)*sizeof(float),cudaMemcpyDeviceToHost);

  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  
  cudaEventElapsedTime(&msecTotal, start, stop);
  cudaThreadSynchronize();

  cout << "The elapsed time in GPU was " <<  msecTotal/1000 <<" sec."<< endl;


  //cout << "uklmc:" << endl <<uklmc << endl;
  nfile = "./data/uklm_histo_"+to_string(K)+"x"+to_string(L)+"x"+to_string(M)+"_subdomain_"+to_string(Ks)+"x"+to_string(Ls)+"x"+to_string(Ms)+"_deltas_"+to_string(nD)+"_TFinal_"+to_string(T)+"_nSteps_"+to_string(nSteps)+".txt";
  saveUklm(Ks,Ls,Ms,h_uklmSum,nfile,skip_k,skip_l,skip_m);
  //saveUklm(K,L,M,h_uklmc,h_xIk,h_yIl,h_zIm,nfile,skip_k,skip_l,skip_m);
  // s = "splot "+string("\'")+ nfile+string("\'") + "using 1:2:3:4 w p lt palette lw 10";
  // plotter(s.c_str());
  // // //pause = cin.get();

  
  // nfile =  "./data/uklm_T_subdomain_histo_x.txt";
  // s = "set title "+string("\'")+"YZ"+string("\'")+";"+ " unset key; set pm3d map; splot "+string("\'")+nfile+string("\'")+ " using 1:2:3 w p lt palette lw 10"; 
  // plotter1(s.c_str());
  // // //pause = cin.get();

  // nfile =  "./data/uklm_T_subdomain_histo_y.txt";
  // s = "set title "+string("\'")+"XZ"+string("\'")+";"+ " unset key; set pm3d map; splot "+string("\'")+nfile+string("\'")+ " using 1:2:3 w p lt palette lw 10"; 
  // //s = "unset key; set pm3d map; splot "+string("\'")+nfile+string("\'")+ " using 1:2:3 w p lt palette lw 10"; 
  // plotter2(s.c_str());
  // // //pause = cin.get();


  // nfile =  "./data/uklm_T_subdomain_histo_z.txt";
  // s = "set title "+string("\'")+"XY"+string("\'")+";"+ " unset key; set pm3d map;  splot "+string("\'")+nfile+string("\'")+ " using 1:2:3 w p lt palette lw 10";
  //s = "set title "+string("\'")+"XY"+string("\'")+";"+ " unset key; set pm3d map; splot "+string("\'")+nfile+string("\'")+ " using 1:2:3 w p lt palette lw 10"; 
  //s = "unset key; set pm3d map; splot "+string("\'")+nfile+string("\'")+ " using 1:2:3 w p lt palette lw 10"; 
  // plotter3(s.c_str());
  // pause = cin.get();



  delete [] h_ml;
  delete [] h_mllR;  delete [] h_mplR;    delete [] h_mllL;   delete [] h_mplL;   delete [] h_mx;  
  delete [] h_mllT;  delete [] h_mplT;    delete [] h_mllBo;  delete [] h_mplBo;  delete [] h_my;
  delete [] h_mllF;  delete [] h_mplF;    delete [] h_mllBa;  delete [] h_mplBa;  delete [] h_mz;
  delete [] h_b;     delete [] h_deltas;  delete [] h_k;      //delete [] h_xIk;    delete [] h_yIl;
  //delete [] h_zIm;
  delete [] h_uklmc;
 
  cudaFree(d_ml);
  cudaFree(d_mllR); cudaFree(d_mplR); cudaFree(d_mllL);  cudaFree(d_mplL);  cudaFree(d_mx);
  cudaFree(d_mllT); cudaFree(d_mplT); cudaFree(d_mllBo); cudaFree(d_mplBo); cudaFree(d_my);
  cudaFree(d_mllF); cudaFree(d_mplF); cudaFree(d_mllBa); cudaFree(d_mplBa); cudaFree(d_mz);
  cudaFree(d_b);  cudaFree(d_deltas); cudaFree(d_k);  //cudaFree(d_xIk);cudaFree(d_yIl); cudaFree(d_zIm);
  cudaFree(d_uklm); cudaFree(d_uklmc);
  cudaFree(d_qklm0); cudaFree(d_qklm1);  cudaFree(d_qklm2);
  cudaFree(d_rhs);

  
  return 0;
}









