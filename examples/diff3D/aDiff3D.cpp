//Para correr:
//./bin/aDiff3D_cpp -1 1 -1 1 -1 1 10 10 10 .001 .1
//./bin/aDiff3D_cpp -1 1 -1 1 -1 1 14 14 14 .001 .1
#include <num/TOperators.hpp>
#include <num/RungeKutta4Order.hpp>
#include <fstream>
#include <utils/GNUplot.hpp>

using namespace num;
using namespace std;

typedef ThreeDimMatrix<double,num::Dynamic,num::Dynamic,num::Dynamic,8> ThreeDimMatrixX8d;
typedef ThreeDimMatrix<double,num::Dynamic,num::Dynamic,num::Dynamic,1> ThreeDimMatrixX1d;
typedef Matrix<double,8,8,1> Matrix8d;
typedef Matrix<double,1,num::Dynamic,1> VectorXd;
typedef Matrix<double,num::Dynamic,1,1> ColumnXd;
typedef Matrix<double,8,1,1> Column8d;
typedef Matrix<double,1,8,1> Vector8d;


Matrix8d ml;
Matrix8d mllR, mplR, mllL,  mplL,  mx;
Matrix8d mllT, mplT, mllBo, mplBo, my;
Matrix8d mllF, mplF, mllBa, mplBa, mz;

VectorXd xIk,yIl,zIm;
VectorXd xkc,ylc,zmc;
Column8d bb,x;

ThreeDimMatrixX8d uklm,qklm0,qklm1,qklm2,rhs;
ThreeDimMatrixX1d uklmc,qklm0c,qklm1c,qklm2c;
ThreeDimMatrixX1d deltas,kM;

Column8d qklmt0,qklm0L,qklm0R;
Column8d qklmt1,qklm1T,qklm1Bo;
Column8d qklmt2,qklm2F,qklm2Ba;
Column8d rhst;

Column8d uklmt;
Column8d uklmL,uklmR;
Column8d uklmT,uklmBo;
Column8d uklmF,uklmBa;
Column8d qklm0t,qklm1t,qklm2t;
Column8d qklm1tt;



void initMatrices(double dx, double dy, double dz){

  ml  = {(dx*dy*dz)/27,  (dx*dy*dz)/54,  (dx*dy*dz)/108, (dx*dy*dz)/54,  (dx*dy*dz)/54,  (dx*dy*dz)/108, (dx*dy*dz)/216, (dx*dy*dz)/108,
         (dx*dy*dz)/54,  (dx*dy*dz)/27,  (dx*dy*dz)/54,  (dx*dy*dz)/108, (dx*dy*dz)/108, (dx*dy*dz)/54,  (dx*dy*dz)/108, (dx*dy*dz)/216,
         (dx*dy*dz)/108, (dx*dy*dz)/54,  (dx*dy*dz)/27,  (dx*dy*dz)/54,  (dx*dy*dz)/216, (dx*dy*dz)/108, (dx*dy*dz)/54,  (dx*dy*dz)/108,
         (dx*dy*dz)/54,  (dx*dy*dz)/108, (dx*dy*dz)/54,  (dx*dy*dz)/27,  (dx*dy*dz)/108, (dx*dy*dz)/216, (dx*dy*dz)/108, (dx*dy*dz)/54,
         (dx*dy*dz)/54,  (dx*dy*dz)/108, (dx*dy*dz)/216, (dx*dy*dz)/108, (dx*dy*dz)/27,  (dx*dy*dz)/54,  (dx*dy*dz)/108, (dx*dy*dz)/54,
         (dx*dy*dz)/108, (dx*dy*dz)/54,  (dx*dy*dz)/108, (dx*dy*dz)/216, (dx*dy*dz)/54,  (dx*dy*dz)/27,  (dx*dy*dz)/54,  (dx*dy*dz)/108,
         (dx*dy*dz)/216, (dx*dy*dz)/108, (dx*dy*dz)/54,  (dx*dy*dz)/108, (dx*dy*dz)/108, (dx*dy*dz)/54,  (dx*dy*dz)/27,  (dx*dy*dz)/54,
         (dx*dy*dz)/108, (dx*dy*dz)/216, (dx*dy*dz)/108, (dx*dy*dz)/54,  (dx*dy*dz)/54,  (dx*dy*dz)/108, (dx*dy*dz)/54,  (dx*dy*dz)/27};


  mllR = {0,  0,  0, 0, 0, 0,  0, 0, 
          0,  0,  0, 0, 0, 0,  0, 0,
          0,  0,  (dy*dz)/9,  (dy*dz)/18,  0,  0,  (dy*dz)/18,   (dy*dz)/36,
          0,  0,  (dy*dz)/18, (dy*dz)/9,   0,  0,  (dy*dz)/36,   (dy*dz)/18,
          0,  0,  0,    0,     0,  0,  0,      0,
          0,  0,  0,    0,     0,  0,  0,      0,
          0,  0,  (dy*dz)/18, (dy*dz)/36,  0,  0,  (dy*dz)/9,    (dy*dz)/18,
          0,  0,  (dy*dz)/36, (dy*dz)/18,  0,  0,  (dy*dz)/18,   (dy*dz)/9};
  

  mplR = {0,     0,     0,   0,   0,     0,     0,   0,
          0,     0,     0,   0,   0,     0,     0,   0,
          (dy*dz)/18,  (dy*dz)/9,   0,   0,   (dy*dz)/36,  (dy*dz)/18,  0,   0,
          (dy*dz)/9,   (dy*dz)/18,  0,   0,   (dy*dz)/18,  (dy*dz)/36,  0,   0,
          0,     0,     0,   0,   0,     0,     0,   0,
          0,     0,     0,   0,   0,     0,     0,   0,
          (dy*dz)/36,  (dy*dz)/18,  0,   0,   (dy*dz)/18,  (dy*dz)/9,   0,   0,
          (dy*dz)/18,  (dy*dz)/36,  0,   0,   (dy*dz)/9,   (dy*dz)/18,  0,   0};
  
  mllL = {(dy*dz)/9,   (dy*dz)/18,  0,  0,  (dy*dz)/18,  (dy*dz)/36,  0,  0,
          (dy*dz)/18,  (dy*dz)/9,   0,  0,  (dy*dz)/36,  (dy*dz)/18,  0,  0,
          0,     0,     0,  0,  0,     0,     0,  0,
          0,     0,     0,  0,  0,     0,     0,  0,
          (dy*dz)/18,  (dy*dz)/36,  0,  0,  (dy*dz)/9,   (dy*dz)/18,  0,  0,
          (dy*dz)/36,  (dy*dz)/18,  0,  0,  (dy*dz)/18,  (dy*dz)/9,   0,  0,
          0,     0,     0,  0,  0,     0,     0,  0,
          0,     0,     0,  0,  0,     0,     0,  0};

  
  
  mplL = {0,  0,  (dy*dz)/18,  (dy*dz)/9,   0,  0,  (dy*dz)/36,  (dy*dz)/18,
          0,  0,  (dy*dz)/9,   (dy*dz)/18,  0,  0,  (dy*dz)/18,  (dy*dz)/36,
          0,  0,  0,     0,     0,  0,  0,     0,
          0,  0,  0,     0,     0,  0,  0,     0,
          0,  0,  (dy*dz)/36,  (dy*dz)/18,  0,  0,  (dy*dz)/18,  (dy*dz)/9,
          0,  0,  (dy*dz)/18,  (dy*dz)/36,  0,  0,  (dy*dz)/9,   (dy*dz)/18,
          0,  0,  0,     0,     0,  0,  0,     0,
          0,  0,  0,     0,     0,  0,  0,     0};

  mx = {-(dy*dz)/18, -(dy*dz)/36, -(dy*dz)/36, -(dy*dz)/18, -(dy*dz)/36, -(dy*dz)/72, -(dy*dz)/72, -(dy*dz)/36,
        -(dy*dz)/36, -(dy*dz)/18, -(dy*dz)/18, -(dy*dz)/36, -(dy*dz)/72, -(dy*dz)/36, -(dy*dz)/36, -(dy*dz)/72,
        (dy*dz)/36,  (dy*dz)/18,  (dy*dz)/18,  (dy*dz)/36,  (dy*dz)/72,  (dy*dz)/36,  (dy*dz)/36,  (dy*dz)/72,
        (dy*dz)/18,  (dy*dz)/36,  (dy*dz)/36,  (dy*dz)/18,  (dy*dz)/36,  (dy*dz)/72,  (dy*dz)/72,  (dy*dz)/36,
        -(dy*dz)/36, -(dy*dz)/72, -(dy*dz)/72, -(dy*dz)/36, -(dy*dz)/18, -(dy*dz)/36, -(dy*dz)/36, -(dy*dz)/18,
        -(dy*dz)/72, -(dy*dz)/36, -(dy*dz)/36, -(dy*dz)/72, -(dy*dz)/36, -(dy*dz)/18, -(dy*dz)/18, -(dy*dz)/36,
         (dy*dz)/72,  (dy*dz)/36,  (dy*dz)/36,  (dy*dz)/72,  (dy*dz)/36,  (dy*dz)/18,  (dy*dz)/18,  (dy*dz)/36,
         (dy*dz)/36,  (dy*dz)/72,  (dy*dz)/72,  (dy*dz)/36,  (dy*dz)/18,  (dy*dz)/36,  (dy*dz)/36,  (dy*dz)/18};

  mllT = {0,    0,          0,         0,   0,     0,          0,      0,
          0, (dx*dz)/9,   (dx*dz)/18,  0,   0, (dx*dz)/18, (dx*dz)/36, 0,
          0, (dx*dz)/18,  (dx*dz)/9,   0,   0, (dx*dz)/36, (dx*dz)/18, 0,
          0,     0,          0,        0,   0,      0,        0,       0,
          0,     0,          0,        0,   0,      0,        0,       0,
          0, (dx*dz)/18,  (dx*dz)/36,  0,   0, (dx*dz)/9, (dx*dz)/18,  0,
          0, (dx*dz)/36,  (dx*dz)/18,  0,   0, (dx*dz)/18, (dx*dz)/9,  0,
          0,      0,         0,        0,   0,      0,         0,      0};
  
  mplT = {0, 0, 0, 0, 0, 0, 0, 0,
          (dx*dz)/9, 0, 0, (dx*dz)/18, (dx*dz)/18, 0, 0, (dx*dz)/36,
          (dx*dz)/18, 0, 0, (dx*dz)/9, (dx*dz)/36, 0, 0, (dx*dz)/18,
          0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0,
          (dx*dz)/18, 0, 0, (dx*dz)/36, (dx*dz)/9, 0, 0, (dx*dz)/18,
          (dx*dz)/36, 0, 0, (dx*dz)/18, (dx*dz)/18, 0, 0, (dx*dz)/9,
          0,    0, 0, 0,    0,    0, 0, 0};
  

  mllBo = {(dx*dz)/9,   0,   0,   (dx*dz)/18,  (dx*dz)/18,  0,  0,  (dx*dz)/36,
           0,     0,   0,   0,     0,     0,  0,  0,
           0,     0,   0,   0,     0,     0,  0,  0,
           (dx*dz)/18,  0,   0,   (dx*dz)/9,   (dx*dz)/36,  0,  0,  (dx*dz)/18,
           (dx*dz)/18,  0,   0,   (dx*dz)/36,  (dx*dz)/9,   0,  0,  (dx*dz)/18,
           0,     0,   0,   0,     0,     0,  0,  0,
           0,     0,   0,   0,     0,     0,  0,  0,
           (dx*dz)/36,  0,   0,   (dx*dz)/18,  (dx*dz)/18,  0,  0, (dx*dz)/9};
  
  mplBo = {0, (dx*dz)/9,  (dx*dz)/18,  0,   0,  (dx*dz)/18,  (dx*dz)/36,  0,
           0, 0,    0,     0,   0,  0,     0,     0,
           0, 0,    0,     0,   0,  0,     0,     0,
           0, (dx*dz)/18, (dx*dz)/9,   0,   0,  (dx*dz)/36,  (dx*dz)/18,  0,
           0, (dx*dz)/18, (dx*dz)/36,  0,   0,  (dx*dz)/9,   (dx*dz)/18,  0,
           0, 0,    0,     0,   0,  0,     0,     0,
           0, 0,    0,     0,   0,  0,     0,     0,
           0, (dx*dz)/36, (dx*dz)/18,  0,   0,  (dx*dz)/18,  (dx*dz)/9,   0};
  
  
  my = {-(dx*dz)/18, -(dx*dz)/18, -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/72, -(dx*dz)/72,
        (dx*dz)/18,  (dx*dz)/18,  (dx*dz)/36,  (dx*dz)/36,  (dx*dz)/36,  (dx*dz)/36,  (dx*dz)/72,  (dx*dz)/72,
        (dx*dz)/36,  (dx*dz)/36,  (dx*dz)/18,  (dx*dz)/18,  (dx*dz)/72,  (dx*dz)/72,  (dx*dz)/36,  (dx*dz)/36,
        -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/18, -(dx*dz)/18, -(dx*dz)/72, -(dx*dz)/72, -(dx*dz)/36, -(dx*dz)/36,
        -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/72, -(dx*dz)/72, -(dx*dz)/18, -(dx*dz)/18, -(dx*dz)/36, -(dx*dz)/36,
        (dx*dz)/36, (dx*dz)/36,    (dx*dz)/72,  (dx*dz)/72,  (dx*dz)/18, (dx*dz)/18, (dx*dz)/36, (dx*dz)/36,
        (dx*dz)/72, (dx*dz)/72,    (dx*dz)/36,  (dx*dz)/36,  (dx*dz)/36, (dx*dz)/36, (dx*dz)/18, (dx*dz)/18,
        -(dx*dz)/72, -(dx*dz)/72, -(dx*dz)/36,  -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/36, -(dx*dz)/18, -(dx*dz)/18};
  
  mllF = {0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, (dx*dy)/9, (dx*dy)/18, (dx*dy)/36, (dx*dy)/18,
          0, 0, 0, 0, (dx*dy)/18, (dx*dy)/9, (dx*dy)/18, (dx*dy)/36,
          0, 0, 0, 0, (dx*dy)/36, (dx*dy)/18, (dx*dy)/9, (dx*dy)/18,
          0, 0, 0, 0, (dx*dy)/18, (dx*dy)/36, (dx*dy)/18, (dx*dy)/9};
  
  mplF = {0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0,
          (dx*dy)/9, (dx*dy)/18, (dx*dy)/36, (dx*dy)/18, 0, 0, 0, 0,
          (dx*dy)/18, (dx*dy)/9, (dx*dy)/18, (dx*dy)/36, 0, 0, 0, 0,
          (dx*dy)/36, (dx*dy)/18, (dx*dy)/9, (dx*dy)/18, 0, 0, 0, 0,
          (dx*dy)/18, (dx*dy)/36, (dx*dy)/18, (dx*dy)/9, 0, 0, 0, 0};
  
  
  mllBa = {(dx*dy)/9, (dx*dy)/18, (dx*dy)/36, (dx*dy)/18, 0, 0, 0, 0,
           (dx*dy)/18, (dx*dy)/9, (dx*dy)/18, (dx*dy)/36, 0, 0, 0, 0,
           (dx*dy)/36, (dx*dy)/18, (dx*dy)/9, (dx*dy)/18, 0, 0, 0, 0,
           (dx*dy)/18, (dx*dy)/36, (dx*dy)/18, (dx*dy)/9, 0, 0, 0, 0,
           0,    0,    0,    0,   0, 0, 0, 0,
           0,    0,    0,    0,   0, 0, 0, 0,
           0,    0,    0,    0,   0, 0, 0, 0,
           0,    0,    0,    0,   0, 0, 0, 0};
  
  mplBa = {0, 0, 0, 0, (dx*dy)/9, (dx*dy)/18, (dx*dy)/36, (dx*dy)/18,
           0, 0, 0, 0, (dx*dy)/18, (dx*dy)/9, (dx*dy)/18, (dx*dy)/36,
           0, 0, 0, 0, (dx*dy)/36, (dx*dy)/18, (dx*dy)/9, (dx*dy)/18,
           0, 0, 0, 0, (dx*dy)/18, (dx*dy)/36, (dx*dy)/18, (dx*dy)/9,
           0, 0, 0, 0, 0,    0,    0,    0,
           0, 0, 0, 0, 0,    0,    0,    0,
           0, 0, 0, 0, 0,    0,    0,    0,
           0, 0, 0, 0, 0,    0,    0,    0};

  mz = {-(dx*dy)/18, -(dx*dy)/36, -(dx*dy)/72, -(dx*dy)/36, -(dx*dy)/18, -(dx*dy)/36, -(dx*dy)/72, -(dx*dy)/36,
        -(dx*dy)/36, -(dx*dy)/18, -(dx*dy)/36, -(dx*dy)/72, -(dx*dy)/36, -(dx*dy)/18, -(dx*dy)/36, -(dx*dy)/72,
        -(dx*dy)/72, -(dx*dy)/36, -(dx*dy)/18, -(dx*dy)/36, -(dx*dy)/72, -(dx*dy)/36, -(dx*dy)/18, -(dx*dy)/36,
        -(dx*dy)/36, -(dx*dy)/72, -(dx*dy)/36, -(dx*dy)/18, -(dx*dy)/36, -(dx*dy)/72, -(dx*dy)/36, -(dx*dy)/18,
        (dx*dy)/18,   (dx*dy)/36, (dx*dy)/72, (dx*dy)/36, (dx*dy)/18, (dx*dy)/36, (dx*dy)/72, (dx*dy)/36,
        (dx*dy)/36,   (dx*dy)/18, (dx*dy)/36, (dx*dy)/72, (dx*dy)/36, (dx*dy)/18, (dx*dy)/36, (dx*dy)/72,
        (dx*dy)/72,   (dx*dy)/36, (dx*dy)/18, (dx*dy)/36, (dx*dy)/72, (dx*dy)/36, (dx*dy)/18, (dx*dy)/36,
        (dx*dy)/36,   (dx*dy)/72, (dx*dy)/36, (dx*dy)/18, (dx*dy)/36, (dx*dy)/72, (dx*dy)/36, (dx*dy)/18};
  
  
}

void knots(float a, float b, float c, float d, float e, float f, int K, int L, int M){
  
  float dx = (b-a)/K;
  float dy = (d-c)/L;
  float dz = (f-e)/M;
  
  xIk.resize(K+3);
  yIl.resize(L+3);
  zIm.resize(L+3);

  
  xkc.resize(K);
  ylc.resize(K);
  zmc.resize(K);

  xIk(0) = a - dx;
  for(int i=1; i < K+2; i++)
    xIk(i) = a + ((b-a)/K)*(i-1);
  xIk(K+2) = b + dx;
  
  yIl(0) = c - dy;
  for(int i=1; i < L+2; i++)
    yIl(i) = c + ((d-c)/L)*(i-1);
  yIl(L+2) = d + dy;

  zIm(0) = e - dz;
  for(int i=1; i < M+2; i++)
    zIm(i) = e + ((f-e)/M)*(i-1);
  zIm(M+2) = f + dz;

  for(int i=1; i < K+1; i++)
    xkc(i-1) = 0.5*(xIk(i)+xIk(i+1));

  for(int j=1; j < L+1; j++)
    ylc(j-1) = 0.5*(yIl(j)+yIl(j+1));

  for(int k=1; k < M+1; k++)
    zmc(k-1) = 0.5*(zIm(k)+zIm(k+1));
  
}


void initUklm(float K, float L, float M, double dx, double dy, double dz){
  //double  b[8];
  double  x[8];
  double delta;
  
  double b[8] = {(dx*dy*dz)/8,
                 (dx*dy*dz)/8,
                 (dx*dy*dz)/8,
                 (dx*dy*dz)/8,
                 (dx*dy*dz)/8,
                 (dx*dy*dz)/8,
                 (dx*dy*dz)/8,
                 (dx*dy*dz)/8};

  for (int k=1; k < K+1; ++k)
    for(int l=1; l < L+1; ++l)
      for(int m=1; m < M+1; ++m){
        delta = deltas(k,l,m,0);

        ml.solve8x8(delta*b[0],delta*b[1],delta*b[2],delta*b[3],delta*b[4],delta*b[5],delta*b[6],delta*b[7],
                    &x[0],&x[1],&x[2],&x[3],&x[4],&x[5],&x[6],&x[7]);
        
        uklm(k,l,m,0) = x[0];
        uklm(k,l,m,1) = x[1];
        uklm(k,l,m,2) = x[2];
        uklm(k,l,m,3) = x[3];
        uklm(k,l,m,4) = x[4];
        uklm(k,l,m,5) = x[5];
        uklm(k,l,m,6) = x[6];
        uklm(k,l,m,7) = x[7];        
        
      }
}


inline double phi0(double x,double y,double z,double xkl12,double xkp12,double yll12,double ylp12,double zml12,double zmp12){
  return ((x-xkp12)/(xkl12-xkp12))*((y-ylp12)/(yll12-ylp12))*((z-zmp12)/(zml12-zmp12));
}
inline double phi1(double x,double y,double z,double xkl12,double xkp12,double yll12,double ylp12,double zml12,double zmp12){
  return ((x-xkp12)/(xkl12-xkp12))*((y-yll12)/(ylp12-yll12))*((z-zmp12)/(zml12-zmp12));
}
inline double phi2(double x,double y,double z,double xkl12,double xkp12,double yll12,double ylp12,double zml12,double zmp12){
  return ((x-xkl12)/(xkp12-xkl12))*((y-yll12)/(ylp12-yll12))*((z-zmp12)/(zml12-zmp12));
}
inline double phi3(double x,double y,double z,double xkl12,double xkp12,double yll12,double ylp12,double zml12,double zmp12){
  return ((x-xkl12)/(xkp12-xkl12))*((y-ylp12)/(yll12-ylp12))*((z-zmp12)/(zml12-zmp12));
}
inline double phi4(double x,double y,double z,double xkl12,double xkp12,double yll12,double ylp12,double zml12,double zmp12){
  return ((x-xkp12)/(xkl12-xkp12))*((y-ylp12)/(yll12-ylp12))*((z-zml12)/(zmp12-zml12));
}
inline double phi5(double x,double y,double z,double xkl12,double xkp12,double yll12,double ylp12,double zml12,double zmp12){
  return ((x-xkp12)/(xkl12-xkp12))*((y-yll12)/(ylp12-yll12))*((z-zml12)/(zmp12-zml12));
}
inline double phi6(double x,double y,double z,double xkl12,double xkp12,double yll12,double ylp12,double zml12,double zmp12){
  return ((x-xkl12)/(xkp12-xkl12))*((y-yll12)/(ylp12-yll12))*((z-zml12)/(zmp12-zml12));
}
inline double phi7(double x,double y,double z,double xkl12,double xkp12,double yll12,double ylp12,double zml12,double zmp12){
  return ((x-xkl12)/(xkp12-xkl12))*((y-ylp12)/(yll12-ylp12))*((z-zml12)/(zmp12-zml12));
}

void centerUklm(size_t K, size_t L, size_t M){ 

  double xkl12, xkp12, yll12, ylp12, zml12, zmp12;
  double xc,yc,zc;
  
  for(size_t k=1; k < K+1; ++k)
    for(size_t l=1; l < L+1; ++l)
      for(size_t m=1; m < M+1; ++m){

        xkl12 = xIk(k);
        xkp12 = xIk(k+1);
        yll12 = yIl(l);
        ylp12 = yIl(l+1);
        zml12 = zIm(m);
        zmp12 = zIm(m+1);

        xc = 0.5*(xkl12+xkp12);
        yc = 0.5*(yll12+ylp12);
        zc = 0.5*(zml12+zmp12);

                
        uklmc(k-1,l-1,m-1,0) =
          uklm(k,l,m,0)*phi0(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12)
          +uklm(k,l,m,1)*phi1(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12)
          +uklm(k,l,m,2)*phi2(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12)
          +uklm(k,l,m,3)*phi3(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12)
          +uklm(k,l,m,4)*phi4(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12)
          +uklm(k,l,m,5)*phi5(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12)
          +uklm(k,l,m,6)*phi6(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12)
          +uklm(k,l,m,7)*phi7(xc,yc,zc,xkl12,xkp12,yll12,ylp12,zml12,zmp12);                               
      }
}

void saveUklm(int K, int L, int M, string nfile){

  double xkl12, xkp12, yll12, ylp12, zml12, zmp12;
  double xc,yc,zc;
  
  ofstream file(nfile);
  std::string token = nfile.substr(0, nfile.find(".txt"));
  ofstream file3;
  ofstream file4;
  ofstream file5;
  

  file3.open ( (token+"_x.txt").c_str());
  file4.open ( (token+"_y.txt").c_str());
  file5.open ( (token+"_z.txt").c_str());
 
  for(int k=1; k < K+1; ++k){
    for(int l=1; l < L+1; ++l){
      for(int m=1; m < M+1; ++m){
        
        xkl12 = xIk(k);
        xkp12 = xIk(k+1);
        yll12 = yIl(l);
        ylp12 = yIl(l+1);
        zml12 = zIm(m);
        zmp12 = zIm(m+1);

        xc = 0.5*(xkl12+xkp12);
        yc = 0.5*(yll12+ylp12);
        zc = 0.5*(zml12+zmp12);

        if(uklmc(k-1,l-1,m-1,0) < 0 )
          file <<  xc <<" " << yc << " " << zc <<" " << 0 << " " << endl;
        else
          file <<  xc <<" " << yc << " " << zc <<" " <<uklmc(k-1,l-1,m-1,0) << " " << endl;        
      }
    }
    file << endl;
  }   
  file.close();

  for(int k=1; k < K+1; ++k){
    for(int l=1; l < L+1; ++l){
      for(int m=1; m < M+1; ++m){
        
        xkl12 = xIk(k);
        xkp12 = xIk(k+1);
        yll12 = yIl(l);
        ylp12 = yIl(l+1);
        zml12 = zIm(m);
        zmp12 = zIm(m+1);

        
        xc = 0.5*(xkl12+xkp12);
        yc = 0.5*(yll12+ylp12);
        zc = 0.5*(zml12+zmp12);
        
        
        if( k == K/2){          
          if(uklmc(k-1, l-1, m-1,0) < 0 ){
            file3 <<  yc <<" " << zc << " "  << 0 << " " << endl;         
          }
          else{              
            file3 <<  yc <<" " << zc << " " << uklmc(k-1,l-1,m-1) << " " << endl;        
          }
        }


        if(l == L/2){
          if(uklmc(k-1,l-1,m-1,0) < 0 ){
            file4 <<  xc <<" " << zc  << " " << 0 << endl;         
          }
          else{              
            file4 <<  xc <<" " << zc << " " << uklmc(k-1,l-1,m-1) << " " << endl;        
          }
        }
        
        
        if( m == M/2){            
          if(uklmc(k-1,l-1,m-1,0) < 0 ){
            file5 <<  xc <<" " << yc << " "  << 0 << " " << endl;         
          }
          else{              
            file5 <<  xc <<" " << yc << " " << uklmc(k-1,l-1,m-1) << " " << endl;        
          }            
        }
        
        
      }
    }    
  }
  
  file3.close();
  file4.close();
  file5.close();

}


void qklmf(int K, int L, int M){

  // Column8d uklmt;
  // Column8d uklmL,uklmR;
  // Column8d uklmT,uklmBo;
  // Column8d uklmF,uklmBa;
  // Column8d qklm0t,qklm1t,qklm2t;
  // Column8d qklm1tt;
  double  x[8];
  
  for(int k=1; k < K+1; ++k)
    for(int l=1; l < L+1; ++l)
      for(int m=1; m < M+1; ++m){
        
        uklmt(0) = uklm(k,l,m,0); uklmt(1) = uklm(k,l,m,1); uklmt(2) = uklm(k,l,m,2); uklmt(3) = uklm(k,l,m,3);
        uklmt(4) = uklm(k,l,m,4); uklmt(5) = uklm(k,l,m,5); uklmt(6) = uklm(k,l,m,6); uklmt(7) = uklm(k,l,m,7);

        // cout << k << " " << l << " " << m <<" "<< uklmt(0) << " " <<uklmt(1) <<" "  <<uklmt(2) << " " <<uklmt(3) <<" " <<
        //   uklmt(4) << " " <<uklmt(5) <<" "  << uklmt(6) << " " <<uklmt(7) << endl;;
        

        uklmR(0) = uklm(k,l+1,m,0); uklmR(1) = uklm(k,l+1,m,1); uklmR(2) = uklm(k,l+1,m,2); uklmR(3) = uklm(k,l+1,m,3);
        uklmR(4) = uklm(k,l+1,m,4); uklmR(5) = uklm(k,l+1,m,5); uklmR(6) = uklm(k,l+1,m,6); uklmR(7) = uklm(k,l+1,m,7);

        // cout << k << " " << l+1 << " " << m <<" "<< uklmR(0) << " " <<uklmR(1) <<" "  <<uklmR(2) << " " <<uklmR(3) <<" " <<
        //   uklmR(4) << " " <<uklmR(5) <<" "  << uklmR(6) << " " <<uklmR(7) << endl;;
       

        uklmL(0) = uklm(k,l-1,m,0); uklmL(1) = uklm(k,l-1,m,1); uklmL(2) = uklm(k,l-1,m,2); uklmL(3) = uklm(k,l-1,m,3);
        uklmL(4) = uklm(k,l-1,m,4); uklmL(5) = uklm(k,l-1,m,5); uklmL(6) = uklm(k,l-1,m,6); uklmL(7) = uklm(k,l-1,m,7);

        // cout << k << " " << l-1 << " " << m <<" "<< uklmL(0) << " " <<uklmL(1) <<" "  <<uklmL(2) << " " <<uklmL(3) <<" " <<
        //   uklmL(4) << " " <<uklmL(5) <<" "  << uklmL(6) << " " <<uklmL(7) << endl;;
       
        
        
        qklm0t =   -0.5*mllR*uklmt - 0.5*mplR*uklmR + 0.5*mllL*uklmt + 0.5*mplL*uklmL + mx*uklmt;

        
        ml.solve8x8(qklm0t(0),qklm0t(1),qklm0t(2),qklm0t(3),qklm0t(4),qklm0t(5),qklm0t(6),qklm0t(7),
                     &x[0],&x[1],&x[2],&x[3],&x[4],&x[5],&x[6],&x[7]);

        
        qklm0(k,l,m,0) = x[0]; qklm0(k,l,m,1) = x[1]; qklm0(k,l,m,2) = x[2]; qklm0(k,l,m,3) = x[3];
        qklm0(k,l,m,4) = x[4]; qklm0(k,l,m,5) = x[5]; qklm0(k,l,m,6) = x[6]; qklm0(k,l,m,7) = x[7];

        
        // cout << "qklm0 " << k << " " << l << " " << m << " " << qklm0(k,l,m,0) << " " << qklm0(k,l,m,1) <<" " << qklm0(k,l,m,2) << " " << qklm0(k,l,m,3) << " " <<
        //   qklm0(k,l,m,4) << " " << qklm0(k,l,m,5) <<" " << qklm0(k,l,m,6) << " " << qklm0(k,l,m,7) << endl;


        uklmT(0) = uklm(k-1,l,m,0); uklmT(1) = uklm(k-1,l,m,1); uklmT(2) = uklm(k-1,l,m,2); uklmT(3) = uklm(k-1,l,m,3);
        uklmT(4) = uklm(k-1,l,m,4); uklmT(5) = uklm(k-1,l,m,5); uklmT(6) = uklm(k-1,l,m,6); uklmT(7) = uklm(k-1,l,m,7);
        
        uklmBo(0) = uklm(k+1,l,m,0); uklmBo(1) = uklm(k+1,l,m,1); uklmBo(2) = uklm(k+1,l,m,2); uklmBo(3) = uklm(k+1,l,m,3);
        uklmBo(4) = uklm(k+1,l,m,4); uklmBo(5) = uklm(k+1,l,m,5); uklmBo(6) = uklm(k+1,l,m,6); uklmBo(7) = uklm(k+1,l,m,7);
        
        qklm1t =   -0.5*mllT*uklmt - 0.5*mplT*uklmT + 0.5*mllBo*uklmt + 0.5*mplBo*uklmBo  + my*uklmt; 
        
        ml.solve8x8(qklm1t(0),qklm1t(1),qklm1t(2),qklm1t(3),qklm1t(4),qklm1t(5),qklm1t(6),qklm1t(7),
                     &x[0],&x[1],&x[2],&x[3],&x[4],&x[5],&x[6],&x[7]);

        qklm1(k,l,m,0) = x[0]; qklm1(k,l,m,1) = x[1]; qklm1(k,l,m,2) = x[2]; qklm1(k,l,m,3) = x[3];
        qklm1(k,l,m,4) = x[4]; qklm1(k,l,m,5) = x[5]; qklm1(k,l,m,6) = x[6]; qklm1(k,l,m,7) = x[7];
        
        if( (qklm1(k,l,m,0)  != 0) || (qklm1(k,l,m,1) != 0) || (qklm1(k,l,m,2) != 0) || (qklm1(k,l,m,3) != 0) ||
            (qklm1(k,l,m,4)  != 0) || (qklm1(k,l,m,5) != 0) || (qklm1(k,l,m,6) != 0) || (qklm1(k,l,m,7) != 0))
          {            
            
            cout << "qklm1 "<<  k << " " << l  << " " << m << " "
                 <<  qklm1(k,l,m,0)  << " " <<  qklm1(k,l,m,1) << " " <<  qklm1(k,l,m,2)  << " " << qklm1(k,l,m,3)  << " "
                 <<   qklm1(k,l,m,4)  << " " <<  qklm1(k,l,m,5)  << " " <<  qklm1(k,l,m,6)  << " " << qklm1(k,l,m,7)  << endl;
          }
        
        // qklm1tt = mplT*uklmT;
        // cout << k << " " << l  << " " << m << " "
        //      << qklm1tt(0) << " " << qklm1tt(1) << " " << qklm1tt(2) << " " << qklm1tt(3) << " "
        //      << qklm1tt(4) << " " << qklm1tt(5) << " " << qklm1tt(6) << " " << qklm1tt(7) << endl;

        // cout << mplT(0,0) << " " << mplT(0,1) << " " << mplT(0,2) << " " << mplT(0,3) << " " 
        //      << mplT(0,4) << " " << mplT(0,5) << " " << mplT(0,6) << " " << mplT(0,7) << endl;
        
        uklmF(0) = uklm(k,l,m+1,0); uklmF(1) = uklm(k,l,m+1,1); uklmF(2) = uklm(k,l,m+1,2); uklmF(3) = uklm(k,l,m+1,3);
        uklmF(4) = uklm(k,l,m+1,4); uklmF(5) = uklm(k,l,m+1,5); uklmF(6) = uklm(k,l,m+1,6); uklmF(7) = uklm(k,l,m+1,7);
        
        uklmBa(0) = uklm(k,l,m-1,0); uklmBa(1) = uklm(k,l,m-1,1); uklmBa(2) = uklm(k,l,m-1,2); uklmBa(3) = uklm(k,l,m-1,3);
        uklmBa(4) = uklm(k,l,m-1,4); uklmBa(5) = uklm(k,l,m-1,5); uklmBa(6) = uklm(k,l,m-1,6); uklmBa(7) = uklm(k,l,m-1,7);
        
        qklm2t =  -0.5*mllF*uklmt - 0.5*mplF*uklmF + 0.5*mllBa*uklmt + 0.5*mplBa*uklmBa  + mz*uklmt; 
        // qklm2t(0) = 1; qklm2t(1) = 1; qklm2t(2) = 1; qklm2t(3) = 1;
        // qklm2t(4) = 1; qklm2t(5) = 1; qklm2t(6) = 1; qklm2t(7) = 1; 
        //qklm2.set(k,l,m,ml/qklm2t);


        ml.solve8x8(qklm2t(0),qklm2t(1),qklm2t(2),qklm2t(3),qklm2t(4),qklm2t(5),qklm2t(6),qklm2t(7),
                    &x[0],&x[1],&x[2],&x[3],&x[4],&x[5],&x[6],&x[7]);

        qklm2(k,l,m,0) = x[0]; qklm2(k,l,m,1) = x[1]; qklm2(k,l,m,2) = x[2]; qklm2(k,l,m,3) = x[3];
        qklm2(k,l,m,4) = x[4]; qklm2(k,l,m,5) = x[5]; qklm2(k,l,m,6) = x[6]; qklm2(k,l,m,7) = x[7];

        // if( (qklm2(k,l,m,0)  != 0) || (qklm2(k,l,m,1) != 0) || (qklm2(k,l,m,2) != 0) || (qklm2(k,l,m,3) != 0) ||
        //     (qklm2(k,l,m,4)  != 0) || (qklm2(k,l,m,5) != 0) || (qklm2(k,l,m,6) != 0) || (qklm2(k,l,m,7) != 0))
        //   {            
            
        //     cout << "qklm2 "<<  k << " " << l  << " " << m << " "
        //          <<  qklm2(k,l,m,0)  << " " <<  qklm2(k,l,m,1) << " " <<  qklm2(k,l,m,2)  << " " << qklm2(k,l,m,3)  << " "
        //          <<   qklm2(k,l,m,4)  << " " <<  qklm2(k,l,m,5)  << " " <<  qklm2(k,l,m,6)  << " " << qklm2(k,l,m,7)  << endl;
        //   }

        
        // cout << "qklm2 "<<k << " " << l  << " " << m << " "
        //      <<   qklm2(k,l,m,0)  << " " <<  qklm2(k,l,m,1)  << " " <<  qklm2(k,l,m,2)  << " " << qklm2(k,l,m,3)  << " " 
        //      <<   qklm2(k,l,m,4)  << " " <<  qklm2(k,l,m,5)  << " " <<  qklm2(k,l,m,6)  << " " << qklm2(k,l,m,7)  << endl;
        
        
        // printf("%d %d %d %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",k,l,m,
        //        uklmT(0),uklmT(1),uklmT(2),uklmT(3),uklmT(4),uklmT(5),uklmT(6),uklmT(7) );

        
        // printf("%d %d %d %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",k,l,m,
        //        uklmt(0),uklmt(1),uklmt(2),uklmt(3),uklmt(4),uklmt(5),uklmt(6),uklmt(7) );
        
        // printf("%d %d %d %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",k,l,m,
        //        qklm0(k,l,m,0),qklm0(k,l,m,1),qklm0(k,l,m,2),qklm0(k,l,m,3),qklm0(k,l,m,4),qklm0(k,l,m,5),qklm0(k,l,m,6),qklm0(k,l,m,7) );

        // printf("qklm1: %d %d %d %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",k,l,m,
        //        qklm1(k,l,m,0),qklm1(k,l,m,1),qklm1(k,l,m,2),qklm1(k,l,m,3),qklm1(k,l,m,4),qklm1(k,l,m,5),qklm1(k,l,m,6),qklm1(k,l,m,7) );
        
        // printf("%d %d %d %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",k,l,m,
        //        qklm2(k,l,m,0),qklm2(k,l,m,1),qklm2(k,l,m,2),qklm2(k,l,m,3),qklm2(k,l,m,4),qklm2(k,l,m,5),qklm2(k,l,m,6),qklm2(k,l,m,7) );
        
        // cout << "qklm0 " << k << " " << l << " " << m << " " << qklm0(k,l,m,0) << " " << qklm0(k,l,m,1) <<" " << qklm0(k,l,m,2) << " " << qklm0(k,l,m,3) << " " <<
        //   qklm0(k,l,m,4) << " " << qklm0(k,l,m,5) <<" " << qklm0(k,l,m,6) << " " << qklm0(k,l,m,7) << endl;


        
      }
  //cout << "qklm0 "<< endl <<qklm0 << endl;
  // cout << "qklm1 "<< endl <<qklm1 << endl;
  // cout << "qklm2 "<< endl <<qklm2 << endl;
}

void bdyConditions(size_t K, size_t L, size_t M){
  
  
  for(int i=1; i < K+1; ++i)
      for(int j=1; j < M+1; ++j){

        //Left band    
        uklm(i,0,j,3) = -uklm(i,1,j,0);
        uklm(i,0,j,2) = -uklm(i,1,j,1);
        uklm(i,0,j,7) = -uklm(i,1,j,4);
        uklm(i,0,j,6) = -uklm(i,1,j,5);
        
        qklm0(i,0,j,3) = qklm0(i,1,j,0);
        qklm0(i,0,j,2) = qklm0(i,1,j,1);   
        qklm0(i,0,j,7) = qklm0(i,1,j,4);
        qklm0(i,0,j,6) = qklm0(i,1,j,5);
        
        qklm1(i,0,j,3) = qklm1(i,1,j,0);
        qklm1(i,0,j,2) = qklm1(i,1,j,1);
        qklm1(i,0,j,7) = qklm1(i,1,j,4);
        qklm1(i,0,j,6) = qklm1(i,1,j,5);
        
        qklm2(i,0,j,3) = qklm2(i,1,j,0);
        qklm2(i,0,j,2) = qklm2(i,1,j,1);
        qklm2(i,0,j,7) = qklm2(i,1,j,4);
        qklm2(i,0,j,6) = qklm2(i,1,j,5);



        //Right band
        uklm(i,L+1,j,0) = -uklm(i,L,j,3);
        uklm(i,L+1,j,1) = -uklm(i,L,j,2);
        uklm(i,L+1,j,4) = -uklm(i,L,j,7);
        uklm(i,L+1,j,5) = -uklm(i,L,j,6);

        qklm0(i,L+1,j,0) = qklm0(i,L,j,3);
        qklm0(i,L+1,j,1) = qklm0(i,L,j,2);
        qklm0(i,L+1,j,4) = qklm0(i,L,j,7);
        qklm0(i,L+1,j,5) = qklm0(i,L,j,6);
        
        qklm1(i,L+1,j,0) = qklm1(i,L,j,3);
        qklm1(i,L+1,j,1) = qklm1(i,L,j,2);
        qklm1(i,L+1,j,4) = qklm1(i,L,j,7);
        qklm1(i,L+1,j,5) = qklm1(i,L,j,6);
        
        qklm2(i,L+1,j,0) = qklm2(i,L,j,3);
        qklm2(i,L+1,j,1) = qklm2(i,L,j,2);
        qklm2(i,L+1,j,4) = qklm2(i,L,j,7);
        qklm2(i,L+1,j,5) = qklm2(i,L,j,6);

        

        
        //Top band
        uklm(i,j,M+1,4) = -uklm(i,j,M,5);
        uklm(i,j,M+1,0) = -uklm(i,j,M,1);
        uklm(i,j,M+1,7) = -uklm(i,j,M,6);
        uklm(i,j,M+1,3) = -uklm(i,j,M,2);
        
        qklm0(i,j,M+1,4) = qklm0(i,j,M,5);
        qklm0(i,j,M+1,0) = qklm0(i,j,M,1);
        qklm0(i,j,M+1,7) = qklm0(i,j,M,6);
        qklm0(i,j,M+1,3) = qklm0(i,j,M,2);
        
        qklm1(i,j,M+1,4) = qklm1(i,j,M,5);
        qklm1(i,j,M+1,0) = qklm1(i,j,M,1);
        qklm1(i,j,M+1,7) = qklm1(i,j,M,6);
        qklm1(i,j,M+1,3) = qklm1(i,j,M,2);
        
        qklm2(i,j,M+1,4) = qklm2(i,j,M,5);
        qklm2(i,j,M+1,0) = qklm2(i,j,M,1);
        qklm2(i,j,M+1,7) = qklm2(i,j,M,6);
        qklm2(i,j,M+1,3) = qklm2(i,j,M,2);

        
        //Bottom band
        uklm(i,j,0,5) = -uklm(i,j,1,4);
        uklm(i,j,0,1) = -uklm(i,j,1,0);
        uklm(i,j,0,6) = -uklm(i,j,1,7);
        uklm(i,j,0,2) = -uklm(i,j,1,3);
        
        qklm0(i,j,0,5) = qklm0(i,j,1,4);
        qklm0(i,j,0,1) = qklm0(i,j,1,0);
        qklm0(i,j,0,6) = qklm0(i,j,1,7);
        qklm0(i,j,0,2) = qklm0(i,j,1,3);
        
        qklm1(i,j,0,5) = qklm1(i,j,1,4);
        qklm1(i,j,0,1) = qklm1(i,j,1,0);
        qklm1(i,j,0,6) = qklm1(i,j,1,7);
        qklm1(i,j,0,2) = qklm1(i,j,1,3);
        
        qklm2(i,j,0,5) = qklm2(i,j,1,4);
        qklm2(i,j,0,1) = qklm2(i,j,1,0);
        qklm2(i,j,0,6) = qklm2(i,j,1,7);
        qklm2(i,j,0,2) = qklm2(i,j,1,3);


        
        //Front band
        uklm(K+1,i,j,0) = -uklm(K,i,j,4);
        uklm(K+1,i,j,1) = -uklm(K,i,j,5);
        uklm(K+1,i,j,3) = -uklm(K,i,j,7);
        uklm(K+1,i,j,2) = -uklm(K,i,j,6);
        
        qklm0(K+1,i,j,0) = qklm0(K,i,j,4);
        qklm0(K+1,i,j,1) = qklm0(K,i,j,5);
        qklm0(K+1,i,j,3) = qklm0(K,i,j,7);
        qklm0(K+1,i,j,2) = qklm0(K,i,j,6);
        
        qklm1(K+1,i,j,0) = qklm1(K,i,j,4);
        qklm1(K+1,i,j,1) = qklm1(K,i,j,5);
        qklm1(K+1,i,j,3) = qklm1(K,i,j,7);
        qklm1(K+1,i,j,2) = qklm1(K,i,j,6);
        
        qklm2(K+1,i,j,0) = qklm2(K,i,j,4);
        qklm2(K+1,i,j,1) = qklm2(K,i,j,5);
        qklm2(K+1,i,j,3) = qklm2(K,i,j,7);
        qklm2(K+1,i,j,2) = qklm2(K,i,j,6);
        

        
        //Back band
        uklm(0,i,j,4) = -uklm(1,i,j,0);
        uklm(0,i,j,5) = -uklm(1,i,j,1);
        uklm(0,i,j,7) = -uklm(1,i,j,3);
        uklm(0,i,j,6) = -uklm(1,i,j,2);

        qklm0(0,i,j,4) = qklm0(1,i,j,0);
        qklm0(0,i,j,5) = qklm0(1,i,j,1);
        qklm0(0,i,j,7) = qklm0(1,i,j,3);
        qklm0(0,i,j,6) = qklm0(1,i,j,2);
        
        qklm1(0,i,j,4) = qklm1(1,i,j,0);
        qklm1(0,i,j,5) = qklm1(1,i,j,1);
        qklm1(0,i,j,7) = qklm1(1,i,j,3);
        qklm1(0,i,j,6) = qklm1(1,i,j,2);
        
        qklm2(0,i,j,4) = qklm2(1,i,j,0);
        qklm2(0,i,j,5) = qklm2(1,i,j,1);
        qklm2(0,i,j,7) = qklm2(1,i,j,3);
        qklm2(0,i,j,6) = qklm2(1,i,j,2);

        
      }
}

void rhsf(int K, int L, int M){

  // Column8d qklmt0,qklm0L,qklm0R;
  // Column8d qklmt1,qklm1T,qklm1Bo;
  // Column8d qklmt2,qklm2F,qklm2Ba;
  // Column8d rhst;
  //Column8d rhstt;
  double kInkR  = 1.0;
  double kInkL  = 1.0;
  double kInkT  = 1.0;
  double kInkBo = 1.0;
  double kInkF  = 1.0;
  double kInkBa = 1.0;
  double x[8];
  
  bdyConditions(K,L,M);
  qklmf(K,L,M);
  
  for(int k=1; k < K+1; ++k)
    for(int l=1; l < L+1; ++l)
      for(int m=1; m < M+1; ++m){

        if( kM(k,l,m,0)+kM(k,l+1,m,0) > 0 )
          kInkR = 2*kM(k,l,m,0)*kM(k,l+1,m,0)/(kM(k,l,m,0)+kM(k,l+1,m,0));
        else
          kInkR = 0;
        
        if( kM(k,l,m,0)+kM(k,l-1,m,0) > 0 )
          kInkL = 2*kM(k,l,m,0)*kM(k,l-1,m,0)/(kM(k,l,m,0)+kM(k,l-1,m,0));
        else
          kInkL = 0;
        
        if( kM(k,l,m,0)+kM(k-1,l,m,0) > 0 )
          kInkT = 2*kM(k,l,m,0)*kM(k-1,l,m,0)/(kM(k,l,m,0)+kM(k-1,l,m,0));
        else
          kInkT = 0;        
        
        if( kM(k,l,m,0)+kM(k+1,l,m,0) > 0 )
          kInkBo = 2*kM(k,l,m,0)*kM(k+1,l,m,0)/(kM(k,l,m,0)+kM(k+1,l,m,0));
        else
          kInkBo = 0;

        if( kM(k,l,m+1,0)+kM(k,l,m+1,0) > 0 )
          kInkF = 2*kM(k,l,m,0)*kM(k,l,m+1,0)/(kM(k,l,m,0)+kM(k,l,m+1,0));
        else
          kInkF = 0;
        
        if( kM(k,l,m,0)+kM(k,l,m-1,0) > 0 )
          kInkBa = 2*kM(k,l,m,0)*kM(k,l,m-1,0)/(kM(k,l,m,0)+kM(k,l,m-1,0));
        else
          kInkBa = 0;
        

        
        qklmt0(0) = qklm0(k,l,m,0); qklmt0(1) = qklm0(k,l,m,1); qklmt0(2) = qklm0(k,l,m,2);  qklmt0(3) = qklm0(k,l,m,3);
        qklmt0(4) = qklm0(k,l,m,4); qklmt0(5) = qklm0(k,l,m,5); qklmt0(6) = qklm0(k,l,m,6);  qklmt0(7) = qklm0(k,l,m,7);        

        // cout << k << " " << l << " " << m << " " << qklmt0(0) << " " << qklmt0(1) << " " <<qklmt0(2) << " " << qklmt0(3) << " ";
        // cout <<  qklmt0(4) << " " << qklmt0(5) << " " << qklmt0(6) << " " << qklmt0(7) << endl;
                
        
        qklm0L(0) = qklm0(k,l-1,m,0); qklm0L(1) = qklm0(k,l-1,m,1); qklm0L(2) = qklm0(k,l-1,m,2); qklm0L(3) = qklm0(k,l-1,m,3);
        qklm0L(4) = qklm0(k,l-1,m,4); qklm0L(5) = qklm0(k,l-1,m,5); qklm0L(6) = qklm0(k,l-1,m,6); qklm0L(7) = qklm0(k,l-1,m,7);
        // cout << k << " " << l << " " << m << " " << qklm0L(0) << " " << qklm0L(1) << " " <<qklm0L(2) << " " << qklm0L(3) << " "
        //      <<  qklm0L(4) << " " << qklm0L(5) << " " << qklm0L(6) << " " << qklm0L(7) << endl;
        
        // cout <<" qklm0L " << k << " " << l << " " << m << " " << qklm0L(0) << " " << qklm0L(1) << " " <<qklm0L(2) << " " << qklm0L(3) << " ";
        // cout <<  qklm0L(4) << " " << qklm0L(5) << " " << qklm0L(6) << " " << qklm0L(7) << endl;

        
        qklm0R(0) = qklm0(k,l+1,m,0); qklm0R(1) = qklm0(k,l+1,m,1); qklm0R(2) = qklm0(k,l+1,m,2); qklm0R(3) = qklm0(k,l+1,m,3);
        qklm0R(4) = qklm0(k,l+1,m,4); qklm0R(5) = qklm0(k,l+1,m,5); qklm0R(6) = qklm0(k,l+1,m,6); qklm0R(7) = qklm0(k,l+1,m,7);
        // cout << k << " " << l << " " << m << " " << qklm0R(0) << " " << qklm0R(1) << " " <<qklm0R(2) << " " << qklm0R(3) << " ";
        // cout <<  qklm0R(4) << " " << qklm0R(5) << " " << qklm0R(6) << " " << qklm0R(7) << endl;
        // cout <<" qklm0R " << k << " " << l << " " << m << " " << qklm0R(0) << " " << qklm0R(1) << " " <<qklm0R(2) << " " << qklm0R(3) << " ";
        // cout <<  qklm0R(4) << " " << qklm0R(5) << " " << qklm0R(6) << " " << qklm0R(7) << endl;


        qklmt1(0) = qklm1(k,l,m,0); qklmt1(1) = qklm1(k,l,m,1); qklmt1(2) = qklm1(k,l,m,2); qklmt1(3) = qklm1(k,l,m,3); 
        qklmt1(4) = qklm1(k,l,m,4); qklmt1(5) = qklm1(k,l,m,5); qklmt1(6) = qklm1(k,l,m,6); qklmt1(7) = qklm1(k,l,m,7);

        // cout <<" rhs qklm1 " << k << " " << l << " " << m << " " << qklmt1(0) << " " << qklmt1(1) << " " <<qklmt1(2) << " " << qklmt1(3) << " ";
        // cout <<  qklmt1(4) << " " << qklmt1(5) << " " << qklmt1(6) << " " << qklmt1(7) << endl;
        
        
        qklm1T(0) = qklm1(k-1,l,m,0); qklm1T(1) = qklm1(k-1,l,m,1); qklm1T(2) = qklm1(k-1,l,m,2); qklm1T(3) = qklm1(k-1,l,m,3);
        qklm1T(4) = qklm1(k-1,l,m,4); qklm1T(5) = qklm1(k-1,l,m,5); qklm1T(6) = qklm1(k-1,l,m,6); qklm1T(7) = qklm1(k-1,l,m,7);
        
        qklm1Bo(0) = qklm1(k+1,l,m,0); qklm1Bo(1) = qklm1(k+1,l,m,1); qklm1Bo(2) = qklm1(k+1,l,m,2); qklm1Bo(3) = qklm1(k+1,l,m,3);
        qklm1Bo(4) = qklm1(k+1,l,m,4); qklm1Bo(5) = qklm1(k+1,l,m,5); qklm1Bo(6) = qklm1(k+1,l,m,6); qklm1Bo(7) = qklm1(k+1,l,m,7);


        
        qklmt2(0) = qklm2(k,l,m,0); qklmt2(1) = qklm2(k,l,m,1); qklmt2(2) = qklm2(k,l,m,2); qklmt2(3) = qklm2(k,l,m,3);
        qklmt2(4) = qklm2(k,l,m,4); qklmt2(5) = qklm2(k,l,m,5); qklmt2(6) = qklm2(k,l,m,6); qklmt2(7) = qklm2(k,l,m,7);
        
        qklm2F(0) =  qklm2(k,l,m+1,0); qklm2F(1) =  qklm2(k,l,m+1,1); qklm2F(2) =  qklm2(k,l,m+1,2); qklm2F(3) =  qklm2(k,l,m+1,3);
        qklm2F(4) =  qklm2(k,l,m+1,4); qklm2F(5) =  qklm2(k,l,m+1,5); qklm2F(6) =  qklm2(k,l,m+1,6); qklm2F(7) =  qklm2(k,l,m+1,7);
        
        qklm2Ba(0) = qklm2(k,l,m-1,0); qklm2Ba(1) = qklm2(k,l,m-1,1); qklm2Ba(2) = qklm2(k,l,m-1,2); qklm2Ba(3) = qklm2(k,l,m-1,3);
        qklm2Ba(4) = qklm2(k,l,m-1,4); qklm2Ba(5) = qklm2(k,l,m-1,5); qklm2Ba(6) = qklm2(k,l,m-1,6); qklm2Ba(7) = qklm2(k,l,m-1,7);

        // rhst = - 0.5*kInkF*mllF*qklmt2 - 0.5*kInkF*mplF*qklm2F + 0.5*kInkBa*mllBa*qklmt2 + 0.5*kInkBa*mplBa*qklm2Ba  + mz*qklmt2;
        // cout << k << " " << l << " " << m << " " << rhst(0,0) << " " << rhst(0,1) << " " <<rhst(0,2) << " " << rhst(0,3) << " ";
        // cout <<  rhst(0,4) << " " << rhst(0,5) << " " <<rhst(0,6) << " " << rhst(0,7) << endl;
        // cout << k << " " << l << " " << m << " " << qklm0R(0) << " " << qklm0R(1) << " " << qklm0R(2) << " " << qklm0R(3) << " ";
        // cout <<  qklm0R(4) << " " << qklm0R(5) << " " << qklm0R(6) << " " << qklm0R(7) << endl;
        //cout << mplR << endl;
        //cout << "mllrqklmt =" << rhst << endl;
        // cout << k << " " << l << " " << m << " " << qklm2Ba(0,0) << " " << qklm2Ba(0,1) << " " <<qklm2Ba(0,2) << " " << qklm2Ba(0,3) << " ";
        // cout <<  qklm2Ba(0,4) << " " << qklm2Ba(0,5) << " " << qklm2Ba(0,6) << " " << qklm2Ba(0,7) << endl;
        // cout << k << " " << l << " " << m << " " << qklm1(k,l,m,0) << " " << qklm1(k,l,m,1) << " " <<qklm1(k,l,m,2) << " " << qklm1(k,l,m,3) << " ";
        // cout <<  qklm1(k,l,m,4) << " " << qklm1(k,l,m,5) << " " << qklm1(k,l,m,6) << " " << qklm1(k,l,m,7) << endl;
        
        //rhst =  - 0.5*kInkR*mllR*qklmt0 - 0.5*kInkR*mplR*qklm0R + 0.5*kInkL*mllL*qklmt0   + 0.5*kInkL*mplL*qklm0L     + mx*qklmt0;
        
        //rhst = - 0.5*kInkT*mllT*qklmt1 - 0.5*kInkT*mplT*qklm1T + 0.5*kInkBo*mllBo*qklmt1 + 0.5*kInkBo*mplBo*qklm1Bo  + my*qklmt1 ;
        //rhst = - 0.5*kInkT*mllT*qklmt1;
        //rhst =  - 0.5*kInkF*mllF*qklmt2 - 0.5*kInkF*mplF*qklm2F + 0.5*kInkBa*mllBa*qklmt2 + 0.5*kInkBa*mplBa*qklm2Ba  + mz*qklmt2;
        
        // if( (rhst(0)  != 0) || (rhst(1) != 0) || (rhst(2) != 0) || (rhst(3) != 0) ||
        //     (rhst(4)  != 0) || (rhst(5) != 0) || (rhst(6) != 0) || (rhst(7) != 0))
        //   {            
            
        //     cout << k << " " << l << " " << m << " " << rhst(0) << " " << rhst(1) << " " <<rhst(2) << " " << rhst(3) << " ";
        //     cout <<  rhst(4) << " " << rhst(5) << " " << rhst(6) << " " << rhst(7) << endl;
            
        //   }

        
        
        rhst =  - 0.5*kInkR*mllR*qklmt0 - 0.5*kInkR*mplR*qklm0R + 0.5*kInkL*mllL*qklmt0   + 0.5*kInkL*mplL*qklm0L     + mx*qklmt0
                - 0.5*kInkT*mllT*qklmt1 - 0.5*kInkT*mplT*qklm1T + 0.5*kInkBo*mllBo*qklmt1 + 0.5*kInkBo*mplBo*qklm1Bo  + my*qklmt1 
                - 0.5*kInkF*mllF*qklmt2 - 0.5*kInkF*mplF*qklm2F + 0.5*kInkBa*mllBa*qklmt2 + 0.5*kInkBa*mplBa*qklm2Ba  + mz*qklmt2;
        

        
        // rhst =  - 0.5*mllR*qklmt0 - 0.5*mplR*qklm0R + 0.5*mllL*qklmt0  + 0.5*mplL*qklm0L    + mx*qklmt0
        //         - 0.5*mllT*qklmt1 - 0.5*mplT*qklm1T + 0.5*mllBo*qklmt1 + 0.5*mplBo*qklm1Bo  + my*qklmt1
        //         - 0.5*mllF*qklmt2 - 0.5*mplF*qklm2F + 0.5*mllBa*qklmt2 + 0.5*mplBa*qklm2Ba  + mz*qklmt2;
        
        

        ml.solve8x8(rhst(0),rhst(1),rhst(2),rhst(3),rhst(4),rhst(5),rhst(6),rhst(7),&x[0],&x[1],&x[2],&x[3],&x[4],&x[5],&x[6],&x[7]);
        
        rhs(k,l,m,0) = x[0]; rhs(k,l,m,1) = x[1]; rhs(k,l,m,2) = x[2]; rhs(k,l,m,3) = x[3];
        rhs(k,l,m,4) = x[4]; rhs(k,l,m,5) = x[5]; rhs(k,l,m,6) = x[6]; rhs(k,l,m,7) = x[7];
        
        // cout << k << " " << l << " " << m << " " << rhs(k,l,m,0) << " " << rhs(k,l,m,1) << " " <<rhs(k,l,m,2) << " " << rhs(k,l,m,3) << " ";
        // cout <<  rhs(k,l,m,4) << " " << rhs(k,l,m,5) << " " << rhs(k,l,m,6) << " " << rhs(k,l,m,7) << endl;
        

        
      }
  //cout << "rhs:" << endl  << rhs << endl;

}

void setVoxels(size_t K, size_t L, size_t M){
  size_t i;
  size_t j;
  size_t k;

  kM(4,6,5,0) = 0;
  kM(4,6,6,0) = 0;
  kM(4,6,7,0) = 0;
  kM(4,6,8,0) = 0;
  kM(4,7,5,0) = 0;
  kM(4,7,6,0) = 0;
  kM(4,7,7,0) = 0;
  kM(4,7,8,0) = 0;
  kM(4,8,5,0) = 0;
  kM(4,8,6,0) = 0;
  kM(4,8,7,0) = 0;
  kM(4,8,8,0) = 0;


  kM(3,6,5,0) = 0;
  kM(3,6,6,0) = 0;
  kM(3,6,7,0) = 0;
  kM(3,6,8,0) = 0;
  kM(3,7,5,0) = 0;
  kM(3,7,6,0) = 0;
  kM(3,7,7,0) = 0;
  kM(3,7,8,0) = 0;
  kM(3,8,5,0) = 0;
  kM(3,8,6,0) = 0;
  kM(3,8,7,0) = 0;
  kM(3,8,8,0) = 0;

  kM(2,6,5,0) = 0;
  kM(2,6,6,0) = 0;
  kM(2,6,7,0) = 0;
  kM(2,6,8,0) = 0;
  kM(2,7,5,0) = 0;
  kM(2,7,6,0) = 0;
  kM(2,7,7,0) = 0;
  kM(2,7,8,0) = 0;
  kM(2,8,5,0) = 0;
  kM(2,8,6,0) = 0;
  kM(2,8,7,0) = 0;
  kM(2,8,8,0) = 0;

  
  
  
  // i = K/2;
  // j = L/2;
  // k = M/2;
  
  // kM(i/2,j/2,k/2,0)   = 0;
  // kM(i/2,j/2+1,k/2,0) = 0;
  // kM(i/2,j/2-1,k/2,0) = 0;

  // kM(i/2+1,j/2,k/2,0)   = 0;
  // kM(i/2+1,j/2+1,k/2,0) = 0;
  // kM(i/2+1,j/2-1,k/2,0) = 0;  

  // kM(i/2-1,j/2,k/2,0)   = 0;
  // kM(i/2-1,j/2+1,k/2,0) = 0;
  // kM(i/2-1,j/2-1,k/2,0) = 0;


  // i = K/2;
  // j = L/2+L/2;
  // k = M/2;
  
  // kM(i/2,j/2,k/2,0)   = 0;
  // kM(i/2,j/2+1,k/2,0) = 0;
  // kM(i/2,j/2-1,k/2,0) = 0;
  // kM(i/2+1,j/2,k/2,0)   = 0;
  // kM(i/2+1,j/2+1,k/2,0) = 0;
  // kM(i/2+1,j/2-1,k/2,0) = 0;  
  // kM(i/2-1,j/2,k/2,0)   = 0;
  // kM(i/2-1,j/2+1,k/2,0) = 0;
  // kM(i/2-1,j/2-1,k/2,0) = 0;



  // i = K/2+K/2;
  // j = L/2;
  // k = M/2;
  
  // kM(i/2,j/2,k/2,0)   = 0;
  // kM(i/2,j/2+1,k/2,0) = 0;
  // kM(i/2,j/2-1,k/2,0) = 0;
  // kM(i/2+1,j/2,k/2,0)   = 0;
  // kM(i/2+1,j/2+1,k/2,0) = 0;
  // kM(i/2+1,j/2-1,k/2,0) = 0;  
  // kM(i/2-1,j/2,k/2,0)   = 0;
  // kM(i/2-1,j/2+1,k/2,0) = 0;
  // kM(i/2-1,j/2-1,k/2,0) = 0;


  // i = K/2+K/2;
  // j = L/2+K/2;
  // k = M/2;
  
  // kM(i/2,j/2,k/2,0)   = 0;
  // kM(i/2,j/2+1,k/2,0) = 0;
  // kM(i/2,j/2-1,k/2,0) = 0;
  // kM(i/2+1,j/2,k/2,0)   = 0;
  // kM(i/2+1,j/2+1,k/2,0) = 0;
  // kM(i/2+1,j/2-1,k/2,0) = 0;  
  // kM(i/2-1,j/2,k/2,0)   = 0;
  // kM(i/2-1,j/2+1,k/2,0) = 0;
  // kM(i/2-1,j/2-1,k/2,0) = 0;


  // cout << kM << endl;
  
}

void filter(size_t K,size_t L, size_t M){

  for(int k=1; k < K+1; ++k)
    for(int l=1; l < L+1; ++l)
      for(int m=1; m < M+1; ++m){
        if( uklmc(k-1,l-1,m-1,0) < 0){          
          uklmc(k-1,l-1,m-1,0) = 0.0;
        }
      }
  
}



void qklmBdyConditions(size_t K, size_t L, size_t M){

  for(int i=1; i < K+1; ++i)
    for(int j=1; j < M+1; ++j){
      
      //left band
      qklm0(i,0,j,3) = qklm0(i,1,j,0);
      qklm0(i,0,j,2) = qklm0(i,1,j,1);   
      qklm0(i,0,j,7) = qklm0(i,1,j,4);
      qklm0(i,0,j,6) = qklm0(i,1,j,5);
      
      qklm1(i,0,j,3) = qklm1(i,1,j,0);
      qklm1(i,0,j,2) = qklm1(i,1,j,1);
      qklm1(i,0,j,7) = qklm1(i,1,j,4);
      qklm1(i,0,j,6) = qklm1(i,1,j,5);

      qklm2(i,0,j,3) = qklm2(i,1,j,0);
      qklm2(i,0,j,2) = qklm2(i,1,j,1);
      qklm2(i,0,j,7) = qklm2(i,1,j,4);
      qklm2(i,0,j,6) = qklm2(i,1,j,5);


      //right band
      qklm0(i,L+1,j,0) = qklm0(i,L,j,3);
      qklm0(i,L+1,j,1) = qklm0(i,L,j,2);
      qklm0(i,L+1,j,4) = qklm0(i,L,j,7);
      qklm0(i,L+1,j,5) = qklm0(i,L,j,6);

      qklm1(i,L+1,j,0) = qklm1(i,L,j,3);
      qklm1(i,L+1,j,1) = qklm1(i,L,j,2);
      qklm1(i,L+1,j,4) = qklm1(i,L,j,7);
      qklm1(i,L+1,j,5) = qklm1(i,L,j,6);
      
      qklm2(i,L+1,j,0) = qklm2(i,L,j,3);
      qklm2(i,L+1,j,1) = qklm2(i,L,j,2);
      qklm2(i,L+1,j,4) = qklm2(i,L,j,7);
      qklm2(i,L+1,j,5) = qklm2(i,L,j,6);

      
      //Top band
      qklm0(i,j,M+1,4) = qklm0(i,j,M,5);
      qklm0(i,j,M+1,0) = qklm0(i,j,M,1);
      qklm0(i,j,M+1,7) = qklm0(i,j,M,6);
      qklm0(i,j,M+1,3) = qklm0(i,j,M,2);
      
      qklm1(i,j,M+1,4) = qklm1(i,j,M,5);
      qklm1(i,j,M+1,0) = qklm1(i,j,M,1);
      qklm1(i,j,M+1,7) = qklm1(i,j,M,6);
      qklm1(i,j,M+1,3) = qklm1(i,j,M,2);

      qklm2(i,j,M+1,4) = qklm2(i,j,M,5);
      qklm2(i,j,M+1,0) = qklm2(i,j,M,1);
      qklm2(i,j,M+1,7) = qklm2(i,j,M,6);
      qklm2(i,j,M+1,3) = qklm2(i,j,M,2);

      //Bottom band
      qklm0(i,j,0,5) = qklm0(i,j,1,4);
      qklm0(i,j,0,1) = qklm0(i,j,1,0);
      qklm0(i,j,0,6) = qklm0(i,j,1,7);
      qklm0(i,j,0,2) = qklm0(i,j,1,3);

      qklm1(i,j,0,5) = qklm1(i,j,1,4);
      qklm1(i,j,0,1) = qklm1(i,j,1,0);
      qklm1(i,j,0,6) = qklm1(i,j,1,7);
      qklm1(i,j,0,2) = qklm1(i,j,1,3);

      qklm2(i,j,0,5) = qklm2(i,j,1,4);
      qklm2(i,j,0,1) = qklm2(i,j,1,0);
      qklm2(i,j,0,6) = qklm2(i,j,1,7);
      qklm2(i,j,0,2) = qklm2(i,j,1,3);


      //Front band
      qklm0(K+1,i,j,0) = qklm0(K,i,j,4);
      qklm0(K+1,i,j,1) = qklm0(K,i,j,5);
      qklm0(K+1,i,j,3) = qklm0(K,i,j,7);
      qklm0(K+1,i,j,2) = qklm0(K,i,j,6);
      
      qklm1(K+1,i,j,0) = qklm1(K,i,j,4);
      qklm1(K+1,i,j,1) = qklm1(K,i,j,5);
      qklm1(K+1,i,j,3) = qklm1(K,i,j,7);
      qklm1(K+1,i,j,2) = qklm1(K,i,j,6);

      qklm2(K+1,i,j,0) = qklm2(K,i,j,4);
      qklm2(K+1,i,j,1) = qklm2(K,i,j,5);
      qklm2(K+1,i,j,3) = qklm2(K,i,j,7);
      qklm2(K+1,i,j,2) = qklm2(K,i,j,6);

      //Back band
      qklm0(0,i,j,4) = qklm0(1,i,j,0);
      qklm0(0,i,j,5) = qklm0(1,i,j,1);
      qklm0(0,i,j,7) = qklm0(1,i,j,3);
      qklm0(0,i,j,6) = qklm0(1,i,j,2);

      qklm1(0,i,j,4) = qklm1(1,i,j,0);
      qklm1(0,i,j,5) = qklm1(1,i,j,1);
      qklm1(0,i,j,7) = qklm1(1,i,j,3);
      qklm1(0,i,j,6) = qklm1(1,i,j,2);

      qklm2(0,i,j,4) = qklm2(1,i,j,0);
      qklm2(0,i,j,5) = qklm2(1,i,j,1);
      qklm2(0,i,j,7) = qklm2(1,i,j,3);
      qklm2(0,i,j,6) = qklm2(1,i,j,2);

      
    }
  
}



int main(int argc, char* argv[]){
  
  if(argc < 12){
    cerr << "To run: ./aDiff3D a b c d e f K L M alpha T" << endl;
    exit(0);
  }

  float a = atof(argv[1]);
  float b = atof(argv[2]);
  float c = atof(argv[3]);
  float d = atof(argv[4]);
  float e = atof(argv[5]);
  float f = atof(argv[6]);
  int   K = atoi(argv[7]);
  int   L = atoi(argv[8]);
  int   M = atoi(argv[9]);
  float alpha = atof(argv[10]);
  float T     = atof(argv[11]);

  //cout << "alpha " << alpha << endl;
  //cout << "T " << T << endl;
  
  double dx = (b-a)/K;
  double dy = (d-c)/L;
  double dz = (f-e)/M;

  ofstream file;
  GNUplot plotter;
  GNUplot plotter1;
  GNUplot plotter2;
  GNUplot plotter3;
  
  string s;
  string nfile;
  int pause;

  double diffCoeff = 1.0;
  

  uklm.resize(K+2,L+2,M+2);
  qklm0.resize(K+2,L+2,M+2);
  qklm1.resize(K+2,L+2,M+2);
  qklm2.resize(K+2,L+2,M+2);
  rhs.resize(K+2,L+2,M+2);
  deltas.resize(K+2,L+2,M+2);
  kM.resize(K+2,L+2,M+2);

  
  uklmc.resize(K,L,M);
  qklm0c.resize(K,L,M);
  qklm1c.resize(K,L,M);
  qklm2c.resize(K,L,M);


  kM = diffCoeff;
  //setVoxels(K,L,M);
  
  knots(a,b,c,d,e,f,K,L,M);
  initMatrices(dx,dy,dz);

  deltas.set(K/2+1,L/2+1,M/2+1,1);
  //cout << deltas << endl;

  initUklm(K,L,M,dx,dy,dz);

  //cout << uklm << endl;
  
  // centerUklm(K,L,M);
  // cout << " Init_uklmc " << uklmc << endl;
  
  // nfile = "./data/uklm_0.txt";
  // saveUklm(K,L,M,nfile);
  // s = "splot "+string("\'")+ nfile+string("\'") + "using 1:2:3:4 w p lt palette";
  // plotter(s.c_str());
  // pause = cin.get();

  clock_t cpu_startTime, cpu_endTime;
  float msecTotal;
  double cpu_ElapsedTime=0;

  double dt   = (alpha/diffCoeff)*(dx*dx+dy*dy+dz*dz);

  int nSteps  = T/dt;
  double time = 0;
  cout << " nSteps: " << nSteps << endl;
  cout << "dt: " << dt << endl;
  cout << " dx " << dx << " dy " << dy << " dz " << dz << endl;

    
  cpu_startTime = clock();

  nSteps = 2;
  for(int t=0; t < nSteps; t++)
    {
      rhsf(K,L,M);      
      //Euler forward

      //uklm = uklm + dt*rhs;      
      //cout << uklm << endl;
      //cout << "----------------Iteration ---------------------------------" <<  t << endl;    
      // cout << "uklm:" << endl << uklm << endl;

      for(int k=0; k < K+1; ++k)
        for(int l=0; l < L+1; ++l)
          for(int m=0; m < M+1; ++m){
            uklm(k,l,m,0) += dt * rhs(k,l,m,0);
            uklm(k,l,m,1) += dt * rhs(k,l,m,1);
            uklm(k,l,m,2) += dt * rhs(k,l,m,2);
            uklm(k,l,m,3) += dt * rhs(k,l,m,3);
            uklm(k,l,m,4) += dt * rhs(k,l,m,4);
            uklm(k,l,m,5) += dt * rhs(k,l,m,5);
            uklm(k,l,m,6) += dt * rhs(k,l,m,6);
            uklm(k,l,m,7) += dt * rhs(k,l,m,7);
            // cout << " uklm " <<k << " " << l << " " << m
          //        << " " << uklm(k,l,m,0) << " " << uklm(k,l,m,1) << " " << uklm(k,l,m,2) << " " <<  uklm(k,l,m,3) << " " 
          //        << " " << uklm(k,l,m,4) << " " << uklm(k,l,m,5) << " " << uklm(k,l,m,6) << " " <<  uklm(k,l,m,7) << endl;
          }
      cout << "-------------------------------------------------------" << endl;    

    }


  // ThreeDimMatrixX8d resu;
  // resu.resize(K+2,L+2,M+2);
  // //float ltime = 0.0;

  // for(int t=0; t < nSteps; ++t){
  //   //RungeKutta 4 order
  //   for(int i=0; i < 5; i++)
  //     {
  //       // ltime = time + rk4c[i]*dt;
  //       rhsf(K,L,M);
  //       resu = rk4a[i]*resu + dt*rhs;
  //       uklm = uklm + rk4b[i]*resu;
  //     }
  //   time += dt;
  // }
  
  
  centerUklm(K,L,M);
  cpu_endTime = clock();
  cpu_ElapsedTime = ((cpu_endTime - cpu_startTime)/(double)CLOCKS_PER_SEC);
  cout << "The elapsed time in CPU was: " <<  cpu_ElapsedTime <<" sec."<< endl;

  //cout << "uklmc " << endl  << uklmc << endl;
  // // filter(K,L,M);
  nfile = "./data/uklm_T_cpu.txt";
  saveUklm(K,L,M,nfile);
  s = "splot "+string("\'")+ nfile+string("\'") + "using 1:2:3:4 w p lt palette lw 10";
  plotter(s.c_str());
  //pause = cin.get();


  nfile =  "./data/uklm_T_cpu_x.txt";
  s = "set title "+string("\'")+"YZ"+string("\'")+";"+ " unset key; set pm3d map; splot "+string("\'")+nfile+string("\'")+ " using 1:2:3 w p lt palette lw 10"; 
  plotter1(s.c_str());
  //pause = cin.get();

  nfile =  "./data/uklm_T_cpu_y.txt";
  s = "set title "+string("\'")+"XZ"+string("\'")+";"+ " unset key; set pm3d map; splot "+string("\'")+nfile+string("\'")+ " using 1:2:3 w p lt palette lw 10"; 
  //s = "unset key; set pm3d map; splot "+string("\'")+nfile+string("\'")+ " using 1:2:3 w p lt palette lw 10"; 
  plotter2(s.c_str());
  // pause = cin.get();

  nfile =  "./data/uklm_T_cpu_z.txt";
  s = "set title "+string("\'")+"XY"+string("\'")+";"+ " unset key; set pm3d map; splot "+string("\'")+nfile+string("\'")+ " using 1:2:3 w p lt palette lw 10"; 
  //s = "unset key; set pm3d map; splot "+string("\'")+nfile+string("\'")+ " using 1:2:3 w p lt palette lw 10"; 
  plotter3(s.c_str());
  pause = cin.get();

  
  return 0;
}





