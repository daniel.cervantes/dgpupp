import plotly.graph_objects as go
import numpy as np

data = np.loadtxt("../../data/uklm_T_reduced.txt")

print(data.shape)

X = data[:,0]
Y = data[:,1]
Z = data[:,2]
values = data[:,3]

# X = XX[::20]
# Y = YY[::20]
# Z = ZZ[::10]
# values = data[::20]

#print(X.shape)
# X, Y, Z = np.mgrid[-1:1:30j, -1:1:30j, -1:1:30j]
# values =    np.sin(np.pi*X) * np.cos(np.pi*Z) * np.sin(np.pi*Y)

# print(X.shape)

# print(X.flatten())



fig = go.Figure(data=go.Volume(
    # x=X.flatten(),
    # y=Y.flatten(),
    # z=Z.flatten(),
    # value=values.flatten(),
    x=X,y=Y,z=Z,value=values,
    isomin=0.0,
    isomax=.3,
    opacity=.1, # needs to be small to see through all surfaces
    surface_count= 400, # needs to be a large number for good volume rendering
    ))
fig.show()

