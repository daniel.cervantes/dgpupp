#include <num/Matrix.hpp>
#include <num/TOperators.hpp>
#include <fstream>
#include <iostream>
#include <math.h>

//./bin/dGenerator_cpp 0 50 0 50 200 200 5 .5 "data/substratesAT.txt" 

using namespace num;
using namespace std;


typedef Matrix<int,Dynamic,Dynamic,1> MatrixXi;
typedef Matrix<float,Dynamic,Dynamic,1> MatrixXf;

void diffCoeffMatrix(float a, float b, float c, float d, int K, int L, string ifile,  string ofile, MatrixXi & mk, float epsilon = 0 ){

  float xi,yj;
  float xip1,yjp1;
  float xia,yja;
  float xa,ya,za,ra;

  int nA,rA;
  float aE,bE;
  float cE,dE;

  MatrixXi mkt;
  
  MatrixXf pulses;
  ifstream imyfile;    
  ofstream omyfile;
  
  float dx = (b-a)/K;
  float dy = (d-c)/L;


  
  imyfile.open(ifile.c_str());    
  omyfile.open(ofile.c_str());    
  
  aE = a - (b-a);
  bE = b + (b-a);  
  cE = c - (d-c);
  dE = d + (d-c);

  
  mkt.resize(3*K,3*L);
  mkt = 1;
  mk.resize(K,L);

  //rA = 4;
  imyfile >> nA;
  
  pulses.resize(nA,4);

  for(int a = 0; a < nA; ++a)
    {
      imyfile >> pulses(a,0) >> pulses(a,1) >> pulses(a,2) >> pulses(a,3);
      //cout << pulses(a,0) << " " <<  pulses(a,1) << " " << pulses(a,2)  << " " << pulses(a,3) << endl;
    }
  
  for(int i=0; i <3*K; i++){
    for(int j=0; j < 3*L; j++){
      
      xi = aE + float(i)*dx;
      yj = cE + float(j)*dy;
        
      xip1 = aE + float(i+1)*dx;
      yjp1 = cE + float(j+1)*dy;
        
      xia = 0.5*(xi+xip1);
      yja = 0.5*(yj+yjp1);


      for(int a = 0; a < nA; ++a){
        xa = pulses(a,0);
        ya = pulses(a,1);
        ra = pulses(a,3);
        
        if(ra > epsilon)
          {
          if(  sqrt( (xi-xa)*(xi-xa) + (yj-ya)*(yj-ya) )         < ra &&
               sqrt( (xip1-xa)*(xip1-xa) + (yj-ya)*(yj-ya))      < ra &&                       
               sqrt( (xip1-xa)*(xip1-xa) + (yjp1-ya)*(yjp1-ya) ) < ra &&
               sqrt( (xi-xa)*(xi-xa) + (yjp1-ya)*(yjp1-ya) )     < ra)            
            mkt(i,j)   = 0;                                
        }
      }
    }
  }
  
  //right
  for(int i=0; i < K; i++)        
    for(int j=0; j < L; j++){        
      if(mkt(K+i,2*L+j) == 0)
        mkt(K+i,L+j) = mkt(K+i,2*L+j);               
    }
  
  //left
  for(int i=0; i < K; i++)
    for(int j=0; j < L; j++){        
      if(mkt(K+i,j) == 0)
        mkt(K+i,L+j) = mkt(K+i,j);       
    }
    
  //top
  for(int i=0; i < K; i++)
    for(int j=0; j < L; j++){
      if(mkt(i,L+j) == 0)
        mkt(K+i,L+j) = mkt(i,L+j);
    }
  
  //buttom
  for(int i=0; i < K; i++)
    for(int j=0; j <L; j++){
      if(mkt(2*K+i,L+j) == 0)
        mkt(K+i,L+j) = mkt(2*K+i,L+j);       
    }
  

  omyfile << K <<"  "<< L << endl;

  for(int k =0; k < K; ++k){
    for(int l=0; l < L ; ++l){
      mk(k,l) =  mkt(K+k,L+l);
      omyfile << mk(k,l) << " ";
    }
    omyfile << endl;
  }

  
  
  imyfile.close();
  omyfile.close();
  
}

void deltasMatrixAndIndices(int K, int L, MatrixXi &mk, string oDeltasMatrixFile, string oDeltasIndicesFile, int skip=2){

  MatrixXi md;
  ofstream omyfile1;    
  ofstream omyfile2;
  int nDeltas = 0;

  md.resize(K,L);
  md = 0;
  
  omyfile1.open(oDeltasMatrixFile);
  omyfile2.open(oDeltasIndicesFile);

  for(int k=0; k < K; k+=skip){
    for(int l=0; l < L; l+=skip){      
      if(mk(k,l) == 1){
        md(k,l) = 1;
        nDeltas++;
      }
    }
  }

  omyfile1 << K << " " << L << endl;
  omyfile2 << nDeltas << endl;
  
  for(int k=0; k < K; ++k){
    for(int l=0; l < L; ++l){
      omyfile1 << md(k,l) << " "; 

      if(md(k,l) == 1){
        omyfile2 << k << " " << l << endl;
      }        
    }
    omyfile1 << endl;
  }
  
  omyfile1.close();
  omyfile2.close();
}


void readAxonsMatrix(string nfile, MatrixXi & m){
  
  ifstream imyfile;      
  imyfile.open(nfile.c_str());
  int K, L;
  
  imyfile >> K >> L;
  m.resize(K,L);
  
  for(int k=0; k < K; ++k){
    for(int l=0; l < L; ++l){      
      imyfile >> m(k,l);
    }
  }
  
  imyfile.close();
}

int main(int argc, char* argv[]){

  if(argc < 10){
    cerr << "To run ./dGenerator a b c d K L skip_delta discard_radius axons_file_name " << endl;
    exit(0);
  }

  float a = atof(argv[1]);
  float b = atof(argv[2]);
  float c = atof(argv[3]);
  float d = atof(argv[4]);
  
  int K  = atoi(argv[5]);
  int L  = atoi(argv[6]);
  int skip  = atoi(argv[7]);
  
  MatrixXi mk;
  
  float epsilon = atof(argv[8]);  
  string iAxonsFile(argv[9]);

  string token = iAxonsFile.substr(0, iAxonsFile.find(".txt"));

  string oAxonsFile  = token+"_axons"+"_Matrix_"+to_string(K)+"x"+to_string(L)+".txt";
  string oDeltasMatrixFile  = token+"_deltas"+"_Matrix_"+to_string(K)+"x"+to_string(L)+".txt";
  string oDeltasIndicesFile = token+"_deltas"+"_Indices_"+to_string(K)+"x"+to_string(L)+".txt";
  
  diffCoeffMatrix(a,b,c,d,K,L,iAxonsFile,oAxonsFile,mk,epsilon);
  //readAxonsMatrix(oAxonsFile,mk);
  //deltasMatrixAndIndices(K,L,mk,oDeltasMatrixFile,oDeltasIndicesFile,skip);
  
  return 0;  
}
