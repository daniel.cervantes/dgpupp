#include <cuda.h>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>
#include <iostream>
#include "common.h"

const int TILE_SIZE = 16;

const int ROWS = 8;
const int COLUMNS = 9;
const bool PARTIAL_PIVOT = false;

void DisplayMatrix(double **Matrix)
{
	for(int i =0;i<ROWS;i++)
	{
		for(int j =0;j<COLUMNS;j++)
		{
			std::cout<<Matrix[i][j]<<"\t";
		}
		std::cout<<std::endl;
	}
}


__global__ void ScaleRowKernel(double* matrix, unsigned int numberOfRows, unsigned int numberOfColumns, double* outputMatrix, int current_column)
{
  int tx = (blockIdx.x*blockDim.x) + threadIdx.x;
  int ty = (blockIdx.y*blockDim.y) + threadIdx.y;
  int tID = (ty*numberOfColumns)+tx;

  if(current_column == ty && ty < numberOfRows)
    {
      if(tx < numberOfColumns)
        {
          outputMatrix[tID] = matrix[tID]/matrix[(current_column*numberOfColumns)+current_column];
        }
    }
}

__global__ void SubtractRowKernel(double* matrix, unsigned int numberOfRows, unsigned int numberOfColumns, double* outputMatrix, int current_column)
{
  int tx = (blockIdx.x*blockDim.x) + threadIdx.x;
  int ty = (blockIdx.y*blockDim.y) + threadIdx.y;
  int tID = (ty*numberOfColumns)+tx;
  if(current_column != ty && ty < numberOfRows)
    {
      if(tx < numberOfColumns)
        {
          outputMatrix[tID] = matrix[tID] - (matrix[(current_column*numberOfColumns)+tx] * matrix[(ty*numberOfColumns)+current_column]);
        }
    }
}


bool GaussianEliminationGPU( double** matrix, unsigned int numberOfRows, unsigned int numberOfColumns, double** outputMatrix, bool partialPivot )
{

  int bytes = numberOfRows * numberOfColumns * sizeof(double);
  double *Md, *Pd;

  double *M = new double[bytes];
  double *P = new double[bytes];
  int count = 0;

  
  for(int i=0;i<numberOfRows;i++)
    {
      for(int j=0;j<numberOfColumns;j++)
        {
          M[count] = matrix[i][j];
          count++;
        }
    }

  // Allocate memory on the device to store each matrix
  cudaMalloc((void**) &Md, bytes);
  cudaMalloc((void**) &Pd, bytes);

  // Copy the host input data to the device
  cudaMemcpy(Md, M, bytes, cudaMemcpyHostToDevice);
  cudaMemcpy(Pd, Md, bytes, cudaMemcpyDeviceToDevice);

  // Specify the size of the grid and the size of the block
  dim3 dimBlock(TILE_SIZE, TILE_SIZE);
  dim3 dimGrid((int)ceil((double)numberOfColumns / (double)TILE_SIZE), (int)ceil((double)numberOfRows / (double)TILE_SIZE));

  //std::cout << "\nnumber of rows: "<<numberOfRows;
  for(int i=0;i<numberOfRows;i++)
    {
      ScaleRowKernel<<<dimGrid, dimBlock>>>(Md, numberOfRows, numberOfColumns, Pd, i);
      cudaDeviceSynchronize();
      cudaMemcpy(Md, Pd, bytes, cudaMemcpyDeviceToDevice);
      SubtractRowKernel<<<dimGrid, dimBlock>>>(Md, numberOfRows, numberOfColumns, Pd, i);
      cudaDeviceSynchronize();
      cudaMemcpy(Md, Pd, bytes, cudaMemcpyDeviceToDevice);
    }
  // Retrieve the result matrix
  cudaMemcpy(P, Md, bytes, cudaMemcpyDeviceToHost);

  
  count = 0;
  for(int i=0;i<numberOfRows;i++)
    {
      for(int j=0;j<numberOfColumns;j++)
        {
          outputMatrix[i][j] = P[count];
          count++;
        }
    }

  // Free device memory
  cudaFree(Md);
  cudaFree(Pd);
  delete[] P;
  delete[] M;

  // Success
  return true;
}





int main(){
  

  double** InputMatrix = new double*[ROWS];
  for(int i =0;i<ROWS;i++)
    InputMatrix[i] = new double[COLUMNS];
  
  
  double** OutputMatrixGPU = new double*[ROWS];
  for(int i =0;i<ROWS;i++)
    OutputMatrixGPU[i] = new double[COLUMNS];
  
  // for(int i =0;i<ROWS;i++)
  //   {
  //     for(int j =0;j<COLUMNS;j++)
  //       InputMatrix[i][j] = (double)(rand())/(double)(RAND_MAX);
  //   }



  InputMatrix[0][0]=0.3905704;  InputMatrix[0][1]=0.1203732;   InputMatrix[0][2]=0.3278683;   InputMatrix[0][3]=0.7988904;   InputMatrix[0][4]=0.8844012;   InputMatrix[0][5]=0.8693223;   InputMatrix[0][6]=0.2959154;   InputMatrix[0][7]=0.7093280;  // 0->
  InputMatrix[1][0]=0.8960144;  InputMatrix[1][1]=0.8573488;   InputMatrix[1][2]=0.4098551;   InputMatrix[1][3]=0.1344433;   InputMatrix[1][4]=0.3965334;   InputMatrix[1][5]=0.9324326;   InputMatrix[1][6]=0.0483210;   InputMatrix[1][7]=0.9077270; // 1->
  InputMatrix[2][0]=0.0788482;  InputMatrix[2][1]=0.4900649;   InputMatrix[2][2]=0.1723956;   InputMatrix[2][3]=0.2801510;   InputMatrix[2][4]=0.4018362;   InputMatrix[2][5]=0.0639029;   InputMatrix[2][6]=0.6679377;   InputMatrix[2][7]=0.7314400; // 2->
  InputMatrix[3][0]=0.7200132;  InputMatrix[3][1]=0.1704138;   InputMatrix[3][2]=0.0661138;   InputMatrix[3][3]=0.0048406;   InputMatrix[3][4]=0.1209832;   InputMatrix[3][5]=0.2424235;   InputMatrix[3][6]=0.3727946;   InputMatrix[3][7]=0.0018467; // 3->
  InputMatrix[4][0]=0.5995938;  InputMatrix[4][1]=0.2268861;   InputMatrix[4][2]=0.7479017;   InputMatrix[4][3]=0.6032921;   InputMatrix[4][4]=0.8963992;   InputMatrix[4][5]=0.9961749;   InputMatrix[4][6]=0.4156670;   InputMatrix[4][7]=0.2061578; // 4->
  InputMatrix[5][0]=0.3838716;  InputMatrix[5][1]=0.8081439;   InputMatrix[5][2]=0.4801946;   InputMatrix[5][3]=0.2760695;   InputMatrix[5][4]=0.4855037;   InputMatrix[5][5]=0.3293748;   InputMatrix[5][6]=0.9835563;   InputMatrix[5][7]=0.8329754; // 5->
  InputMatrix[6][0]=0.3285128;  InputMatrix[6][1]=0.6435567;   InputMatrix[6][2]=0.5604067;   InputMatrix[6][3]=0.9287390;   InputMatrix[6][4]=0.0181781;   InputMatrix[6][5]=0.5781228;   InputMatrix[6][6]=0.9688212;   InputMatrix[6][7]=0.1973067; // 6->
  InputMatrix[7][0]=0.7697594;  InputMatrix[7][1]=0.3863994;   InputMatrix[7][2]=0.6437610;   InputMatrix[7][3]=0.6473042;   InputMatrix[7][4]=0.2657015;   InputMatrix[7][5]=0.6510384;   InputMatrix[7][6]=0.1421472;   InputMatrix[7][7]=0.3517238; // 7->

  InputMatrix[0][8] =0.296125;
  InputMatrix[1][8]=0.463490;
  InputMatrix[2][8]=0.767997;
  InputMatrix[3][8]=0.737384;
  InputMatrix[4][8]=0.445600;
  InputMatrix[5][8]=0.322170;
  InputMatrix[6][8]=0.537043;
  InputMatrix[7][8]=0.055739;

  
  std::cout<<"\noperating on a "<<ROWS<<" x "<<COLUMNS<<" matrix"<<std::endl;
  GaussianEliminationGPU(InputMatrix, ROWS, COLUMNS, OutputMatrixGPU, PARTIAL_PIVOT);
  DisplayMatrix(OutputMatrixGPU);
  
  
  for (int i=0; i<ROWS; i++)
    delete [] InputMatrix[i];
  delete [] InputMatrix;
  
  
  for (int i=0; i<ROWS; i++)
    delete [] OutputMatrixGPU[i];
  delete [] OutputMatrixGPU;
  
  
  return 0;
  
}
