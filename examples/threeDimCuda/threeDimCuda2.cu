#include <stdio.h>
#include <stdlib.h>

//#define BLOCK_SIZE 16


// __global__ void ScaleRowKernel(double* matrix, unsigned int numberOfRows, unsigned int numberOfColumns, double* outputMatrix, int current_column)
// {
//   int tx = (blockIdx.x*blockDim.x) + threadIdx.x;
//   int ty = (blockIdx.y*blockDim.y) + threadIdx.y;
//   int tID = (ty*numberOfColumns)+tx;

//   if(current_column == ty && ty < numberOfRows)
//     {
//       if(tx < numberOfColumns)
//         {
//           outputMatrix[tID] = matrix[tID]/matrix[(current_column*numberOfColumns)+current_column];
//         }
//     }
// }

// __global__ void SubtractRowKernel(double* matrix, unsigned int numberOfRows, unsigned int numberOfColumns, double* outputMatrix, int current_column)
// {
//   int tx = (blockIdx.x*blockDim.x) + threadIdx.x;
//   int ty = (blockIdx.y*blockDim.y) + threadIdx.y;
//   int tID = (ty*numberOfColumns)+tx;
//   if(current_column != ty && ty < numberOfRows)
//     {
//       if(tx < numberOfColumns)
//         {
//           outputMatrix[tID] = matrix[tID] - (matrix[(current_column*numberOfColumns)+tx] * matrix[(ty*numberOfColumns)+current_column]);
//         }
//     }
// }


// __global__ void ThreeNestedLoops(){
  
//   int i = threadIdx.x + blockIdx.x * blockDim.x;
//   int j = threadIdx.y + blockIdx.y * blockDim.y;
//   int k = threadIdx.z + blockIdx.z * blockDim.z;

//   printf("GPU - i = %d, j = %d, k = %d\n",i,j,k);
  
// }



// __global__ void gaussElim(float * ma,  int n, int m, int l, int r){
  
//   int ii = threadIdx.x + blockIdx.x * blockDim.x;
//   int jj = threadIdx.y + blockIdx.y * blockDim.y;
//   int kk = threadIdx.z + blockIdx.z * blockDim.z;

  
//   if(ii < n && jj < m && kk < l){

//     //printf("%d %d %d\n",ii,jj,kk);
//     //printf("E1: ii->%d jj->%d \n",ii,jj);
    
//     float c;
//     int R = 8;
//     int C = 8;

//     // printf( "00-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[0],ma[1],ma[2],ma[3],ma[4],ma[5],ma[6],ma[7],ma[8]);
//     // printf(" 01-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[9],ma[10],ma[11],ma[12],ma[13],ma[14],ma[15],ma[16],ma[17]);
//     // printf(" 02-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[18],ma[19],ma[20],ma[21],ma[22],ma[23],ma[24],ma[25],ma[26]);
//     // printf(" 03-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[27],ma[28],ma[29],ma[30],ma[31],ma[32],ma[33],ma[34],ma[35]);
//     // printf(" 04-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[36],ma[37],ma[38],ma[39],ma[40],ma[41],ma[42],ma[43],ma[44]);
//     // printf(" 05-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[45],ma[46],ma[47],ma[48],ma[49],ma[50],ma[51],ma[52],ma[53]);
//     // printf(" 06-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[54],ma[55],ma[56],ma[57],ma[58],ma[59],ma[60],ma[61],ma[62]);
//     // printf(" 07-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[63],ma[64],ma[65],ma[66],ma[67],ma[68],ma[69],ma[70],ma[71]);

    

//     //for(int j=0; j < R; ++j){
//     for(int i=r; i < R; ++i){        
//       if(i!=r){
//         //printf(" i=%d \n",i);
//         c = ma[i*(C+1)+r]/ma[r*(C+1)+r];        
//         printf("ii=%d jj=%d kk=%d i=%d r=%d %.6f %.6f %.6f\n",ii,jj,kk,i,r,ma[i*(C+1)+r],ma[r*(C+1)+r],c);
//         //for(int  k=0; k < C+1; ++k){
//         //printf("%d  %.3f  %.3f\n",i*(C+1)+r,ma[i*(C+1)+r],c);
//         //ma[i*(C+1)+k] =  ma[i*(C+1)+k] - c*ma[r*(C+1)+k];
//         //printf("0ima = %d  man=%.3f mad=%.3f  c=%.3f\n",i*(C+1)+r,ma[i*(C+1)+r],ma[r*(C+1)+r],c);
//         //printf("i= %d r=%d ma=%.6f c*ma = %.6f \n",i,r,ma[i*(C+1)],c*ma[r*(C+1)]);
//         ma[i*(C+1)]   =  ma[i*(C+1)]   - c*ma[r*(C+1)];
//         ma[i*(C+1)+1] =  ma[i*(C+1)+1] - c*ma[r*(C+1)+1];
//         ma[i*(C+1)+2] =  ma[i*(C+1)+2] - c*ma[r*(C+1)+2];
//         ma[i*(C+1)+3] =  ma[i*(C+1)+3] - c*ma[r*(C+1)+3];
//         ma[i*(C+1)+4] =  ma[i*(C+1)+4] - c*ma[r*(C+1)+4];
//         ma[i*(C+1)+5] =  ma[i*(C+1)+5] - c*ma[r*(C+1)+5];
//         ma[i*(C+1)+6] =  ma[i*(C+1)+6] - c*ma[r*(C+1)+6];
//         ma[i*(C+1)+7] =  ma[i*(C+1)+7] - c*ma[r*(C+1)+7];

//         printf("ii=%d jj=%d kk=%d i=%d r=%d ma=%.6f ma=%.6f ma=%.6f ma=%.6f ma=%.6f ma=%.6f ma=%.6f ma=%.6f \n",
//                ii,jj,kk,i,r,ma[i*(C+1)],ma[i*(C+1)+1],ma[i*(C+1)+2],ma[i*(C+1)+3],ma[i*(C+1)+4],ma[i*(C+1)+5],ma[i*(C+1)+6],ma[i*(C+1)+7]);
        
//         //printf("i=%d r=%d man=%.6f iman=%d  mad=%.6f c=%.6f ma =%.6f\n",i,r,ma[i*(C+1)+r],i*(C+1)+r,ma[r*(C+1)+r],c,ma[i*(C+1)+k]);
//         //}
//         //printf(" ma=%.3f \n",ma[i*(C+1)]);

//       }
//     }
//       //}

//     printf( "G00-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[0],ma[1],ma[2],ma[3],ma[4],ma[5],ma[6],ma[7],ma[8]);
//     printf(" G01-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[9],ma[10],ma[11],ma[12],ma[13],ma[14],ma[15],ma[16],ma[17]);
//     printf(" G02-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[18],ma[19],ma[20],ma[21],ma[22],ma[23],ma[24],ma[25],ma[26]);
//     printf(" G03-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[27],ma[28],ma[29],ma[30],ma[31],ma[32],ma[33],ma[34],ma[35]);
//     printf(" G04-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[36],ma[37],ma[38],ma[39],ma[40],ma[41],ma[42],ma[43],ma[44]);
//     printf(" G05-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[45],ma[46],ma[47],ma[48],ma[49],ma[50],ma[51],ma[52],ma[53]);
//     printf(" G06-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[54],ma[55],ma[56],ma[57],ma[58],ma[59],ma[60],ma[61],ma[62]);
//     printf(" G07-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[63],ma[64],ma[65],ma[66],ma[67],ma[68],ma[69],ma[70],ma[71]);

    
//     // for(i=0; i < R; ++i){
//     //   for(j=0; j < C; ++j)
//     //     printf("%.f ",ma[i*C+j]);
//     //   printf("\n");
//     // }
    

//     // for(j=1; j < C+1; ++j){
//     //   c=0.0;
//     //   for(i=R-1; i > R-j; i--)      
//     //     c = c + ma[(R-j)*(C+1)+i]*x[i];
//     //   x[C-j] = (ma[(C-j)*(C+1)+C] - c)/(ma[(C-j)*(C+1)+(C-j)]);
//     // }


//     // Valores reales
//     //  1.0858
//     //  7.6335
//     // -4.9111
//     //  3.4012
//     //  5.6306
//     // -3.3618
//     // -2.3985
//     // -4.9360


    
//     //printf("x-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f  \n",x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7]);
    
//   }


// }


// __global__ void gaussElim1(float * ma, int n, int m, int l){
  
//   int ii = threadIdx.x + blockIdx.x * blockDim.x;
//   int jj = threadIdx.y + blockIdx.y * blockDim.y;
//   //int kk = threadIdx.z + blockIdx.z * blockDim.z;

  
//   //if(ii < n && jj < m && kk < l){
//   if(ii < n && jj < m ){
//     //printf("%d %d %d\n",ii,jj,kk);
//     //printf("E1: ii->%d jj->%d \n",ii,jj);
    
//     int i;
//     int j;
//     int k;

//     float c;

//     int R = 8;
//     int C = 8;

//     printf( "00-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[0],ma[1],ma[2],ma[3],ma[4],ma[5],ma[6],ma[7],ma[8]);
//     printf(" 01-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[9],ma[10],ma[11],ma[12],ma[13],ma[14],ma[15],ma[16],ma[17]);
//     printf(" 02-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[18],ma[19],ma[20],ma[21],ma[22],ma[23],ma[24],ma[25],ma[26]);
//     printf(" 03-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[27],ma[28],ma[29],ma[30],ma[31],ma[32],ma[33],ma[34],ma[35]);
//     printf(" 04-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[36],ma[37],ma[38],ma[39],ma[40],ma[41],ma[42],ma[43],ma[44]);
//     printf(" 05-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[45],ma[46],ma[47],ma[48],ma[49],ma[50],ma[51],ma[52],ma[53]);
//     printf(" 06-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[54],ma[55],ma[56],ma[57],ma[58],ma[59],ma[60],ma[61],ma[62]);
//     printf(" 07-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[63],ma[64],ma[65],ma[66],ma[67],ma[68],ma[69],ma[70],ma[71]);

    

//     for(j=0; j < R; ++j){
//       for(i=j; i < C; ++i){        
//         if(i!=j){
//           c = ma[i*(C+1)+j]/ma[j*(C+1)+j];
//           for(k=0; k < C+1; ++k){          
//             ma[i*(C+1)+k] =  ma[i*(C+1)+k] - c*ma[j*(C+1)+k];
//             //printf("c=%.6f ma =%.6f\n",c,ma[i*(C+1)+k]);
//           }

//         }
//       }
//     }

//     __syncthreads();
//     printf( "G00-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[0],ma[1],ma[2],ma[3],ma[4],ma[5],ma[6],ma[7],ma[8]);
//     printf(" G01-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[9],ma[10],ma[11],ma[12],ma[13],ma[14],ma[15],ma[16],ma[17]);
//     printf(" G02-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[18],ma[19],ma[20],ma[21],ma[22],ma[23],ma[24],ma[25],ma[26]);
//     printf(" G03-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[27],ma[28],ma[29],ma[30],ma[31],ma[32],ma[33],ma[34],ma[35]);
//     printf(" G04-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[36],ma[37],ma[38],ma[39],ma[40],ma[41],ma[42],ma[43],ma[44]);
//     printf(" G05-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[45],ma[46],ma[47],ma[48],ma[49],ma[50],ma[51],ma[52],ma[53]);
//     printf(" G06-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[54],ma[55],ma[56],ma[57],ma[58],ma[59],ma[60],ma[61],ma[62]);
//     printf(" G07-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[63],ma[64],ma[65],ma[66],ma[67],ma[68],ma[69],ma[70],ma[71]);

    
//     // for(i=0; i < R; ++i){
//     //   for(j=0; j < C; ++j)
//     //     printf("%.f ",ma[i*C+j]);
//     //   printf("\n");
//     // }
    

//     // for(j=1; j < C+1; ++j){
//     //   c=0.0;
//     //   for(i=R-1; i > R-j; i--)      
//     //     c = c + ma[(R-j)*(C+1)+i]*x[i];
//     //   x[C-j] = (ma[(C-j)*(C+1)+C] - c)/(ma[(C-j)*(C+1)+(C-j)]);
//     // }


//     // Valores reales
//     //  1.0858
//     //  7.6335
//     // -4.9111
//     //  3.4012
//     //  5.6306
//     // -3.3618
//     // -2.3985
//     // -4.9360


    
//     //printf("x-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f  \n",x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7]);
    
//   }


// }

// __global__ void gaussElim2(float * ma, float *x, int n, int m, int l){

//   int ii = threadIdx.x + blockIdx.x * blockDim.x;
//   int jj = threadIdx.y + blockIdx.y * blockDim.y;
//   //  int kk = threadIdx.z + blockIdx.z * blockDim.z;

  
//   //if(ii < n && jj < m && kk < l){
//   if(ii < n && jj < m){
//     //printf("%d %d %d\n",ii,jj,kk);
//     //printf("E2: ii->%d jj->%d \n",ii,jj);
//     int i;
//     int j;
//     //int k;

//     float c;

//     int R = 8;
//     int C = 8;

//     // printf( "10-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[0],ma[1],ma[2],ma[3],ma[4],ma[5],ma[6],ma[7],ma[8]);
//     // printf(" 11-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[9],ma[10],ma[11],ma[12],ma[13],ma[14],ma[15],ma[16],ma[17]);
//     // printf(" 12-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[18],ma[19],ma[20],ma[21],ma[22],ma[23],ma[24],ma[25],ma[26]);
//     // printf(" 13-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[27],ma[28],ma[29],ma[30],ma[31],ma[32],ma[33],ma[34],ma[35]);
//     // printf(" 14-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[36],ma[37],ma[38],ma[39],ma[40],ma[41],ma[42],ma[43],ma[44]);
//     // printf(" 15-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[45],ma[46],ma[47],ma[48],ma[49],ma[50],ma[51],ma[52],ma[53]);
//     // printf(" 16-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[54],ma[55],ma[56],ma[57],ma[58],ma[59],ma[60],ma[61],ma[62]);
//     // printf(" 17-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[63],ma[64],ma[65],ma[66],ma[67],ma[68],ma[69],ma[70],ma[71]);

    

//     // for(j=0; j < R; ++j){
//     //   for(i=j; i < C; ++i){        
//     //     if(i!=j){
//     //       c = ma[i*(C+1)+j]/ma[j*(C+1)+j];
//     //       for(k=0; k < C+1; ++k)
//     //         ma[i*(C+1)+k] =  ma[i*(C+1)+k] - c*ma[j*(C+1)+k];
//     //     }
//     //   }
//     // }


//     // printf( "G0-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[0],ma[1],ma[2],ma[3],ma[4],ma[5],ma[6],ma[7],ma[8]);
//     // printf(" G1-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[9],ma[10],ma[11],ma[12],ma[13],ma[14],ma[15],ma[16],ma[17]);
//     // printf(" G2-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[18],ma[19],ma[20],ma[21],ma[22],ma[23],ma[24],ma[25],ma[26]);
//     // printf(" G3-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[27],ma[28],ma[29],ma[30],ma[31],ma[32],ma[33],ma[34],ma[35]);
//     // printf(" G4-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[36],ma[37],ma[38],ma[39],ma[40],ma[41],ma[42],ma[43],ma[44]);
//     // printf(" G5-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[45],ma[46],ma[47],ma[48],ma[49],ma[50],ma[51],ma[52],ma[53]);
//     // printf(" G6-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[54],ma[55],ma[56],ma[57],ma[58],ma[59],ma[60],ma[61],ma[62]);
//     // printf(" G7-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[63],ma[64],ma[65],ma[66],ma[67],ma[68],ma[69],ma[70],ma[71]);

    
//     // for(i=0; i < R; ++i){
//     //   for(j=0; j < C; ++j)
//     //     printf("%.f ",ma[i*C+j]);
//     //   printf("\n");
//     // }
    

//     for(j=1; j < C+1; ++j){
//       c=0.0;
//       for(i=R-1; i > R-j; i--)      
//         c = c + ma[(R-j)*(C+1)+i]*x[i];
//       x[C-j] = (ma[(C-j)*(C+1)+C] - c)/(ma[(C-j)*(C+1)+(C-j)]);
//     }


//     // Valores reales
//     //  1.0858
//     //  7.6335
//     // -4.9111
//     //  3.4012
//     //  5.6306
//     // -3.3618
//     // -2.3985
//     // -4.9360


    
//     printf("x-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f  \n",x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7]);
    
//   }


// }


// __global__ void setM(float * m, int i, int j, int k, int dim, int N, int M, int L, float v){
  
//   int ii = threadIdx.x + blockIdx.x * blockDim.x;
//   int jj = threadIdx.y + blockIdx.y * blockDim.y;
//   int kk = threadIdx.z + blockIdx.z * blockDim.z;

  
// }


__global__ void ThreeNestedLoops(int  limitx, int limity, int limitz, int numberOfThreads){

  int threadId = threadIdx.x + blockIdx.x * blockDim.x;
  if(threadId > numberOfThreads -1) return;

  int i = threadId%limitx;
  int j = threadId%limity;
  int k = threadId%limitz;
  
  printf("GPU - i =%d, j=%d, k=%d\n",i,j,k);
}

int main(){

  int limitx = 200;
  int limity = 200;
  int limitz = 200;

  int threadLimitPerBlock = 1024;
  int numberOfThreads = limitx*limity*limitz;
  
  int requiredNumberOfBlocks = (numberOfThreads/threadLimitPerBlock)+1;

  dim3 dimGrid(requiredNumberOfBlocks,1,1);
  dim3 dimBlock(threadLimitPerBlock,1,1);  

  // cudaMalloc((void**) &d_ma,bytesM);
  // cudaMalloc((void**) &d_x,bytesV);

  // cudaMemcpy(d_ma,ma,bytesM,cudaMemcpyHostToDevice);
  
  
  //for(int i=0; i < r; ++i){
  ThreeNestedLoops<<<dimGrid, dimBlock>>>(limitx,limity,limitz,numberOfThreads);
  cudaDeviceSynchronize();
    // gaussElim2<<<dimGrid, dimBlock>>>(d_ma,d_x,n,m,l);

    //}
  // cudaMemcpy(ma,d_ma,bytesM,cudaMemcpyDeviceToHost);

  // for(int i=0; i < r; ++i){
  //   for(int j=0; j < c+1; ++j)
  //     printf("%.6f ",ma[i*(c+1)+j]);
  //   printf("\n");
  // }

  
  // int K = 20;
  // int L = 20;
  // int M = 20;

  // unsigned int grid_i = ((K+2) + BLOCK_SIZE - 1) / BLOCK_SIZE;
  // unsigned int grid_j = ((L+2) + BLOCK_SIZE - 1) / BLOCK_SIZE;
  // unsigned int grid_k = ((M+2) + BLOCK_SIZE - 1) / BLOCK_SIZE;

  
  // dim3 dimGrid(10,10,10);
  // dim3 dimBlock(10, 10, 10);  

  // cudaDeviceProp prop;   cudaGetDeviceProperties( &prop, 0);
  // ThreeNestedLoops <<< dimGrid, dimBlock >>>();
  // cudaDeviceSynchronize();

  return 0;  
}
