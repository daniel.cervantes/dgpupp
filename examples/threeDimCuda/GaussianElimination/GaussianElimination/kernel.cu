﻿/*   Gaussian Elimination.
*    
*    Copyright (C) 2012-2013 Orange Owl Solutions.  
*
*    This file is part of Bluebird Library.
*    Gaussian Elimination is free software: you can redistribute it and/or modify
*    it under the terms of the Lesser GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Gaussian Elimination is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    Lesser GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Gaussian Elimination.  If not, see <http://www.gnu.org/licenses/>.
*
*
*    For any request, question or bug reporting please visit http://www.orangeowlsolutions.com/
*    or send an e-mail to: info@orangeowlsolutions.com
*
*
*/







#include <conio.h>
#include <fstream>
#include <iostream> 
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#include <cublas_v2.h>

#include "TimingCPU.h"
#include "TimingGPU.cuh"

// --- This project contains CPU and GPU routines for solving a linear system of equations by Gaussian elimination without pivoting.
//     Besides providing standard CPU Gaussian elimination and solution of an upper triangular system, sequential and parallel codes have been developed
//     based on the paper
//
//     Manuel Carcenac, "From tile algorithm to stripe algorithm: a CUBLAS-based parallel implementation on GPUs of Gauss method for the resolution
//                       of extremely large dense linear systems stored on an array of solid state devices", Journal of Supercomputing, DOI 10.1007/s11227-013-1043-3
//
//     and on the presentation "Application: linear system resolution with Gauss method" available at the Author's webpage.
//     Five GPU approaches have been targeted, namely:
//     1) "Tiling" with kernels not using shared memory;
//     2) "Tiling" with kernels using shared memory;
//     3) "Brute-force" cuBLAS based approach;
//     4) "Tiling" approach using cuBLAS;
//     5) "Tiling & stripping" approach using cuBLAS;
//
// --- All the CPU routines use a row-major ordering. The CPU code introduced and commented within the GPU procedures uses a column-major ordering.
//     Column-major ordering is indeed used by cuBLAS and so the CPU code needs to be converted to such an ordering to match the GPU results.
//     All the routines return "in-place" results. This will require some useless memory transactions for the GPU case when passing from Gaussian
//     elimination to the solution of an upper triangular system. However, this point can be easily improved by any user.
//
// --- There is also a Matlab simple script to check the consistency of the C/C++ results against Matlab's. Do not expect exact match, since the
//     included routines do not perform pivoting, while Matlab is using a more sophisticated technique to obtain the inversion.
//
// --- IMPLEMENTED PROCEDURES
//
//     forward_elimination_CPU: This procedure performs standard sequential Gaussian elimination without pivoting
//
//	   solution_of_a_triangular_system_CPU: This procedure solves an upper triangular linear system without pivoting on the CPU.
//
//     forward_elimination_CPU_alternative: This procedure performs standard sequential Gaussian elimination without pivoting. The difference with forward_elimination_CPU is that the
//     procedure outputs the "pivoting" matrix R also.
//
//     forward_elimination_CPU_tiling: This procedure performs standard sequential Gaussian elimination without pivoting with the tiling technique. 
//     It is for demonstration only, as it can serve for better understanding of the related parallel routine using tiling.
//
//     forward_elimination_CPU_tiling_stripping: This procedure performs standard sequential Gaussian elimination without pivoting with the tiling technique. It is for demonstration only, as it
//     can serve for better understanding of the related parallel routine using tiling and stripping.
//
//     forward_elimination_GPU_tiling: This procedure performs parallel Gaussian elimination without pivoting. It implements both a version without and with using shared memory.
//     Please, comment/uncomment the related lines
//
//	   solution_of_a_triangular_system_GPU: This procedure solves an upper triangular linear system without pivoting on the GPU using cuBLAS.
//
//     forward_elimination_GPU_cuBLAS: This procedure performs parallel Gaussian elimination without pivoting. It is a "brute-force" cuBLAS implementation.
//
//     forward_elimination_GPU_tiling_cuBLAS: This procedure performs parallel Gaussian elimination without pivoting. It is a tiling based cuBLAS implementation.
//
//     forward_elimination_GPU_blocks_stripping_cuBLAS: This procedure performs parallel Gaussian elimination without pivoting. It is a tiling-stripping based cuBLAS implementation.

using namespace std;

#define N					256
#define Matrix_Block_Size	32
#define Matrix_Stripe_Size	8

#define THRESHOLD_PIVOT		1e-20

/***********************/
/* CUDA ERROR CHECKING */
/***********************/
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

/**********************/
/* cuBLAS ERROR CHECK */
/**********************/
#ifndef cublasSafeCall
#define cublasSafeCall(err)     __cublasSafeCall(err, __FILE__, __LINE__)
#endif

inline void __cublasSafeCall(cublasStatus_t err, const char *file, const int line)
{
    if( CUBLAS_STATUS_SUCCESS != err) {
		fprintf(stderr, "CUBLAS error in file '%s', line %d\n \nerror %d \nterminating!\n",__FILE__, __LINE__,err); 
		getch(); cudaDeviceReset(); assert(0); 
    }
}

/********************************/
/* MATRIX RANDOM INITIALIZATION */
/********************************/
void init_matrices(double* A, double* B) {

	srand (time(NULL));
	
	for (int i=0; i<N; i++) {
		B[i] = rand();
		for (int j=0; j<N; j++)
			A[i*N+j] = rand();
	}

}

/*****************/
/* MATRIX SAVING */
/*****************/
void save_matrix(double* A, int len, char* filenameA) {
	
	ofstream outfile;
	outfile.open(filenameA);
	for(int i=0; i<len; i++)  outfile << A[i] << "\n";
	outfile.close();

}

/***************************/
/* FORWARD ELIMINATION CPU */
/***************************/
// --- This procedure performs standard sequential Gaussian elimination without pivoting
void forward_elimination_CPU(double* A, double* B) {
	
	for (int i=0; i<N-1; i++) {
		#pragma omp parallel for
		for (int j=i+1; j<N; j++) { 
			double ratio = A[j*N+i]/A[i*N+i]; 
            B[j] -= ratio*B[i]; 
			for (int k=i; k<N; k++) { 
                A[j*N+k] -= ratio*A[i*N+k]; 
			} 
		} 
	}

	// --- Alternative version
	//for (int i=0; i<N-1; i++) {
		//#pragma omp parallel for
	//	for (int j=i+1; j<N; j++) { 
	//		double ratio = A[i*N+i]/A[j*N+i]; 
 //           B[j] = B[i]-ratio*B[j]; 
	//		for (int k=i; k<N; k++) { 
 //               A[j*N+k] = A[i*N+k]-ratio*A[j*N+k]; 
	//		} 
	//	} 
	//}

}

/*************************************************/
/* SOLUTION OF AN UPPER TRINAGULAR SYSTEM ON CPU */
/*************************************************/
// --- This procedure solves an upper triangular linear system without pivoting on the CPU.
int solution_of_a_triangular_system_CPU(double* A, double* B, double* x) {
	
	if (fabs(A[(N-1)*N+(N-1)]) < THRESHOLD_PIVOT) return 0;

	x[N-1] = B[N-1] / A[(N-1)*N+(N-1)];
	
	for (int i=N-2; i>=0; i--) {
		double s = 0;

		for (int j=i+1; j<N; j++)
			s += A[i*N+j] * x[j];

		x[i] = (B[i] - s) / A[i*N+i];
	}

	return 1;
}



/***************************/
/* FORWARD ELIMINATION CPU */
/***************************/
// --- This procedure performs standard sequential Gaussian elimination without pivoting. The difference with forward_elimination_CPU is that the
//     procedure outputs the "pivoting" matrix R also.
void forward_elimination_CPU_alternative(double* A, double* B, double* R) {
	
	for (int i=0; i<N-1; i++) {
		#pragma omp parallel for
		for (int j=i+1; j<N; j++) {
			R[j] = A[j*N+i]/A[i*N+i]; 
			B[j] -= R[j] * B[i];
			for (int k=i; k<N; k++) A[j*N+k] -= R[j]*A[i*N+k]; 
		}
	}
}

/***************************************/
/* FORWARD ELIMINATION CPU WITH TILING */
/***************************************/
// --- This procedure performs standard sequential Gaussian elimination without pivoting with the tiling technique. It is for demonstration only, as it
//     can serve for better understanding of the related parallel routine using tiling.
int forward_elimination_CPU_tiling(double* A, double* B) {
	
	int Num_Blocks = N / Matrix_Block_Size + (N % Matrix_Block_Size ? 1 : 0);

	double* Ratios = (double*)malloc(N*Matrix_Block_Size*sizeof(double));
	
	for (int iB=0; iB<Num_Blocks; iB++) {

		int i0 = iB * N / Num_Blocks; 
		int i1 = (iB+1) * N / Num_Blocks;
		
		// ---
		// <iB_iB>
		// ---
		for (int i=i0; i<i1; i++) {
			if (fabs(A[i*N+i]) < THRESHOLD_PIVOT) return 0;
			for (int j=i+1; j<i1; j++) {
				Ratios[j*Matrix_Block_Size+(i-i0)] = A[j*N+i]/A[i*N+i];
				B[j] -= Ratios[j*Matrix_Block_Size+(i-i0)]*B[i]; 
				for (int k=i; k<i1; k++)
					A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)]*A[i*N+k]; 
			}
		}

		// ---
		// <iB_kB>
		// ---
		for (int kB = iB+1; kB < Num_Blocks; kB++) {
			int k0 = kB * N / Num_Blocks; 
			int k1 = (kB+1) * N / Num_Blocks;
			for (int k = k0; k < k1; k++)
				for (int i = i0; i < i1; i++)
					for (int j = i+1; j < i1; j++) {
						A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)]*A[i*N+k];
					}
		}

		if (iB<Num_Blocks-1) {

			// ---
			// <jB_iB>
			// ---
			for (int jB = iB+1; jB < Num_Blocks; jB++) {
				int j0 = jB * N / Num_Blocks; 
				int j1 = (jB+1) * N / Num_Blocks;
				for (int j = j0; j < j1; j++)
					for (int i = i0; i < i1; i++) {
						Ratios[j*Matrix_Block_Size+(i-i0)] = A[j*N+i] / A[i*N+i];
						B[j] -= Ratios[j*Matrix_Block_Size+(i-i0)] * B[i];
						for (int k = i; k < i1; k++)
							A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)] * A[i*N+k];
					}
			}
		
			// ---
			// <jB_kB>
			// ---
			for (int jB = iB + 1; jB < Num_Blocks; jB++) {
				int j0 = jB * N / Num_Blocks; 
				int j1 = (jB + 1) * N / Num_Blocks;
				for (int kB = iB + 1; kB < Num_Blocks; kB++) {
					int k0 = kB * N / Num_Blocks; 
					int k1 = (kB + 1) * N / Num_Blocks;
					for (int j = j0; j < j1; j++)
						for (int k = k0; k < k1; k++)
							for (int i = i0; i < i1; i++)
								A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)] * A[i*N+k];
				}
	
			}

		}
	}

	return 1;

}

/******************************************/
/* ELIMINATION KERNEL iB - iB - NO SHARED */
/******************************************/
__global__ void kernel_elim_block_iB_iB(int* pokpiv, double* A, double* B, double* R, int i0, int i1) {

	for (int i = i0; i < i1; i++) {

		if (fabs(A[i*N+i]) < THRESHOLD_PIVOT) *pokpiv = 0;
		else {
			*pokpiv = 1;
			for (int j = i+1; j < i1; j++) {
				R[j*Matrix_Block_Size+(i-i0)] = A[j*N+i] / A[i*N+i];
				//printf("Matrix kernel1 %i %i %f\n",j,i-i0,R[j*Matrix_Block_Size+(i-i0)]);
				B[j] -= R[j*Matrix_Block_Size+(i-i0)] * B[i];
				for (int k = i; k < i1; k++) {
					A[j*N+k] -= R[j*Matrix_Block_Size+(i-i0)] * A[i*N+k]; }
				//	printf("kernel_elim_block_iB_iB %i %i %f\n",j,k,A[j*N+k]);
				//}
			}
		}
	}
}

/******************************************/
/* ELIMINATION KERNEL iB - kB - NO SHARED */
/******************************************/
__global__ void kernel_elim_block_iB_kB(double* A, double* B, double* R, int Num_Blocks, int iB, int i0, int i1) {

	int kB = (iB + 1) + blockIdx.x; 
	
	int k0 = kB * N / Num_Blocks;
	int k1 = (kB + 1) * N / Num_Blocks; 
	int k = k0 + threadIdx.x;

	if (k < k1) {
		for (int i = i0; i < i1 ; i++)
			for (int j = i + 1; j < i1 ; j++) {
				A[j*N+k] -= R[j*Matrix_Block_Size+(i-i0)] * A[i*N+k]; }
				//printf("kernel_elim_block_iB_kB %i %i %f\n",j,k,A[j*N+k]);
				////printf("Matrix kernel2 %i %i %f\n",j,i-i0,R[j*Matrix_Block_Size+(i-i0)]); }
	}
}

/******************************************/
/* ELIMINATION KERNEL jB - iB - NO SHARED */
/******************************************/
__global__ void kernel_elim_block_jB_iB(double* A, double* B, double* R, int Num_Blocks, int iB, int i0, int i1) {

	int jB = (iB + 1) + blockIdx.x; 
	
	int j0 = jB * N / Num_Blocks;
	int j1 = (jB + 1) * N / Num_Blocks; 
	int j = j0 + threadIdx.x;

	if (j < j1)
		for (int i = i0 ; i < i1 ; i++) {
			R[j*Matrix_Block_Size+(i-i0)] = A[j*N+i] / A[i*N+i];
			B[j] -= R[j*Matrix_Block_Size+(i-i0)] * B[i];
			for (int k = i ; k < i1 ; k++) {
				A[j*N+k] -= R[j*Matrix_Block_Size+(i-i0)] * A[i*N+k]; }
				//printf("kernel_elim_block_jB_iB %i %i %f\n",j,k,A[j*N+k]); } 
		}
}

/******************************************/
/* ELIMINATION KERNEL jB - kB - NO SHARED */
/******************************************/
__global__ void kernel_elim_block_jB_kB_no_shared(double* A, double* B, double* R, int Num_Blocks, int iB, int i0, int i1) {

	int jB = (iB + 1) + blockIdx.x; 
	int j0 = jB * N / Num_Blocks; 
	int j1 = (jB + 1) * N / Num_Blocks; 	
	int j = j0 + threadIdx.x;
	
	int kB = (iB + 1) + blockIdx.y; 
	int k0 = kB * N / Num_Blocks;
	int k1 = (kB + 1) * N / Num_Blocks; 
	int k = k0 + threadIdx.y;

	if (j < j1 && k < k1)
		for (int i = i0; i < i1; i++) {
			A[j*N+k] -= R[j*Matrix_Block_Size+(i-i0)] * A[i*N+k]; }
}

/***************************************/
/* ELIMINATION KERNEL jB - kB - SHARED */
/***************************************/
__global__ void kernel_elim_block_jB_kB_shared(double* A, double* B, double* R, int Num_Blocks, int iB, int i0, int i1) {

	int jB = (iB+1) + blockIdx.x;
	int j0 = jB * N / Num_Blocks;
	int j1 = (jB+1) * N / Num_Blocks;
	int dj = threadIdx.x;
	int j = j0 + dj;

	int kB = (iB+1) + blockIdx.y;
	int k0 = kB * N / Num_Blocks;
	int k1 = (kB+1) * N / Num_Blocks;
	int dk = threadIdx.y;
	int k = k0 + dk;

	int di;
	int i1_i0 = i1 - i0;

	double Ajk;

	__shared__ double __R__[Matrix_Block_Size][Matrix_Block_Size], __A__[Matrix_Block_Size][Matrix_Block_Size];

	if (j < j1 && k < k1)
		Ajk = A[j*N+k];

	if (j < j1 && dk < i1_i0) //// thread (j , k) loads R(j , i)
		__R__[dj][dk] = R[j*Matrix_Block_Size+dk];

	if (dj < i1_i0 && k < k1) //// thread (j , k) loads A(i , k)
		__A__[dj][dk] = A[(i0+dj)*N+k];

	__syncthreads();

	if (j < j1 && k < k1) {
		for (di = 0; di < i1_i0; di++) 
			Ajk -= __R__[dj][di] * __A__[di][dk];

		A[j*N+k] = Ajk;
	}
}

/***************************************/
/* FORWARD ELIMINATION GPU WITH TILING */
/***************************************/
// --- This procedure performs parallel Gaussian elimination without pivoting. It implements both a version without and with using shared memory.
//     Please, comment/uncomment the related lines
int forward_elimination_GPU_tiling(double* A, double* B) {

	int Num_Blocks = N / Matrix_Block_Size + (N % Matrix_Block_Size ? 1 : 0);

	// --- GPU memory allocations
	double *d_A; size_t sA = sizeof(double) * N * N;					gpuErrchk(cudaMalloc((void**)&d_A,sA));
	double *d_B; size_t sB = sizeof(double) * N;						gpuErrchk(cudaMalloc((void**)&d_B,sB));
	double *d_R; size_t sR = sizeof(double) * N * Matrix_Block_Size;	gpuErrchk(cudaMalloc((void**)&d_R,sR));

	int ok_pivoting, *d_ok_pivoting;									gpuErrchk(cudaMalloc((void**)&d_ok_pivoting,sizeof(int)));

	// --- CPU->GPU matrix copies
	gpuErrchk(cudaMemcpy(d_A,A,sA,cudaMemcpyHostToDevice));
	gpuErrchk(cudaMemcpy(d_B,B,sB,cudaMemcpyHostToDevice));

	for (int iB=0; iB<Num_Blocks; iB++) {

		int i0 = iB * N / Num_Blocks; 
		int i1 = (iB+1) * N / Num_Blocks;

		kernel_elim_block_iB_iB<<<1,1>>>(d_ok_pivoting,d_A,d_B,d_R,i0,i1);
		gpuErrchk(cudaThreadSynchronize());

		gpuErrchk(cudaMemcpy(&ok_pivoting, d_ok_pivoting, sizeof(int), cudaMemcpyDeviceToHost));
		if (!ok_pivoting) return 0;

		kernel_elim_block_iB_kB<<<Num_Blocks-(iB+1),Matrix_Block_Size>>>(d_A,d_B,d_R,Num_Blocks,iB,i0,i1);
		gpuErrchk(cudaThreadSynchronize());

		if (iB < Num_Blocks-1) {
	
			kernel_elim_block_jB_iB<<<Num_Blocks-(iB+1),Matrix_Block_Size>>>(d_A,d_B,d_R,Num_Blocks,iB,i0,i1);
			gpuErrchk(cudaThreadSynchronize());

			dim3 blocks(Num_Blocks-(iB+1),Num_Blocks-(iB+1));
			dim3 threads(Matrix_Block_Size,Matrix_Block_Size);
	
			// kernel_elim_block_jB_kB_no_shared <<<blocks,threads>>>(d_A,d_B,d_R,Num_Blocks,iB,i0,i1);
			kernel_elim_block_jB_kB_shared <<<blocks,threads>>>(d_A,d_B,d_R,Num_Blocks,iB,i0,i1);
			gpuErrchk(cudaThreadSynchronize());

		}
	}

	gpuErrchk(cudaMemcpy(A,d_A,sA,cudaMemcpyDeviceToHost));
	gpuErrchk(cudaMemcpy(B,d_B,sB,cudaMemcpyDeviceToHost));

	gpuErrchk(cudaFree(d_A)); gpuErrchk(cudaFree(d_B));
	gpuErrchk(cudaFree(d_R)); gpuErrchk(cudaFree(d_ok_pivoting));

	return 1;

}

/*******************************************************/
/* FORWARD ELIMINATION GPU WITH cuBLAS - FIRST VERSION */
/*******************************************************/
// --- This procedure performs parallel Gaussian elimination without pivoting. It is a "brute-force" cuBLAS implementation.
int forward_elimination_GPU_cuBLAS(double* A, double* B)
{
	double Aii, Bi, alpha, beta;

	cublasHandle_t handle;

	double *d_A; size_t sA = sizeof(double) * N * N;			gpuErrchk(cudaMalloc((void**)&d_A,sA));
	double *d_B; size_t sB = sizeof(double) * N;				gpuErrchk(cudaMalloc((void**)&d_B,sB));
	double *d_R; size_t sR = sizeof(double) * N;				gpuErrchk(cudaMalloc((void**)&d_R,sR));

	cublasSafeCall(cublasCreate(&handle));

	cublasSafeCall(cublasSetMatrix(N, N, sizeof(double), A, N, d_A, N));
	cublasSafeCall(cublasSetVector(N, sizeof(double), B, 1, d_B, 1));

	for (int i=0; i<N-1; i++) {

		gpuErrchk(cudaMemcpy(&Aii, d_A+i+i*N, sizeof(double), cudaMemcpyDeviceToHost));
		if (fabs(Aii) < THRESHOLD_PIVOT) return 0;
		
		// for (j = i + 1 ; j < n ; j++) R[j] = Mij(A , n , j , i) / Mij(A , n , i , i);
		cublasSafeCall(cublasDcopy(handle,N-(i+1), d_A+(i+1)+i*N, 1, d_R+(i+1), 1));
		beta = 1.0/Aii;
		cublasSafeCall(cublasDscal(handle,N-(i+1), &beta, d_R+(i+1), 1));

		// for (j = i + 1 ; j < n ; j++) B[j] -= R[j] * B[i];
		gpuErrchk(cudaMemcpy(&Bi, d_B+i, sizeof(double), cudaMemcpyDeviceToHost));
		beta = -Bi;
		cublasSafeCall(cublasDaxpy(handle, N-(i+1), &beta, d_R+(i+1), 1, d_B+(i+1), 1));

		// for (j = i + 1 ; j < n ; j++) for (k = i ; k < n ; k++) Mij(A , n , j , k) -= R[j] * Mij(A , n , i , k);
		alpha = -1.; beta = 1.;
		cublasSafeCall(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, N-(i+1), N-i, 1, &alpha, d_R+(i+1), N, d_A+i+i*N, N, &beta, d_A+(i+1)+i*N, N));

	}
	
	gpuErrchk(cudaMemcpy(A,d_A,sizeof(double)*N*N,cudaMemcpyDeviceToHost));
	gpuErrchk(cudaMemcpy(B,d_B,sizeof(double)*N,cudaMemcpyDeviceToHost));

	return 1;
}

/************************************************/
/* FORWARD ELIMINATION GPU WITH TILING - cuBLAS */
/************************************************/
// --- This procedure performs parallel Gaussian elimination without pivoting. It is a tiling based cuBLAS implementation.
int forward_elimination_GPU_tiling_cuBLAS(double* A, double* B) {
	
	double Aii, Bi, alpha, beta;

	int Num_Blocks = N / Matrix_Block_Size + (N % Matrix_Block_Size ? 1 : 0);

	double* Ratios = (double*)malloc(N*Matrix_Block_Size*sizeof(double));

	double* A2		= (double*)malloc(N*N*sizeof(double));
	double* B2		= (double*)malloc(N*sizeof(double));
	double* Ratios2 = (double*)malloc(N*Matrix_Block_Size*sizeof(double));
	
	cublasHandle_t handle;

	double *d_A; size_t sA = sizeof(double) * N * N;							gpuErrchk(cudaMalloc((void**)&d_A,sA));
	double *d_B; size_t sB = sizeof(double) * N;								gpuErrchk(cudaMalloc((void**)&d_B,sB));
	double *d_R; size_t sR = sizeof(double) * Matrix_Block_Size * N;			gpuErrchk(cudaMalloc((void**)&d_R,sR));
			
	cublasSafeCall(cublasCreate(&handle));

	cublasSafeCall(cublasSetMatrix(N, N, sizeof(double), A, N, d_A, N));
	cublasSafeCall(cublasSetVector(N, sizeof(double), B, 1, d_B, 1));

	// ---
	// <iB_iB>
	// ---
	for (int iB=0; iB<Num_Blocks; iB++) {

		int i0 = iB * N / Num_Blocks; 
		int i1 = (iB+1) * N / Num_Blocks;
		
		for (int i=i0; i<i1; i++) {

			gpuErrchk(cudaMemcpy(&Aii, d_A+i+i*N, sizeof(double), cudaMemcpyDeviceToHost));
			if (fabs(Aii) < THRESHOLD_PIVOT) return 0;

			//// --- CPU
			//for (int j=i+1; j<i1; j++) {
			//	Ratios[j+(i-i0)*N] = A[j+i*N]/A[i+i*N];
			//	B[j] -= Ratios[j+(i-i0)*N]*B[i]; 
			//	//Ratios[j*Matrix_Block_Size+(i-i0)] = A[j*N+i]/A[i*N+i];
			//	//B[j] -= Ratios[j*Matrix_Block_Size+(i-i0)]*B[i]; 
			//	for (int k=i; k<i1; k++)
			//		A[j+k*N] -= Ratios[j+(i-i0)*N]*A[i+k*N]; 
			//}

			// for (int j=i+1; j<i1; j++) Ratios[j+(i-i0)*N] = A[j+i*N]/A[i+i*N];
			cublasSafeCall(cublasDcopy(handle, i1-(i+1), d_A+(i+1)+i*N, 1, d_R+(i+1)+(i-i0)*N, 1));
			beta = 1./Aii;
			cublasSafeCall(cublasDscal(handle, i1-(i+1), &beta, d_R+(i+1)+(i-i0)*N, 1));

			// for (int j=i+1; j<i1; j++) B[j] -= Ratios[j+(i-i0)*N]*B[i]; 
			gpuErrchk(cudaMemcpy(&Bi, d_B+i, sizeof(double), cudaMemcpyDeviceToHost));
			beta = -Bi;
			cublasSafeCall(cublasDaxpy(handle, i1-(i+1), &beta, d_R+(i+1)+(i-i0)*N, 1, d_B+(i+1), 1));

			// for (int j=i+1; j<i1; j++) for (int k=i; k<i1; k++) A[j+k*N] -= Ratios[j+(i-i0)*N]*A[i+k*N]; 
			alpha = -1.; beta = 1.;
			cublasSafeCall(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, i1-(i+1), i1-i, 1, &alpha, d_R+(i+1)+(i-i0)*N, N, d_A+i+i*N, N, &beta, d_A+(i+1)+i*N, N));

		}

		//// --- CPU
		//for (int kB = iB+1; kB < Num_Blocks; kB++) {
		//	int k0 = kB * N / Num_Blocks; 
		//	int k1 = (kB+1) * N / Num_Blocks;
		//	for (int i = i0; i < i1; i++)
		//		for (int k = k0; k < k1; k++)
		//			for (int j = i+1; j < i1; j++) {
		//				//A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)]*A[i*N+k];
		//				A[j+k*N] -= Ratios[j+(i-i0)*N]*A[i+k*N];
		//			}
		//}

		// ---
		// <iB_kB>
		// ---
		alpha = -1.; beta = 1.;
		for (int kB = iB+1; kB < Num_Blocks; kB++) {
			int k0 = kB * N / Num_Blocks; 
			int k1 = (kB+1) * N / Num_Blocks;
			for (int i = i0; i < i1; i++)
				// for (int k = k0; k < k1; k++) for (int j = i+1; j < i1; j++) A[j*N+k] -= Ratios[j+(i-i0)*N]*A[i+k*N];
				cublasSafeCall(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, i1-(i+1), k1-k0, 1, &alpha, d_R+(i+1)+(i-i0)*N, N, d_A+i+k0*N, N, &beta, d_A+(i+1)+k0*N, N));
		}

		if (iB<Num_Blocks-1) {

			//// --- CPU
			//for (int jB = iB+1; jB < Num_Blocks; jB++) {
			//	int j0 = jB * N / Num_Blocks; 
			//	int j1 = (jB+1) * N / Num_Blocks;
			//	for (int i = i0; i < i1; i++) 
			//		for (int j = j0; j < j1; j++) {
			//			//Ratios[j*Matrix_Block_Size+(i-i0)] = A[j*N+i] / A[i*N+i];
			//			//B[j] -= Ratios[j*Matrix_Block_Size+(i-i0)] * B[i];
			//			Ratios[j+(i-i0)*N] = A[j+i*N] / A[i+i*N];
			//			B[j] -= Ratios[j+(i-i0)*N] * B[i];
			//			for (int k = i; k < i1; k++)
			//				//A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)] * A[i*N+k];
			//				A[j+k*N] -= Ratios[j+(i-i0)*N] * A[i+k*N];
			//		}
			//}
		
			double* vectAii = (double*)malloc((i1-i0)*sizeof(double));
			double* vectBi	= (double*)malloc((i1-i0)*sizeof(double));
			cublasSafeCall(cublasGetVector(i1-i0, sizeof(double), d_A+i0+i0*N, N+1, vectAii, 1));
			cublasSafeCall(cublasGetVector(i1-i0, sizeof(double), d_B+i0, 1, vectBi, 1));

			// ---
			// <jB_iB>
			// ---
			for (int jB = iB+1; jB < Num_Blocks; jB++) {
				int j0 = jB * N / Num_Blocks; 
				int j1 = (jB+1) * N / Num_Blocks;
						
				for (int i = i0; i < i1; i++) {
					
					// for (int j = j0; j < j1; j++) Ratios[j+(i-i0)*N] = A[j+i*N] / A[i+i*N];
					cublasSafeCall(cublasDcopy(handle, j1-j0, d_A+j0+i*N, 1, d_R+j0+(i-i0)*N, 1));
					beta = 1.0/vectAii[i-i0];
					cublasSafeCall(cublasDscal(handle, j1-j0, &beta, d_R+j0+(i-i0)*N, 1));
					
					// for (int j = j0; j < j1; j++) B[j] -= Ratios[j+(i-i0)*N] * B[i];
					beta = - vectBi[i-i0];
					cublasSafeCall(cublasDaxpy(handle, j1-j0, &beta, d_R+j0+(i-i0)*N, 1, d_B+j0, 1));
					
					// for (int j = j0; j < j1; j++) for (int k = i; k < i1; k++) A[j+k*N] -= Ratios[j+(i-i0)*N] * A[i+k*N]; 
					alpha = -1.; beta = 1.;
					cublasSafeCall(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, j1-j0, i1-i, 1, &alpha, d_R+j0+(i-i0)*N, N, d_A+i+i*N, N, &beta, d_A+j0+i*N, N)); 

				}
			}

			//// --- CPU
			//for (int jB = iB + 1; jB < Num_Blocks; jB++) {
			//	int j0 = jB * N / Num_Blocks; 
			//	int j1 = (jB + 1) * N / Num_Blocks;
			//	for (int kB = iB + 1; kB < Num_Blocks; kB++) {
			//		int k0 = kB * N / Num_Blocks; 
			//		int k1 = (kB + 1) * N / Num_Blocks;
			//		for (int j = j0; j < j1; j++)
			//			for (int k = k0; k < k1; k++)
			//				for (int i = i0; i < i1; i++)
			//					//A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)] * A[i*N+k];
			//					A[j+k*N] -= Ratios[j+(i-i0)*N] * A[i+k*N];
			//	}
			//}
	
			// ---
			// <jB_kB>
			// ---
			for (int jB = iB + 1; jB < Num_Blocks; jB++) {
				int j0 = jB * N / Num_Blocks; 
				int j1 = (jB + 1) * N / Num_Blocks;
				for (int kB = iB + 1; kB < Num_Blocks; kB++) {
					int k0 = kB * N / Num_Blocks; 
					int k1 = (kB + 1) * N / Num_Blocks;

					// for (int j = j0; j < j1; j++) for (int k = k0; k < k1; k++) for (int i = i0; i < i1; i++) A[j+k*N] -= Ratios[j+(i-i0)*N] * A[i+k*N];
					alpha = -1.; beta = 1.;
					cublasSafeCall(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, j1-j0, k1-k0, i1-i0, &alpha, d_R+j0, N, d_A+i0+k0*N, N, &beta, d_A+j0+k0*N, N));
				}
			}

		}
	}

	gpuErrchk(cudaMemcpy(A,d_A,sizeof(double)*N*N,cudaMemcpyDeviceToHost));
	gpuErrchk(cudaMemcpy(B,d_B,sizeof(double)*N,cudaMemcpyDeviceToHost));

	return 1;

}

/*****************************************************/
/* FORWARD ELIMINATION CPU WITH TILING AND STRIPPING */
/*****************************************************/
// --- This procedure performs standard sequential Gaussian elimination without pivoting with the tiling technique. It is for demonstration only, as it
//     can serve for better understanding of the related parallel routine using tiling and stripping.
int forward_elimination_CPU_tiling_stripping(double* A, double* B) {
	
	int Num_Blocks	= N / Matrix_Block_Size + (N % Matrix_Block_Size ? 1 : 0);

	double* Ratios = (double*)malloc(N*Matrix_Block_Size*sizeof(double));
	
	for (int iB=0; iB<Num_Blocks; iB++) {

		int i0 = iB * N / Num_Blocks; 
		int i1 = (iB+1) * N / Num_Blocks;
		
		// ---
		// <iB_iB>
		// ---
		for (int i=i0; i<i1; i++) {
			if (fabs(A[i*N+i]) < THRESHOLD_PIVOT) return 0;
			for (int j=i+1; j<i1; j++) {
				Ratios[j*Matrix_Block_Size+(i-i0)] = A[j*N+i]/A[i*N+i];
				B[j] -= Ratios[j*Matrix_Block_Size+(i-i0)]*B[i]; 
				for (int k=i; k<i1; k++)
					A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)]*A[i*N+k]; 
			}
		}

		// ---
		// <iB_kB>
		// ---

		for (int kB = iB+1; kB < Num_Blocks; kB++) {
			int k0 = kB * N / Num_Blocks; 
			int k1 = (kB+1) * N / Num_Blocks;

			int Num_Stripes = (i1 - i0) / Matrix_Stripe_Size + ((i1 - i0) % Matrix_Stripe_Size ? 1 : 0);
		
			for (int iQ=0; iQ<Num_Stripes; iQ++) { 
	
				int i_0 = i0 + iQ * (i1 - i0) / Num_Stripes; 
				int i_1 = i0 + (iQ + 1) * (i1 - i0) / Num_Stripes;

				for (int i=i_0; i<i_1; i++)
					for (int j=i+1; j<i_1; j++)
						for (int k=k0; k<k1; k++)
							A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)] * A[i*N+k];

				for (int i=i_0; i<i_1; i++)
					for (int j=i_1; j<i1; j++)
						for (int k=k0; k<k1; k++)
							A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)] * A[i*N+k];
			}

		}

		if (iB<Num_Blocks-1) {

			// ---
			// <jB_iB>
			// ---
			for (int jB = iB+1; jB < Num_Blocks; jB++) {
				int j0 = jB * N / Num_Blocks; 
				int j1 = (jB+1) * N / Num_Blocks;

				int Num_Stripes = (i1 - i0) / Matrix_Stripe_Size + ((i1 - i0) % Matrix_Stripe_Size ? 1 : 0);

				for (int iQ=0; iQ<Matrix_Stripe_Size; iQ++) { 
					int i_0 = i0 + iQ * (i1 - i0) / Matrix_Stripe_Size; 
					int i_1 = i0 + (iQ + 1) * (i1 - i0) / Matrix_Stripe_Size;

					for (int i=i_0; i<i_1; i++) {
						for (int j=j0; j<j1; j++)
							Ratios[j*Matrix_Block_Size+(i-i0)] = A[j*N+i] / A[i*N+i]; 

						for (int j=j0; j<j1; j++)
							B[j] -= Ratios[j*Matrix_Block_Size+(i-i0)] * B[i];

						for (int j=j0; j<j1; j++)
							for (int k=i; k<i_1; k++)
								A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)] * A[i*N+k];
					}

					for (int j=j0; j<j1; j++)
						for (int k=i_1; k<i1; k++)
							for (int i=i_0; i<i_1; i++)
								A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)] * A[i*N+k];
				}			
			}

			//for (int jB = iB+1; jB < Num_Blocks; jB++) {
			//	int j0 = jB * N / Num_Blocks; 
			//	int j1 = (jB+1) * N / Num_Blocks;
			//	for (int j = j0; j < j1; j++)
			//		for (int i = i0; i < i1; i++) {
			//			Ratios[j*Matrix_Block_Size+(i-i0)] = A[j*N+i] / A[i*N+i];
			//			B[j] -= Ratios[j*Matrix_Block_Size+(i-i0)] * B[i];
			//			for (int k = i; k < i1; k++)
			//				A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)] * A[i*N+k];
			//		}
			//}
		
			// ---
			// <jB_kB>
			// ---
			for (int jB = iB + 1; jB < Num_Blocks; jB++) {
				int j0 = jB * N / Num_Blocks; 
				int j1 = (jB + 1) * N / Num_Blocks;
				for (int kB = iB + 1; kB < Num_Blocks; kB++) {
					int k0 = kB * N / Num_Blocks; 
					int k1 = (kB + 1) * N / Num_Blocks;
					for (int j = j0; j < j1; j++)
						for (int k = k0; k < k1; k++)
							for (int i = i0; i < i1; i++)
								A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)] * A[i*N+k];
				}
	
			}

		}
	}

	return 1;

}

/**************************************************************/
/* FORWARD ELIMINATION GPU WITH TILING AND STRIPPING - cuBLAS */
/**************************************************************/
// --- This procedure performs parallel Gaussian elimination without pivoting. It is a tiling-stripping based cuBLAS implementation.
int forward_elimination_GPU_blocks_stripping_cuBLAS(double* A, double* B) {
	
	double Aii, Bi, alpha, beta;

	int Num_Blocks = N / Matrix_Block_Size + (N % Matrix_Block_Size ? 1 : 0);

	cublasHandle_t handle;

	double *d_A; size_t sA = sizeof(double) * N * N;							gpuErrchk(cudaMalloc((void**)&d_A,sA));
	double *d_B; size_t sB = sizeof(double) * N;								gpuErrchk(cudaMalloc((void**)&d_B,sB));
	double *d_R; size_t sR = sizeof(double) * Matrix_Block_Size * N;			gpuErrchk(cudaMalloc((void**)&d_R,sR));
			
	cublasSafeCall(cublasCreate(&handle));

	cublasSafeCall(cublasSetMatrix(N, N, sizeof(double), A, N, d_A, N));
	cublasSafeCall(cublasSetVector(N, sizeof(double), B, 1, d_B, 1));

	// ---
	// <iB_iB>
	// ---
	for (int iB=0; iB<Num_Blocks; iB++) {

		int i0 = iB * N / Num_Blocks; 
		int i1 = (iB+1) * N / Num_Blocks;
		
		for (int i=i0; i<i1; i++) {

			gpuErrchk(cudaMemcpy(&Aii, d_A+i+i*N, sizeof(double), cudaMemcpyDeviceToHost));
			if (fabs(Aii) < THRESHOLD_PIVOT) return 0;

			//// --- CPU
			//for (int j=i+1; j<i1; j++) {
			//	Ratios[j+(i-i0)*N] = A[j+i*N]/A[i+i*N];
			//	B[j] -= Ratios[j+(i-i0)*N]*B[i]; 
			//	//Ratios[j*Matrix_Block_Size+(i-i0)] = A[j*N+i]/A[i*N+i];
			//	//B[j] -= Ratios[j*Matrix_Block_Size+(i-i0)]*B[i]; 
			//	for (int k=i; k<i1; k++)
			//		A[j+k*N] -= Ratios[j+(i-i0)*N]*A[i+k*N]; 
			//}

			// for (int j=i+1; j<i1; j++) Ratios[j+(i-i0)*N] = A[j+i*N]/A[i+i*N];
			cublasSafeCall(cublasDcopy(handle, i1-(i+1), d_A+(i+1)+i*N, 1, d_R+(i+1)+(i-i0)*N, 1));
			beta = 1./Aii;
			cublasSafeCall(cublasDscal(handle, i1-(i+1), &beta, d_R+(i+1)+(i-i0)*N, 1));

			// for (int j=i+1; j<i1; j++) B[j] -= Ratios[j+(i-i0)*N]*B[i]; 
			gpuErrchk(cudaMemcpy(&Bi, d_B+i, sizeof(double), cudaMemcpyDeviceToHost));
			beta = -Bi;
			cublasSafeCall(cublasDaxpy(handle, i1-(i+1), &beta, d_R+(i+1)+(i-i0)*N, 1, d_B+(i+1), 1));

			// for (int j=i+1; j<i1; j++) for (int k=i; k<i1; k++) A[j+k*N] -= Ratios[j+(i-i0)*N]*A[i+k*N]; 
			alpha = -1.; beta = 1.;
			cublasSafeCall(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, i1-(i+1), i1-i, 1, &alpha, d_R+(i+1)+(i-i0)*N, N, d_A+i+i*N, N, &beta, d_A+(i+1)+i*N, N));

		}

		//// --- CPU
		//for (int kB = iB+1; kB < Num_Blocks; kB++) {
		//	int k0 = kB * N / Num_Blocks; 
		//	int k1 = (kB+1) * N / Num_Blocks;
		//	for (int i = i0; i < i1; i++)
		//		for (int k = k0; k < k1; k++)
		//			for (int j = i+1; j < i1; j++) {
		//				//A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)]*A[i*N+k];
		//				A[j+k*N] -= Ratios[j+(i-i0)*N]*A[i+k*N];
		//			}
		//}

		// ---
		// <iB_kB>
		// ---
		alpha = -1.; beta = 1.;
		for (int kB = iB+1; kB < Num_Blocks; kB++) {
			int k0 = kB * N / Num_Blocks; 
			int k1 = (kB+1) * N / Num_Blocks;

			int Num_Stripes = (i1 - i0) / Matrix_Stripe_Size + ((i1 - i0) % Matrix_Stripe_Size ? 1 : 0);

			for (int iQ=0; iQ<Num_Stripes; iQ++) { 
				int i_0 = i0 + iQ * (i1 - i0) / Num_Stripes; 
				int i_1 = i0 + (iQ + 1) * (i1 - i0) / Num_Stripes;

				for (int i=i_0; i<i_1; i++) 
					cublasSafeCall(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, i_1-(i+1), k1-k0, 1, &alpha, d_R+(i+1)+(i-i0)*N, N, d_A+i+k0*N, N, &beta, d_A+(i+1)+k0*N, N));

				cublasSafeCall(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, i1-i_1, k1-k0, i_1-i_0, &alpha, d_R+i_1+(i_0-i0)*N, N, d_A+i_0+k0*N, N, &beta, d_A+i_1+k0*N, N)); 
			}
		}

		if (iB<Num_Blocks-1) {

			//// --- CPU
			//for (int jB = iB+1; jB < Num_Blocks; jB++) {
			//	int j0 = jB * N / Num_Blocks; 
			//	int j1 = (jB+1) * N / Num_Blocks;
			//	for (int i = i0; i < i1; i++) 
			//		for (int j = j0; j < j1; j++) {
			//			//Ratios[j*Matrix_Block_Size+(i-i0)] = A[j*N+i] / A[i*N+i];
			//			//B[j] -= Ratios[j*Matrix_Block_Size+(i-i0)] * B[i];
			//			Ratios[j+(i-i0)*N] = A[j+i*N] / A[i+i*N];
			//			B[j] -= Ratios[j+(i-i0)*N] * B[i];
			//			for (int k = i; k < i1; k++)
			//				//A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)] * A[i*N+k];
			//				A[j+k*N] -= Ratios[j+(i-i0)*N] * A[i+k*N];
			//		}
			//}
		
			double* vectAii = (double*)malloc((i1-i0)*sizeof(double));
			double* vectBi	= (double*)malloc((i1-i0)*sizeof(double));
			cublasSafeCall(cublasGetVector(i1-i0, sizeof(double), d_A+i0+i0*N, N+1, vectAii, 1));
			cublasSafeCall(cublasGetVector(i1-i0, sizeof(double), d_B+i0, 1, vectBi, 1));

			// ---
			// <jB_iB>
			// ---
			for (int jB = iB+1; jB < Num_Blocks; jB++) {
				int j0 = jB * N / Num_Blocks; 
				int j1 = (jB+1) * N / Num_Blocks;
						
				int Num_Stripes = (i1 - i0) / Matrix_Stripe_Size + ((i1 - i0) % Matrix_Stripe_Size ? 1 : 0);
				for (int iQ=0; iQ<Num_Stripes; iQ++) { 
					int i_0 = i0 + iQ * (i1 - i0) / Num_Stripes; 
					int i_1 = i0 + (iQ + 1) * (i1 - i0) / Num_Stripes;

					for (int i=i_0; i<i_1; i++) {
						cublasSafeCall(cublasDcopy(handle, j1-j0, d_A+j0+i*N, 1, d_R+j0+(i-i0)*N, 1));
						beta = 1.0 / vectAii[i-i0];
						cublasSafeCall(cublasDscal(handle, j1-j0, &beta, d_R+j0+(i-i0)*N , 1));
						beta = - vectBi[i-i0];
						cublasSafeCall(cublasDaxpy(handle, j1-j0, &beta, d_R+j0+(i-i0)*N, 1, d_B+j0, 1));

						alpha = -1.; beta = 1.;
						cublasSafeCall(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, j1-j0, i_1-i, 1, &alpha, d_R+j0+(i-i0)*N, N, d_A+i+i*N, N, &beta, d_A+j0+i*N, N));
					}

					alpha = -1.; beta = 1.;
					cublasSafeCall(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, j1-j0, i1-i_1, i_1-i_0, &alpha, d_R+j0+(i_0-i0)*N, N, d_A+i_0+i_1*N, N, &beta, d_A+j0+i_1*N, N));
				}
			}

			//for (int jB = iB+1; jB < Num_Blocks; jB++) {
			//	int j0 = jB * N / Num_Blocks; 
			//	int j1 = (jB+1) * N / Num_Blocks;
			//			
			//	for (int i = i0; i < i1; i++) {
			//		
			//		// for (int j = j0; j < j1; j++) Ratios[j+(i-i0)*N] = A[j+i*N] / A[i+i*N];
			//		cublasSafeCall(cublasDcopy(handle, j1-j0, d_A+j0+i*N, 1, d_R+j0+(i-i0)*N, 1));
			//		beta = 1.0/vectAii[i-i0];
			//		cublasSafeCall(cublasDscal(handle, j1-j0, &beta, d_R+j0+(i-i0)*N, 1));
			//		
			//		// for (int j = j0; j < j1; j++) B[j] -= Ratios[j+(i-i0)*N] * B[i];
			//		beta = - vectBi[i-i0];
			//		cublasSafeCall(cublasDaxpy(handle, j1-j0, &beta, d_R+j0+(i-i0)*N, 1, d_B+j0, 1));
			//		
			//		// for (int j = j0; j < j1; j++) for (int k = i; k < i1; k++) A[j+k*N] -= Ratios[j+(i-i0)*N] * A[i+k*N]; 
			//		alpha = -1.; beta = 1.;
			//		cublasSafeCall(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, j1-j0, i1-i, 1, &alpha, d_R+j0+(i-i0)*N, N, d_A+i+i*N, N, &beta, d_A+j0+i*N, N)); 

			//	}
			//}

			//// --- CPU
			//for (int jB = iB + 1; jB < Num_Blocks; jB++) {
			//	int j0 = jB * N / Num_Blocks; 
			//	int j1 = (jB + 1) * N / Num_Blocks;
			//	for (int kB = iB + 1; kB < Num_Blocks; kB++) {
			//		int k0 = kB * N / Num_Blocks; 
			//		int k1 = (kB + 1) * N / Num_Blocks;
			//		for (int j = j0; j < j1; j++)
			//			for (int k = k0; k < k1; k++)
			//				for (int i = i0; i < i1; i++)
			//					//A[j*N+k] -= Ratios[j*Matrix_Block_Size+(i-i0)] * A[i*N+k];
			//					A[j+k*N] -= Ratios[j+(i-i0)*N] * A[i+k*N];
			//	}
			//}
	
			// ---
			// <jB_kB>
			// ---
			for (int jB = iB + 1; jB < Num_Blocks; jB++) {
				int j0 = jB * N / Num_Blocks; 
				int j1 = (jB + 1) * N / Num_Blocks;
				for (int kB = iB + 1; kB < Num_Blocks; kB++) {
					int k0 = kB * N / Num_Blocks; 
					int k1 = (kB + 1) * N / Num_Blocks;

					// for (int j = j0; j < j1; j++) for (int k = k0; k < k1; k++) for (int i = i0; i < i1; i++) A[j+k*N] -= Ratios[j+(i-i0)*N] * A[i+k*N];
					alpha = -1.; beta = 1.;
					cublasSafeCall(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, j1-j0, k1-k0, i1-i0, &alpha, d_R+j0, N, d_A+i0+k0*N, N, &beta, d_A+j0+k0*N, N));
				}
			}

		}
	}

	gpuErrchk(cudaMemcpy(A,d_A,sizeof(double)*N*N,cudaMemcpyDeviceToHost));
	gpuErrchk(cudaMemcpy(B,d_B,sizeof(double)*N,cudaMemcpyDeviceToHost));

	return 1;

}

/*************************************************/
/* SOLUTION OF AN UPPER TRINAGULAR SYSTEM ON CPU */
/*************************************************/
// --- This procedure solves an upper triangular linear system without pivoting on the GPU using cuBLAS.
int solution_of_a_triangular_system_GPU(double* A, double* B, double* x) {

	double Aii, alpha;

	int Num_Blocks = N / Matrix_Block_Size + (N % Matrix_Block_Size ? 1 : 0);

	cublasHandle_t handle;

	double *d_A; size_t sA = sizeof(double) * N * N;							gpuErrchk(cudaMalloc((void**)&d_A,sA));
	double *d_B; size_t sB = sizeof(double) * N;								gpuErrchk(cudaMalloc((void**)&d_B,sB));
	
	cublasSafeCall(cublasSetMatrix(N, N, sizeof(double), A, N, d_A, N));
	cublasSafeCall(cublasSetVector(N, sizeof(double), B, 1, d_B, 1));

	gpuErrchk(cudaMemcpy(&Aii, d_A+(N-1)+(N-1)*N, sizeof(double), cudaMemcpyDeviceToHost));

	if (fabs(Aii) < THRESHOLD_PIVOT) return 0;

	cublasSafeCall(cublasCreate(&handle));

	alpha = 1.;
	cublasSafeCall(cublasDtrsm(handle, CUBLAS_SIDE_LEFT, CUBLAS_FILL_MODE_UPPER, CUBLAS_OP_N, CUBLAS_DIAG_NON_UNIT, N, 1, &alpha, d_A, N, d_B, N));

	cublasSafeCall(cublasGetVector(N, sizeof(double), d_B, 1, x, 1));	
	
	return 1;
}

/********/
/* MAIN */
/********/
int main() {

	// --- All the CPU routines use a row-major ordering. The CPU code introduced and commented within the GPU procedures uses a column-major ordering.
	//     Column-major ordering is indeed used by cuBLAS and so the CPU code needs to be converted to such an ordering to match the GPU results.
	
	TimingCPU timerCPU;
	TimingGPU timerGPU;

	double* A = (double*)malloc(N*N*sizeof(double));
	double* B = (double*)malloc(N*sizeof(double));
	double* x = (double*)malloc(N*sizeof(double));

	// --- Matrix initialization
	init_matrices(A,B);

	// --- Saving the original matrices to disk
	save_matrix(A,N*N,"A.txt");
	save_matrix(B,N,"B.txt");
	
	// --- Running the CPU procedures
	//timerCPU.StartCounter();
	////forward_elimination_CPU_tiling(A,B);
	//forward_elimination_CPU(A,B);
	//solution_of_a_triangular_system_CPU(A,B,x);
	////forward_elimination_CPU_tiling_stripping(A,B);
	//printf("CPU time [ms]: %f\n",timerCPU.GetCounter());

	//double* R = (double*)malloc(N*sizeof(double));
	//timerCPU.StartCounter();
	//forward_elimination_CPU_alternative(A,B,R);
	//printf("CPU time [ms]: %f\n",timerCPU.GetCounter());

	// --- Running the GPU procedures
	timerGPU.StartCounter();
	//forward_elimination_GPU_tiling(A,B);
	//forward_elimination_GPU_cuBLAS(A,B);
	forward_elimination_GPU_blocks_stripping_cuBLAS(A,B);
	solution_of_a_triangular_system_GPU(A,B,x);
	printf("%f\n",timerGPU.GetCounter());
	
	// --- Saving the data
	save_matrix(A,N*N,"A_prime.txt");
	save_matrix(B,N,"B_prime.txt");
	save_matrix(x,N,"x_prime.txt");
		
	printf("Going to sleep\n");
	getch();
	
	return 0;

}

