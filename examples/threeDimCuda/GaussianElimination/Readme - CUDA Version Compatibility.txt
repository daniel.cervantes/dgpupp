If you receive an error on opening this solutions, maybe you have to properly set your CUDA version in the *.vcxproj file.

Open the *.vcxproj  file (for ex: ..\thisProjectName.vcxproj) with Visual Studio or a text editor and change the following tags:

<Import Project="$(VCTargetsPath)\BuildCustomizations\CUDA xx.yy.targets" />
<Import Project="$(VCTargetsPath)\BuildCustomizations\CUDA xx.yy.props" />

with your CUDA version:

<Import Project="$(VCTargetsPath)\BuildCustomizations\CUDA 4.2.targets" />
<Import Project="$(VCTargetsPath)\BuildCustomizations\CUDA 4.2.props" />

Now, you may reopen your project.