#include <stdio.h>
#include <stdlib.h>

// double* gauss2(double**  a, double*  b, int R, int C){

//   double m[R][C+1];
//   double *x = new double[C];
//   double c;
  
//   int i,j,k;
//   for(i=0; i < R; i++){
//     for(j=0; j < C; j++)
//       m[i][j] = a[i][j];
//     m[i][j] = b[i];
//   }



  
//   for(j=0; j < R; ++j){
//     for(i=j; i < C; ++i){

//       if(i!=j){
//         c = m[i][j]/m[j][j];
//         for(k=0; k < C+1; ++k)
//           m[i][k] = m[i][k] - c*m[j][k];
//       }
//     }
//   }
  

//  //  for(i=0;  i< R; ++i){
//  //    for(j=0; j < C+1; ++j){
//  //      printf("%.6f ", m[i][j]);
//  //    }
//  //    printf("\n");
//  // }

  
//   for(j=1; j < C+1; ++j){
//     c=0.0;
//     for(i=R-1; i > R-j; i--)      
//       c = c + m[R-j][i]*x[i];
//     x[C-j] = (m[C-j][C] - c)/(m[C-j][C-j]);
//   }
    
  

 
//   return x;
// }


void gauss(double* m, double*  x, int R, int C){

  int i,j,k;
  double c;

  printf("M \n");
  for(i=0;  i< R; ++i){
    for(j=0; j < C+1; ++j){
      printf("%.6f ", m[i*(C+1)+j]);
    }
    printf("\n");
  }
  printf("\n");

  
  for(j=0; j < R; ++j){
    for(i=j; i < C; ++i){
      if(i!=j){
        c = m[i*(C+1)+j]/m[j*(C+1)+j];
        for(k=0; k < C+1; ++k)
          m[i*(C+1)+k] = m[i*(C+1)+k] - c*m[j*(C+1)+k];
      }
    }
  }
  

  printf("TM \n");
  for(i=0;  i< R; ++i){
    for(j=0; j < C+1; ++j){
      printf("%.6f ", m[i*(C+1)+j]);
    }
    printf("\n");
  }
  printf("\n");

  
  for(j=1; j < C+1; ++j){
    c=0.0;
    for(i=R-1; i > R-j; i--)      
      c = c + m[(R-j)*(C+1)+i]*x[i];
    x[C-j] = (m[(C-j)*(C+1)+C] - c)/(m[(C-j)*(C+1)+C-j]);
  }
    
}



int main(){

  int R = 8;
  int C = 8;
  
  double *ma;
  double *x;

  
  ma = new double[R*(C+1)];
  x = new double[C];

  // for(int i=0; i < R*(C+1); ++i)
  //   m[i] = rand();
  


  ma[0]=0.3905704;    ma[1]=0.1203732;    ma[2]=0.3278683;    ma[3]=0.7988904;    ma[4]=0.8844012;    ma[5]=0.8693223;    ma[6]=0.2959154;    ma[7]=0.7093280;  // 0->
  ma[9]=0.8960144;    ma[10]=0.8573488;   ma[11]=0.4098551;   ma[12]=0.1344433;   ma[13]=0.3965334;   ma[14]=0.9324326;   ma[15]=0.0483210;   ma[16]=0.9077270; // 1->
  ma[18]=0.0788482;   ma[19]=0.4900649;   ma[20]=0.1723956;   ma[21]=0.2801510;   ma[22]=0.4018362;   ma[23]=0.0639029;   ma[24]=0.6679377;   ma[25]=0.7314400; // 2->
  ma[27]=0.7200132;   ma[28]=0.1704138;   ma[29]=0.0661138;   ma[30]=0.0048406;   ma[31]=0.1209832;   ma[32]=0.2424235;   ma[33]=0.3727946;   ma[34]=0.0018467; // 3->
  ma[36]=0.5995938;   ma[37]=0.2268861;   ma[38]=0.7479017;   ma[39]=0.6032921;   ma[40]=0.8963992;   ma[41]=0.9961749;   ma[42]=0.4156670;   ma[43]=0.2061578; // 4->
  ma[45]=0.3838716;   ma[46]=0.8081439;   ma[47]=0.4801946;   ma[48]=0.2760695;   ma[49]=0.4855037;   ma[50]=0.3293748;   ma[51]=0.9835563;   ma[52]=0.8329754; // 5->
  ma[54]=0.3285128;   ma[55]=0.6435567;   ma[56]=0.5604067;   ma[57]=0.9287390;   ma[58]=0.0181781;   ma[59]=0.5781228;   ma[60]=0.9688212;   ma[61]=0.1973067; // 6->
  ma[63]=0.7697594;   ma[64]=0.3863994;   ma[65]=0.6437610;   ma[66]=0.6473042;   ma[67]=0.2657015;   ma[68]=0.6510384;   ma[69]=0.1421472;   ma[70]=0.3517238; // 7->

  ma[8] =0.296125;
  ma[17]=0.463490;
  ma[26]=0.767997;
  ma[35]=0.737384;
  ma[44]=0.445600;
  ma[53]=0.322170;
  ma[62]=0.537043;
  ma[71]=0.055739;

  
  

  for(int i=0; i < R; ++i){
    for(int j=0; j < C; ++j)
      printf("%.3f ",ma[i*(C+1)+j]);
    printf("\n");
  }

  printf("\n");
  
  for(int i=0; i < C; ++i)
    printf("%.3f \n",ma[i*(C+1)+C]);

  printf("\n");


    
  gauss(ma,x,R,C);

  //printf("\n");
  for(int i=0; i < C; ++i)
    printf("%.10f \n",x[i]);
  //printf("\n");
  
  
  delete [] ma;
  delete [] x;

}

