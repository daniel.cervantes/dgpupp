#include <stdio.h>
#include <stdlib.h>

#define BLOCK_SIZE 16


// __global__ void ScaleRowKernel(double* matrix, unsigned int numberOfRows, unsigned int numberOfColumns, double* outputMatrix, int current_column)
// {
//   int tx = (blockIdx.x*blockDim.x) + threadIdx.x;
//   int ty = (blockIdx.y*blockDim.y) + threadIdx.y;
//   int tID = (ty*numberOfColumns)+tx;

//   if(current_column == ty && ty < numberOfRows)
//     {
//       if(tx < numberOfColumns)
//         {
//           outputMatrix[tID] = matrix[tID]/matrix[(current_column*numberOfColumns)+current_column];
//         }
//     }
// }

// __global__ void SubtractRowKernel(double* matrix, unsigned int numberOfRows, unsigned int numberOfColumns, double* outputMatrix, int current_column)
// {
//   int tx = (blockIdx.x*blockDim.x) + threadIdx.x;
//   int ty = (blockIdx.y*blockDim.y) + threadIdx.y;
//   int tID = (ty*numberOfColumns)+tx;
//   if(current_column != ty && ty < numberOfRows)
//     {
//       if(tx < numberOfColumns)
//         {
//           outputMatrix[tID] = matrix[tID] - (matrix[(current_column*numberOfColumns)+tx] * matrix[(ty*numberOfColumns)+current_column]);
//         }
//     }
// }


// __global__ void ThreeNestedLoops(){
  
//   int i = threadIdx.x + blockIdx.x * blockDim.x;
//   int j = threadIdx.y + blockIdx.y * blockDim.y;
//   int k = threadIdx.z + blockIdx.z * blockDim.z;

//   printf("GPU - i = %d, j = %d, k = %d\n",i,j,k);
  
// }



// __global__ void gaussElim(float * ma,  int n, int m, int l, int r){
  
//   int ii = threadIdx.x + blockIdx.x * blockDim.x;
//   int jj = threadIdx.y + blockIdx.y * blockDim.y;
//   int kk = threadIdx.z + blockIdx.z * blockDim.z;

  
//   if(ii < n && jj < m && kk < l){

//     //printf("%d %d %d\n",ii,jj,kk);
//     //printf("E1: ii->%d jj->%d \n",ii,jj);
    
//     float c;
//     int R = 8;
//     int C = 8;

//     // printf( "00-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[0],ma[1],ma[2],ma[3],ma[4],ma[5],ma[6],ma[7],ma[8]);
//     // printf(" 01-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[9],ma[10],ma[11],ma[12],ma[13],ma[14],ma[15],ma[16],ma[17]);
//     // printf(" 02-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[18],ma[19],ma[20],ma[21],ma[22],ma[23],ma[24],ma[25],ma[26]);
//     // printf(" 03-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[27],ma[28],ma[29],ma[30],ma[31],ma[32],ma[33],ma[34],ma[35]);
//     // printf(" 04-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[36],ma[37],ma[38],ma[39],ma[40],ma[41],ma[42],ma[43],ma[44]);
//     // printf(" 05-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[45],ma[46],ma[47],ma[48],ma[49],ma[50],ma[51],ma[52],ma[53]);
//     // printf(" 06-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[54],ma[55],ma[56],ma[57],ma[58],ma[59],ma[60],ma[61],ma[62]);
//     // printf(" 07-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[63],ma[64],ma[65],ma[66],ma[67],ma[68],ma[69],ma[70],ma[71]);

    

//     //for(int j=0; j < R; ++j){
//     for(int i=r; i < R; ++i){        
//       if(i!=r){
//         //printf(" i=%d \n",i);
//         c = ma[i*(C+1)+r]/ma[r*(C+1)+r];        
//         printf("ii=%d jj=%d kk=%d i=%d r=%d %.6f %.6f %.6f\n",ii,jj,kk,i,r,ma[i*(C+1)+r],ma[r*(C+1)+r],c);
//         //for(int  k=0; k < C+1; ++k){
//         //printf("%d  %.3f  %.3f\n",i*(C+1)+r,ma[i*(C+1)+r],c);
//         //ma[i*(C+1)+k] =  ma[i*(C+1)+k] - c*ma[r*(C+1)+k];
//         //printf("0ima = %d  man=%.3f mad=%.3f  c=%.3f\n",i*(C+1)+r,ma[i*(C+1)+r],ma[r*(C+1)+r],c);
//         //printf("i= %d r=%d ma=%.6f c*ma = %.6f \n",i,r,ma[i*(C+1)],c*ma[r*(C+1)]);
//         ma[i*(C+1)]   =  ma[i*(C+1)]   - c*ma[r*(C+1)];
//         ma[i*(C+1)+1] =  ma[i*(C+1)+1] - c*ma[r*(C+1)+1];
//         ma[i*(C+1)+2] =  ma[i*(C+1)+2] - c*ma[r*(C+1)+2];
//         ma[i*(C+1)+3] =  ma[i*(C+1)+3] - c*ma[r*(C+1)+3];
//         ma[i*(C+1)+4] =  ma[i*(C+1)+4] - c*ma[r*(C+1)+4];
//         ma[i*(C+1)+5] =  ma[i*(C+1)+5] - c*ma[r*(C+1)+5];
//         ma[i*(C+1)+6] =  ma[i*(C+1)+6] - c*ma[r*(C+1)+6];
//         ma[i*(C+1)+7] =  ma[i*(C+1)+7] - c*ma[r*(C+1)+7];

//         printf("ii=%d jj=%d kk=%d i=%d r=%d ma=%.6f ma=%.6f ma=%.6f ma=%.6f ma=%.6f ma=%.6f ma=%.6f ma=%.6f \n",
//                ii,jj,kk,i,r,ma[i*(C+1)],ma[i*(C+1)+1],ma[i*(C+1)+2],ma[i*(C+1)+3],ma[i*(C+1)+4],ma[i*(C+1)+5],ma[i*(C+1)+6],ma[i*(C+1)+7]);
        
//         //printf("i=%d r=%d man=%.6f iman=%d  mad=%.6f c=%.6f ma =%.6f\n",i,r,ma[i*(C+1)+r],i*(C+1)+r,ma[r*(C+1)+r],c,ma[i*(C+1)+k]);
//         //}
//         //printf(" ma=%.3f \n",ma[i*(C+1)]);

//       }
//     }
//       //}

//     printf( "G00-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[0],ma[1],ma[2],ma[3],ma[4],ma[5],ma[6],ma[7],ma[8]);
//     printf(" G01-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[9],ma[10],ma[11],ma[12],ma[13],ma[14],ma[15],ma[16],ma[17]);
//     printf(" G02-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[18],ma[19],ma[20],ma[21],ma[22],ma[23],ma[24],ma[25],ma[26]);
//     printf(" G03-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[27],ma[28],ma[29],ma[30],ma[31],ma[32],ma[33],ma[34],ma[35]);
//     printf(" G04-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[36],ma[37],ma[38],ma[39],ma[40],ma[41],ma[42],ma[43],ma[44]);
//     printf(" G05-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[45],ma[46],ma[47],ma[48],ma[49],ma[50],ma[51],ma[52],ma[53]);
//     printf(" G06-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[54],ma[55],ma[56],ma[57],ma[58],ma[59],ma[60],ma[61],ma[62]);
//     printf(" G07-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[63],ma[64],ma[65],ma[66],ma[67],ma[68],ma[69],ma[70],ma[71]);

    
//     // for(i=0; i < R; ++i){
//     //   for(j=0; j < C; ++j)
//     //     printf("%.f ",ma[i*C+j]);
//     //   printf("\n");
//     // }
    

//     // for(j=1; j < C+1; ++j){
//     //   c=0.0;
//     //   for(i=R-1; i > R-j; i--)      
//     //     c = c + ma[(R-j)*(C+1)+i]*x[i];
//     //   x[C-j] = (ma[(C-j)*(C+1)+C] - c)/(ma[(C-j)*(C+1)+(C-j)]);
//     // }


//     // Valores reales
//     //  1.0858
//     //  7.6335
//     // -4.9111
//     //  3.4012
//     //  5.6306
//     // -3.3618
//     // -2.3985
//     // -4.9360


    
//     //printf("x-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f  \n",x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7]);
    
//   }


// }


// __global__ void gaussElim1(float * ma, int n, int m, int l){
  
//   int ii = threadIdx.x + blockIdx.x * blockDim.x;
//   int jj = threadIdx.y + blockIdx.y * blockDim.y;
//   //int kk = threadIdx.z + blockIdx.z * blockDim.z;

  
//   //if(ii < n && jj < m && kk < l){
//   if(ii < n && jj < m ){
//     //printf("%d %d %d\n",ii,jj,kk);
//     //printf("E1: ii->%d jj->%d \n",ii,jj);
    
//     int i;
//     int j;
//     int k;

//     float c;

//     int R = 8;
//     int C = 8;

//     printf( "00-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[0],ma[1],ma[2],ma[3],ma[4],ma[5],ma[6],ma[7],ma[8]);
//     printf(" 01-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[9],ma[10],ma[11],ma[12],ma[13],ma[14],ma[15],ma[16],ma[17]);
//     printf(" 02-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[18],ma[19],ma[20],ma[21],ma[22],ma[23],ma[24],ma[25],ma[26]);
//     printf(" 03-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[27],ma[28],ma[29],ma[30],ma[31],ma[32],ma[33],ma[34],ma[35]);
//     printf(" 04-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[36],ma[37],ma[38],ma[39],ma[40],ma[41],ma[42],ma[43],ma[44]);
//     printf(" 05-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[45],ma[46],ma[47],ma[48],ma[49],ma[50],ma[51],ma[52],ma[53]);
//     printf(" 06-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[54],ma[55],ma[56],ma[57],ma[58],ma[59],ma[60],ma[61],ma[62]);
//     printf(" 07-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[63],ma[64],ma[65],ma[66],ma[67],ma[68],ma[69],ma[70],ma[71]);

    

//     for(j=0; j < R; ++j){
//       for(i=j; i < C; ++i){        
//         if(i!=j){
//           c = ma[i*(C+1)+j]/ma[j*(C+1)+j];
//           for(k=0; k < C+1; ++k){          
//             ma[i*(C+1)+k] =  ma[i*(C+1)+k] - c*ma[j*(C+1)+k];
//             //printf("c=%.6f ma =%.6f\n",c,ma[i*(C+1)+k]);
//           }

//         }
//       }
//     }

//     __syncthreads();
//     printf( "G00-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[0],ma[1],ma[2],ma[3],ma[4],ma[5],ma[6],ma[7],ma[8]);
//     printf(" G01-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[9],ma[10],ma[11],ma[12],ma[13],ma[14],ma[15],ma[16],ma[17]);
//     printf(" G02-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[18],ma[19],ma[20],ma[21],ma[22],ma[23],ma[24],ma[25],ma[26]);
//     printf(" G03-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[27],ma[28],ma[29],ma[30],ma[31],ma[32],ma[33],ma[34],ma[35]);
//     printf(" G04-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[36],ma[37],ma[38],ma[39],ma[40],ma[41],ma[42],ma[43],ma[44]);
//     printf(" G05-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[45],ma[46],ma[47],ma[48],ma[49],ma[50],ma[51],ma[52],ma[53]);
//     printf(" G06-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[54],ma[55],ma[56],ma[57],ma[58],ma[59],ma[60],ma[61],ma[62]);
//     printf(" G07-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[63],ma[64],ma[65],ma[66],ma[67],ma[68],ma[69],ma[70],ma[71]);

    
//     // for(i=0; i < R; ++i){
//     //   for(j=0; j < C; ++j)
//     //     printf("%.f ",ma[i*C+j]);
//     //   printf("\n");
//     // }
    

//     // for(j=1; j < C+1; ++j){
//     //   c=0.0;
//     //   for(i=R-1; i > R-j; i--)      
//     //     c = c + ma[(R-j)*(C+1)+i]*x[i];
//     //   x[C-j] = (ma[(C-j)*(C+1)+C] - c)/(ma[(C-j)*(C+1)+(C-j)]);
//     // }


//     // Valores reales
//     //  1.0858
//     //  7.6335
//     // -4.9111
//     //  3.4012
//     //  5.6306
//     // -3.3618
//     // -2.3985
//     // -4.9360


    
//     //printf("x-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f  \n",x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7]);
    
//   }


// }

// __global__ void gaussElim2(float * ma, float *x, int n, int m, int l){

//   int ii = threadIdx.x + blockIdx.x * blockDim.x;
//   int jj = threadIdx.y + blockIdx.y * blockDim.y;
//   //  int kk = threadIdx.z + blockIdx.z * blockDim.z;

  
//   //if(ii < n && jj < m && kk < l){
//   if(ii < n && jj < m){
//     //printf("%d %d %d\n",ii,jj,kk);
//     //printf("E2: ii->%d jj->%d \n",ii,jj);
//     int i;
//     int j;
//     //int k;

//     float c;

//     int R = 8;
//     int C = 8;

//     // printf( "10-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[0],ma[1],ma[2],ma[3],ma[4],ma[5],ma[6],ma[7],ma[8]);
//     // printf(" 11-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[9],ma[10],ma[11],ma[12],ma[13],ma[14],ma[15],ma[16],ma[17]);
//     // printf(" 12-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[18],ma[19],ma[20],ma[21],ma[22],ma[23],ma[24],ma[25],ma[26]);
//     // printf(" 13-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[27],ma[28],ma[29],ma[30],ma[31],ma[32],ma[33],ma[34],ma[35]);
//     // printf(" 14-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[36],ma[37],ma[38],ma[39],ma[40],ma[41],ma[42],ma[43],ma[44]);
//     // printf(" 15-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[45],ma[46],ma[47],ma[48],ma[49],ma[50],ma[51],ma[52],ma[53]);
//     // printf(" 16-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[54],ma[55],ma[56],ma[57],ma[58],ma[59],ma[60],ma[61],ma[62]);
//     // printf(" 17-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[63],ma[64],ma[65],ma[66],ma[67],ma[68],ma[69],ma[70],ma[71]);

    

//     // for(j=0; j < R; ++j){
//     //   for(i=j; i < C; ++i){        
//     //     if(i!=j){
//     //       c = ma[i*(C+1)+j]/ma[j*(C+1)+j];
//     //       for(k=0; k < C+1; ++k)
//     //         ma[i*(C+1)+k] =  ma[i*(C+1)+k] - c*ma[j*(C+1)+k];
//     //     }
//     //   }
//     // }


//     // printf( "G0-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[0],ma[1],ma[2],ma[3],ma[4],ma[5],ma[6],ma[7],ma[8]);
//     // printf(" G1-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[9],ma[10],ma[11],ma[12],ma[13],ma[14],ma[15],ma[16],ma[17]);
//     // printf(" G2-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[18],ma[19],ma[20],ma[21],ma[22],ma[23],ma[24],ma[25],ma[26]);
//     // printf(" G3-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[27],ma[28],ma[29],ma[30],ma[31],ma[32],ma[33],ma[34],ma[35]);
//     // printf(" G4-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[36],ma[37],ma[38],ma[39],ma[40],ma[41],ma[42],ma[43],ma[44]);
//     // printf(" G5-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[45],ma[46],ma[47],ma[48],ma[49],ma[50],ma[51],ma[52],ma[53]);
//     // printf(" G6-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[54],ma[55],ma[56],ma[57],ma[58],ma[59],ma[60],ma[61],ma[62]);
//     // printf(" G7-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",ma[63],ma[64],ma[65],ma[66],ma[67],ma[68],ma[69],ma[70],ma[71]);

    
//     // for(i=0; i < R; ++i){
//     //   for(j=0; j < C; ++j)
//     //     printf("%.f ",ma[i*C+j]);
//     //   printf("\n");
//     // }
    

//     for(j=1; j < C+1; ++j){
//       c=0.0;
//       for(i=R-1; i > R-j; i--)      
//         c = c + ma[(R-j)*(C+1)+i]*x[i];
//       x[C-j] = (ma[(C-j)*(C+1)+C] - c)/(ma[(C-j)*(C+1)+(C-j)]);
//     }


//     // Valores reales
//     //  1.0858
//     //  7.6335
//     // -4.9111
//     //  3.4012
//     //  5.6306
//     // -3.3618
//     // -2.3985
//     // -4.9360


    
//     printf("x-> %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f  \n",x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7]);
    
//   }


// }


__global__ void setM(float * m, int i, int j, int k, int dim, int N, int M, int L, float v){
  
  int ii = threadIdx.x + blockIdx.x * blockDim.x;
  int jj = threadIdx.y + blockIdx.y * blockDim.y;
  int kk = threadIdx.z + blockIdx.z * blockDim.z;

  
}

int main(){

  int n = 5;
  int m = 5;
  int l = 5;
  int dim = 8;

  int r = 8;
  int c = 8;

  int bytesM   = r*(c+1)*sizeof(float);
  int bytesV   = c*sizeof(float);
  int bytes3DM = n*m*l*dim*sizeof(float);
  
  float* ma = new float [bytesM];
  float* x  = new float [bytesV];

  float* d_ma;
  float* d_x;
  
  // for(int i=0; i < r; ++i)  
  //   for(int j=0; j < c+1; ++j)
  //     ma[i*(c+1)+j] = rand();

  ma[0]=0.3905704;    ma[1]=0.1203732;    ma[2]=0.3278683;    ma[3]=0.7988904;    ma[4]=0.8844012;    ma[5]=0.8693223;    ma[6]=0.2959154;    ma[7]=0.7093280;  // 0->
  ma[9]=0.8960144;    ma[10]=0.8573488;   ma[11]=0.4098551;   ma[12]=0.1344433;   ma[13]=0.3965334;   ma[14]=0.9324326;   ma[15]=0.0483210;   ma[16]=0.9077270; // 1->
  ma[18]=0.0788482;   ma[19]=0.4900649;   ma[20]=0.1723956;   ma[21]=0.2801510;   ma[22]=0.4018362;   ma[23]=0.0639029;   ma[24]=0.6679377;   ma[25]=0.7314400; // 2->
  ma[27]=0.7200132;   ma[28]=0.1704138;   ma[29]=0.0661138;   ma[30]=0.0048406;   ma[31]=0.1209832;   ma[32]=0.2424235;   ma[33]=0.3727946;   ma[34]=0.0018467; // 3->
  ma[36]=0.5995938;   ma[37]=0.2268861;   ma[38]=0.7479017;   ma[39]=0.6032921;   ma[40]=0.8963992;   ma[41]=0.9961749;   ma[42]=0.4156670;   ma[43]=0.2061578; // 4->
  ma[45]=0.3838716;   ma[46]=0.8081439;   ma[47]=0.4801946;   ma[48]=0.2760695;   ma[49]=0.4855037;   ma[50]=0.3293748;   ma[51]=0.9835563;   ma[52]=0.8329754; // 5->
  ma[54]=0.3285128;   ma[55]=0.6435567;   ma[56]=0.5604067;   ma[57]=0.9287390;   ma[58]=0.0181781;   ma[59]=0.5781228;   ma[60]=0.9688212;   ma[61]=0.1973067; // 6->
  ma[63]=0.7697594;   ma[64]=0.3863994;   ma[65]=0.6437610;   ma[66]=0.6473042;   ma[67]=0.2657015;   ma[68]=0.6510384;   ma[69]=0.1421472;   ma[70]=0.3517238; // 7->

  ma[8] =0.296125;
  ma[17]=0.463490;
  ma[26]=0.767997;
  ma[35]=0.737384;
  ma[44]=0.445600;
  ma[53]=0.322170;
  ma[62]=0.537043;
  ma[71]=0.055739;



  // for(int i=0; i < r; ++i){  
  //   for(int j=0; j < c+1; ++j)
  //     printf("%f   ",ma[i*(c+1) + j]);
  //   printf("\n");
  // }
  // printf("\n");


  
  // for(int i=0; i < r; ++i)
  //   printf("%f   \n",ma[i + c*c]);
  
  // printf("\n");
  
  // unsigned int grid_n = (n + BLOCK_SIZE - 1) / BLOCK_SIZE;
  // unsigned int grid_m = (m + BLOCK_SIZE - 1) / BLOCK_SIZE;
  // unsigned int grid_l = (l + BLOCK_SIZE - 1) / BLOCK_SIZE;
  
  // dim3 dimGrid(grid_n,grid_m,grid_l);
  // dim3 dimBlock(BLOCK_SIZE,BLOCK_SIZE,BLOCK_SIZE);  

  dim3 dimGrid(5,5,5);
  dim3 dimBlock(1,1,1);  

  cudaMalloc((void**) &d_ma,bytesM);
  cudaMalloc((void**) &d_x,bytesV);

  cudaMemcpy(d_ma,ma,bytesM,cudaMemcpyHostToDevice);
  
  
  //for(int i=0; i < r; ++i){
  gaussElim<<<dimGrid, dimBlock>>>(d_ma,n,m,l,0);
  cudaDeviceSynchronize();
    // gaussElim2<<<dimGrid, dimBlock>>>(d_ma,d_x,n,m,l);

    //}
  cudaMemcpy(ma,d_ma,bytesM,cudaMemcpyDeviceToHost);

  for(int i=0; i < r; ++i){
    for(int j=0; j < c+1; ++j)
      printf("%.6f ",ma[i*(c+1)+j]);
    printf("\n");
  }
  
  // int K = 20;
  // int L = 20;
  // int M = 20;

  // unsigned int grid_i = ((K+2) + BLOCK_SIZE - 1) / BLOCK_SIZE;
  // unsigned int grid_j = ((L+2) + BLOCK_SIZE - 1) / BLOCK_SIZE;
  // unsigned int grid_k = ((M+2) + BLOCK_SIZE - 1) / BLOCK_SIZE;

  
  // dim3 dimGrid(10,10,10);
  // dim3 dimBlock(10, 10, 10);  

  // cudaDeviceProp prop;   cudaGetDeviceProperties( &prop, 0);
  // ThreeNestedLoops <<< dimGrid, dimBlock >>>();
  // cudaDeviceSynchronize();

  return 0;  
}
