function test

  clf
  clear
  close all
  xkp12 =  1;
  xkl12 = -1;
  ylp12 =  1;
  yll12 = -1;


  x = linspace(xkp12,xkl12);
  y = linspace(ylp12,yll12);
  
  [xx,yy]= meshgrid(x,y);
  figure
  plot3(xx,yy,phi0(xkl12,xkp12,yll12,ylp12,xx,yy));

  figure
  plot3(xx,yy,phi1(xkl12,xkp12,yll12,ylp12,xx,yy));

  figure
  plot3(xx,yy,phi2(xkl12,xkp12,yll12,ylp12,xx,yy));

  figure
  plot3(xx,yy,phi3(xkl12,xkp12,yll12,ylp12,xx,yy));

phi0(0,0)

end


function out = phi0(xkl12,xkp12,yll12,ylp12,x,y)
  out = ((x-xkp12)./(xkl12-xkp12)).*((y-ylp12)./(yll12-ylp12));
end

function out = phi1(xkl12,xkp12,yll12,ylp12,x,y)
  out = ((x-xkl12)./(xkp12-xkl12)).*((y-ylp12)./(yll12-ylp12));
end

function out = phi2(xkl12,xkp12,yll12,ylp12,x,y)
  out = ((x-xkl12)./(xkp12-xkl12)).*((y-yll12)./(ylp12-yll12));
end

function out = phi3(xkl12,xkp12,yll12,ylp12,x,y)
  out = ((x-xkp12)./(xkl12-xkp12)).*((y-yll12)./(ylp12-yll12));
end


                 
