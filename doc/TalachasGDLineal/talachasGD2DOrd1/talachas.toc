\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Problem statement}{1}
\contentsline {section}{\numberline {3}Discontinuous Galerkin Method}{1}
\contentsline {subsection}{\numberline {3.1}Initial conditions}{6}
\contentsline {subsection}{\numberline {3.2}Dirichlet boundary conditions}{6}
\contentsline {subsection}{\numberline {3.3}Basis functions on $\mathcal {C}_{k,l}$}{7}
\contentsline {section}{\numberline {4}Results}{7}
\contentsline {section}{\numberline {5}References}{7}
