phi0(x,y):=((x-xkp12)/(xkl12-xkp12))*((y-ylp12)/(yll12-ylp12));
phi1(x,y):=((x-xkl12)/(xkp12-xkl12))*((y-ylp12)/(yll12-ylp12));
phi2(x,y):=((x-xkl12)/(xkp12-xkl12))*((y-yll12)/(ylp12-yll12));
phi3(x,y):=((x-xkp12)/(xkl12-xkp12))*((y-yll12)/(ylp12-yll12));



////////////ML
string(factor(integrate(integrate(phi0(x,y)*phi0(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/9
string(factor(integrate(integrate(phi1(x,y)*phi0(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/18
string(factor(integrate(integrate(phi2(x,y)*phi0(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/36
string(factor(integrate(integrate(phi3(x,y)*phi0(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/18

string(factor(integrate(integrate(phi0(x,y)*phi1(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/18
string(factor(integrate(integrate(phi1(x,y)*phi1(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/9
string(factor(integrate(integrate(phi2(x,y)*phi1(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/18
string(factor(integrate(integrate(phi3(x,y)*phi1(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/36

string(factor(integrate(integrate(phi0(x,y)*phi2(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/36
string(factor(integrate(integrate(phi1(x,y)*phi2(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/18
string(factor(integrate(integrate(phi2(x,y)*phi2(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/9
string(factor(integrate(integrate(phi3(x,y)*phi2(x,y),x,xkl12,xkp12),y,yll12,ylp12)));  
((xkp12-xkl12)*(ylp12-yll12))/18                                                                                        

string(factor(integrate(integrate(phi0(x,y)*phi3(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/18
string(factor(integrate(integrate(phi1(x,y)*phi3(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/36
string(factor(integrate(integrate(phi2(x,y)*phi3(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/18
string(factor(integrate(integrate(phi3(x,y)*phi3(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
((xkp12-xkl12)*(ylp12-yll12))/9



///////////////// M--xkp12
string(factor(integrate(phi0(xkp12,y)*phi0(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi1(xkp12,y)*phi0(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi2(xkp12,y)*phi0(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi3(xkp12,y)*phi0(xkp12,y),y,yll12,ylp12)));
0


string(factor(integrate(phi0(xkp12,y)*phi1(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi1(xkp12,y)*phi1(xkp12,y),y,yll12,ylp12)));
(ylp12-yll12)/3
string(factor(integrate(phi2(xkp12,y)*phi1(xkp12,y),y,yll12,ylp12)));
(ylp12-yll12)/6
string(factor(integrate(phi3(xkp12,y)*phi1(xkp12,y),y,yll12,ylp12)));
0


string(factor(integrate(phi0(xkp12,y)*phi2(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi1(xkp12,y)*phi2(xkp12,y),y,yll12,ylp12)));
(ylp12-yll12)/6
string(factor(integrate(phi2(xkp12,y)*phi2(xkp12,y),y,yll12,ylp12)));
(ylp12-yll12)/3
string(factor(integrate(phi3(xkp12,y)*phi2(xkp12,y),y,yll12,ylp12)));
0

string(factor(integrate(phi0(xkp12,y)*phi3(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi1(xkp12,y)*phi3(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi2(xkp12,y)*phi3(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi3(xkp12,y)*phi3(xkp12,y),y,yll12,ylp12)));
0






////////////// M+-xkp12
phi0pE(x,y):=((x-xkp32)/(xkp12-xkp32))*((y-ylp12)/(yll12-ylp12));
phi1pE(x,y):=((x-xkp12)/(xkp32-xkp12))*((y-ylp12)/(yll12-ylp12));
phi2pE(x,y):=((x-xkp12)/(xkp32-xkp12))*((y-yll12)/(ylp12-yll12));
phi3pE(x,y):=((x-xkp32)/(xkp12-xkp32))*((y-yll12)/(ylp12-yll12));





string(factor(integrate(phi0E(xkp12,y)*phi0(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi1E(xkp12,y)*phi0(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi2E(xkp12,y)*phi0(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi3E(xkp12,y)*phi0(xkp12,y),y,yll12,ylp12)));
0

string(factor(integrate(phi0pE(xkp12,y)*phi1(xkp12,y),y,yll12,ylp12)));
(ylp12-yll12)/3
string(factor(integrate(phi1pE(xkp12,y)*phi1(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi2pE(xkp12,y)*phi1(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi3pE(xkp12,y)*phi1(xkp12,y),y,yll12,ylp12)));
(ylp12-yll12)/6


string(factor(integrate(phi0pE(xkp12,y)*phi2(xkp12,y),y,yll12,ylp12)));
(ylp12-yll12)/6
string(factor(integrate(phi1pE(xkp12,y)*phi2(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi2pE(xkp12,y)*phi2(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi3pE(xkp12,y)*phi2(xkp12,y),y,yll12,ylp12)));
(ylp12-yll12)/3


string(factor(integrate(phi0pE(xkp12,y)*phi3(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi1pE(xkp12,y)*phi3(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi2pE(xkp12,y)*phi3(xkp12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi3pE(xkp12,y)*phi3(xkp12,y),y,yll12,ylp12)));
0



////////// M--xkl12
string(factor(integrate(phi0(xkl12,y)*phi0(xkl12,y),y,yll12,ylp12)));
(ylp12-yll12)/3
string(factor(integrate(phi1(xkl12,y)*phi0(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi2(xkl12,y)*phi0(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi3(xkl12,y)*phi0(xkl12,y),y,yll12,ylp12)));
(ylp12-yll12)/6


string(factor(integrate(phi0(xkl12,y)*phi1(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi1(xkl12,y)*phi1(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi2(xkl12,y)*phi1(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi3(xkl12,y)*phi1(xkl12,y),y,yll12,ylp12)));
0


string(factor(integrate(phi0(xkl12,y)*phi2(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi1(xkl12,y)*phi2(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi2(xkl12,y)*phi2(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi3(xkl12,y)*phi2(xkl12,y),y,yll12,ylp12)));
0


string(factor(integrate(phi0(xkl12,y)*phi3(xkl12,y),y,yll12,ylp12)));
(ylp12-yll12)/6
string(factor(integrate(phi1(xkl12,y)*phi3(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi2(xkl12,y)*phi3(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi3(xkl12,y)*phi3(xkl12,y),y,yll12,ylp12)));
(ylp12-yll12)/3


//////////M+-xkl12
phi0pW(x,y):=((x-xkl12)/(xkl32-xkl12))*((y-ylp12)/(yll12-ylp12));
phi1pW(x,y):=((x-xkl32)/(xkl12-xkl32))*((y-ylp12)/(yll12-ylp12));
phi2pW(x,y):=((x-xkl32)/(xkl12-xkl32))*((y-yll12)/(ylp12-yll12));
phi3pW(x,y):=((x-xkl12)/(xkl32-xkl12))*((y-yll12)/(ylp12-yll12));

string(factor(integrate(phi0pW(xkl12,y)*phi0(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi1pW(xkl12,y)*phi0(xkl12,y),y,yll12,ylp12)));
(ylp12-yll12)/3
string(factor(integrate(phi2pW(xkl12,y)*phi0(xkl12,y),y,yll12,ylp12)));
(ylp12-yll12)/6
string(factor(integrate(phi3pW(xkl12,y)*phi0(xkl12,y),y,yll12,ylp12)));
0

string(factor(integrate(phi0pW(xkl12,y)*phi1(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi1pW(xkl12,y)*phi1(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi2pW(xkl12,y)*phi1(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi3pW(xkl12,y)*phi1(xkl12,y),y,yll12,ylp12)));
0


string(factor(integrate(phi0pW(xkl12,y)*phi2(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi1pW(xkl12,y)*phi2(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi2pW(xkl12,y)*phi2(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi3pW(xkl12,y)*phi2(xkl12,y),y,yll12,ylp12)));
0


string(factor(integrate(phi0pW(xkl12,y)*phi3(xkl12,y),y,yll12,ylp12)));
0
string(factor(integrate(phi1pW(xkl12,y)*phi3(xkl12,y),y,yll12,ylp12)));
(ylp12-yll12)/6
string(factor(integrate(phi2pW(xkl12,y)*phi3(xkl12,y),y,yll12,ylp12)));
(ylp12-yll12)/3
string(factor(integrate(phi3pW(xkl12,y)*phi3(xkl12,y),y,yll12,ylp12)));
0



//////////////////M phij dxphii

dxphi0(x,y):=diff(phi0(x,y),x);
dxphi1(x,y):=diff(phi1(x,y),x);
dxphi2(x,y):=diff(phi2(x,y),x);
dxphi3(x,y):=diff(phi3(x,y),x);


string(factor(integrate(integrate(phi0(x,y)*dxphi0(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(ylp12-yll12)/6
string(factor(integrate(integrate(phi1(x,y)*dxphi0(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(ylp12-yll12)/6
string(factor(integrate(integrate(phi2(x,y)*dxphi0(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(ylp12-yll12)/12
string(factor(integrate(integrate(phi3(x,y)*dxphi0(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(ylp12-yll12)/12


string(factor(integrate(integrate(phi0(x,y)*dxphi1(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(ylp12-yll12)/6
string(factor(integrate(integrate(phi1(x,y)*dxphi1(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(ylp12-yll12)/6
string(factor(integrate(integrate(phi2(x,y)*dxphi1(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(ylp12-yll12)/12
string(factor(integrate(integrate(phi3(x,y)*dxphi1(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(ylp12-yll12)/12


string(factor(integrate(integrate(phi0(x,y)*dxphi2(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(ylp12-yll12)/12
string(factor(integrate(integrate(phi1(x,y)*dxphi2(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(ylp12-yll12)/12
string(factor(integrate(integrate(phi2(x,y)*dxphi2(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(ylp12-yll12)/6
string(factor(integrate(integrate(phi3(x,y)*dxphi2(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(ylp12-yll12)/6


string(factor(integrate(integrate(phi0(x,y)*dxphi3(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(ylp12-yll12)/12
string(factor(integrate(integrate(phi1(x,y)*dxphi3(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(ylp12-yll12)/12
string(factor(integrate(integrate(phi2(x,y)*dxphi3(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(ylp12-yll12)/6
string(factor(integrate(integrate(phi3(x,y)*dxphi3(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(ylp12-yll12)/6




///////////////////////////////////////

//////M--ylp12
string(factor(integrate(phi0(x,ylp12)*phi0(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi1(x,ylp12)*phi0(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi2(x,ylp12)*phi0(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi3(x,ylp12)*phi0(x,ylp12),x,xkl12,xkp12)));
0


string(factor(integrate(phi0(x,ylp12)*phi1(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi1(x,ylp12)*phi1(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi2(x,ylp12)*phi1(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi3(x,ylp12)*phi1(x,ylp12),x,xkl12,xkp12)));
0

string(factor(integrate(phi0(x,ylp12)*phi2(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi1(x,ylp12)*phi2(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi2(x,ylp12)*phi2(x,ylp12),x,xkl12,xkp12)));
(xkp12-xkl12)/3
string(factor(integrate(phi3(x,ylp12)*phi2(x,ylp12),x,xkl12,xkp12)));
(xkp12-xkl12)/6

string(factor(integrate(phi0(x,ylp12)*phi3(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi1(x,ylp12)*phi3(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi2(x,ylp12)*phi3(x,ylp12),x,xkl12,xkp12)));
(xkp12-xkl12)/6
string(factor(integrate(phi3(x,ylp12)*phi3(x,ylp12),x,xkl12,xkp12)));
(xkp12-xkl12)/3



//////M+-ylp12

phi0pN(x,y):=((x-xkp12)/(xkl12-xkp12))*((y-ylp32)/(ylp12-ylp32));
phi1pN(x,y):=((x-xkl12)/(xkp12-xkl12))*((y-ylp32)/(ylp12-ylp32));
phi2pN(x,y):=((x-xkl12)/(xkp12-xkl12))*((y-ylp12)/(ylp32-ylp12));
phi3pN(x,y):=((x-xkp12)/(xkl12-xkp12))*((y-ylp12)/(ylp32-ylp12));


string(factor(integrate(phi0pN(x,ylp12)*phi0(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi1pN(x,ylp12)*phi0(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi2pN(x,ylp12)*phi0(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi3pN(x,ylp12)*phi0(x,ylp12),x,xkl12,xkp12)));
0

string(factor(integrate(phi0pN(x,ylp12)*phi1(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi1pN(x,ylp12)*phi1(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi2pN(x,ylp12)*phi1(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi3pN(x,ylp12)*phi1(x,ylp12),x,xkl12,xkp12)));
0

string(factor(integrate(phi0pN(x,ylp12)*phi2(x,ylp12),x,xkl12,xkp12)));
(xkp12-xkl12)/6
string(factor(integrate(phi1pN(x,ylp12)*phi2(x,ylp12),x,xkl12,xkp12)));
(xkp12-xkl12)/3
string(factor(integrate(phi2pN(x,ylp12)*phi2(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi3pN(x,ylp12)*phi2(x,ylp12),x,xkl12,xkp12)));
0


string(factor(integrate(phi0pN(x,ylp12)*phi3(x,ylp12),x,xkl12,xkp12)));
(xkp12-xkl12)/3
string(factor(integrate(phi1pN(x,ylp12)*phi3(x,ylp12),x,xkl12,xkp12)));
(xkp12-xkl12)/6
string(factor(integrate(phi2pN(x,ylp12)*phi3(x,ylp12),x,xkl12,xkp12)));
0
string(factor(integrate(phi3pN(x,ylp12)*phi3(x,ylp12),x,xkl12,xkp12)));
0



/////M--yll12
string(factor(integrate(phi0(x,yll12)*phi0(x,yll12),x,xkl12,xkp12)));
(xkp12-xkl12)/3
string(factor(integrate(phi1(x,yll12)*phi0(x,yll12),x,xkl12,xkp12)));
(xkp12-xkl12)/6
string(factor(integrate(phi2(x,yll12)*phi0(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi3(x,yll12)*phi0(x,yll12),x,xkl12,xkp12)));
0


string(factor(integrate(phi0(x,yll12)*phi1(x,yll12),x,xkl12,xkp12)));
(xkp12-xkl12)/6
string(factor(integrate(phi1(x,yll12)*phi1(x,yll12),x,xkl12,xkp12)));
(xkp12-xkl12)/3
string(factor(integrate(phi2(x,yll12)*phi1(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi3(x,yll12)*phi1(x,yll12),x,xkl12,xkp12)));
0


string(factor(integrate(phi0(x,yll12)*phi2(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi1(x,yll12)*phi2(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi2(x,yll12)*phi2(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi3(x,yll12)*phi2(x,yll12),x,xkl12,xkp12)));
0


string(factor(integrate(phi0(x,yll12)*phi3(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi1(x,yll12)*phi3(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi2(x,yll12)*phi3(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi3(x,yll12)*phi3(x,yll12),x,xkl12,xkp12)));
0

phi0pS(x,y):=((x-xkp12)/(xkl12-xkp12))*((y-yll12)/(yll32-yll12));
phi1pS(x,y):=((x-xkl12)/(xkp12-xkl12))*((y-yll12)/(yll32-yll12));
phi2pS(x,y):=((x-xkl12)/(xkp12-xkl12))*((y-yll32)/(yll12-yll32));
phi3pS(x,y):=((x-xkp12)/(xkl12-xkp12))*((y-yll32)/(yll12-yll32));




/////M+-yll12
string(factor(integrate(phi0pS(x,yll12)*phi0(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi1pS(x,yll12)*phi0(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi2pS(x,yll12)*phi0(x,yll12),x,xkl12,xkp12)));
(xkp12-xkl12)/6
string(factor(integrate(phi3pS(x,yll12)*phi0(x,yll12),x,xkl12,xkp12)));
(xkp12-xkl12)/3


string(factor(integrate(phi0pS(x,yll12)*phi1(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi1pS(x,yll12)*phi1(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi2pS(x,yll12)*phi1(x,yll12),x,xkl12,xkp12)));
(xkp12-xkl12)/3
string(factor(integrate(phi3pS(x,yll12)*phi1(x,yll12),x,xkl12,xkp12)));
(xkp12-xkl12)/6


string(factor(integrate(phi0pS(x,yll12)*phi2(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi1pS(x,yll12)*phi2(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi2pS(x,yll12)*phi2(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi3pS(x,yll12)*phi2(x,yll12),x,xkl12,xkp12)));
0

string(factor(integrate(phi0pS(x,yll12)*phi3(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi1pS(x,yll12)*phi3(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi2pS(x,yll12)*phi3(x,yll12),x,xkl12,xkp12)));
0
string(factor(integrate(phi3pS(x,yll12)*phi3(x,yll12),x,xkl12,xkp12)));
0

///////Mphj dyphi

dyphi0(x,y):=diff(phi0(x,y),y);
dyphi1(x,y):=diff(phi1(x,y),y);
dyphi2(x,y):=diff(phi2(x,y),y);
dyphi3(x,y):=diff(phi3(x,y),y);


string(factor(integrate(integrate(phi0(x,y)*dyphi0(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(xkp12-xkl12)/6
string(factor(integrate(integrate(phi1(x,y)*dyphi0(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(xkp12-xkl12)/12
string(factor(integrate(integrate(phi2(x,y)*dyphi0(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(xkp12-xkl12)/12
string(factor(integrate(integrate(phi3(x,y)*dyphi0(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(xkp12-xkl12)/6

string(factor(integrate(integrate(phi0(x,y)*dyphi1(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(xkp12-xkl12)/12
string(factor(integrate(integrate(phi1(x,y)*dyphi1(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(xkp12-xkl12)/6
string(factor(integrate(integrate(phi2(x,y)*dyphi1(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(xkp12-xkl12)/6
string(factor(integrate(integrate(phi3(x,y)*dyphi1(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
-(xkp12-xkl12)/12

string(factor(integrate(integrate(phi0(x,y)*dyphi2(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(xkp12-xkl12)/12
string(factor(integrate(integrate(phi1(x,y)*dyphi2(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(xkp12-xkl12)/6
string(factor(integrate(integrate(phi2(x,y)*dyphi2(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(xkp12-xkl12)/6
string(factor(integrate(integrate(phi3(x,y)*dyphi2(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(xkp12-xkl12)/12


string(factor(integrate(integrate(phi0(x,y)*dyphi3(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(xkp12-xkl12)/6
string(factor(integrate(integrate(phi1(x,y)*dyphi3(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(xkp12-xkl12)/12
string(factor(integrate(integrate(phi2(x,y)*dyphi3(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(xkp12-xkl12)/12
string(factor(integrate(integrate(phi3(x,y)*dyphi3(x,y),x,xkl12,xkp12),y,yll12,ylp12)));
(xkp12-xkl12)/6




phi0(x,y,xkl12,xkp12,yll12,ylp12):=((x-xkp12)/(xkl12-xkp12))*((y-ylp12)/(yll12-ylp12));
phi1(x,y,xkl12,xkp12,yll12,ylp12):=((x-xkl12)/(xkp12-xkl12))*((y-ylp12)/(yll12-ylp12));
phi2(x,y,xkl12,xkp12,yll12,ylp12):=((x-xkl12)/(xkp12-xkl12))*((y-yll12)/(ylp12-yll12));
phi3(x,y,xkl12,xkp12,yll12,ylp12):=((x-xkp12)/(xkl12-xkp12))*((y-yll12)/(ylp12-yll12));


ukl0*phi0(xl12,y,xl32,xl12,yll12,ylp12)+ukl1*phi1(xl12,y,xl32,xl12,yll12,ylp12)+ukl2*phi2(xl12,y,xl32,xl12,yll12,ylp12)+ukl3*phi3(xl12,y,xl32,xl12,yll12,ylp12)

ukl0*phi0(xl12,y,xl12,xp12,yll12,ylp12)+ukl1*phi1(xl12,y,xl12,xp12,yll12,ylp12)+ukl2*phi2(xl12,y,xl12,xp12,yll12,ylp12)+ukl3*phi3(xl12,y,xl12,xp12,yll12,ylp12)
