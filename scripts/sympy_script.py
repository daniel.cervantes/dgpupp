import sympy as sy
x1,x2,x3,x4,x5,x6,x7,x8         =  sy.symbols("x1 x2 x3 x4 x5 x6 x7 x8")
a11,a12,a13,a14,a15,a16,a17,a18 =  sy.symbols("a11 a12 a13 a14 a15 a16 a17 a18")
a21,a22,a23,a24,a25,a26,a27,a28 =  sy.symbols("a21 a22 a23 a24 a25 a26 a27 a28")
a31,a32,a33,a34,a35,a36,a37,a38 =  sy.symbols("a31 a32 a33 a34 a35 a36 a37 a38")
a41,a42,a43,a44,a45,a46,a47,a48 =  sy.symbols("a41 a42 a43 a44 a45 a46 a47 a48")
a51,a52,a53,a54,a55,a56,a57,a58 =  sy.symbols("a51 a52 a53 a54 a55 a56 a57 a58")
a61,a62,a63,a64,a65,a66,a67,a68 =  sy.symbols("a61 a62 a63 a64 a65 a66 a67 a68")
a71,a72,a73,a74,a75,a76,a77,a78 =  sy.symbols("a71 a72 a73 a74 a75 a76 a77 a78")
a81,a82,a83,a84,a85,a86,a87,a88 =  sy.symbols("a81 a82 a83 a84 a85 a86 a87 a88")

y1,y2,y3,y4,y5,y6,y7,y8         =  sy.symbols("y1 y2 y3 y4 y5 y6 y7 y8")


eq1 = sy.Eq(a11*x1+a12*x2+a13*x3+a14*x4+a15*x5+a16*x6+a17*x7+a18*x8,y1)
eq2 = sy.Eq(a21*x2+a22*x2+a23*x3+a24*x4+a25*x5+a26*x6+a27*x7+a28*x8,y2)
eq3 = sy.Eq(a31*x3+a32*x2+a33*x3+a34*x4+a35*x5+a36*x6+a37*x7+a38*x8,y3)
eq4 = sy.Eq(a41*x4+a42*x2+a43*x3+a44*x4+a45*x5+a46*x6+a47*x7+a48*x8,y4)
eq5 = sy.Eq(a51*x5+a52*x2+a53*x3+a54*x4+a55*x5+a56*x6+a57*x7+a58*x8,y5)
eq6 = sy.Eq(a61*x6+a62*x2+a63*x3+a64*x4+a65*x5+a66*x6+a67*x7+a68*x8,y6)
eq7 = sy.Eq(a71*x7+a72*x2+a73*x3+a74*x4+a75*x5+a76*x6+a77*x7+a78*x8,y7)
eq8 = sy.Eq(a81*x8+a82*x2+a83*x3+a84*x4+a85*x5+a86*x6+a87*x7+a88*x8,y8)

result = sy.solve([eq1,eq2,eq3,eq4,eq5,eq6,eq7,eq8],(x1,x2,x3,x4,x5,x6,x7,x8))

print(result)
