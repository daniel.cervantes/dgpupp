#include<string>
#include <Eigen/Core>
#include <fstream>
#include <iostream>


namespace utils{

  using Eigen::MatrixXf;
  //  typedef Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> MatrixXf;
  using namespace std;

  
  
  class DataWriter{
    string _namefile;
    int n1,n2;
    
  public:
    DataWriter(string namefile):_namefile(namefile){};
    DataWriter(MatrixXf&, MatrixXf&, string);
    void write(MatrixXf&, MatrixXf&);
    void write(MatrixXf&, MatrixXf&,MatrixXf&);
    void write(MatrixXf&, MatrixXf&,MatrixXf&,MatrixXf&);
    void write(float,float,float,float,MatrixXf&);
    void write(float,float,float,float,int,int,MatrixXf&);
    void write(float,float,float,float,int,int, MatrixXf&,MatrixXf&);
    void write(float,float,float,float,int,int,MatrixXf&,string);
    void write(float,float,float,float,int,int,MatrixXf&,float,float,float,float,float(*)(float,float,float,float,float,float));
    void write(float a,float b,float c,float d,int K,int L, MatrixXf& uklN,float xc,float yc,float fT,float kMax,int nP,float (*)(float,float,float,float,float,float,int));
    void write(float a, float b, float c, float d, int K, int L, MatrixXf& uklN, float mux, float muy, MatrixXf& sig, float detSig, float (*)(float,float,float,float,MatrixXf &,float));
  };  
}
