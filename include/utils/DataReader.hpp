#include <string>
#include <Eigen/Core>
#include <fstream>
#include <iostream>
#include <vector>
#include <num/Matrix.hpp>

namespace utils {
  
  using   Eigen::MatrixXf;
  typedef Eigen::Matrix<float,1,3> T;
  typedef Eigen::Matrix<float,1,4> F;

  //typedef Eigen::Matrix<int,1,2> Di;
  typedef Eigen::Matrix<int,Eigen::Dynamic,Eigen::Dynamic> MatrixXi;
  
  using namespace std;
  
  class DataReader{    
    string _namefile;
    string _namefile2;
    float* _substrates;
    float* _pulses;
    int _nS,_nP;
    
  public:
    DataReader(){}
    DataReader(string namefile):_namefile(namefile){}
    DataReader(string namefile, string namefile2):_namefile(namefile),_namefile2(namefile2){};
    void read(MatrixXf&);
    void read(vector<T>& pulses, vector<T>& substrates);
    void read();

    void readP(vector<T>&,string);
    void readS(vector<T>&,string, float alpha = 10e-4);

    void readPulses(vector<F>&,string);
    void readVoxels(vector<F>&,string);

    
       
    float* substrates(){return _substrates;}
    float* pulses(){return _pulses;}
    int* readDeltas(string nfile, int* deltasI, int &nP);        
    void readDeltas(string nfile, vector<T>& pulses,int &nP);
    void readDeltas(string nfile, vector<pair<int,int> >& pulses, int &nP);
    void readDeltas(string nfile, MatrixXi& pulses);
    
    void readDeltas(string nfile, MatrixXf & dM);
    void readDCMatrix(string nfile, MatrixXf & k);
    int nS(){return _nS;}
    int nP(){return _nP;}
    
  };
}
