#ifndef DMATRIX_H
#define DMATRIX_H

using namespace std;

template <typename T, int ROWS, int COLS, int DIM>
class DMatrix 
{
protected:
  int _rows, _cols, _dim;
  T * _data = 0;
  
public:
  
  DMatrix();            //Default constructor
  DMatrix(int, int);    // Contructor
  DMatrix(int, int, int);    // Contructor
  ~DMatrix(void);       // Destructor
  __device__ __host__ int rows(){return _rows;}
  __device__ __host__ int cols(){return _cols;}
  __device__ __host__ int dim(){return _dim;}
  __device__ inline T& operator ()(int i, int j) {return _data[i*_cols + j];}  
  __device__ inline const T& operator ()(int i, int j) const {return _data[i*_cols + j];}  
  __device__ inline T& operator ()(int i, int j, int d) {return _data[i*_cols*_dim+j*_dim+d];}  
  __device__ inline const T& operator ()(int i, int j, int d) const {return _data[i*_cols*_dim+j*_dim+d];}    
  __device__ inline T& operator ()(int i) {return _data[i];}  
  __device__ void operator *(double s);
  
  __device__ __host__ T* data(){return _data;}  
  __device__  T* solve(const DMatrix<T,ROWS,1,1>& b);
  __device__  T* solve(T* b);
  __device__  T* multByVect(T* b);
  __device__  T* multByVect8(T b0, T b1, T b2, T b3, T b4, T b5, T b6, T b7);
  __device__  void multByVect8x8(T b0, T b1, T b2, T b3, T b4, T b5, T b6, T b7,
                                 T* x0, T* x1, T* x2, T* x3, T* x4, T* x5, T* x6, T* x7);
  __device__  void solve8x8(T b0, T b1, T b2, T b3, T b4, T b5, T b6, T b7,
                          T* x0, T* x1, T* x2, T* x3, T* x4, T* x5, T* x6, T* x7);

  void resize(int,int,int);
  void resize(int,int);
  void resize(int); 

};


template <typename T, int ROWS, int COLS, int DIM>
__device__  void DMatrix<T,ROWS,COLS,DIM>::multByVect8x8(T b0, T b1, T b2, T b3, T b4, T b5, T b6, T b7,
                               T* x0, T* x1, T* x2, T* x3, T* x4, T* x5, T* x6, T* x7){


  *x0 = (*this)(0,0,0)*b0 + (*this)(0,1,0)*b1 + (*this)(0,2,0)*b2 + (*this)(0,3,0)*b3
      + (*this)(0,4,0)*b4 + (*this)(0,5,0)*b5 + (*this)(0,6,0)*b6 + (*this)(0,7,0)*b7;  
  
  *x1 = (*this)(1,0,0)*b0 + (*this)(1,1,0)*b1 + (*this)(1,2,0)*b2 + (*this)(1,3,0)*b3
       + (*this)(1,4,0)*b4 + (*this)(1,5,0)*b5 + (*this)(1,6,0)*b6 + (*this)(1,7,0)*b7;  

  *x2 = (*this)(2,0,0)*b0 + (*this)(2,1,0)*b1 + (*this)(2,2,0)*b2 + (*this)(2,3,0)*b6
       + (*this)(2,4,0)*b4 + (*this)(2,5,0)*b5 + (*this)(2,6,0)*b6 + (*this)(2,7,0)*b7;  

  *x3 = (*this)(3,0,0)*b0 + (*this)(3,1,0)*b1 + (*this)(3,2,0)*b2 + (*this)(3,3,0)*b3
       + (*this)(3,4,0)*b4 + (*this)(3,5,0)*b5 + (*this)(3,6,0)*b6 + (*this)(3,7,0)*b7;  

  *x4 = (*this)(4,0,0)*b0 + (*this)(4,1,0)*b1 + (*this)(4,2,0)*b2 + (*this)(4,3,0)*b3
       + (*this)(4,4,0)*b4 + (*this)(4,5,0)*b5 + (*this)(4,6,0)*b6 + (*this)(4,7,0)*b7;  

  *x5 = (*this)(5,0,0)*b0 + (*this)(5,1,0)*b1 + (*this)(5,2,0)*b2 + (*this)(5,3,0)*b3
       + (*this)(5,4,0)*b4 + (*this)(5,5,0)*b5 + (*this)(5,6,0)*b6 + (*this)(5,7,0)*b7;  

  *x6 = (*this)(6,0,0)*b0 + (*this)(6,1,0)*b1 + (*this)(6,2,0)*b2 + (*this)(6,3,0)*b3
       + (*this)(6,4,0)*b4 + (*this)(6,5,0)*b5 + (*this)(6,6,0)*b6 + (*this)(6,7,0)*b7;  
  
  *x7 = (*this)(7,0,0)*b0 + (*this)(7,1,0)*b1 + (*this)(7,2,0)*b2 + (*this)(7,3,0)*b3
       + (*this)(7,4,0)*b4 + (*this)(7,5,0)*b5 + (*this)(7,6,0)*b6 + (*this)(7,7,0)*b7;  


}

template <typename T, int ROWS, int COLS, int DIM>
void DMatrix<T,ROWS,COLS,DIM>::solve8x8(T b0, T b1, T b2, T b3, T b4, T b5, T b6, T b7,
                                    T* x0, T*x1, T* x2, T*x3, T* x4, T*x5, T* x6, T* x7){

  T * m = new T[72];
  int C = 8;
  T c;

  
  m[0] = (*this)(0,0,0); m[1] = (*this)(0,1,0); m[2] = (*this)(0,2,0); m[3] = (*this)(0,3,0);
  m[4] = (*this)(0,4,0); m[5] = (*this)(0,5,0); m[6] = (*this)(0,6,0); m[7] = (*this)(0,7,0); m[8] = b0;

  m[9]  = (*this)(1,0,0); m[10] = (*this)(1,1,0); m[11] = (*this)(1,2,0); m[12] = (*this)(1,3,0);
  m[13] = (*this)(1,4,0); m[14] = (*this)(1,5,0); m[15] = (*this)(1,6,0); m[16] = (*this)(1,7,0); m[17] = b1;

  m[18]  = (*this)(2,0,0); m[19] = (*this)(2,1,0); m[20] = (*this)(2,2,0); m[21] = (*this)(2,3,0);
  m[22] = (*this)(2,4,0); m[23] = (*this)(2,5,0); m[24] = (*this)(2,6,0); m[25] = (*this)(2,7,0); m[26] = b2;

  m[27]  = (*this)(3,0,0); m[28] = (*this)(3,1,0); m[29] = (*this)(3,2,0); m[30] = (*this)(3,3,0);
  m[31] = (*this)(3,4,0); m[32] = (*this)(3,5,0); m[33] = (*this)(3,6,0); m[34] = (*this)(3,7,0); m[35] = b3;
  
  m[36] = (*this)(4,0,0); m[37] = (*this)(4,1,0); m[38] = (*this)(4,2,0); m[39] = (*this)(4,3,0);
  m[40] = (*this)(4,4,0); m[41] = (*this)(4,5,0); m[42] = (*this)(4,6,0); m[43] = (*this)(4,7,0); m[44] = b4;

  m[45]  = (*this)(5,0,0); m[46] = (*this)(5,1,0); m[47] = (*this)(5,2,0); m[48] = (*this)(5,3,0);
  m[49] = (*this)(5,4,0); m[50] = (*this)(5,5,0); m[51] = (*this)(5,6,0); m[52] = (*this)(5,7,0); m[53] = b5;

  m[54]  = (*this)(6,0,0); m[55] = (*this)(6,1,0); m[56] = (*this)(6,2,0); m[57] = (*this)(6,3,0);
  m[58] = (*this)(6,4,0); m[59] = (*this)(6,5,0); m[60] = (*this)(6,6,0); m[61] = (*this)(6,7,0); m[62] = b6;

  m[63]  = (*this)(7,0,0); m[64] = (*this)(7,1,0); m[65] = (*this)(7,2,0); m[66] = (*this)(7,3,0);
  m[67] = (*this)(7,4,0); m[68] = (*this)(7,5,0); m[69] = (*this)(7,6,0); m[70] = (*this)(7,7,0); m[71] = b7;

      
  for(int j=0; j < 8; ++j){
    for(int i=j; i < 8; ++i){
      if(i!=j){
        c = m[i*(C+1)+j]/m[j*(C+1)+j];
        for(int k=0; k < C+1; ++k)
          m[i*(C+1)+k] = m[i*(C+1)+k] - c*m[j*(C+1)+k];
      }
    }
  }

  *x7 = (m[71]/m[70]);
  *x6 = (m[62]-m[61]*(*x7))/m[60];
  *x5 = (m[53]-m[51]*(*x6)-m[52]*(*x7))/m[50];
  *x4 = (m[44]-m[41]*(*x5)-m[42]*(*x6)-m[43]*(*x7))/m[40];
  *x3 = (m[35]-m[31]*(*x4)-m[32]*(*x5)-m[33]*(*x6)-m[34]*(*x7))/m[30];
  *x2 = (m[26]-m[21]*(*x3)-m[22]*(*x4)-m[23]*(*x5)-m[24]*(*x6)-m[25]*(*x7))/m[20];
  *x1 = (m[17]-m[11]*(*x2)-m[12]*(*x3)-m[13]*(*x4)-m[14]*(*x5)-m[15]*(*x6)-m[16]*(*x7))/m[10];
  *x0 = (m[8]-m[1]*(*x1)-m[2]*(*x2)-m[3]*(*x3)-m[4]*(*x4)-m[5]*(*x5)-m[6]*(*x6)-m[7]*(*x7))/m[0];
    
  delete [] m;


}




template <typename T, int ROWS, int COLS, int DIM>
T* DMatrix<T,ROWS,COLS,DIM>::multByVect8(T b0, T b1, T b2, T b3, T b4, T b5, T b6, T b7){

  T* x = new T[8];
  
  x[0] = (*this)(0,0,0)*b0 + (*this)(0,1,0)*b1 + (*this)(0,2,0)*b2 + (*this)(0,3,0)*b3
       + (*this)(0,4,0)*b4 + (*this)(0,5,0)*b5 + (*this)(0,6,0)*b6 + (*this)(0,7,0)*b7;  

  x[1] = (*this)(1,0,0)*b0 + (*this)(1,1,0)*b1 + (*this)(1,2,0)*b2 + (*this)(1,3,0)*b3
       + (*this)(1,4,0)*b4 + (*this)(1,5,0)*b5 + (*this)(1,6,0)*b6 + (*this)(1,7,0)*b7;  

  x[2] = (*this)(2,0,0)*b0 + (*this)(2,1,0)*b1 + (*this)(2,2,0)*b2 + (*this)(2,3,0)*b6
       + (*this)(2,4,0)*b4 + (*this)(2,5,0)*b5 + (*this)(2,6,0)*b6 + (*this)(2,7,0)*b7;  

  x[3] = (*this)(3,0,0)*b0 + (*this)(3,1,0)*b1 + (*this)(3,2,0)*b2 + (*this)(3,3,0)*b3
       + (*this)(3,4,0)*b4 + (*this)(3,5,0)*b5 + (*this)(3,6,0)*b6 + (*this)(3,7,0)*b7;  

  x[4] = (*this)(4,0,0)*b0 + (*this)(4,1,0)*b1 + (*this)(4,2,0)*b2 + (*this)(4,3,0)*b3
       + (*this)(4,4,0)*b4 + (*this)(4,5,0)*b5 + (*this)(4,6,0)*b6 + (*this)(4,7,0)*b7;  

  x[5] = (*this)(5,0,0)*b0 + (*this)(5,1,0)*b1 + (*this)(5,2,0)*b2 + (*this)(5,3,0)*b3
       + (*this)(5,4,0)*b4 + (*this)(5,5,0)*b5 + (*this)(5,6,0)*b6 + (*this)(5,7,0)*b7;  

  x[6] = (*this)(6,0,0)*b0 + (*this)(6,1,0)*b1 + (*this)(6,2,0)*b2 + (*this)(6,3,0)*b3
       + (*this)(6,4,0)*b4 + (*this)(6,5,0)*b5 + (*this)(6,6,0)*b6 + (*this)(6,7,0)*b7;  

  x[7] = (*this)(7,0,0)*b0 + (*this)(7,1,0)*b1 + (*this)(7,2,0)*b2 + (*this)(7,3,0)*b3
       + (*this)(7,4,0)*b4 + (*this)(7,5,0)*b5 + (*this)(7,6,0)*b6 + (*this)(7,7,0)*b7;  

  
  return x;
}






template <typename T, int ROWS, int COLS, int DIM>
T* DMatrix<T,ROWS,COLS,DIM>::multByVect(T* b){
  
  int R = 8;
  int C = 8;
  T tsum;
  T* x = new T[R];
  
  for(int i=0; i < R; ++i){      
    tsum = 0;
    for(int j=0; j < C; ++j)        
      tsum = tsum + (*this)(i,j,0)*b[j];   
    x[i] = tsum;
  }

  return x;
}

template <typename T, int ROWS, int COLS, int DIM>
T* DMatrix<T,ROWS,COLS,DIM>::solve(T* b){

  int R = 8;
  int C = 8;

  T * x = new T[C];
  T * m = new T[R*(C+1)];
  T c;
  
  int i,j,k;

  for(i=0; i < R; ++i){
    for(j=0; j < C; ++j)
      m[i*(C+1)+j] = (*this)(i,j,0);
    m[i*(C+1)+j] = b[i];    
  }

  for(j=0; j < R; ++j){
    for(i=j; i < C; ++i){
      if(i!=j){
        c = m[i*(C+1)+j]/m[j*(C+1)+j];
        for(k=0; k < C+1; ++k)
          m[i*(C+1)+k] = m[i*(C+1)+k] - c*m[j*(C+1)+k];
      }
    }
  }


  for(j=1; j < C+1; ++j){
    c=0.0;
    for(i=R-1; i > R-j; i--)      
      c = c + m[(R-j)*(C+1)+i]*x[i];
    x[C-j] = (m[(C-j)*(C+1)+C] - c)/(m[(C-j)*(C+1)+C-j]);
  }

  delete [] m;

  return x;     
}



template <typename T, int ROWS, int COLS, int DIM>
T* DMatrix<T,ROWS,COLS,DIM>::solve(const DMatrix<T,ROWS,1,1>& b){

  int R = 8;
  int C = 8;

  T * x = new T[C];
  T * m = new T[R*(C+1)];
  T c;
  
  int i,j,k;

  for(i=0; i < R; ++i){
    for(j=0; j < C; ++j)
      m[i*(C+1)+j] = (*this)(i,j,0);
    m[i*(C+1)+j] = b(i,0);    
  }

  for(j=0; j < R; ++j){
    for(i=j; i < C; ++i){
      if(i!=j){
        c = m[i*(C+1)+j]/m[j*(C+1)+j];
        for(k=0; k < C+1; ++k)
          m[i*(C+1)+k] = m[i*(C+1)+k] - c*m[j*(C+1)+k];
      }
    }
  }


  for(j=1; j < C+1; ++j){
    c=0.0;
    for(i=R-1; i > R-j; i--)      
      c = c + m[(R-j)*(C+1)+i]*x[i];
    x[C-j] = (m[(C-j)*(C+1)+C] - c)/(m[(C-j)*(C+1)+C-j]);
  }

  delete [] m;

  return x;     
}






// template <typename T, int ROWS, int COLS, int DIM>
// DMatrix<T,1,1,ROWS>& DMatrix<T,ROWS,COLS,DIM>::operator/(const DMatrix<T,ROWS,1,1>& b){

//   DMatrix<T,1,1,ROWS>* x = new DMatrix<T,1,1,ROWS>();      
//   DMatrix<T,ROWS,COLS+1,1>* am = new DMatrix<T,ROWS,COLS+1,1>();  
//   T c;

//   int i,j;
//   for(i=0; i < ROWS;i++){
//     for(j=0; j < COLS; j++){
//       (*am)(i,j,0) = (*this)(i,j,0);           
//     }
//     (*am)(i,j,0) = b(i,0);
//   }
  
  
//   for(int j=0; j < ROWS; ++j)
//     for(int i=0; i < ROWS; ++i)
//       if(i!=j){
//         c = (*am)(i,j,0)/(*am)(j,j,0);
//         for(int k=0; k < COLS+1; ++k)
//           (*am)(i,k,0) = (*am)(i,k,0) - c*(*am)(j,k,0);                           
//       }
  
//   for(int i=0; i < ROWS; ++i)
//     (*x)(i) = (*am)(i,COLS)/(*am)(i,i,0); 
  
//   return *x;     
  
// }

template <typename T, int ROWS, int COLS, int DIM>
DMatrix<T,ROWS,COLS,DIM>::DMatrix():_cols(COLS),_rows(ROWS),_dim(DIM){

  // printf("ROWS = %d COLS=%d  DIM=%d\n",ROWS,COLS,DIM);
  // printf("rows = %d col=%d  dim=%d\n",_rows,_cols,_dim);
  
  cudaMalloc((void**)&_data,_rows*_cols*_dim*sizeof(T));
}

template <typename T, int ROWS, int COLS, int DIM>
DMatrix<T,ROWS,COLS,DIM>::DMatrix(int n, int m, int dim){
  resize(n,m,dim);
}

template <typename T, int ROWS, int COLS, int DIM>
DMatrix<T,ROWS,COLS,DIM>::DMatrix(int n, int m){resize(n,m);}


template <typename T, int ROWS, int COLS, int DIM>
DMatrix<T,ROWS,COLS,DIM>::~DMatrix(){
  _rows = _cols = 0;
  if(!_data)
    cudaFree(_data);
  _data = 0;
}

template <typename T, int ROWS, int COLS, int DIM>
void DMatrix<T,ROWS,COLS,DIM>::resize(int n, int m, int dim){
  
  if( (_rows != 0) && (_cols != 0) ){  
    if(_data != 0)
      cudaFree(_data);
  }
  
  _rows = n;
  _cols = m;
  _dim = dim;
  
  cudaMalloc((void**)&_data,_rows*_cols*_dim*sizeof(T));    
}

template <typename T, int ROWS, int COLS, int DIM>
void DMatrix<T,ROWS,COLS,DIM>::resize(int n, int m){
  
  if( (_rows != 0) && (_cols != 0) ){  
    if(_data != 0)
      cudaFree(_data);
  }
  
  _rows = n;
  _cols = m;
  _dim = DIM;
  
  cudaMalloc((void**)&_data,_rows*_cols*_dim*sizeof(T));    
}



template <typename T, int ROWS, int COLS, int DIM>
void DMatrix<T,ROWS,COLS,DIM>::resize(int m){
  
  if( (_rows != 0) && (_cols != 0) ){  
    if(_data != 0)
      cudaFree(_data);
  }
  
  _rows = ROWS;
  _cols = m;
  _dim = DIM;
  
  cudaMalloc((void**)&_data,_rows*_cols*_dim*sizeof(T));    
}


//#include "DMatrix.cu"

#endif

