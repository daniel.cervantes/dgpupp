#ifndef DVECTOR_H
#define DVECTOR_H

#include <myCudaLA/DMatrix.hpp>

using namespace std;

template <typename T>
class DVector : public DMatrix<T>
{
public:
  DVector():DMatrix<T>(){};            
  DVector(int n):DMatrix<T>(1,n){};
  __device__ __host__ int length(){return DMatrix<T>::_m;}
  __device__ T & operator ()(int i) {return DMatrix<T>::elems[i];}  
  void resize(int n){ DMatrix<T>::resize(1,n); }
  
};

#endif

