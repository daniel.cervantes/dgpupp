#ifndef DMATRIX_H
#define DMATRIX_H

using namespace std;

template <typename T>
class DMatrix 
{
protected:
  int _n,_m;
  T * elems;
  
public:
  
  DMatrix();            //Default constructor
  DMatrix(int, int);    // Contructor
  ~DMatrix(void);       // Destructor
  __device__ __host__ int rows(){return _n;}
  __device__ __host__ int cols(){return _m;}
  __device__ inline T& operator ()(int i, int j) {return elems[i + j*_n];}
  __device__ inline T& operator ()(int i){return elems[i];}  
  __device__ __host__ T* data(){return elems;}
  void resize(int,int); 
  
};


template <typename T>
DMatrix<T>::DMatrix(){
  _n = _m =0;
  elems = 0;
}

template <typename T>
DMatrix<T>::DMatrix(int n, int m)
{
  _n = n;
  _m = m;
  
  cudaMalloc(&elems, _n*_m*sizeof(T));  
}

template <typename T>
DMatrix<T>::~DMatrix(){
  _n = _m = 0;
  if(!elems)
    cudaFree(elems);
}

template <typename T>
void DMatrix<T>::resize(int n, int m){
  
  if( (_n != 0) && (_m != 0) ){  
    if(elems != 0)
      cudaFree(elems);
  }
  
  _n = n;
  _m = m;
  cudaMalloc((void**)&elems,_n*_m*sizeof(T));  
  
}

//#include "DMatrix.cu"

#endif

