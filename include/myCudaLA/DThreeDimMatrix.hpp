#ifndef DTHREEDIMMATRIX_H
#define DTHREEDIMMATRIX_H

#include <myCudaLA/DMatrix.hpp>

using namespace std;


//const int Dynamic = 1;

template <typename T,int N, int M, int L, int DIM>
class DThreeDimMatrix 
{
protected:
  int _n,_m,_l;
  int _dim;
  T * _data;
  
public:
  
  DThreeDimMatrix();            //Default constructor
  DThreeDimMatrix(int,int,int);        // Contructor
  DThreeDimMatrix(int,int,int,int);    // Contructor
  
  ~DThreeDimMatrix(void);       // Destructor
  __device__ __host__ int n(){return _n;}
  __device__ __host__ int m(){return _m;}
  __device__ __host__ int l(){return _l;}
  __device__ __host__ int dim(){return _dim;}

  __device__ inline  T& operator ()(int i, int j, int k, int d)  {return _data[i*_m*_dim+k*_n*_m*_dim+j*_dim+d];}
  //__device__ inline const T& operator ()(int i, int j, int k, int d) const {return _data[i*_m*_dim+k*_n*_m*_dim+j*_dim+d];}  
  __device__ DMatrix<T,N,M,DIM>& operator ()(int i, int j, int k);
  __device__ __host__ T* data(){return _data;}
  
  void resize(int,int,int);
  void resize(int,int,int,int); 
  
};

template <typename T,int N, int M, int L, int DIM>
DMatrix<T,N,M,DIM>& DThreeDimMatrix<T,N,M,L,DIM>::operator ()(int i, int j, int k)  {
  DMatrix<T,N,M,DIM> * cr = new DMatrix<T,N,M,DIM>(DIM,1);
  for(int d = 0; d < DIM; ++d)      
    (*cr)(d,1) = (*this)(i,j,k,d);
  return *cr;
}

template <typename T,int N, int M, int L, int DIM>
DThreeDimMatrix<T,N,M,L,DIM>::DThreeDimMatrix():_n(N),_m(M),_l(L),_dim(DIM){
  cudaMalloc((void **)&_data,_n*_m*_l*_dim*sizeof(double)); 
}

template <typename T,int N, int M, int L, int DIM>
DThreeDimMatrix<T,N,M,L,DIM>::DThreeDimMatrix(int n, int m, int l, int dim){resize(n,m,l,dim);}

template <typename T,int N, int M, int L, int DIM>
DThreeDimMatrix<T,N,M,L,DIM>::DThreeDimMatrix(int n, int m, int l){resize(n,m,l);}


template <typename T,int N, int M, int L, int DIM>
void DThreeDimMatrix<T,N,M,L,DIM>::resize(int n, int m, int l, int dim){
  if(!_data){
    cudaFree(_data);
    _data = 0;
  }
  _n = n;
  _m = m;
  _l = l;
  cudaMalloc(&_data,_n*_m*_l*_dim*sizeof(T)); 
}

template <typename T,int N, int M, int L, int DIM>
void DThreeDimMatrix<T,N,M,L,DIM>::resize(int n, int m, int l){
  if(!_data){
    cudaFree(_data);
    _data = 0;
  }
  _n = n;
  _m = m;
  _l = l;
  cudaMalloc(&_data,_n*_m*_l*DIM*sizeof(T)); 
}


template <typename T,int N, int M, int L, int DIM>
DThreeDimMatrix<T,N,M,L,DIM>::~DThreeDimMatrix(){
  _n = _m = _l = _dim = 0;

  if(!_data)
    cudaFree(_data);
}



//#include "DThreeDimMatrix.cu"

#endif

