#ifndef _MATRIXADD_H_
#define _MATRIXADD_H_

//using namespace std;

namespace num {

  template<class L, class R>
  class MatrixAdd{
  private:
    const L& _l;
    const R& _r;
  public:
    using value_type = typename L::value_type;    
    MatrixAdd(const L& l, const R& r) : _r(r),_l(l) {}
    value_type operator()(size_t i, size_t j, size_t d) const {return _l(i,j,d)+_r(i,j,d);}
    value_type operator()(size_t i, size_t j) const {return _l(i,j)+_r(i,j);}
  };

}    
#endif
