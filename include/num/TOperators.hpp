#ifndef _TOPERATORS_H_
#define _TOPERATORS_H_

#include <num/Matrix.hpp>
#include <num/MatrixAdd.hpp>
#include <num/MatrixSubtraction.hpp>
#include <num/MatrixScalarMult.hpp>
#include <num/MatrixMultMult.hpp>
#include <num/ThreeDimMatrix.hpp>
#include <num/ThreeDimMatrixAdd.hpp>
#include <num/ThreeDimMatrixScalarMult.hpp>


using namespace std;

namespace num{

  //Add
  template<typename T, size_t ROW, size_t COL, size_t DIM>
  inline MatrixAdd<Matrix<T,ROW,COL,DIM>,Matrix<T,ROW,COL,DIM> >
  operator+(const Matrix<T,ROW,COL,DIM> &l, const Matrix<T,ROW,COL,DIM> &r){
    return MatrixAdd<Matrix<T,ROW,COL,DIM>,Matrix<T,ROW,COL,DIM> >(l,r);
  }
  
  template<class L, class R,typename T, size_t ROW, size_t COL, size_t DIM>
  inline MatrixAdd<MatrixAdd<L,R>,Matrix<T,ROW,COL,DIM> >
  operator+(const MatrixAdd<L,R>& msl, const Matrix<T,ROW,COL,DIM>& mr){
    return MatrixAdd<MatrixAdd<L,R>,Matrix<T,ROW,COL,DIM> >(msl,mr);
  }
  
  template< class LL, class LR, class RL, class RR>
  inline MatrixAdd<MatrixMultMult<LL,LR>,MatrixMultMult<RL,RR> >  
  operator+(const MatrixMultMult<LL,LR> &ml, const MatrixMultMult<RL,RR> &mr){
    return MatrixAdd <MatrixMultMult<LL,LR>, MatrixMultMult<RL,RR> >(ml,mr);
  }

  template< class LL, class RL, class RR>
  inline MatrixAdd<LL,MatrixMultMult<RL,RR> >  
  operator+(const LL &ml, const MatrixMultMult<RL,RR> &mr){
    return MatrixAdd <LL, MatrixMultMult<RL,RR> >(ml,mr);
  }

  template<typename S, class R, size_t ROW, size_t COL, size_t DIM>
  inline MatrixAdd<MatrixScalarMult<S,R>,Matrix<S,ROW,COL,DIM> >
  operator+(const MatrixScalarMult<S,R>& lsmm, const Matrix<S,ROW,COL,DIM>& rm){
    return MatrixAdd<MatrixScalarMult<S,R>,Matrix<S,ROW,COL,DIM> >(lsmm,rm);
  }

  template<typename S, class A, class B, class C>
  inline MatrixAdd<MatrixAdd<A,B>,MatrixScalarMult<S,C> >
  operator+(const MatrixAdd<A,B>& lam, const MatrixScalarMult<S,C>& rsmm){
    return MatrixAdd<MatrixAdd<A,B>,MatrixScalarMult<S,C> >(lam,rsmm);
  }

  template<typename S, class L, class R>
  inline MatrixAdd<MatrixScalarMult<S,L>,MatrixScalarMult<S,R> >
  operator+(const MatrixScalarMult<S,L>& lsmm, const MatrixScalarMult<S,R>& rsmm){
    return MatrixAdd<MatrixScalarMult<S,L>,MatrixScalarMult<S,R> >(lsmm,rsmm);
  }

  template<typename T, size_t ROW, size_t COL, size_t DIM>
  inline MatrixScalarMult<T,Matrix<T,ROW,COL,DIM> > operator-(const Matrix<T,ROW,COL,DIM> & mr){
    return MatrixScalarMult<T,Matrix<T,ROW,COL,DIM> >(-1,mr);
  }

  ////
  //Subtraction
  template<typename T, size_t ROW, size_t COL, size_t DIM>
  inline MatrixSubtraction<Matrix<T,ROW,COL,DIM>,Matrix<T,ROW,COL,DIM> >
  operator-(const Matrix<T,ROW,COL,DIM> &l, const Matrix<T,ROW,COL,DIM> &r){
    return MatrixSubtraction<Matrix<T,ROW,COL,DIM>,Matrix<T,ROW,COL,DIM> >(l,r);
  }
  
  template<class L, class R,typename T, size_t ROW, size_t COL, size_t DIM>
  inline MatrixSubtraction<MatrixSubtraction<L,R>,Matrix<T,ROW,COL,DIM> >
  operator-(const MatrixSubtraction<L,R>& msl, const Matrix<T,ROW,COL,DIM>& mr){
    return MatrixSubtraction<MatrixSubtraction<L,R>,Matrix<T,ROW,COL,DIM> >(msl,mr);
  }
  
  template< class LL, class LR, class RL, class RR>
  inline MatrixSubtraction<MatrixMultMult<LL,LR>,MatrixMultMult<RL,RR> >  
  operator-(const MatrixMultMult<LL,LR> &ml, const MatrixMultMult<RL,RR> &mr){
    return MatrixSubtraction <MatrixMultMult<LL,LR>, MatrixMultMult<RL,RR> >(ml,mr);
  }

  template< class LL, class RL, class RR>
  inline MatrixSubtraction<LL,MatrixMultMult<RL,RR> >  
  operator-(const LL &ml, const MatrixMultMult<RL,RR> &mr){
    return MatrixSubtraction <LL, MatrixMultMult<RL,RR> >(ml,mr);
  }

  template<typename S, class R, size_t ROW, size_t COL, size_t DIM>
  inline MatrixSubtraction<MatrixScalarMult<S,R>,Matrix<S,ROW,COL,DIM> >
  operator-(const MatrixScalarMult<S,R>& lsmm, const Matrix<S,ROW,COL,DIM>& rm){
    return MatrixSubtraction<MatrixScalarMult<S,R>,Matrix<S,ROW,COL,DIM> >(lsmm,rm);
  }

  template<typename S, class A, class B, class C>
  inline MatrixSubtraction<MatrixSubtraction<A,B>,MatrixScalarMult<S,C> >
  operator-(const MatrixSubtraction<A,B>& lam, const MatrixScalarMult<S,C>& rsmm){
    return MatrixSubtraction<MatrixSubtraction<A,B>,MatrixScalarMult<S,C> >(lam,rsmm);
  }

  template<typename S, class L, class R>
  inline MatrixSubtraction<MatrixScalarMult<S,L>,MatrixScalarMult<S,R> >
  operator-(const MatrixScalarMult<S,L>& lsmm, const MatrixScalarMult<S,R>& rsmm){
    return MatrixSubtraction<MatrixScalarMult<S,L>,MatrixScalarMult<S,R> >(lsmm,rsmm);
  }



  
  // template< class LL, class LR, class RL, class RR>
  // inline MatrixAdd<MatrixMultMult<LL,LR>,MatrixMultMult<RL,RR> >  
  // operator-(const MatrixMultMult<LL,LR> &ml, const MatrixMultMult<RL,RR> &mr){
  //   return MatrixAdd <MatrixMultMult<LL,LR>, MatrixMultMult<RL,RR> >(ml,-mr);
  // }

  // template<typename T, size_t ROW, size_t COL, size_t DIM>
  // inline MatrixAdd<MatrixMultMult<Matrix<T,ROW,COL,DIM>,Matrix<T,ROW,1,1> >,MatrixMultMult<Matrix<T,ROW,COL,DIM>,Matrix<T,ROW,1,1> > >  
  // operator-(const MatrixMultMult<Matrix<T,ROW,COL,DIM>,Matrix<T,ROW,1,1> > &ml, const MatrixMultMult<Matrix<T,ROW,COL,DIM>,Matrix<T,ROW,1,1> >  &mr){
  //   return MatrixAdd < MatrixMultMult<Matrix<T,ROW,COL,DIM>,Matrix<T,ROW,1,1> >,MatrixMultMult<Matrix<T,ROW,COL,DIM>,Matrix<T,ROW,1,1> > >(ml,-mr);
  // }

  // template<typename T, size_t ROW, size_t COL, size_t DIM>
  // inline MatrixScalarMult< T, MatrixMultMult<Matrix<T,ROW,COL,DIM>,Matrix<T,ROW,1,1> > >
  // operator-(const MatrixMultMult< Matrix<T,ROW,COL,DIM>, Matrix<T,ROW,1,1> > & mr){
  //   return MatrixScalarMult<T,MatrixMultMult<Matrix<T,ROW,COL,DIM>,Matrix<T,ROW,1,1> > >(1,-mr);
  // }

  // template<typename T, class R>
  // inline MatrixScalarMult<T,R> operator-(const R & mr){
  //   return MatrixScalarMult<T,R>(-1,mr);
  // }

  // template<typename T, class L, class R>
  // inline MatrixScalarMult<T,MatrixMultMult<L,R> > operator-(const MatrixMultMult<L,R> & mr){
  //   return MatrixScalarMult<T,MatrixMultMult<L,R> >(-1,mr);
  // }

  // template<typename S, class RM>
  // inline MatrixScalarMult<S,RM> operator*(const S& s, const RM& mr){
  //   return MatrixScalarMult<S,RM> (s,mr);
  // }

  
  template<typename T, size_t ROW, size_t COL, size_t DIM>
  inline MatrixScalarMult<T,Matrix<T,ROW,COL,DIM> > operator*(const T& s, const Matrix<T,ROW,COL,DIM> & mr){
    return MatrixScalarMult<T,Matrix<T,ROW,COL,DIM> >(s,mr);
  }

  template<typename T, size_t ROW1, size_t COL1, size_t ROW2, size_t COL2, size_t DIM>
  inline MatrixMultMult<Matrix<T,ROW1,COL1,DIM>,Matrix<T,ROW2,COL2,DIM> > operator*(const Matrix<T,ROW1,COL1,DIM> & ml, const Matrix<T,ROW2,COL2,DIM> & mr){
    return MatrixMultMult<Matrix<T,ROW1,COL1,DIM>,Matrix<T,ROW2,COL2,DIM> >(ml,mr);
  }
  
  template<class L, typename T, size_t ROWS, size_t COLS, size_t DIM>
  inline MatrixMultMult<L,Matrix<T,ROWS,COLS,DIM> > operator*(const L & ml, const Matrix<T,ROWS,COLS,DIM> & mr){
    return MatrixMultMult<L,Matrix<T,ROWS,COLS,DIM> >(ml,mr);
  }

  




  //////////////////////////////////
  
  template<typename T, size_t N, size_t M, size_t L, size_t DIM>
  inline ThreeDimMatrixAdd<ThreeDimMatrix<T,N,M,L,DIM>,ThreeDimMatrix<T,N,M,L,DIM> >
  operator+(const ThreeDimMatrix<T,N,M,L,DIM> &l, const ThreeDimMatrix<T,N,M,L,DIM> &r){
    return ThreeDimMatrixAdd<ThreeDimMatrix<T,N,M,L,DIM>,ThreeDimMatrix<T,N,M,L,DIM> >(l,r);
  }
  
  template<typename T, size_t N, size_t M, size_t L, size_t DIM, class Left, class Right>
  inline ThreeDimMatrixAdd<ThreeDimMatrixAdd<Left,Right>,ThreeDimMatrix<T,N,M,L,DIM> >
  operator+(const ThreeDimMatrixAdd<Left,Right>& msl, const ThreeDimMatrix<T,N,M,L,DIM>& mr){
    return ThreeDimMatrixAdd<ThreeDimMatrixAdd<Left,Right>,ThreeDimMatrix<T,N,M,L,DIM> >(msl,mr);
  }
  
  template<typename T, size_t N, size_t M, size_t L, size_t DIM>
  inline ThreeDimMatrixScalarMult<T,ThreeDimMatrix<T,N,M,L,DIM> > operator*(const T& s, const ThreeDimMatrix<T,N,M,L,DIM> & mr){
    return ThreeDimMatrixScalarMult<T,ThreeDimMatrix<T,N,M,L,DIM> >(s,mr);
  }

  // template<typename S, class RM>
  // inline ThreeDimMatrixScalarMult<S,RM> operator*(const S& s, const RM& mr){
  //   return ThreeDimMatrixScalarMult<S,RM> (s,mr);
  // }

  template<typename S, class R, size_t L, size_t M, size_t N, size_t DIM>
  inline ThreeDimMatrixAdd<ThreeDimMatrixScalarMult<S,R>,ThreeDimMatrix<S,N,M,L,DIM> >
  operator+(const ThreeDimMatrixScalarMult<S,R>& lsmm, const ThreeDimMatrix<S,N,M,L,DIM>& rm){
    return ThreeDimMatrixAdd<ThreeDimMatrixScalarMult<S,R>,ThreeDimMatrix<S,N,M,L,DIM> >(lsmm,rm);
  }

  template<typename S, class L, class R>
  inline ThreeDimMatrixAdd<ThreeDimMatrixScalarMult<S,L>,ThreeDimMatrixScalarMult<S,R> >
  operator+(const ThreeDimMatrixScalarMult<S,L>& lsmm, const ThreeDimMatrixScalarMult<S,R>& rsmm){
    return ThreeDimMatrixAdd<ThreeDimMatrixScalarMult<S,L>,ThreeDimMatrixScalarMult<S,R> >(lsmm,rsmm);
  }


  template<typename S, class A, class B, class C>
  inline ThreeDimMatrixAdd<ThreeDimMatrixAdd<A,B>,ThreeDimMatrixScalarMult<S,C> >
  operator+(const ThreeDimMatrixAdd<A,B>& lam, const ThreeDimMatrixScalarMult<S,C>& rsmm){
    return ThreeDimMatrixAdd<ThreeDimMatrixAdd<A,B>,ThreeDimMatrixScalarMult<S,C> >(lam,rsmm);
  }


  template<typename T, size_t L, size_t M, size_t N, size_t DIM>
  inline ThreeDimMatrixAdd<ThreeDimMatrix<T,L,N,M,DIM>,ThreeDimMatrixScalarMult<T,ThreeDimMatrix<T,N,M,L,DIM> > >
  operator+(const ThreeDimMatrix<T,L,N,M,DIM> & lsmm, const ThreeDimMatrixScalarMult<T,ThreeDimMatrix<T,N,M,L,DIM> > & rm){
    return ThreeDimMatrixAdd<ThreeDimMatrix<T,L,N,M,DIM>,ThreeDimMatrixScalarMult<T,ThreeDimMatrix<T,N,M,L,DIM> > >(lsmm,rm);
  }

  
  
}
#endif
