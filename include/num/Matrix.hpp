#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <num/MatrixAdd.hpp>
#include <num/MatrixSubtraction.hpp>
#include <num/MatrixScalarMult.hpp>
#include <num/MatrixMultMult.hpp>
#include <vector>

using namespace std;

namespace num {

  
  template<typename T, size_t ROWS, size_t COLS, size_t DIM>
  class Matrix{

  private:
    T*_data = 0;

  public:    
    using value_type = T;
    size_t _rows;
    size_t _cols;
    size_t _dim;

    
    T* data(){return _data;}
       
    Matrix():_cols(COLS),_rows(ROWS),_dim(DIM){
      srand(time(0));
      _data = new T[COLS*ROWS*DIM];
    }

    Matrix(size_t rows, size_t cols, size_t dim){resize(rows,cols,dim);}
    Matrix(size_t rows, size_t cols){resize(rows,cols,DIM);}
    Matrix(size_t cols){resize(ROWS,cols,DIM);}
    
    void resize(size_t rows, size_t cols){

      if(_data != 0){
        delete  [] _data;
        _data = 0;
      }
      
      _rows = rows;
      _cols = cols;
      
      _data = new T[_rows*_cols*DIM];
    }
    
    
    void resize(size_t rows, size_t cols, size_t dim){

      if(_data != 0){
        delete  [] _data;
        _data = 0;
      }
      
      _rows = rows;
      _cols = cols;
      _dim = dim;
      
      _data = new T[_rows*_cols*_dim];
    }

    void resize(size_t cols){
      resize(ROWS,cols,DIM);
    }
    
    ~Matrix(){
      if(_data){
        delete  [] _data;
        _data = 0;

      }
      
    }


    T& operator()(size_t i,size_t j,size_t d){
      return _data[i*_cols*_dim+j*_dim+d];
    }
    
    const T& operator()(size_t i, size_t j, size_t d) const {
      return _data[i*_cols*_dim+j*_dim+d];
    }

    T& operator()(size_t i, size_t j){
      return _data[i*_cols+j];
    }

    const T& operator()(size_t i, size_t j) const {
      return _data[i*_cols+j];
    }

    T& operator()(size_t j){      
      return _data[j];
    }

    const T& operator()(size_t j) const {
      return _data[j];
    }

    size_t rows() const {return _rows;}
    size_t cols() const {return _cols;}    
    size_t dim()  const {return _dim;}

    void random(int a=0, int b=1){
      for(int i=0; i < _rows; i++)
        for(int j=0; j < _cols; j++)
          for(int d=0; d < _dim; d++)
            _data[i*_cols*_dim+j*_dim+d] = a + static_cast <T> (rand()) / ( static_cast <T> (RAND_MAX/(b-a)));          
    }

    Matrix<T,ROWS,COLS,DIM>& operator=(const Matrix<T,COLS,ROWS,DIM> & mr) {
      if(this != &mr){
        for (size_t i=0; i < _rows; ++i) {
          for(size_t j=0; j < _cols; ++j){
            for(int d =0; d < _dim; d++)
              _data[i*_cols*_dim+j*_dim+d] = mr(i,j,d);
          }
        }
      }
      return *this;
    }

    
    void solve8x8(T b0, T b1, T b2, T b3, T b4, T b5, T b6, T b7, T* x0, T*x1, T* x2, T*x3, T* x4, T*x5, T* x6, T* x7){
      
      T * m = new T[72];
      int C = 8;
      T c;
      
      
      m[0] = (*this)(0,0,0); m[1] = (*this)(0,1,0); m[2] = (*this)(0,2,0); m[3] = (*this)(0,3,0);
      m[4] = (*this)(0,4,0); m[5] = (*this)(0,5,0); m[6] = (*this)(0,6,0); m[7] = (*this)(0,7,0); m[8] = b0;
      
      m[9]  = (*this)(1,0,0); m[10] = (*this)(1,1,0); m[11] = (*this)(1,2,0); m[12] = (*this)(1,3,0);
      m[13] = (*this)(1,4,0); m[14] = (*this)(1,5,0); m[15] = (*this)(1,6,0); m[16] = (*this)(1,7,0); m[17] = b1;
      
      m[18]  = (*this)(2,0,0); m[19] = (*this)(2,1,0); m[20] = (*this)(2,2,0); m[21] = (*this)(2,3,0);
      m[22] = (*this)(2,4,0); m[23] = (*this)(2,5,0); m[24] = (*this)(2,6,0); m[25] = (*this)(2,7,0); m[26] = b2;
      
      m[27]  = (*this)(3,0,0); m[28] = (*this)(3,1,0); m[29] = (*this)(3,2,0); m[30] = (*this)(3,3,0);
      m[31] = (*this)(3,4,0); m[32] = (*this)(3,5,0); m[33] = (*this)(3,6,0); m[34] = (*this)(3,7,0); m[35] = b3;
      
      m[36] = (*this)(4,0,0); m[37] = (*this)(4,1,0); m[38] = (*this)(4,2,0); m[39] = (*this)(4,3,0);
      m[40] = (*this)(4,4,0); m[41] = (*this)(4,5,0); m[42] = (*this)(4,6,0); m[43] = (*this)(4,7,0); m[44] = b4;
      
      m[45]  = (*this)(5,0,0); m[46] = (*this)(5,1,0); m[47] = (*this)(5,2,0); m[48] = (*this)(5,3,0);
      m[49] = (*this)(5,4,0); m[50] = (*this)(5,5,0); m[51] = (*this)(5,6,0); m[52] = (*this)(5,7,0); m[53] = b5;
      
      m[54]  = (*this)(6,0,0); m[55] = (*this)(6,1,0); m[56] = (*this)(6,2,0); m[57] = (*this)(6,3,0);
      m[58] = (*this)(6,4,0); m[59] = (*this)(6,5,0); m[60] = (*this)(6,6,0); m[61] = (*this)(6,7,0); m[62] = b6;
      
      m[63]  = (*this)(7,0,0); m[64] = (*this)(7,1,0); m[65] = (*this)(7,2,0); m[66] = (*this)(7,3,0);
      m[67] = (*this)(7,4,0); m[68] = (*this)(7,5,0); m[69] = (*this)(7,6,0); m[70] = (*this)(7,7,0); m[71] = b7;
      
      
      for(int j=0; j < 8; ++j){
        for(int i=j; i < 8; ++i){
          if(i!=j){
            c = m[i*(C+1)+j]/m[j*(C+1)+j];
            for(int k=0; k < C+1; ++k)
              m[i*(C+1)+k] = m[i*(C+1)+k] - c*m[j*(C+1)+k];
          }
        }
      }
      
      *x7 = (m[71]/m[70]);
      *x6 = (m[62]-m[61]*(*x7))/m[60];
      *x5 = (m[53]-m[51]*(*x6)-m[52]*(*x7))/m[50];
      *x4 = (m[44]-m[41]*(*x5)-m[42]*(*x6)-m[43]*(*x7))/m[40];
      *x3 = (m[35]-m[31]*(*x4)-m[32]*(*x5)-m[33]*(*x6)-m[34]*(*x7))/m[30];
      *x2 = (m[26]-m[21]*(*x3)-m[22]*(*x4)-m[23]*(*x5)-m[24]*(*x6)-m[25]*(*x7))/m[20];
      *x1 = (m[17]-m[11]*(*x2)-m[12]*(*x3)-m[13]*(*x4)-m[14]*(*x5)-m[15]*(*x6)-m[16]*(*x7))/m[10];
      *x0 = (m[8]-m[1]*(*x1)-m[2]*(*x2)-m[3]*(*x3)-m[4]*(*x4)-m[5]*(*x5)-m[6]*(*x6)-m[7]*(*x7))/m[0];
      
      delete [] m;
      
    }
    
    Matrix<T,1,1,ROWS>& operator/(const Matrix<T,ROWS,1,1>& b){
      
      Matrix<T,1,1,ROWS>* x = new Matrix<T,1,1,ROWS>();      
      Matrix<T,ROWS,COLS+1,1>* am = new Matrix<T,ROWS,COLS+1,1>();  
       T c;
       
       int i,j;
       for(i=0; i < ROWS;i++){
         for(j=0; j < COLS; j++){
           (*am)(i,j) = (*this)(i,j);           
         }
         (*am)(i,j) = b(i);
       }
       
       
       for(int j=0; j < ROWS; ++j)
         for(int i=0; i < ROWS; ++i)
           if(i!=j){
             c = (*am)(i,j)/(*am)(j,j);
             for(int k=0; k < COLS+1; ++k)
               (*am)(i,k) = (*am)(i,k) - c*(*am)(j,k);       
           }

       for(int i=0; i < ROWS; ++i)
         (*x)(i) = (*am)(i,COLS)/(*am)(i,i); 
       
       return *x;     
    }


    
    

    template<size_t P, size_t Q>
    Matrix<T,P,Q,DIM>& block(size_t i, size_t j) {
      Matrix <T,P,Q,DIM> *b = new Matrix <T,P,Q,DIM>();
      for (size_t ii=i, iii=0; ii < i+P; ++ii, ++iii) {
        for(size_t jj=j, jjj = 0; jj < j+Q; ++jj,++jjj){
          for(int d =0; d < _dim; d++){
            (*b)(iii,jjj,d) = (*this)(ii,jj,d);
          }
        }
      }
      return *b;
    }



    Matrix<T,ROWS,COLS,DIM>& block(size_t i, size_t j, size_t p, size_t q) {

      Matrix <T,ROWS,COLS,DIM> *b = new Matrix();
      (*b).resize(p,q);
      for (size_t ii=i, iii=0; ii < i+p; ++ii, ++iii) {
        for(size_t jj=j, jjj = 0; jj < j+q; ++jj,++jjj){
          for(int d =0; d < _dim; d++){
            (*b)(iii,jjj,d) = (*this)(ii,jj,d);
          }
        }
      }
      return *b;
    }


    void set(size_t i, size_t j, size_t p, size_t q, const T c){

      for (size_t ii=i; ii < i+p; ++ii) {
        for(size_t jj=j; jj < j+q; ++jj){
          for(int d =0; d < _dim; d++){
            (*this)(ii,jj,d) = c;
          }
        }
      }
    }
    
    void operator=(const T c){
      for (size_t i=0; i < _rows; ++i) {
        for(size_t j=0; j < _cols; ++j){
          for(int d =0; d < _dim; d++)
            _data[i*_cols*_dim+j*_dim+d] = c;
        }
      }
    }
    
    void operator=(const std::initializer_list<T> & l ) {
      vector<T> v;
      v = l;
      int k = 0;
      for (size_t i=0; i < _rows; ++i) {
        for(size_t j=0; j < _cols; ++j){
          for(int d =0; d < _dim; d++)
            _data[i*_cols*_dim+j*_dim+d] = v[k++];
        }
      }      
    }

    template <class L, class R>
    Matrix<T,ROWS,COLS,DIM>& operator=(const MatrixAdd<L,R> mrs){
      for(size_t i=0; i <_rows; ++i)
        for(size_t j=0; j < _cols; ++j)
          for(size_t d=0; d < _dim; ++d)
            _data[i*_cols*_dim+j*_dim+d] = mrs(i,j,d);

      return *this;
    }

    template <class L, class R>
    Matrix<T,ROWS,COLS,DIM>& operator=(const MatrixSubtraction<L,R> mrs){
      for(size_t i=0; i <_rows; ++i)
        for(size_t j=0; j < _cols; ++j)
          for(size_t d=0; d < _dim; ++d)
            _data[i*_cols*_dim+j*_dim+d] = mrs(i,j,d);
      
      return *this;
    }

    
    template <typename s, class R>
    Matrix<T,ROWS,COLS,DIM>& operator=(const MatrixScalarMult<s,R &> mrs) {
      for(size_t i=0; i <_rows; ++i)
        for(size_t j=0; j < _cols; ++j)
          for(size_t d=0; d < _dim; ++d)
            _data[i*_cols*_dim+j*_dim+d] = mrs(i,j,d);

      return *this;
    }

    // template<size_t ROWS1, size_t COLS1, size_t ROWS2, size_t COLS2>
    // Matrix<T,ROWS1,COLS2,DIM>& operator=(const MatrixMultMult< Matrix<T,ROWS1,COLS1,DIM>, Matrix<T,ROWS2,COLS2,DIM> >& mrm){
      
    //   delete [] _data;
    //   _data = new T[ROWS1*COLS2*DIM];
      
    //   for(size_t i=0; i < ROWS1; ++i)
    //     for(size_t j=0; j < COLS2; ++j)
    //       (*this)(i,j) = mrm(i,j);
      
    //   return *this;
    // }


    template<class L, class R>
    Matrix<T,ROWS,COLS,DIM>& operator=(const MatrixMultMult< L,R>& mrm){
      
      delete [] _data;
      _data = new T[ROWS*COLS*DIM];
      
      for(size_t i=0; i < ROWS; ++i)
        for(size_t j=0; j < COLS; ++j)
          (*this)(i,j) = mrm(i,j);
      
      return *this;
    }

    
    
    Matrix<T,ROWS,1,DIM>& operator=(const MatrixMultMult< Matrix<T,ROWS,COLS,DIM> , Matrix<T,ROWS,1,DIM> >& mrm){
      Matrix<T,ROWS,1,DIM>* mr = new Matrix<T,ROWS,1,DIM>();

      delete [] _data;
      _data = new T[ROWS];
      
      for(size_t i=0; i < ROWS; ++i)
        (*mr)(i,0) = mrm(i,0);
      
      return *mr;
    }

    
    friend ostream & operator << (ostream & out, Matrix<T,ROWS,COLS,DIM> & m){
      int cols = m.cols();
      int rows = m.rows();
      int dim  = m.dim();
      
      
      for(int i=0; i < rows; i++){
        for(int j=0; j < cols; j++){                  
          for(int d=0; d < dim; d++)
            out << setw(10) << m(i,j,d) << " " ;          
          cout << "   ";
        }        
        out << endl;
      }
      return out;
    }
  };    
    
}    
#endif
