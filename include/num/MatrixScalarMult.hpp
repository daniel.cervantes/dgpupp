#ifndef _MATRIXSCALARMULT_H_
#define _MATRIXSCALARMULT_H_

namespace num{
  template<typename S, class R>
  class MatrixScalarMult{
  private:
    const R& _r;
    const S& _s;
  public:
    using value_type = typename R::value_type;
    MatrixScalarMult(const S& s, const R& r):_s(s),_r(r){}
    value_type operator()(size_t i, size_t j, size_t d) const{return _s*_r(i,j,d);}
    value_type operator()(size_t i, size_t j) const{return _s*_r(i,j);}
    value_type operator()(size_t i) const{return _s*_r(i);}
    size_t cols() const{return _r.cols();}
    size_t rows() const{return _r.rows();}
  };
}
#endif
