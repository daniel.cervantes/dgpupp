#ifndef _MATRIXMULTMULT_H_
#define _MATRIXMULTMULT_H_

namespace num{
  template<class L, class R>
  class MatrixMultMult{
  private:
    const R& _r;
    const L& _l;
  public:
    using value_type = typename R::value_type;
    MatrixMultMult(const L& l, const R& r):_l(l),_r(r){}

    value_type operator()(size_t i, size_t j, size_t d) const{
      return (*this)(i,j);
    }
    
    value_type operator()(size_t i, size_t j) const{
      size_t ml;
      size_t nr;
      value_type r = 0;

      ml = _l.cols();
      nr = _r.rows();
      
      if(ml == nr)
        for(int k=0; k < ml; ++k)            
          r += _l(i,k)*_r(k,j); 
            
      return r;
    }

  };
}
#endif
