#ifndef _THREEDIMMATRIXADD_H_
#define _THREEDIMMATRIXADD_H_

//using namespace std;

namespace num {

  template<class L, class R>
  class ThreeDimMatrixAdd{
  private:
    const L& _l;
    const R& _r;
  public:
    using value_type = typename L::value_type;    
    ThreeDimMatrixAdd(const L& l, const R& r) : _r(r),_l(l) {}
    value_type operator()(size_t i, size_t j, size_t k, size_t d) const {return _l(i,j,k,d)+_r(i,j,k,d);}
  };

}    
#endif
