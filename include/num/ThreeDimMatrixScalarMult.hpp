#ifndef _THREEDIMMATRIXSCALARMULT_H_
#define _THREEDIMMATRIXSCALARMULT_H_

namespace num{
  template<typename S, class R>
  class ThreeDimMatrixScalarMult{
  private:
    const R& _r;
    const S& _s;
  public:
    using value_type = typename R::value_type;
    ThreeDimMatrixScalarMult(const S& s, const R& r):_s(s),_r(r){}
    value_type operator()(size_t i, size_t j, size_t k, size_t d) const{return _s*_r(i,j,k,d);}
  };
}
#endif
