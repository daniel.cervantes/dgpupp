#ifndef _THREEDIMMATRIX_H_
#define _THREEDIMMATRIX_H_

#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <num/Matrix.hpp>


using namespace std;

namespace num {

  template <class, class> class ThreeDimMatrixAdd;
  template <class, class> class ThreeDimMatrixScalarMult;

  const size_t Dynamic = 1;
  template<typename T, size_t N, size_t M, size_t L, size_t DIM>
  class ThreeDimMatrix{

  private:
    T*_data;
    size_t _n;
    size_t _m;
    size_t _l;
    size_t _dim;

  public:    
    using value_type = T;
    T* data(){return _data;}
       
    ThreeDimMatrix():_n(N),_m(M),_l(L),_dim(DIM){
      srand(time(0));
      _data = new T[N*L*M*DIM];
    }
    
    ThreeDimMatrix(size_t n, size_t m, size_t l, size_t dim){resize(n,m,l,dim);}
    ThreeDimMatrix(size_t n, size_t m, size_t l){resize(n,m,l);}
    

    void resize(size_t n, size_t m, size_t l, size_t dim){
      if(_data != 0){
        delete  [] _data;
        _data = 0;
      }
      
      _n = n;
      _m = m;
      _l = l;
      _data = new T[_n*_m*_l*_dim];

      for(int i=0; i < _n*_m*_l*_dim; ++i)
        _data[0] = 0;
    }


    void resize(size_t n, size_t m, size_t l){
      if(_data != 0){
        delete  [] _data;
        _data = 0;
      }
      
      _n = n;
      _m = m;
      _l = l;
      _data = new T[_n*_m*_l*DIM];

      for(int i=0; i < _n*_m*_l*DIM; ++i)
        _data[i] = 0;

    }

    
    ~ThreeDimMatrix(){
      if(_data != 0){
        delete  [] _data;
        _data = 0;
      }
    }


    // T& operator()(size_t i,size_t j,size_t k,size_t d){return _data[i*_m*_dim+k*_n*_m*_dim+j*_dim+d];}    
    // const T& operator()(size_t i, size_t j, size_t k, size_t d) const {return _data[i*_m*_dim+k*_n*_m*_dim+j*_dim+d];}


    T& operator()(size_t i,size_t j,size_t k,size_t d){return _data[i*_n*_m*_dim + j*_m*_dim+k*_dim+d];}    
    const T& operator()(size_t i, size_t j, size_t k, size_t d) const {return _data[i*_n*_m*_dim + j*_m*_dim+k*_dim+d];}

    
    Matrix<T,DIM,1,1>& operator()(size_t i, size_t j, size_t k){
      Matrix<T,DIM,1,1> * cr = new Matrix<T,DIM,1,1>();
      for(int d =0; d < DIM; d++)
        (*cr)(d) = (*this)(i,j,k,d);
      return *cr;
    }


    void operator()(size_t i, size_t j, size_t k, Matrix<T,DIM,1,1>& cr){
      for(int d =0; d < DIM; d++)
        cr(d) = (*this)(i,j,k,d);      
    }

    int l(){return _l;}
    int n(){return _n;}    
    int m(){return _m;}    
    int dim(){return _dim;}

    void random(int a=0, int b=1) {            
      for(size_t i=0; i < _n; i++)
        for(size_t j=0; j < _m; j++)          
          for(size_t k=0; k < _l; k++)            
            for(size_t d=0; d < _dim; d++)
              _data[i*_m*_dim+k*_n*_m*_dim+j*_dim+d] = a + static_cast <T> (rand()) / ( static_cast <T> (RAND_MAX/(b-a)));          
    }
    
    ThreeDimMatrix<T,N,M,L,DIM>& operator=(const T c) {
      for (size_t i=0; i < _n; ++i) 
        for(size_t j=0; j < _m; ++j)
          for(size_t k=0; k < _l; ++k)
            for(size_t d =0; d < _dim; d++)             
              _data[i*_m*_dim+k*_n*_m*_dim+j*_dim+d] = c;
      
      return *this;
    }


    // ThreeDimMatrix<T,N,M,L,DIM>& operator*=(const T c) {
    //   for (size_t i=0; i < _n; ++i) 
    //     for(size_t j=0; j < _m; ++j)
    //       for(size_t k=0; k < _l; ++k)
    //         for(size_t d =0; d < _dim; d++)             
    //           _data[i*_m*_dim+k*_n*_m*_dim+j*_dim+d] = c;
      
    //   return *this;
    // }


    ThreeDimMatrix<T,N,M,L,DIM>& operator=(const ThreeDimMatrix<T,N,M,L,DIM> & mr) {
      for (size_t i=0; i < _n; ++i) 
        for(size_t j=0; j < _m; ++j)
          for(size_t k=0; k < _l; ++k)
            for(size_t d =0; d < _dim; d++)             
              _data[i*_m*_dim+k*_n*_m*_dim+j*_dim+d] = mr(i,j,k,d);
      
      return *this;
    }

    
    template <class Left, class Right>
    ThreeDimMatrix<T,N,L,M,DIM>& operator=(const ThreeDimMatrixAdd<Left,Right> mrs){
      for(size_t i=0; i <_n; ++i)
        for(size_t j=0; j < _m; ++j)
          for(size_t k=0; k < _l; ++k)
            for(size_t d=0; d < _dim; ++d)        
              _data[i*_m*_dim+k*_n*_m*_dim+j*_dim+d] = mrs(i,j,k,d);
      
      return *this;
    }
    
    template <typename s, class R>
    ThreeDimMatrix<T,N,L,M,DIM>& operator=(const ThreeDimMatrixScalarMult<s,R> mrs){
      for(size_t i=0; i <_n; ++i)
        for(size_t j=0; j < _m; ++j)
          for(size_t k=0; k < _l; ++k)
            for(size_t d=0; d < _dim; ++d)
              _data[i*_m*_dim+k*_n*_m*_dim+j*_dim+d] = mrs(i,j,k,d);
      
      return *this;
    }
    
    
    friend ostream & operator << (ostream & out, ThreeDimMatrix<T,N,M,L,DIM> & mat){
      int n = mat.n();
      int m = mat.m();
      int l = mat.l();      
      int dim  = mat.dim();

      for(int d=0; d <dim; d++){          
        for(int k=0; k <l; k++){
          out << endl
              <<"(:,:,"+std::to_string(k)+","+std::to_string(d)+")="
              << endl;
          for(int i=0; i < n; i++){
            for(int j=0; j < m; j++)
              out << setw(2) << mat(i,j,k,d) << " " ;
            out << endl ;
          }
        }
        // cout << endl ;
      }
      
      return out;
    }

    void set(size_t i, size_t j, size_t k, size_t p, size_t q, size_t r, T c){
      for(int ii=i; ii < i+p; ++ii)
        for(int jj=j; jj < j+q; ++jj)
          for(int kk=k; kk < kk + r; ++kk)
            for(int d=0; d < _dim; ++d)
              (*this)(ii,jj,kk,d) = c;
            
    }
    
    void set(size_t i, size_t j, size_t k, T c){      
      size_t p = 1;
      size_t q = 1;
      size_t r = 1;

      for(int ii=i; ii < i+p; ++ii)
        for(int jj=j; jj < j+q; ++jj)
          for(int kk=k; kk < k+r; ++kk)
            for(int d=0; d < _dim; ++d){
              (*this)(ii,jj,kk,d) = c;
            }

    }
    
    void set(size_t i, size_t j, size_t k, const Matrix<T,1,1,DIM> & m){
      for(int d = 0; d < DIM; ++d)
        (*this)(i,j,k,d) = m(d);
    }
    
    template<typename S,class R>
    void set(size_t i, size_t j, size_t k, const MatrixScalarMult<S,R> & m){
      for(int d = 0; d < DIM; ++d)
        (*this)(i,j,k,d) = m(d);
    }

    
  };    
    
}    
#endif
